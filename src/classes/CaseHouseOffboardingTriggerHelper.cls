/*

Created by Baibhav - WarpDrive Tech Works

Purpsoe: to handle all House offboarding Related case Trigger

*/

public class CaseHouseOffboardingTriggerHelper 
{
public Static id ownerSettlement=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
public Static id  nestawaySettlement=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();

/*
Created by baibhav - WarpDrive Tech Works

Purpose: To through error if a House already have Open Case (OnBeforeInsert Case)
*/
public static void houseOffboardingCaseInsertValidation(List<Case> newList)
{

Set<Id> houSet = new Set<Id>();
for(Case ca:newList)
{
if(ca.HouseForOffboarding__c!=null)
{
  houSet.add(ca.HouseForOffboarding__c);
}
}

Map<id,List<Case>> casemap = new Map<id,List<Case>>();
List<case> offcaseList = CaseSelector.getCasesFromHouseIdSet(houSet);


for(Case ca:offcaseList)
{
List<case> caselist = new List<case>();
if(casemap.containsKey(ca.HouseForOffboarding__c))
{
    caselist=casemap.get(ca.HouseForOffboarding__c);
}

caselist.add(ca);

casemap.put(ca.HouseForOffboarding__c,caselist);

}

for(Case caNew:newList)
{
if(caNew.HouseForOffboarding__c!=null)
{
List<case> caselist =new List<case>();  
if(casemap.containsKey(caNew.HouseForOffboarding__c)){
caselist = casemap.get(caNew.HouseForOffboarding__c);
}
if(!caselist.isEmpty())
{
for(Case caOld:caselist)
{
    if(caOld.status == Constants.CASE_STATUS_OPEN)
       caNew.addError(label.Offboarding_case_Insertion_Error);
    
}
}
}

}
}
/*End*/


/*
Created by baibhav - WarpDrive Tech Works

Purpose: To Create Task To HRM To Call Owner (OnAfterInsert Case)
*/

public static id workOrderZAM = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_OFFBOARDING_CALL_TO_OWNER).getRecordTypeId();
public static Id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();

public static void houseOffboardingWorkOrderCreationOnCaseInsert(List<Case> offcaseList) 
{   
try
{
List<WorkOrder> workList = new List<WorkOrder>();
Set<id> caseid = new Set<Id>(); 

for(Case ca:offcaseList)
{
 caseid.add(ca.id);
}
List<Zone__c> zonelist = ZoneAndUserMappingSelector.getZoneRecordsForRecordTypeId(OffboardingZoneRTId);
Map<String,Zone__c> zoneMapTocode = new  Map<String,Zone__c>();
for(Zone__c zo:zonelist)
{
zoneMapTocode.put(zo.Zone_code__c,zo); 
}
list<Case> caselist = new list<Case>(); 
if(!caseid.isEmpty()){
 caselist = CaseSelector.getCasesFromCaseIdSet(caseid);
}
List<Case> caseListUpdate = new List<Case>();  

for(Case ca:caselist )
{
  WorkOrder wko = new WorkOrder();
  wko.CaseId= ca.id;
  wko.ContactId = ca.ContactId;
  wko.RecordTypeId = workOrderZAM;
  wko.Subject = 'HRM Task';
  wko.House__c = ca.HouseForOffboarding__c;
  wko.Offboarding_Reason__c = ca.Offboarding_Reason__c;
  wko.Subject = 'Owner Retantion Call - House:' + ca.HouseForOffboarding__r.Name + '- ID:' + ca.HouseForOffboarding__r.HouseId__c;  

  if(ca.OwnerId != null)   
    wko.OwnerId = ca.OwnerId;

  if(ca.HouseForOffboarding__r.ZAM__c != null)
    wko.ZAM_for_Approval__c = ca.HouseForOffboarding__r.ZAM__c;

  
  wko.Accountid = ca.HouseForOffboarding__r.House_Owner__c;
  wko.Status=Constants.WORK_STATUS_OPEN;
  
  workList.add(wko);
}
if(!workList.isEmpty()){
insert workList;
}
if(!caseListUpdate.isEmpty()){
update caseListUpdate;
 }
}  catch(Exception e)
  {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
      UtilityClass.insertGenericErrorLog(e);                    
  }
}

/***************************************************
Created By : Mohan
Purpose    : Populate NestAway Settlement Case Fields
***************************************************/
  public static void populateNestAwaySettlementCaseFields(List<Case> caseList){

  try{
        Set<Id> caseIdSet = new Set<Id>();
        Set<Id> accIdSet = new Set<Id>();

        for(Case c: caseList){
          caseIdSet.add(c.ParentId);
        }

        Map<Id, Case> caseMap = new Map<Id, Case>(CaseSelector.getCasesFromCaseIdSet(caseIdSet));

        for(Case c: caseMap.values()){
          accIdSet.add(c.AccountId);
        }

        List<Bank_Detail__c> bnkDList = BankDetailSelector.getbankdetailbyAccount(accIdSet);
        Map<Id,Bank_Detail__c> bankDetailsMap =new Map<Id,Bank_Detail__c>(); 
        for(Bank_Detail__c bkd:bnkDList)
        {
          bankDetailsMap.put(bkd.Related_Account__c,bkd);
        }    

        for(Case c: caseList){
          Case parentCase = caseMap.get(c.ParentId);

          c.AccountId = parentCase.AccountId;
          c.ContactId = parentCase.ContactId;
          c.Subject = 'NestAway Settlement Case';
          c.House1__c = parentCase.HouseForOffboarding__c;
          c.PAN__c = parentCase.Account.PAN_Number__c;
          c.Account_Number__c = bankDetailsMap.get(parentCase.AccountId).Account_Number__c;
          c.Bank_IFSC_code__c = bankDetailsMap.get(parentCase.AccountId).IFSC_Code__c;
          c.A_C_holder_name__c = bankDetailsMap.get(parentCase.AccountId).Name;
        }    

    } catch(Exception e){
        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
        UtilityClass.insertGenericErrorLog(e, 'NestAway Settlement Case Creation');      
    }    
  }

/***************************************************
Created By : Mohan
Purpose    : Populate Owner Settlement Case Fields
***************************************************/
  public static void populateOwnerSettlementCaseFields(List<Case> caseList){

    try{
          Set<Id> caseIdSet = new Set<Id>();

          for(Case c: caseList){
            caseIdSet.add(c.ParentId);
          }

          Map<Id, Case> caseMap = new Map<Id, Case>(CaseSelector.getCasesFromCaseIdSet(caseIdSet));

          for(Case c: caseList){
            Case parentCase = caseMap.get(c.ParentId);

            c.AccountId = parentCase.AccountId;
            c.ContactId = parentCase.ContactId;
            c.Subject = 'Owner Settlement Case';
            c.House1__c = parentCase.HouseForOffboarding__c;
          }     
    } catch(Exception e){
        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
        UtilityClass.insertGenericErrorLog(e, 'Owner ');      
    }
  }  


/*
Created by baibhav - WarpDrive Tech Works

Purpose: To Create Task To HRM To Call Owner (OnAfterInsert Case)

Date: 14-08-2017
*/

public static void nestSattlementCaseAssignment(list<Case> caseList)
{
try 
{
//list<Case> caseListupdate = new  list<Case>();
List<Group> userGrp = [SELECT DeveloperName,Id FROM Group where (DeveloperName ='Finance' or DeveloperName ='Accounts' or DeveloperName ='Payment') and Type= 'Queue'];
Map<String,Group> grpMapName = new Map<String,Group>();
for(Group gr:userGrp)
{
  grpMapName.put(gr.DeveloperName,gr);
}
for(Case ca:caseList)
{
  if(ca.Status==Constants.CASE_STATUS_NEW && grpMapName.get('Finance').id!=null)
    ca.Ownerid=grpMapName.get('Finance').id;
  else if(ca.Status==Constants.CASE_STATUS_IN_NEW_PAYMENT_VERIFIED && grpMapName.get('Accounts').id!=null)
  {
    SYStem.debug('******************************************BAIBAHV***********');
    ca.Ownerid=grpMapName.get('Accounts').id;
  }
  else if(ca.Status==Constants.CASE_STATUS_IN_PAID && grpMapName.get('Payment').id!=null)
    ca.Ownerid=grpMapName.get('Payment').id;

 // caseListupdate.add(ca);
}
/*if(caseListupdate!=null)
update caseListupdate;*/
} catch(Exception e)
  {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
      UtilityClass.insertGenericErrorLog(e);                    
  }
}
public static void ownerSattlementCaseAssignment(list<Case> caseList)
{
try 
{
//list<Case> caseListupdate = new  list<Case>();
List<Group> userGrp = [SELECT DeveloperName,Id FROM Group where (DeveloperName ='Finance' or DeveloperName ='Accounts' or DeveloperName ='Payment') and Type= 'Queue'];
Map<String,Group> grpMapName = new Map<String,Group>();
for(Group gr:userGrp)
{
  grpMapName.put(gr.DeveloperName,gr);
}
for(Case ca:caseList)
{
  if(ca.Status==Constants.CASE_STATUS_CASH_DEPOSITED && grpMapName.get('Finance').id!=null)
    ca.Ownerid=grpMapName.get('Finance').id;
  else if(ca.Status==Constants.CASE_STATUS_INVOICES_UPDATE && grpMapName.get('Accounts').id!=null)
    ca.Ownerid=grpMapName.get('Accounts').id;

 // caseListupdate.add(ca);
}
/*if(caseListupdate!=null)
update caseListupdate;*/
} catch(Exception e)
  {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
      UtilityClass.insertGenericErrorLog(e);                    
  }
}

/*****************************************************************
Created By : Mohan
Purpose    : Update house Initiate Offboarding 
******************************************************************/
  public static void updateHouseInitiateOffboarding(List<Case> caseList){

  try{
        List<House__c> houseList = new List<House__c>();
        for(Case c: caseList){
          if(c.HouseForOffboarding__c != null){
            House__c house = new House__c();
            house.Id = c.HouseForOffboarding__c;
            house.Initiate_Offboarding__c = 'No';
            houseList.add(house);
          }
        }

        if(!houseList.isEmpty())
          update houseList;
    } catch(Exception e){
        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
        UtilityClass.insertGenericErrorLog(e);      
    }
  }

/*****************************************************************
Created By : Mohan
Purpose    : Assign the Case to respective Queue - NestAway settlement Case/ Owner Settlement Case
******************************************************************/ 
  public static void updateCaseOwnerToQueue(List<Case> caseList){

    try{
          Map<String, Id> queueMap = GroupAndGroupMemberSelector.getQueueMap();
          for(Case c: caseList){
            if(c.Status == Constants.CASE_STATUS_IN_NEW_PAYMENT_VERIFIED){
              c.OwnerId = queueMap.get(Constants.QUEUE_NAME_PAYMENT);
            } else if(c.Status == Constants.CASE_STATUS_IN_PAID || c.Stage__c == Constants.CASE_STAGE_INVOICE_UPDATED_AND_CHECKED){
              c.OwnerId = queueMap.get(Constants.QUEUE_NAME_ACCOUNT);
            } else if(c.Status == Constants.CASE_STATUS_NEW){
              c.OwnerId = queueMap.get(Constants.QUEUE_NAME_FINANCE);
            }
          }
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'Case Owner - Queue Assignment');      
        }    
  }  
}