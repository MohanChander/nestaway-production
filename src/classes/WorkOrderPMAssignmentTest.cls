@istest
public class WorkOrderPMAssignmentTest {
 public static Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get('Property Management Zone').getRecordTypeId();
    public static Id zuApmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Assistance Property Manager').getRecordTypeId();
    public static Id zuPmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Property Manager').getRecordTypeId();
    
    public Static TestMethod Void  TestMethod1(){
        Test.startTest() ;
       zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;       
      
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
       
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
        House__c hos= new House__c();
        hos.name='house1';
        //hos.City__c='bangalore';
        hos.Property_Manager__c = newuser.id;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
		WorkOrder wo = new WorkOrder();
		wo.House__c=hos.id;
		insert wo;        
         Test.stopTest();
        List<WorkOrder> woList =  new List<WorkOrder>();
        woList.add(wo);
        
        WorkOrderAPMAssignment.assignmentOfOwners(woList);
    }
     
    public Static TestMethod Void  TestMethod2(){
        Test.startTest() ;
       zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;       
      
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
       
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
        House__c hos= new House__c();
        hos.name='house1';
       // hos.City__c='bangalore';
       // hos.apm__c = newuser.id;
       hos.Property_Manager__c = newuser.id;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
		WorkOrder wo = new WorkOrder();
		wo.House__c=hos.id;
		insert wo;        
         Test.stopTest();
        List<WorkOrder> woList =  new List<WorkOrder>();
        woList.add(wo);
        
        WorkOrderPMAssignment.assignmentOfOwners(woList);
    }
}