/*	Created By: Deepak - WarpDrive Tech Works
Purpose   : WorkOrder Trigger Helper Methods */

public class WorkOrderTriggerHelperMoveout {
    
    /* Created By : deepak
Purpose   : Create WorkOrder Line Items for the WorkOrder - Move OUt Check based on Booking Type */
    public static void createWorkOrderLineItemsForMoveoutCheck(Map<Id, WorkOrder> woMap){
        
        try{
            system.debug('*****woMap'+woMap);
            Set<Id> caseIdSet = new Set<Id>();
            Set<Id> houseIdSet = new Set<Id>();
            Map<Id, Case> caseMap;
            List<House_Inspection_Checklist__c> hicList = new List<House_Inspection_Checklist__c>();
            List<Room_Inspection__c> ricList = new List<Room_Inspection__c>();
            List<Bathroom__c> bicList = new List<Bathroom__c>();
            Map<Id, House_Inspection_Checklist__c> hicMap = new Map<Id, House_Inspection_Checklist__c>();               //Map of House Id, Hic
            Map<Id, List<Room_Inspection__c>> ricMap = new Map<Id, List<Room_Inspection__c>>();                         //Map of House Id, Ric list
            Map<Id, List<Bathroom__c>> bicMap = new Map<Id, List<Bathroom__c>>();					                    //Map of House Id, Bic list
            
            RelationshipUtiltiy relUtil = new RelationshipUtiltiy();
            
            for(WorkOrder wo: woMap.values()){
                if(wo.CaseId != null)
                    caseIdSet.add(wo.CaseId);
            }
            
            if(!caseIdSet.isEmpty())
                caseMap = new Map<Id, Case>(CaseSelector.getCasesFromCaseIdSet(caseIdSet));
            
            System.debug('************CaseMap: ' + caseMap);
            
            for(Case c: caseMap.values()){
                if(case.House__c != null){
                    houseIdSet.add(c.House__c);
                }
            }
            
            System.debug('*************houseIdSet: ' + houseIdSet);
            
            if(!houseIdSet.isEmpty()){
                hicList = [select Id, Name, House__c from House_Inspection_Checklist__c where House__c =: houseIdSet 
                           and Type_Of_HIC__c =: Constants.CHECKLIST_TYPE_HIC and Type_Of_HIC__c !='Move out Check'];
                
                ricList = [select Id, Name, House_Inspection_Checklist__r.House__c from Room_Inspection__c 
                           where House_Inspection_Checklist__r.House__c =: houseIdSet and 
                           Type_of_RIC__c !='Move out Check'];
                
                bicList = [select Id, Name, House__c, Room_Inspection__c, Type__c, Water_Heater__c, Water_Heater_Type__c,
                           Mirror__c, Bucket_Mug__c, Toilet_Brush__c, Toilet_Type__c
                           from Bathroom__c where House__c =: houseIdSet
                           and Type_of_BIC__c !='Move out Check'];			
            }
            
            System.debug('***************hicList: ' + hicList);
            
            for(House_Inspection_Checklist__c hic: hicList)
                hicMap.put(hic.House__c, hic);
            
            System.debug('************hicMap: ' + hicMap);
            
            
            for(Room_Inspection__c ric: ricList){
                if(!ricMap.isEmpty() && ricMap.containsKey(ric.House_Inspection_Checklist__r.House__c))
                    ricMap.get(ric.House_Inspection_Checklist__r.House__c).add(ric);
                else
                    ricMap.put(ric.House_Inspection_Checklist__r.House__c, new List<Room_Inspection__c>{ric});
            }
            System.debug('************ricMap: ' + ricMap);
            for(Bathroom__c bic: bicList){
                if(!bicMap.isEmpty() && bicMap.containsKey(bic.House__c))
                    bicMap.get(bic.House__c).add(bic);
                else 
                    bicMap.put(bic.House__c, new List<Bathroom__c>{bic});
            }
            System.debug('************bicMap: ' + bicMap);
            //list of records to be inserted
            List<WorkOrderLineItem> woliList = new List<WorkOrderLineItem>();
            List<House_Inspection_Checklist__c> newHicList = new List<House_Inspection_Checklist__c>();
            List<Room_Inspection__c> newRicList = new List<Room_Inspection__c>();
            List<Bathroom__c> newBicList = new List<Bathroom__c>();
            
            //Fetch the RecordType Id's for Move in Check
            Id moveInHicRTId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MOVE_OUT_CHECK).getRecordTypeId();
            Id moveInRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
            Id moveInBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();				
            
            for(WorkOrder wo: woMap.values()){
                
                Id houseId = caseMap.get(wo.CaseId).House__c;
                
                if(wo.CaseId != null && !caseMap.isEmpty() && caseMap.containsKey(wo.CaseId) && caseMap.get(wo.CaseId).Booked_Object_Type__c == Constants.CASE_BOOKING_OBJECT_TYPE_HOUSE){
                    
                    //create duplicate HIC record
                    House_Inspection_Checklist__c hic = new House_Inspection_Checklist__c();
                    hic = hicMap.get(houseId).clone(false, true, false, false);
                    hic.RecordTypeId = moveInHicRTId;
                    hic.Type_Of_HIC__c ='Move out Check';
                    
                    WorkOrderLineItem woli = new WorkOrderLineItem();
                    woli.WorkOrderId = wo.Id;
                    //woli.Subject = 'Hello Test';
                    
                    relUtil.registerRelationship(woli, hic, WorkOrderLineItem.House_Inspection__c);
                    newHicList.add(hic);
                    woliList.add(woli);
                    
                    for(Room_Inspection__c ric: ricMap.get(houseId)){
                        Room_Inspection__c innerRic = new Room_Inspection__c();
                        innerRic = ric.clone(false, true, false, false);
                        innerRic.RecordTypeId = moveInRicRTId;
                        innerRic.Type_of_RIC__c ='Move out Check';
                        
                        WorkOrderLineItem roomWoli = new WorkOrderLineItem();
                        //roomWoli.Subject = 'Room Check';
                        roomWoli.WorkOrderId = wo.Id;	
                        relUtil.registerRelationship(roomWoli, innerRic, WorkOrderLineItem.Room_Inspection__c);		
                        
                        newRicList.add(innerRic);
                        woliList.add(roomWoli);							
                    }
                    
                    for(Bathroom__c bathroom: bicMap.get(houseId)){
                        Bathroom__c innerBathroom = new Bathroom__c();
                        innerBathroom = bathroom.clone(false, true, false, false);
                        innerBathroom.RecordTypeId = moveInBicRTId;
                        innerBathroom.Type_of_BIC__c = 'Move out Check';
                        
                        WorkOrderLineItem bathroomWoli = new WorkOrderLineItem();
                        //bathroomWoli.Subject = 'Bathroom Check';
                        bathroomWoli.WorkOrderId = wo.Id;	
                        relUtil.registerRelationship(bathroomWoli, innerBathroom, WorkOrderLineItem.Bathroom_Inspection__c);		
                        
                        newBicList.add(innerBathroom);
                        woliList.add(bathroomWoli);										
                    }
                    
                } else if(wo.CaseId != null && !caseMap.isEmpty() && caseMap.containsKey(wo.CaseId) && (caseMap.get(wo.CaseId).Booked_Object_Type__c == Constants.CASE_BOOKING_OBJECT_TYPE_ROOM || caseMap.get(wo.CaseId).Booked_Object_Type__c == Constants.CASE_BOOKING_OBJECT_TYPE_BED)){
                    
                    //create duplicate HIC record
                    House_Inspection_Checklist__c hic = new House_Inspection_Checklist__c();
                    hic = hicMap.get(houseId).clone(false, true, false, false);
                    hic.RecordTypeId = moveInHicRTId;
                    hic.Type_Of_HIC__c = 'Move out Check';
                    
                    WorkOrderLineItem woli = new WorkOrderLineItem();
                    //woli.Subject = 'House Check';
                    woli.WorkOrderId = wo.Id;
                    
                    relUtil.registerRelationship(woli, hic, WorkOrderLineItem.House_Inspection__c);
                    newHicList.add(hic);
                    woliList.add(woli);
                    
                    for(Room_Inspection__c ric: ricMap.get(houseId)){
                        
                        if(ric.Id == caseMap.get(wo.CaseId).Room_Term__r.Room_Inspection__c){
                            Room_Inspection__c innerRic = new Room_Inspection__c();
                            innerRic = ric.clone(false, true, false, false);
                            innerRic.RecordTypeId = moveInRicRTId;
                            innerRic.Type_of_RIC__c ='Move out Check';
                            
                            WorkOrderLineItem roomWoli = new WorkOrderLineItem();
                            //roomWoli.Subject = 'Room Check';
                            roomWoli.WorkOrderId = wo.Id;	
                            relUtil.registerRelationship(roomWoli, innerRic, WorkOrderLineItem.Room_Inspection__c);		
                            
                            newRicList.add(innerRic);
                            woliList.add(roomWoli);	
                            
                            for(Bathroom__c bic: bicMap.get(houseId)){
                                if(bic.Room_Inspection__c == ric.Id){
                                    Bathroom__c innerBathroom = new Bathroom__c();
                                    innerBathroom = bic.clone(false, true, false, false);
                                    innerBathroom.RecordTypeId = moveInBicRTId;
                                    innerBathroom.Type_of_BIC__c = 'Move out Check';
                                    
                                    WorkOrderLineItem bathroomWoli = new WorkOrderLineItem();
                                    //bathroomWoli.Subject = 'Bathroom Check';
                                    bathroomWoli.WorkOrderId = wo.Id;	
                                    relUtil.registerRelationship(bathroomWoli, innerBathroom, WorkOrderLineItem.Bathroom_Inspection__c);		
                                    
                                    newBicList.add(innerBathroom);
                                    woliList.add(bathroomWoli);										
                                }
                            }
                        }						
                    }
                }  // end of if else block
            } // end of for loop
            
            insert newHicList;
            insert newRicList;
            insert newBicList;
            
            relUtil.resolve('WorkOrderLineItem');
            insert woliList;
            
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'Move out Check WOLI Creation');		   	
        }
        
    } // end of method
    
    
   
}