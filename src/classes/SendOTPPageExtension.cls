/*********************************************/
global class SendOTPPageExtension {

    global WorkOrder wo {get; set;}
    global Boolean didValidationPass {get; set;}

    global SendOTPPageExtension(ApexPages.StandardController stdController) {
        this.wo = (WorkOrder)stdController.getRecord();
        this.didValidationPass = true;

        WorkOrder workOrderObj = [select Id, StartDate, Auto_Generated_OTP__c, Work_Start_Time__c,
                                  Material_Cost__c from WorkOrder where Id =: wo.Id];

        //If Auto Generated OTP is generated that indicates Start Work button has been clicked
        if(workOrderObj.Work_Start_Time__c != null){
            this.didValidationPass = false;
            String addErrorMessage='Work is already started on the Work Order.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, addErrorMessage));           
        }

        //Material Cost should be entered before starting the work
        if(workOrderObj.Material_Cost__c == null){
            this.didValidationPass = false;
            String addErrorMessage='Please enter the Material cost before starting the work';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, addErrorMessage));           
        }        
    }
  
    @RemoteAction
    global static Boolean autoGeneratedOTP(Id woId){

        System.debug('**autoGeneratedOTP Executed');

        WorkOrder wo = new WorkOrder();
        wo.Id = woId;               

        try{
                Integer autoGeneratedOTP  = Integer.valueOf(Math.random() * 10000);
                
                if(autoGeneratedOTP < 1000){
                    autoGeneratedOTP = 1202;
                }

                WorkOrder workOrderObj = [select Id, CaseId, Material_Cost__c, Labour_Cost__c,
                                          Owner.Name from WorkOrder where Id  =: woId];
                Case c = [Select Id, CaseNumber, Created_Automatically_during_MiMo__c from Case where Id =: workOrderObj.CaseId];

                HttpRequest req = new HttpRequest();
                
                List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].SnM_Notification__c + '?auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.SendOTPRequestJSON OTPreq = new WBMIMOJSONWrapperClasses.SendOTPRequestJSON();
                OTPreq.method = 'send_otp_verification';                
                OTPreq.ticket_number = c.CaseNumber;
                OTPreq.otp_code = autoGeneratedOTP;             
                OTPreq.material_cost = workOrderObj.Material_Cost__c;               
                OTPreq.labour_cost = workOrderObj.Labour_Cost__c;               
                OTPreq.technician_name = workOrderObj.Owner.Name;                             
                
                jsonBody = JSON.serialize(OTPreq);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
                    
                if(res.getStatusCode() == 200) {                                        
                    wo.Send_OTP_API_Success__c = res.getStatus();
                    wo.Send_OTP_API_Info__c = res.getBody();
                    wo.Auto_Generated_OTP__c = autoGeneratedOTP;
                    wo.Status = Constants.WORKORDER_STATUS_OTP_SENT;
                    update wo;                  
                    return true;
                } else{
                    wo.Send_OTP_API_Success__c = res.getStatus();
                    wo.Send_OTP_API_Info__c = res.getBody();    
                    update wo;          
                    return false;
                }             
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Work Order OTP Generation'); 
            wo.Send_OTP_API_Success__c = 'Exception';
            wo.Send_OTP_API_Info__c = '**Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                                                        e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() +
                                                        '\n Cause: ' + e.getCause() + '\nStack Trace ' + 
                                                        e.getStackTraceString();                                        
            update wo;              
            return false;   
        }               
    }    
}