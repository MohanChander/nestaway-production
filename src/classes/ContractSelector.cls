/*  Created By   : Mohan - WarpDrive Tech works
    Created Date : 29/05/2017
    Purpose      : All the queries related to Contract Object will be here */
public class ContractSelector {

    //returns List of Contracts for the given Set of House Id's
    public static List<Contract> getContractListFromHouseIdSet(Set<Id> houseIdSet){

        return [select Id, House__c, Furnishing_Type__c,Approval_Status__c,Booking_Type__c, Furnishing_Plan__c from Contract where House__c =: houseIdSet];
    }

    //Query for Contracts from Opportunities
    public static List<Contract> getContractListFromOpportunities(Set<Id> oppIdSet){

        return [select Id, Status, Furnishing_Type__c,Manually_Approved_By_ZM__c,Security_Deposit_Payment_Mode__c,
         AccountId, Who_pays_Deposit__c,Approval_Status__c, SD_Upfront_Amount__c, Base_House_Rent__c, Opportunity__c, Booking_Type__c, Name__c,
          Agreement_Type__c from Contract where Opportunity__c =: oppIdSet];
    }


    //Query for Contract and House Status
    public static List<Contract> getContractsWithHouseStatus(Set<Id> contractIdset){

        return [select Id, House__c, House__r.Stage__c, Furnishing_Type__c, Booking_Type__c,                             
                Furnishing_Plan__c,Status,Approval_Status__c,Manually_Approved_By_ZM__c,Security_Deposit_Payment_Mode__c,
                AccountId, Who_pays_Deposit__c, SD_Upfront_Amount__c,Security_Deposit_Payment_Details__c, Base_House_Rent__c, Opportunity__c,Opportunity__r.OwnerId, Name__c, Agreement_Type__c
                from Contract where Id =: contractIdset];
    }
}