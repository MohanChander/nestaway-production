@isTest
public class CreateIssuesPageControllerTest {

    @isTest
    public static void testCreateIssues(){
        PageReference pageRef = Page.CaseIssuesPage;
        Test.setCurrentPage(pageRef);

        Test_library.createNestAwayCustomSetting(); 

        Case c = Test_library.createMoveOutCase();

        insert c;

        Id woRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK).getRecordTypeId();

        WorkOrder wo = Test_library.createWorkOrder();
        wo.RecordTypeId = woRtId;
        wo.CaseId = c.Id;
        wo.House__c = c.House__c;

        insert wo;
        
        ApexPages.StandardController stdController=new ApexPages.StandardController(wo);
        CreateIssuesPageController createIssueObj = new CreateIssuesPageController(stdController);

        createIssueObj.addRow();
        createIssueObj.deleteRow();     

        List<CreateIssuesPageController.IssueWrapper> issueWrapList = new List<CreateIssuesPageController.IssueWrapper>();

        Issue__c issue = Test_library.createIssue();

        CreateIssuesPageController.IssueWrapper iw = new CreateIssuesPageController.IssueWrapper(issue);
        issueWrapList.add(iw);

        createIssueObj.issueWrapperList = issueWrapList;

        createIssueObj.onSave();

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MoveOutSettlementCallOutMock());

        createIssueObj.onSaveCreateRp();

        Test.stopTest();
    }
}