/*
* Description: It's controller class of 'SampleContractPDF' page. This will display with contract data as PDF format.
*/

public without sharing class SampleContractPDFController {

    public Contract naContract { get; private set; }
    public List<Room_Terms__c> roomsTermsList { get; private set; }
    public Order salesOrder { get; private set; }
    public Opportunity houseOpportunity { get; private set; }
    public House_Inspection_Checklist__c hic { get; private set; }
    public Account naAccount { get; private set; }
    public Bank_Detail__c naBankDetails { get; private set; }
    public String permanentAddress { get; private set; }
    
    public AddressDetails addressDetails {get; private set; }
    
    public Boolean isSampleContractApproved { get; private set; }
    public Boolean isFinalContractApproved { get; private set; }

    
    public SampleContractPDFController(ApexPages.StandardController controller) {
        Id id = ApexPages.currentPage().getParameters().get('id');
        system.debug('check id' +id+ApexPages.currentPage().getParameters().get('contractId'));
        
        naContract = [SELECT ID, Opportunity__c, Booking_Type__c, Tenancy_Type__c, Base_House_Rent__c, Onboarded_With_Existing_Tenats__c,
                                  Number_of_Bedrooms__c, Number_of_Bathrooms__c, Maximum_number_of_tenants_allowed__c, Tenancy_Approval_time_period__c,
                                  Is_MG_Applicable__c, MG_Amount__c, MG_Start_Date__c, MG_Applicable__c, MG_Payout_Frequency__c,
                                  MG_Valid_Until__c, MG_End_Date__c, Who_pays_Move_in_Charges__c, Who_pays_DTH__c, Who_pays_Society_Maintenance__c,
                                  Who_pays_Water_Bill__c, Who_pays_Electricity_Bill__c, Who_pays_for_Internet__c, Who_pays_Deposit__c,
                                  Deposit_Payment_Date__c, Security_Deposit_Payment_Mode__c, Deposit_Amount_Equivalent_to__c, SD_Upfront_Amount__c,
                                  Restrict_SD_to_maximum_SD__c, Maximum_SD_Amount__c, Furnishing_Type__c, Furnishing_Package__c, Furnishing_Plan__c,
                                  Furnishing_Commission_Percentage__c, Total_furnishing_cost__c, Part_of_furnishing_borne_by_owner__c,Agreement_Type__c,
                                  Part_of_furnishing_borne_by_NestAway__c, Furnishing_Vendor_Name__c, Furnishing_Purchase_type__c, Furniture_Buyback_Option_Available__c,
                                  Furniture_Buyback_Percentage__c, Furniture_Buyback_duration__c, Number_Of_Common_Bathrooms__c, Number_of_attached_Bathrooms__c,
                                  Rental_Plan__c, Annual_Rental_Increase__c, Expected_Monthly_Rent__c, Monthly_Rental_Commission__c, Fixed_Monthly_Rent__c, Name__c, 
                                  Marital_Status__c, Date_Of_Birth__c, Age__c, Father_Husband_Name__c, Relation_with_Guardian__c, Primary_Phone__c, Secondary_Phone__c, 
                                  Service_Agreement_Making_Date__c, Expected_Date_Of_House_Going_Live__c, Service_Agreement_In_Effect_Date__c, Do_You_Want_To_Add_A_POA__c, 
                                  POA_Name__c, POA_Marital_Status__c, POA_Father_Husband_Name__c, POA_Relation_with_Guardian__c, POA_Date__c, POA_Permanent_Address__c, 
                                  POA_Age__c, POA_Witness_Name__c, Security_Deposit_Payment_Details__c, Approval_Status__c
                     FROM Contract WHERE Id = :id];
        
        houseOpportunity = [SELECT Id,Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode,Account.BillingCountry ,Door_Apartment_Number__c, Street__c, State__c, Country__c, Pin_Code__c,Contact_Email__c, City__c, Primary_Contact_Number__c, Secondary_Phone__c,
                                   Account.ShippingStreet, Account.ShippingCity, Account.ShippingState, Account.ShippingPostalCode, Alternate_Contact_Number__c,
                                   Account.ShippingCountry, Account.id From Opportunity where Id=: naContract.Opportunity__C];
        
        List<Bank_Detail__c> naBankDetailsList = [select Name, Account_Number__c, Bank_Name__c, Branch_Name__c, IFSC_Code__c from Bank_Detail__c where Related_Account__c=: houseOpportunity.Account.id];              
        if(!naBankDetailsList.isEmpty()){
            naBankDetails = naBankDetailsList.get(naBankDetailsList.size() - 1);
        }
        
        List<House_Inspection_Checklist__c> hicList = [SELECT ID,Parking_availability__c, Number_of_bike_parking__c, Number_of_car_parking__c, 
                                                       Type_of_parking_available__c from House_Inspection_Checklist__c where 
                                                       (RecordType.Name = 'House Inspection Checklist' or Type_Of_HIC__c = 'House Inspection Checklist')
                                                       and Opportunity__c=: naContract.Opportunity__C];
        
        if(!hicList.isEmpty()){
            hic = hicList.get(hicList.size() - 1);
        }
        roomsTermsList = [select id,Name, Bedroom_Number__c, Monthly_Base_Rent_Per_Bed__c, Monthly_Base_Rent_Per_Room__c, Number_of_beds__c, Rent_Starts_From__c 
                             from Room_Terms__c where Contract__c=: naContract.id order by Id];
    
        if(houseOpportunity != null){
            
            addressDetails = new AddressDetails(houseOpportunity.Account.ShippingStreet, houseOpportunity.Account.ShippingCity, 
                                                houseOpportunity.Account.ShippingState, houseOpportunity.Account.ShippingPostalCode, houseOpportunity.Account.ShippingCountry);
        }
        
        if(naContract != null){
            if(naContract.Approval_Status__c == 'Approved by ZM' || 
               naContract.Approval_Status__c == 'Sample Contract Approved by Owner' || 
               naContract.Approval_Status__c == 'Awaiting HRM Approval' || 
               naContract.Approval_Status__c == 'Approved by HRM' || 
               naContract.Approval_Status__c == 'Approved by Central Team' || 
               naContract.Approval_Status__c == 'Sample Contract Manually Approved by ZM'){
               isSampleContractApproved = true;
            }
            
            if(naContract.Approval_Status__c == 'Approved by Central Team'  || naContract.Approval_Status__c == 'Approved by Owner' ||
               naContract.Approval_Status__c == 'Cloned Contract Manually Approved by ZM'){
               isFinalContractApproved = true;
            }
            
        }
    }
    
    public String getPermanentAddress(){
        return permanentAddress;
    }
    
    public boolean getIsSampleContractApproved(){
        return isSampleContractApproved;
    }
    
    public boolean getIsFinalContractApproved(){
        return isFinalContractApproved;
    }
    
    
    public class AddressDetails{
        public String street { get; private set; }
        public String city { get; private set; }
        public String state { get; private set; }
        public String pincode { get; private set; }
        public String country { get; private set; }
        public String shippingAdress { get; private set; }
        
        
        
        public AddressDetails(String street, String city, String state, String pincode, String country){
            this.street = street;
            this.city = city;
            this.state = state;
            this.pincode = pincode;
            this.country = country;
            this.shippingAdress = this.street+', '+this.city+', '+this.state+', '+this.pincode+', '+this.country;
        }
        
        public String street(){
            return this.street;
        }
        
        public String city(){
            return this.city;
        }
        
        public String state(){
            return this.state;
        }
        
        public String pincode(){
            return this.pincode;
        }
        
        public String country(){
            return this.country;
        }
        
        public String shippingAdress(){
            return this.shippingAdress;
        }
    
    }

}