// added by deepak-warpdrivetech
// to handle the insurance related to house
public class InsuranceTriggerHandler {
    Public Static Id insHouseRTId = Schema.SObjectType.Insurance__c.getRecordTypeInfosByName().get(Constants.HOUSE_INSURANCE_RT).getRecordTypeId();
    Public Static Void afterUpdate(Map<Id,Insurance__c> newMap,Map<Id,Insurance__c> oldMap){
        try{
            system.debug('InsuranceTriggerHandler');
            System.debug('newMap'+newMap);
            System.debug('oldMap'+oldMap);
            Set<Id> setOfInsurance = new Set<Id>();
            for(Insurance__c  each:newMap.values()){
                if(each.Status__c != null && each.Status__c != oldMap.get(each.id).Status__c &&  each.Status__c == 'To Be Queued') {
                    system.debug(' it is coming here');
                    SendAPIRequests.sendInsuranceDetailsToWebAppFuture(each.id);    
                }
                if(each.Status__c != null && each.Status__c != oldMap.get(each.id).Status__c &&  each.Status__c == 'new' && oldMap.get(each.id).Status__c =='To Be Queued') {
                    newmap.get(each.id).adderror('Status cannot be changed from To Be Queued to New');
                }
            }
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
    Public Static Void beforeInsert(List<Insurance__c> InsuranceList){
        try{
            system.debug('afterInsert');
            Set<Id> setOfHouse = new Set<Id>();
            List<Insurance__c> insList = new List<Insurance__c>();
            List<Insurance__c> updateInsList = new List<Insurance__c>();
            for(Insurance__c  each:InsuranceList){
                if(each.RecordTypeId != null && each.RecordTypeId == insHouseRTId){
                    setOfHouse.add(each.House__c);
                }
            }
            system.debug('setOfHouse'+setOfHouse);
            if(!setOfHouse.isEmpty()){
                insList = [Select id,Status__c  from Insurance__c where house__c  in:setOfHouse];
            }
            system.debug('insList'+insList);
            if(!insList.isEmpty()){
                for(Insurance__c each:insList){
                    each.Status__c= 'Expired';
                    updateInsList.add(each);
                }
            }
            
            if(!updateInsList.isEmpty()){
                update updateInsList;
            }
            system.debug('updateInsList'+updateInsList);
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
    Public Static Void afterInsert(List<Insurance__c> insList){
        try{
            system.debug('afterInsert');
            Set<Id> setOfInsurance = new Set<Id>();
            for(Insurance__c  each:insList){
                if(each.Status__c != null &&  each.Status__c == Constants.INSURANCE_STATUS_TYPE_TO_BE_QUEUED) {
                    system.debug(' it is coming here');
                    SendAPIRequests.sendInsuranceDetailsToWebAppFuture(each.id);    
                }
            } 
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
    public static void changeInsuranceStatus(List<Case> caseList){
        system.debug('changeInsuranceStatus');
        Set<Id> setOfHouse = new Set<Id>();
        List<Insurance__c> insList = new List<Insurance__c>();
        List<House__c> houseList = new List<House__c>();
        for(Case each : caseList){
            if(each.house__c != null){
                setOfHouse.add(each.house__c);
            }
        }
        if(!setOfHouse.isEmpty()){
            houseList = [Select id,Contract__r.Agreement_Type__c,(Select id,Status__c  from  Insurances__r where Status__c = 'new') 
                         from house__c where id in:setofHouse ];
            system.debug('houseList'+houseList);
            if(!houseList.isEmpty()){
                for(House__c each : houseList){
                    if(each.Insurances__r != null && each.Insurances__r.size() > 0){
                        if(each.Contract__r.Agreement_Type__c == Constants.CONTRACT_AGREEMENT_TYPE_E_STAMPED || each.Contract__r.Agreement_Type__c == Constants.CONTRACT_AGREEMENT_TYPE_E_NOTARIZED ){
                            for(Insurance__c ins  : each.Insurances__r){
                                ins.status__c = Constants.INSURANCE_STATUS_TYPE_TO_BE_QUEUED; 
                                insList.add(ins);
                            }
                        }
                    }
                }
            }
                         system.debug('insList');
            if(!insList.isEmpty()){
                update insList;
            }
        }
    }  
}