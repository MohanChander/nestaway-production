public class OrderItemTriggerHandler {

	public static void ChangePOStageToVerified(Map<Id, OrderItem> newMap, Map<Id, OrderItem> oldMap){

		System.debug('ChangePOStageToVerified is executed');

		Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>>();

		for(OrderItem oi: newMap.values()){
			if(oi.Verified__c == true && oldMap.get(oi.Id).Verified__c == false){
				if(orderItemMap.containsKey(oi.OrderId)){
					orderItemMap.get(oi.OrderId).add(oi);
				} else {
					orderItemMap.put(oi.OrderId, new List<OrderItem>{oi});
				}
			}
		}

		List<Order> ordList = [select Id, Status, (select Id, Verified__c from OrderItems) from Order
							   where RecordType.Name = 'Purchase Order' and Id =: orderItemMap.keySet()];

		List<Order> updatedOrdList = new List<Order>();							   

		for(Order ord: ordList){

			Boolean poVerified = true; 

			for(OrderItem oi: ord.OrderItems){
				if(oi.verified__c == false){
					poVerified = false;
				}
			}

			ord.Status = 'PO Verified';

			if(poVerified){
				updatedOrdList.add(ord);
			}
		}

		System.debug(updatedOrdList);

		if(!updatedOrdList.isEmpty()){
			update updatedOrdList;
		}

		if(!orderItemMap.isEmpty()){
			InvoiceUtility.createInvoicesForOrders(orderItemMap);
		}
	}
}