/*
Created By Baibhav - WarpDrive
*/

public class CaseOffboardingTriggerHandler 
{
/*
 Created By Baibhav - WarpDrive
*/
  public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
  public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
  public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
  public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
  public static id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
  public static id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
  

  public static void onBeforeInsertCaseOffboarding(List<case> caseList)
  {
  try{
   List<case> newcaseList = new List<case>();
   List<Case> ownerSattlementcaseList = new List<Case>();
   List<Case> nestawySattlementcaseList = new List<Case>();

    for(case ca:caseList)
    {
      if(ca.RecordTypeId ==caseOccupiedFurnishedRecordtype || ca.RecordTypeId ==caseOccupiedUnfurnishedRecordtype||
         ca.RecordTypeId ==caseUnoccupiedFurnishedRecordtype||ca.RecordTypeId ==caseUnoccupiedUnfurnishedRecordtype){
          newcaseList.add(ca); 
      }

      //populate NestAway Settlement Case Fields
      if(ca.RecordTypeId == nestSattlementCaseRecordtype){
        nestawySattlementcaseList.add(ca);
      }

      //populate Owner Settlement Case Fields
      if(ca.RecordTypeId == ownerSattlementCaseRecordtype){
        ownerSattlementcaseList.add(ca);
      }      
      
    } 

    if(!newcaseList.isEmpty()){
      CaseHouseOffboardingTriggerHelper.houseOffboardingCaseInsertValidation(newcaseList);
     // CaseHouseOffboaringAssignment.houseOffboardingCaseAssignment(newcaseList);      
    }

    if(!nestawySattlementcaseList.isEmpty()){
      CaseHouseOffboardingTriggerHelper.populateNestAwaySettlementCaseFields(nestawySattlementcaseList);
      CaseHouseOffboardingTriggerHelper.updateCaseOwnerToQueue(nestawySattlementcaseList);
    }

    if(!ownerSattlementcaseList.isEmpty()){
      CaseHouseOffboardingTriggerHelper.populateOwnerSettlementCaseFields(ownerSattlementcaseList);
    }    

    } catch(Exception e)
     {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
      UtilityClass.insertGenericErrorLog(e);  
     }
   }

/*
 Created By Baibhav - WarpDrive
*/
  public static void onAfterInsertCaseOffboarding(List<case> caseList, Map<id,Case> oldMap)
  {
    try{
    List<case> offcaseList = new List<case>();
     for(Case ca:caseList)
    {
      if(ca.HouseForOffboarding__c!=null && (ca.RecordTypeId ==caseOccupiedFurnishedRecordtype || ca.RecordTypeId ==caseOccupiedUnfurnishedRecordtype||
         ca.RecordTypeId ==caseUnoccupiedFurnishedRecordtype||ca.RecordTypeId ==caseUnoccupiedUnfurnishedRecordtype))
      {
          offcaseList.add(ca);
      }
          
    }
  

   if(!offcaseList.isEmpty())
   CaseHouseOffboardingTriggerHelper.houseOffboardingWorkOrderCreationOnCaseInsert(offcaseList);
 } catch(Exception e)
     {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
      UtilityClass.insertGenericErrorLog(e);  
     }
   
  }

  public static void onAfterUpdateCaseOffboarding(List<case> caseList, Map<Id, Case> oldMap)
  {
    try{
    List<Case> ownerRetainedCases = new List<Case>();
    for(Case ca:caseList)
    {
      if(ca.RecordTypeId ==caseOccupiedFurnishedRecordtype || ca.RecordTypeId ==caseOccupiedUnfurnishedRecordtype||
         ca.RecordTypeId ==caseUnoccupiedFurnishedRecordtype||ca.RecordTypeId ==caseUnoccupiedUnfurnishedRecordtype){

           //When the Offboarding Case is Owner Retained - Update the House
           if(ca.Stage__c == Constants.CASE_STAGE_OWNER_RETAINED && ca.Stage__c != oldMap.get(ca.Id).Stage__c){
              ownerRetainedCases.add(ca);
           }
         } 
    }

      if(!ownerRetainedCases.isEmpty()){
        CaseHouseOffboardingTriggerHelper.updateHouseInitiateOffboarding(ownerRetainedCases);
      }
  } catch(Exception e)
     {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
      UtilityClass.insertGenericErrorLog(e);  
     }

  }
  public static void onBeforeUpdateandInsert(List<Case> caseList)
  { 
    List<case> nestawySattlementcaseList = new List<case>();
    List<case> ownerSattlementcaseList = new List<case>();

     for(Case ca:caseList)
     {
      if(ca.RecordTypeId ==nestSattlementCaseRecordtype)
      nestawySattlementcaseList.add(ca);

      if(ca.RecordTypeId ==ownerSattlementCaseRecordtype)
      ownerSattlementcaseList.add(ca);
     }

      if(!nestawySattlementcaseList.isEmpty()){
        CaseHouseOffboardingTriggerHelper.nestSattlementCaseAssignment(nestawySattlementcaseList);
      }

      if(!ownerSattlementcaseList.isEmpty()){
        CaseHouseOffboardingTriggerHelper.ownerSattlementCaseAssignment(ownerSattlementcaseList);
      }
  } 

   public static void onBeforeUpdate(List<Case> caseList, Map<id,case> oldmap)
   {
     try{
    List<case> caseCreationList = new List<case>();
    List<Case> queueAssignmentCaseList = new List<Case>();

    for(Case ca:caseList)
    {
      Case oldCase = oldMap.get(ca.Id);

      if(ca.RecordTypeId ==caseOccupiedFurnishedRecordtype || ca.RecordTypeId ==caseOccupiedUnfurnishedRecordtype||
         ca.RecordTypeId ==caseUnoccupiedFurnishedRecordtype||ca.RecordTypeId ==caseUnoccupiedUnfurnishedRecordtype){

        /* Condition is not valid - Commented By Mohan
        if(ca.stage__c==Constants.CASE_STAGE_KEY_SUBMITTED && oldmap.get(ca.id).stage__c!=Constants.CASE_STAGE_KEY_SUBMITTED){
          ca.status=Constants.CASE_STATUS_CASE_CLOSED;
        }  */

        if(ca.Agreement_Cancelled__c == 'Yes' &&  ca.Stage__c != Constants.CASE_STAGE_AGREEMENT_CANCELLED && 
            ca.Agreement_Cancelled__c != oldMap.get(ca.id).Agreement_Cancelled__c){
          ca.addError('Please Ensure that Case Stage is Agreement Cancellation');
        }  
        
        if(ca.Keys_Submitted__c == 'Yes' && ca.Keys_Submitted__c != oldmap.get(ca.id).Keys_Submitted__c &&
           ca.Stage__c != Constants.CASE_STAGE_KEY_SUBMITTED){
              ca.addError('Please Ensure that Case Stage is Keys Submission');
        }       

        if(ca.Agreement_Cancelled__c == 'Yes' &&  ca.Agreement_Cancelled__c != oldmap.get(ca.id).Agreement_Cancelled__c){
          ca.Stage__c = Constants.CASE_STAGE_KEY_SUBMITTED;
        }
        
        if(ca.Keys_Submitted__c == 'Yes' && ca.Keys_Submitted__c != oldmap.get(ca.id).Keys_Submitted__c){
          ca.Stage__c = Constants.CASE_STAGE_OFFBOARDING_COMPLET;
          ca.Status = Constants.CASE_STATUS_CLOSED;
        } 

        //when Case is Owner Retained the Case Status is Closed
        if(ca.Stage__c == Constants.CASE_STAGE_OWNER_RETAINED && ca.Stage__c != oldMap.get(ca.Id).Stage__c)
          ca.Status = Constants.CASE_STATUS_CLOSED;
      }

      if(ca.RecordTypeId == ownerSattlementCaseRecordtype){
        if(ca.Cheque_Status__c == Constants.CASE_CHEQUE_STATUS_CHEQUE_NOT_RECEIVED &&
          ca.Cheque_Status__c != oldMap.get(ca.Id).Cheque_Status__c){
            ca.Status = Constants.CASE_STATUS_CLOSED;
        } 

        if(ca.Cheque_Status__c == Constants.CASE_CHEQUE_STATUS_CHEQUE_BOUNCED && 
          ca.Cheque_Status__c != oldMap.get(ca.Id).Cheque_Status__c){
            ca.Status = Constants.CASE_STATUS_CLOSED; 
        }

        if(ca.Stage__c == Constants.CASE_STAGE_ACCOUNTING_ENTRY_PASSED &&
           ca.Stage__c != oldMap.get(ca.Id).Stage__c){
            ca.Status = Constants.CASE_STATUS_CLOSED;
        }        
      }

      //when Case NestAway Settlement - Status is changed Assigned to the respective Queue
      if(ca.RecordTypeId ==nestSattlementCaseRecordtype && (ca.Status == Constants.CASE_STATUS_IN_NEW_PAYMENT_VERIFIED ||
          ca.Status == Constants.CASE_STATUS_IN_PAID) && oldCase.Status != ca.Status){
          queueAssignmentCaseList.add(ca);
      }     

      //when Case Owner Settlement - Stage is changed
      if(ca.RecordTypeId ==ownerSattlementCaseRecordtype && ca.Stage__c == Constants.CASE_STAGE_INVOICE_UPDATED_AND_CHECKED &&
          oldCase.Stage__c != ca.Stage__c){
          queueAssignmentCaseList.add(ca);
      }  
   }

   if(!queueAssignmentCaseList.isEmpty())
    CaseHouseOffboardingTriggerHelper.updateCaseOwnerToQueue(queueAssignmentCaseList);
} catch(Exception e)
     {
      System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
      UtilityClass.insertGenericErrorLog(e);  
     }
}
}