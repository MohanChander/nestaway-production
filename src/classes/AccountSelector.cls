/*  Created By : Mohan
    Purpose    : All the Queries related to Account are accessed from here */

public class AccountSelector {

    //get Account details from accountIdSet
    public static List<Account> getAccountDetailsFromSet(Set<Id> accountIdSet){
        return [select Id, PersonEmail,Api_Success__c from Account where Id =: accountIdSet];
    }

    //get NestAway Account Id
    public static Account getNestAwayAccount(){
        return [select Id, Name from Account where PersonEmail =: Label.API_User_Email];
    }
    //get map of Account
    public static Map<id,Account> getAccountMapfromAccId(Set<id> accidSet)
    {
     return new Map<id,Account>([SELECT id,BillingCity,BillingCountry,BillingCountryCode,BillingGeocodeAccuracy,BillingLatitude,BillingLongitude,BillingPostalCode,BillingState,BillingStateCode,BillingStreet,ShippingCity,
        ShippingCountry,ShippingCountryCode,ShippingGeocodeAccuracy,ShippingLatitude,ShippingLongitude,ShippingPostalCode,
        ShippingState,ShippingStateCode,ShippingStreet FROM Account where id=:accidSet]);
    }
}