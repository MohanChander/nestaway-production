@Istest()
public class HouseRentPageExtension02Test {
 
    Public Static TestMethod Void doTest(){
        User newUser = Test_library.createStandardUser(1);
        newUser.isActive=true;
        newUser.isAvailableForAssignment__c = true;
        insert newuser;      
        
        City__c newcity= new City__c();
        newcity.name='bangalore';
        insert newcity;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City_Master__c=newcity.id;
        hos.Onboarding_Zone_Code__c  = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.City__c='Bangalore';
        hos.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;
           ApexPages.currentPage().getParameters().put('id',hos.Id);
        ApexPages.StandardController sC = new ApexPages.standardController(hos);
        HouseRentPageExtension02 chp= new HouseRentPageExtension02(sC);
        chp.updateRent();
    }
}