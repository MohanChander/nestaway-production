/**********************************************************************************
Added by Baibhav
Purpose: For handling function of Onboarding Workflowing

**********************************************************************************/
public class workOrderOnboardingHelper {

/**********************************************************************************
Added by Baibhav
Purpose: For Bringing HIC RIC and BIC on Onboading workflow
		 1) Invoke on Creation of Maintances Onboarding Workorder
		 2) Invoke on Creation of Furnishing Onboarding Workorder
		 3) Invoke on Creation of Varification Onboarding Workorder
**********************************************************************************/
	public static void BringHicRicBicOnWorkOrder(List<workOrder> wrkList){

		try{
			Id hicOnFurBHKId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Furnished_BHK).getRecordTypeId();
			Id hicOnFurRoomId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Furnished_Room).getRecordTypeId();
			Id hicOnFurStudioId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Furnished_Studio).getRecordTypeId();

			Id hicOnPreFurBHKId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_BHK).getRecordTypeId();
			Id hicOnPreFurStudioId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_Studio).getRecordTypeId();
			Id hicOnPreFurRoomId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_Room).getRecordTypeId();

			Id hicOnPreUNFurBHKId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_BHK).getRecordTypeId();
			Id hicOnPreUNFurStudioId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Studio).getRecordTypeId();
			Id hicOnPreUNFurRoomId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Room).getRecordTypeId();
	        
			Id hicOnVerFurBHKId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Furnished_BHK).getRecordTypeId();
			Id hicOnVerFurStudioId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Furnished_Studio).getRecordTypeId();
			Id hicOnVerFurRoomId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Furnished_Room).getRecordTypeId();

			Id hicOnVerUNFurBHKId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Unfurnished_BHK).getRecordTypeId();
			Id hicOnVerUNFurStudioId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Unfurnished_Studio).getRecordTypeId();
			Id hicOnVerUNFurRoomId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Verification_Unfurnished_Room).getRecordTypeId();

			Id ricFurRoomId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_Furnished).getRecordTypeId();

			Id ricPreFurRoomId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_Prec_furnished).getRecordTypeId();
			Id ricPreUnFurRoomId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_Prec_Unfurnished).getRecordTypeId();
	     
	        Id ricVerFurRoomId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_Verif_Furnished).getRecordTypeId();
			Id ricVerUnFurRoomId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_Verif_Unfurnished).getRecordTypeId();

			Id bicFurRoomId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Furnishing).getRecordTypeId();

			Id bicPreFurRoomId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Prec_Furnished).getRecordTypeId();
			Id bicPreUnFurRoomId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Prec_Unfurnished).getRecordTypeId();
	     
	        Id bicVerFurRoomId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Verif_Furnished).getRecordTypeId();
			Id bicVerUnFurRoomId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Verif_Unfurnished).getRecordTypeId();

			Set<Id> houIdSet=new Set<Id>();
			for(workorder wrk:wrklist){
	    		houIdSet.add(wrk.House__c);
			}

			List<House_Inspection_Checklist__c>  existingHICLst  = new List<House_Inspection_Checklist__c>();
			Map<id,House__c> houMaP= new Map<id,House__c>();
	        List<Bathroom__c> bicList=new List<Bathroom__c>();

	        if(!houIdSet.isEmpty()){
	        	// Querrying HOuse
	        	houMaP =new Map<id,House__c>([Select id,House_Layout__c,Furnishing_Type__c from house__c where id=:houIdSet]);

	        	//querrying Hic and (RIC,comman Bathroom) in house
	         	existingHICLst  = [select id,name,House__c,Furnishing_condition__c,Onboarding_House_Type__c,recordTypeId,
	                                            recordtype.name,(select id,name,recordTypeId,Type_of_RIC__c,recordtype.name from Room_Inspection__r where (Type_of_RIC__c=:Constants.ROOM_INSPECTION_TYPE_OF_RIC_ROOM_INSPECTION or Type_of_RIC__c=null)),
	                                            (select id,name,recordTypeId,recordtype.name from Bathrooms__r where (Type_of_BIC__c=:Constants.BATHROOM_TYPE_OF_BIC_BATHROOM_INSPECTION or Type_of_BIC__c=null))
	                                            from House_Inspection_Checklist__c
	                                            where house__c=:houIdSet and Type_Of_HIC__c =:Constants.CHECKLIST_TYPE_HIC];

	            bicList=[select id,name,recordTypeId,recordtype.name,house__c from Bathroom__c where House__c=:houIdSet];

	        }
	        System.debug('****Onboard**HIC**'+existingHICLst);
	        System.debug('****Onboard**HIC**'+bicList);

	        Map<id,List<Room_Inspection__c>> mapListRicToHic=new Map<id,List<Room_Inspection__c>>();
	        Map<id,List<Bathroom__c>> mapListBicTohou=new Map<id,List<Bathroom__c>>();
	        Map<id,House_Inspection_Checklist__c> mapHic=new Map<id,House_Inspection_Checklist__c>();
	        

	         if(!existingHICLst.isEmpty()){
	                 for(House_Inspection_Checklist__c hic:existingHICLst){
	                     // Creating Hic map to House
	                 	mapHic.put(hic.House__c,hic);
	         
	                     // Creating List of RIC map to House
	                     for(Room_Inspection__c ric:hic.Room_Inspection__r){
	                     	List<Room_Inspection__c> riList=new List<Room_Inspection__c>();
	                     	if(mapListRicToHic.containsKey(hic.house__c)){
	                            riList=mapListRicToHic.get(hic.house__c);
	                     	}
	                     	riList.add(ric);
	                     	mapListRicToHic.put(hic.house__c,riList);
	                     }
	                 }
	         }

	               if(!bicList.isEmpty()){
	                   // Creating List of BIC map to House
		                 for(Bathroom__c bic:bicList){
		                    	List<Bathroom__c> biList=new List<Bathroom__c>();
		                    	if(mapListBicTohou.containsKey(bic.house__c)){
		                           biList=mapListBicTohou.get(bic.house__c);
		                    	}
		                    	biList.add(bic);
		                    	mapListBicTohou.put(bic.house__c,biList);
		                    }
	                }
	       /***************************** logic to change recordtypr*******************************************/
	        List<House_Inspection_Checklist__c> updateHIC=new List<House_Inspection_Checklist__c>();
	        List<Room_Inspection__c> updateRIC=new List<Room_Inspection__c>();
	        List<Bathroom__c> updateBIC=new List<Bathroom__c>();


	       for(Workorder wrk:wrkList){
	       
	        	System.debug('****Onboard***');
	          if(houMaP.containsKey(wrk.house__c)){ 
	            if(mapHic.containsKey(wrk.house__c)){
	            	// Fetching the HIC RIC BIC linked to Workorder

	            	House_Inspection_Checklist__c hic=new House_Inspection_Checklist__c(); 
	            	List<Room_Inspection__c> ricLis = new List<Room_Inspection__c>();
	            	List<Bathroom__c> bicLis =new List<Bathroom__c>();

	            	if(mapHic.containsKey(wrk.house__c)) {
    		              hic=mapHic.get(wrk.house__c);
    		            }
    		        if(mapListRicToHic.containsKey(wrk.house__c)){   
         	        	      ricLis=mapListRicToHic.get(wrk.house__c);
         	        	  }
	        	    if(mapListBicTohou.containsKey(wrk.house__c)){
	    	        	  bicLis=mapListBicTohou.get(wrk.house__c);
	    	        	}
	    	        	
	                  //checking frnish type
		         	  if(houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_SEMI_FURNISHED 
		         	 	|| houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_FURNISHED){
		 
			         	 	if(houMaP.get(wrk.house__c).House_Layout__c.contains('BHK')){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreFurBHKId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT){
			                       hic.recordTypeId=hicOnFurBHKId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerFurBHKId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   else if(houMaP.get(wrk.house__c).House_Layout__c=='1 R' || houMaP.get(wrk.house__c).House_Layout__c=='1 RK'){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreFurRoomId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT){
			                       hic.recordTypeId=hicOnFurRoomId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerFurRoomId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   else if(houMaP.get(wrk.house__c).House_Layout__c=='Studio'){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreFurStudioId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT){
			                       hic.recordTypeId=hicOnFurStudioId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerFurStudioId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   if(!ricLis.isEmpty()){
			        	 	   for(Room_Inspection__c ric:ricLis){
			        	 	   	 if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       ric.recordTypeId=ricPreFurRoomId;
			                       ric.Work_Order__c=wrk.id;
			                       updateRIC.add(ric);
				         	 	   }
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT){
				                       ric.recordTypeId=ricFurRoomId;
				                       ric.Work_Order__c=wrk.id;
				                       updateRIC.add(ric);
				         	 	   }
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
				                       ric.recordTypeId=ricVerFurRoomId;
				                       ric.Work_Order__c=wrk.id;
				                       updateRIC.add(ric);
				         	 	   }
			        	 	   }
			        	 	}
			        	 	if(!bicLis.isEmpty()){
			        	 	   for(Bathroom__c bic:bicLis){
			        	 	   	 if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       bic.recordTypeId=bicPreFurRoomId;
			                       bic.Work_Order__c=wrk.id;
			                       updateBIC.add(bic);
				         	 	   }
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT){
				                       bic.recordTypeId=bicFurRoomId;
				                       bic.Work_Order__c=wrk.id;
				                       updateBIC.add(bic);
				         	 	   }
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
				                       bic.recordTypeId=bicVerFurRoomId;
				                       bic.Work_Order__c=wrk.id;
				                       updateBIC.add(bic);
				         	 	   }
			        	 	   }
			        	 	}
			         	}
			         	else if(houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_UNFURNISHED){
		 
			         	 	if(houMaP.get(wrk.house__c).House_Layout__c.contains('BHK')){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreUNFurBHKId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	 
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerUNFurBHKId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   else if(houMaP.get(wrk.house__c).House_Layout__c=='1 R' || houMaP.get(wrk.house__c).House_Layout__c=='1 RK'){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreUNFurRoomId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	  
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerUNFurRoomId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   else if(houMaP.get(wrk.house__c).House_Layout__c=='Studio'){

			         	 	   if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       hic.recordTypeId=hicOnPreUNFurStudioId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
			         	 	  
			         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
			                       hic.recordTypeId=hicOnVerUNFurStudioId;
			                       hic.Work_Order__c=wrk.id;
			                       updateHIC.add(hic);
			         	 	   }
		        	 	   }
		        	 	   if(!ricLis.isEmpty()){
			        	 	   for(Room_Inspection__c ric:ricLis){
			        	 	   	 if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       ric.recordTypeId=ricPreUNFurRoomId;
			                       ric.Work_Order__c=wrk.id;
			                       updateRIC.add(ric);
				         	 	   }
				         	 	  
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
				                       ric.recordTypeId=ricVerUNFurRoomId;
				                       ric.Work_Order__c=wrk.id;
				                       updateRIC.add(ric);
				         	 	   }
			        	 	   }
			        	 	}
			        	 	if(!bicLis.isEmpty()){
			        	 	   for(Bathroom__c bic:bicLis){
			        	 	   	 if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
			                       bic.recordTypeId=bicPreUNFurRoomId;
			                       bic.Work_Order__c=wrk.id;
			                       updateBIC.add(bic);
				         	 	   }
				         	 	
				         	 	   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
				                       bic.recordTypeId=bicVerUNFurRoomId;
				                       bic.Work_Order__c=wrk.id;
				                       updateBIC.add(bic);
				         	 	   }
			        	 	   }
			        	 	}
			         	}

	        	}
	          }
	       }

	       if(!updateHIC.isEmpty()){
	        System.debug('****Onboard**h*');
	       	update updateHIC;
	       }

	       if(!updateRIC.isEmpty()){
	        System.debug('****Onboard**R**');
	       	update updateRIC;
	       }

	       System.debug('**updateBIC' + updateBIC);

	       if(!updateBIC.isEmpty()){
	        System.debug('****Onboard**B**');
	       	update updateBIC;
	       }
	     }Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder Hic RIC');
            }        
   	}

/**********************************************************************************
Added by Baibhav
Purpose: For Changing On boarding Case satge
		 1) Invoke on closing of Key handling Onboarding Workorder
		 2) Invoke on Closing of Onboarding Workorder
		
**********************************************************************************/
public static void OnboardingcaseStage(List<Workorder> wrklist){
	try{
	    Set<id> casIdset=new Set<id>();
	    Set<id> houIdSet=new Set<id>();
        System.debug('****test**');
	    for(workorder wrk : wrkLIst){
	    	casIdset.add(wrk.CaseId);
	    	houIdSet.add(wrk.house__c);
	    }
	    Map<id,case> casMap=new Map<id,case>();
	    Map<id,House__c> houMap=new Map<id,House__c>();
	    List<case> updatecas=new List<case>();
	    // Fetching case details
	    if(!casIdset.isEmpty()){
	    	casMap=new Map<id,case>([Select id,Status from case where id=:casIdset]);
	    }
	    // Fetching house details
	    if(!houIdSet.isEmpty()){ 
	        houMaP =new Map<id,House__c>([Select id,House_Layout__c,Furnishing_Type__c from house__c where id=:houIdSet]);
	    }

	    for(workorder wrk : wrkLIst){
	    	if(wrk.Case_Status__c==Constants.CASE_STATUS_KEY_HANDLING){
	    		SYStem.debug('******Key**');
	            casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_MAINTENANCE;     
	    	}
	    	else if(wrk.Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
	    		SYStem.debug('******main**');
	           if(houMaP.containsKey(wrk.house__c) && houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_SEMI_FURNISHED 
		           || houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_FURNISHED){

	           	  casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;     
	           }  
	           else if(houMaP.containsKey(wrk.house__c) && houMaP.get(wrk.house__c).Furnishing_Type__c==Constants.FURNISHING_CONDITION_UNFURNISHED) {
	           	casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_VERIFICATION;
	           } 
	    	}
	    	else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT && wrk.subject.contains('Furnishing')){
	            casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_VERIFICATION;     
	    	}
	    	else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
	            casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_PHOTOGRAPHY;     
	    	}
	    	else if(wrk.Case_Status__c==Constants.CASE_STATUS_PHOTOGRAPHY){
	            casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;     
	    	}
	    	else if(wrk.Case_Status__c==Constants.CASE_STATUS_MAKE_HOUSE_LIVE){
	            casMap.get(wrk.caseId).Status=Constants.CASE_STATUS_CLOSED;     
	    	}
	    	updatecas.add(casmap.get(wrk.caseid));

	    } 
	    if(!updatecas.isEmpty()){
	    	System.debug('***case**'+updatecas);
	    	update updatecas;
	    }
	}Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder close');
            }
}
/**********************************************************************************
Added by Baibhav
Purpose: For Validation on Onboarding workorder
		 1) Invoke on Change of Key handling Onboarding Workorder
		 2) Invoke on change of Onboarding Workorder
		
**********************************************************************************/
public static void OnboardingWrkValidation(List<Workorder> wrklist,Map<id,workorder> oldMap){

	try{
		 Set<id> wrkIdSet =new Set<id>();
		 Set<id> houIdSet =new Set<id>();
		 for(Workorder wrk:wrklist){
		 	wrkIdSet.add(wrk.id);
		 	houIdSet.add(wrk.house__c);
		 }
		 list<Detail__c> keyList= new list<Detail__c>();	
		 List<House_Inspection_Checklist__c> hiclist=new List<House_Inspection_Checklist__c>();
		 List<Room_Inspection__c> ricList=new List<Room_Inspection__c>();
		 List<Bathroom__c> bicList=new List<Bathroom__c>();
		 Map<id,house__c> houMap=new Map<id,house__c>();
		 if(!wrkIdSet.isEmpty()){
		 	houMap=new Map<id,house__c>([Select id,Opportunity__c,Rent_Verified__c,Locality__c,Sub_Locality__c from house__c where id=:houIdSet]);
		 	keyList=[Select id,Work_Order__c from Detail__c where Work_Order__c=:wrkIdSet];
		 	hiclist=[Select id,Work_Order__c,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c from House_Inspection_Checklist__c where Work_Order__c=:wrkIdSet];
		 	ricList=[Select id,Work_Order__c,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c from Room_Inspection__c where Work_Order__c=:wrkIdSet];
		 	bicList=[Select id,Work_Order__c,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c from Bathroom__c where Work_Order__c=:wrkIdSet];	 		 	
		 }
		 Map<id,House_Inspection_Checklist__c> hicMAp= new Map<id,House_Inspection_Checklist__c>();
		 Map<id,List<Room_Inspection__c>> riclistMAp= new Map<id,List<Room_Inspection__c>>();
		 Map<id,List<Bathroom__c>> biclistMAp= new Map<id,List<Bathroom__c>>();
		 Map<id,List<Detail__c>> keylistMAp= new Map<id,List<Detail__c>>();
		 Map<Id,House_Inspection_Checklist__c> dccMap=new Map<Id,House_Inspection_Checklist__c>();
		 Set<ID> oppIdSet=new Set<ID>(); 
		 for(House__c ho:houMap.values()){
             oppIdSet.add(ho.Opportunity__c);
		 }

		 List<House_Inspection_Checklist__c> dccList = new List<House_Inspection_Checklist__c>(); 
          
          if(!oppIdSet.isEmpty()){
          	  dccList = HouseInspectionCheckListSelector.getDCCListForOpportunities(oppIdSet);
          	}
                
                if(!dccList.isEmpty()){
                        for(House_Inspection_Checklist__c dcc: dccList){
                            dccMap.put(dcc.Opportunity__c, dcc);
                        }
                    }


         if(!hiclist.isEmpty()){
     		 for(House_Inspection_Checklist__c hic:hiclist){
     		 	hicMAp.put(hic.Work_Order__c,hic);
     		 }
     		}
     	 if(!ricList.isEmpty()){	
 	 		 for(Room_Inspection__c ric:ricList){
 	 		 	list<Room_Inspection__c> rlist=new list<Room_Inspection__c>();
 	 		 	if(riclistMAp.containsKey(ric.Work_Order__c)){
 	 		 		rlist=riclistMAp.get(ric.Work_Order__c);
 	 		 	}
 	 		 	rlist.add(ric);
 	 		 	riclistMAp.put(ric.Work_Order__c,rlist);
 	 		 }
 	 		}
 	 		if(!bicList.isEmpty()){
				 for(Bathroom__c bic:bicList){
				 	list<Bathroom__c> blist=new list<Bathroom__c>();
				 	if(biclistMAp.containsKey(bic.Work_Order__c)){
				 		blist=biclistMAp.get(bic.Work_Order__c);
				 	}
				 	blist.add(bic);
				 	biclistMAp.put(bic.Work_Order__c,blist);
				 }
				}
		  if(!keyList.isEmpty()){		
	  		  for(Detail__c ke:keyList){
	  		 	list<Detail__c> klist=new list<Detail__c>();
	  		 	if(keylistMAp.containsKey(ke.Work_Order__c)){
	  		 		klist=keylistMAp.get(ke.Work_Order__c);
	  		 	}
	  		 	klist.add(ke);
	  		 	keylistMAp.put(ke.Work_Order__c,klist);
	  		 }
	  		}
	/*************************************************************/
		  for(Workorder wrk:wrklist){
		  	if(oldMap.get(wrk.id).Case_Status__c==Constants.CASE_STATUS_MAINTENANCE){
		  	 if(hicMAp.containsKey(wrk.id) && hicMAp.get(wrk.id).Maintenance_Verified__c!='Yes'){
	            wrk.addError('Maintance of Hic is not comleted');
		  	 }
		  	 if(riclistMAp.containsKey(wrk.id)){
  	 		  	 for(Room_Inspection__c ri:riclistMAp.get(wrk.id)){
  	 		  	 	if(ri.Maintenance_Verified__c!='Yes'){
  	 		  	      wrk.addError('Maintance of Ric is not comleted');		
  	 		  	 	}
  	 
  	 		  	 }
  	 		  	}
  	 		 if(biclistMAp.containsKey(wrk.id)){	
	 		  	 for(Bathroom__c bi:biclistMAp.get(wrk.id)){
	 		  	 	if(bi.Maintenance_Verified__c!='Yes'){
	 		  	      wrk.addError('Maintance of Bic is not comleted');		
	 		  	 	}
	 		     }
	 		    }
	 		  }
		  else if(wrk.Case_Status__c==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT 
	          &&  wrk.subject.contains('Furnishing')){

		  	 if(hicMAp.containsKey(wrk.id) && hicMAp.get(wrk.id).Furnishing_Verified__c!='Yes'){
	            wrk.addError('Furnishing of Hic is not comleted');
		  	 }
		  	 if(biclistMAp.containsKey(wrk.id)){
  	 		  	 for(Bathroom__c bi:biclistMAp.get(wrk.id)){
  	 		  	 	if(bi.Furnishing_Verified__c!='Yes'){
  	 		  	      wrk.addError('Furnishing of Bic is not comleted');		
  	 		  	 	}
  	 
  	 		  	 }
  	 		  	}
  	 		 if(riclistMAp.containsKey(wrk.id)){ 	
		 		  	 for(Room_Inspection__c ri:riclistMAp.get(wrk.id)){
		 		  	 	if(ri.Furnishing_Verified__c!='Yes'){
		 		  	      wrk.addError('Furnishing of Ric is not comleted');		
		 		  	 	}
		 		     }
		 		   }
		 		}
		   else if(wrk.Case_Status__c==Constants.CASE_STATUS_VERIFICATION){
		   	
		  	 if(hicMAp.containsKey(wrk.id) && hicMAp.get(wrk.id).Verification_Done__c!='Yes'){
	            wrk.addError('verification of Hic is not comleted');
		  	 }
		  	 if(biclistMAp.containsKey(wrk.id)){
  	 		  	 for(Bathroom__c bi:biclistMAp.get(wrk.id)){
  	 		  	 	if(bi.Verification_Done__c!='Yes'){
  	 		  	      wrk.addError('verification of Bic is not comleted');		
  	 		  	 	}
  	 
  	 		  	 }
  	 		  	}
  	 		 if(riclistMAp.containsKey(wrk.id)){ 	
 		 		  	 for(Room_Inspection__c ri:riclistMAp.get(wrk.id)){
 		 		  	 	if(ri.Verification_Done__c!='Yes'){
 		 		  	      wrk.addError('verification of Ric is not comleted');		
 		 		  	 	}
 		 		     }
 		 		   }
 		 		}
		   else if(wrk.Case_Status__c==Constants.CASE_STATUS_PHOTOGRAPHY){
		        map<id,boolean> returnedmap = UtilityClass.ValidateAllPhotosPresentForHouse(wrk.House__c);
	            System.debug('********returnedmap '+ returnedmap);
	            if(returnedmap != null && returnedmap.values()[0] == false){
	               wrk.addError('You Cannot close Photography tasks unless you have uploaded atleast one Image and have marked it visible for each section of the house.');
	            } 
		   	}
		   	else if(wrk.Case_Status__c==Constants.CASE_STATUS_KEY_HANDLING){
		   		System.debug('****Keyhandling***');
		   		if(keylistMAp.isEmpty() || !keylistMAp.containsKey(wrk.id) || keylistMAp.get(wrk.id)==null ||keylistMAp.get(wrk.id).size()==0){
	              wrk.addError('No Key is present');
		   		}
		   	}
		   	else if(wrk.Case_Status__c==Constants.CASE_STATUS_MAKE_HOUSE_LIVE){
		   		System.debug('****MakeHouseLisve***');
		   		if(houMap.containsKey(wrk.house__c) && houMap.get(wrk.house__c).Rent_Verified__c!=true){
	              wrk.addError('Update the Room Rents/related information for the House');
		   		}
		   		if(!houMap.isEmpty() && houMap.containsKey(wrk.House__c) && (houMap.get(wrk.House__c).Locality__c == null  ||houMap.get(wrk.House__c).Sub_Locality__c== null	)){
                            system.debug('Please fill the');
                            wrk.addError('Please fill the Locailty Section using Get Locailty Button');
                        }

                if(!houMap.isEmpty() && houMap.containsKey(wrk.House__c) && houMap.get(wrk.House__c).Opportunity__c != null && !dccMap.isEmpty() && dccMap.containsKey(houMap.get(wrk.House__c).Opportunity__c) && !dccMap.get(houMap.get(wrk.House__c).Opportunity__c).Doc_Reviewer_Approved__c){
                                  wrk.addError('You Can not make house Live because document checklist is not yet approved by Doc Reviewer');
                      }
		   	}
	     }
	 }Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder Validation' + String.valueOf(wrklist));
            }
}
   // Added by deepak
   // To send mail ZAM, ZOM, Onboarding Manager, Opportunity Owner if insurnace is not issued before house goes life
    Public static void sendMailForInsurance(List<WorkOrder> woList){
     /*   Set<Id> setOfHouse = new Set<Id>();
        List<House__c> updateHouseList = new List<House__c>();
        List<House__c> houseList = new List<House__c>();
        for(WorkOrder each : woList){
            if(each.house__c != NULL){
             setOfHouse.add(each.house__c);
        }        
        }
        if(!setOfHouse.isEmpty()){
        houseList = [Select id,(Select id,status__c from Insurances__r where status__c ='New' or  status__C ='Request Rejected') from house__c where id in:setOfHouse];
        }
        if(!houseList.isEmpty()){
            for(House__c each : houseList){
            if((each.Insurances__r).size() >0 ){
                each.Check_Insurance_Status__c = true ;
                updateHouseList.add(each);
            }
        }
        }
        if(updateHouseList.isEmpty()){
            update updateHouseList;
        }*/
    }

}