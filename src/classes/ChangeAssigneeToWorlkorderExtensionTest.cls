@Istest
public class ChangeAssigneeToWorlkorderExtensionTest
 {
  public static TestMethod void  TestChangeAssigneeToWorlkorderExtensionTest()
  {
    
    Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Profile pro=[select id from Profile where name='Partner'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
        acc.Vendor_Type__c='Inhouse';
        insert acc;

         Account acc1=new Account();
        acc1.name='vendor2';
         acc1.RecordtypeId=venderRecordTypeID;
        acc1.Vendor_Type__c='3rd Party Aggreguters';
        insert acc1;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;
       
        Contact co1=new Contact();
        co1.firstname='v12';
        co1.lastname='test2';
        co1.Accountid=acc.id;
        co1.Type_of_Contact__c='Technician';
        insert co1;

        Contact co2=new Contact();
        co2.firstname='v123';
        co2.lastname='test3';
        co2.Accountid=acc.id;
        co2.Type_of_Contact__c='Technician';
        insert co2;

        Contact co11=new Contact();
        co11.firstname='v112';
        co11.lastname='test12';
        co11.Accountid=acc.id;
        co11.Type_of_Contact__c='Technician';
        insert co11;

         Contact co3=new Contact();
        co3.firstname='v21';
        co3.lastname='test';
        co3.Accountid=acc1.id;
        co3.Type_of_Contact__c='vendor';
        insert co3;

        user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname=co.firstname;
        us.lastname=co.lastname;
        us.TimeZoneSidKey='Asia/Kolkata';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.Phone='9876543212';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

        user us1=new User();
        us1.Alias='Vt2';
        us1.Email='Vt2.@gmail.com';
        us1.Username='Vt2.@gmail.com';
        us1.IsActive=true;
        us1.Contactid=co1.id;
        us1.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us1.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us1.firstname=co1.firstname;
        us1.lastname=co1.lastname;
        us1.Phone='9876543212';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='   ISO-8859-1';
        us1.LanguageLocaleKey='en_US';
        us1.portalrole='Manager';
        insert us1;

         user us11=new User();
        us11.Alias='Vt223';
        us11.Email='Vt221.@gmail.com';
        us11.Username='Vt234.@gmail.com';
        us11.IsActive=true;
        us11.Contactid=co11.id;
        us11.Phone='9876543212';
        us11.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us11.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us11.firstname=co11.firstname;
        us11.lastname=co11.lastname;
        us11.TimeZoneSidKey='Asia/Kolkata';
        us11.LocaleSidKey='en_IN';
        us11.EmailEncodingKey='   ISO-8859-1';
        us11.LanguageLocaleKey='en_US';
        us11.portalrole='Manager';
        insert us11;

        user us2=new User();
        us2.Alias='Vt22';
        us2.Email='Vt21.@gmail.com';
        us2.Username='Vt22.@gmail.com';
        us2.IsActive=true;
        us2.Contactid=co2.id;
        us2.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us2.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us2.firstname=co2.firstname;
        us2.lastname=co2.lastname;
        us2.Phone='9876543212';
        us2.TimeZoneSidKey='Asia/Kolkata';
        us2.LocaleSidKey='en_IN';
        us2.EmailEncodingKey='   ISO-8859-1';
        us2.LanguageLocaleKey='en_US';
        us2.portalrole='Manager';
        insert us2;

        user us3=new User();
        us3.Alias='Vt222';
        us3.Email='Vt221.@gmail.com';
        us3.Username='Vt222.@gmail.com';
        us3.IsActive=true;
        us3.Contactid=co3.id;
        us3.Phone='9876543212';
        us3.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us3.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname=co3.firstname;
        us3.lastname=co3.lastname;
        us3.TimeZoneSidKey='Asia/Kolkata';
        us3.LocaleSidKey='en_IN';
        us3.EmailEncodingKey='   ISO-8859-1';
        us3.LanguageLocaleKey='en_US';
        us3.portalrole='Manager';
        insert us3;

               zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;
         
        Zone_and_OM_Mapping__c zm1=new Zone_and_OM_Mapping__c();
        zm1.Zone__c=zc.id;
        zm1.isActive__c=true;
        zm1.User__c=us1.id;
        insert zm1;

        Zone_and_OM_Mapping__c zm3=new Zone_and_OM_Mapping__c();
        zm3.Zone__c=zc.id;
        zm3.isActive__c=true;
        zm3.User__c=us3.id;
        insert zm3;

        Zone_and_OM_Mapping__c zm2=new Zone_and_OM_Mapping__c();
        zm2.Zone__c=zc.id;
        zm2.isActive__c=true;
        zm2.User__c=us2.id;
        insert zm2;
        
        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        insert accOw;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.HouseId__c='1423';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.subject='qwertyuyuiop';
        cas.Preferred_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Description='qwertyuioplkjhgfdsaZXCVBNM';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;


       id recordTypeVendor= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Workorder wrk=new Workorder();
          wrk.StartDate=System.now().addHours(6);
          wrk.EndDate=System.now().addHours(7);
          wrk.Tenant__c=cas.Accountid;
          wrk.Tenant_PhoneNo__c='1234567890';
          wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
          wrk.Caseid=cas.id;
          wrk.RecordTypeId=recordTypeVendor;
          wrk.subject=cas.subject;
          wrk.Problem__c=cas.Problem__c;
          wrk.Bill_To__c='Nestaway';
          wrk.Description=cas.Description;
          wrk.House__c=cas.House1__c;
          wrk.HouseID__c=cas.House1__r.HouseId__c; 
          user us22=[select id,name, Contact.Type_of_Contact__c from User where id=:us.id]; 
          wrk.ownerID=us22.id;
          insert wrk;


        Holiday__c hol=new Holiday__c();
        hol.User__c=us22.id;
        hol.Start_Date_Time__c=System.now();
        hol.End_Date_Time__c=System.now().addHours(13);
        insert hol;

        workOrder wrk1=new Workorder();
        wrk1.StartDate=System.now().addHours(3);
        wrk1.EndDate=System.now().addHours(15);
        wrk1.OwnerId=us2.id;
        wrk1.subject='test';
        insert wrk1;

        System.debug('******zhhghjg12*****');

         ApexPages.StandardController stdController=new ApexPages.StandardController(wrk);
         ChangeAssigneeToWorlkorderExtension myPage =new ChangeAssigneeToWorlkorderExtension(stdController);

    
        System.runAs(us)
        {
            System.debug('******zhhghjg*****'+us11.name+' '+us1.name+' '+us2.name+' '+us3.name);
            test.startTest();
            {        
            myPage.StartTime=System.now().addHours(7);
            myPage.EndTime=System.now().addHours(8);
            user ue=[select id,name from user where id=:us11.id];
            myPage.techniName=ue.name;
            myPage.vendor=true;
            myPage.techni=true;
           myPage.UpdateWrk();
            myPage.cancel();
            } test.stoptest();
        }
      

       
  }
    public static TestMethod void  Test2ChangeAssigneeToWorlkorderExtensionTest()
  {
    Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Profile pro1=[select id from Profile where name='Partner'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
        acc.Vendor_Type__c='Inhouse';
        insert acc;

         Account acc1=new Account();
        acc1.name='vendor2';
         acc1.RecordtypeId=venderRecordTypeID;
        acc1.Vendor_Type__c='3rd Party Aggreguters';
        insert acc1;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;
       
        Contact co1=new Contact();
        co1.firstname='v12';
        co1.lastname='test2';
        co1.Accountid=acc.id;
        co1.Type_of_Contact__c='Technician';
        insert co1;

        Contact co2=new Contact();
        co2.firstname='v123';
        co2.lastname='test3';
        co2.Accountid=acc.id;
        co2.Type_of_Contact__c='Technician';
        insert co2;

        Contact co11=new Contact();
        co11.firstname='v112';
        co11.lastname='test12';
        co11.Accountid=acc.id;
        co11.Type_of_Contact__c='Technician';
        insert co11;

         Contact co3=new Contact();
        co3.firstname='v21';
        co3.lastname='test';
        co3.Accountid=acc1.id;
        co3.Type_of_Contact__c='vendor';
        insert co3;

      

        user us1=new User();
        us1.Alias='Vt2';
        us1.Email='Vt2.@gmail.com';
        us1.Username='Vt2.@gmail.com';
        us1.IsActive=true;
        us1.Contactid=co1.id;
        us1.Phone='9876543212';
        us1.ProfileId=pro1.id;
       // us1.Accountid=acc.id;
        us1.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us1.firstname='bony';
        us1.lastname='qwer';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='   ISO-8859-1';
        us1.LanguageLocaleKey='en_US';
        us1.portalrole='Manager';
        insert us1;

         user us11=new User();
        us11.Alias='Vt223';
        us11.Email='Vt221.@gmail.com';
        us11.Username='Vt234.@gmail.com';
        us11.IsActive=true;
        us11.Contactid=co11.id;
        us11.ProfileId=pro1.id;
       // us1.Accountid=acc.id;
        us11.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us11.firstname='bony3';
        us11.Phone='9876543212';
        us11.lastname='qwer';
        us11.TimeZoneSidKey='Asia/Kolkata';
        us11.LocaleSidKey='en_IN';
        us11.EmailEncodingKey='   ISO-8859-1';
        us11.LanguageLocaleKey='en_US';
        us11.portalrole='Manager';
        insert us11;

        user us2=new User();
        us2.Alias='Vt22';
        us2.Email='Vt21.@gmail.com';
        us2.Username='Vt22.@gmail.com';
        us2.IsActive=true;
        us2.Contactid=co2.id;
        us2.ProfileId=pro1.id;
       // us1.Accountid=acc.id;
        us2.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us2.firstname='bony2';
        us2.lastname='qwerrt';
        us2.TimeZoneSidKey='Asia/Kolkata';
        us2.Phone='9876543212';
        us2.LocaleSidKey='en_IN';
        us2.EmailEncodingKey='   ISO-8859-1';
        us2.LanguageLocaleKey='en_US';
        us2.portalrole='Manager';
        insert us2;

        user us3=new User();
        us3.Alias='Vt222';
        us3.Email='Vt221.@gmail.com';
        us3.Username='Vt222.@gmail.com';
        us3.IsActive=true;
        us3.Contactid=co3.id;
        us3.ProfileId=pro1.id;
       // us1.Accountid=acc.id;
        us3.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us3.firstname='vend2';
        us3.lastname='qweeer';
        us3.TimeZoneSidKey='Asia/Kolkata';
        us3.LocaleSidKey='en_IN';
        us3.Phone='9876543212';
        us3.EmailEncodingKey='   ISO-8859-1';
        us3.LanguageLocaleKey='en_US';
        us3.portalrole='Manager';
        insert us3;

               zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

       
         
        Zone_and_OM_Mapping__c zm1=new Zone_and_OM_Mapping__c();
        zm1.Zone__c=zc.id;
        zm1.isActive__c=true;
        zm1.User__c=us1.id;
        insert zm1;

        Zone_and_OM_Mapping__c zm3=new Zone_and_OM_Mapping__c();
        zm3.Zone__c=zc.id;
        zm3.isActive__c=true;
        zm3.User__c=us3.id;
        insert zm3;

        Zone_and_OM_Mapping__c zm2=new Zone_and_OM_Mapping__c();
        zm2.Zone__c=zc.id;
        zm2.isActive__c=true;
        zm2.User__c=us2.id;
        insert zm2;
        
        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        insert accOw;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.HouseId__c='1423';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.subject='qwertyuyuiop';
        cas.Preferred_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Description='qwertyuioplkjhgfdsaZXCVBNM';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;


        id recordTypeVendor= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Workorder wrk=new Workorder();
          wrk.StartDate=System.now().addHours(6);
          wrk.EndDate=System.now().addHours(7);
          wrk.Tenant__c=cas.Accountid;
          wrk.Tenant_PhoneNo__c='1234567890';
          wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
          wrk.Caseid=cas.id;
          wrk.RecordTypeId=recordTypeVendor;
          wrk.subject=cas.subject;
          wrk.Problem__c=cas.Problem__c;
          wrk.Bill_To__c='Nestaway';
          wrk.Description=cas.Description;
          wrk.House__c=cas.House1__c;
          wrk.HouseID__c=cas.House1__r.HouseId__c;  
          wrk.ownerID=us2.id;
          insert wrk;

           Holiday__c hol=new Holiday__c();
        hol.User__c=us1.id;
        hol.Start_Date_Time__c=System.now();
        hol.End_Date_Time__c=System.now().addHours(13);
        insert hol;

      ApexPages.StandardController stdController=new ApexPages.StandardController(wrk);
      ChangeAssigneeToWorlkorderExtension myPage =new ChangeAssigneeToWorlkorderExtension(stdController);
       
        System.runAs(us2)
        {
               
                myPage.StartTime=System.now().addHours(7);
                myPage.EndTime=System.now().addHours(8);
               // myPage.UpdateWrk();
                myPage.cancel();
        }
  }
  public static TestMethod void  Test3ChangeAssigneeToWorlkorderExtensionTest()
  {
    Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Profile pro=[select id from Profile where name='Partner'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
        acc.Vendor_Type__c='Inhouse';
        insert acc;

         Account acc1=new Account();
        acc1.name='vendor2';
         acc1.RecordtypeId=venderRecordTypeID;
        acc1.Vendor_Type__c='3rd Party Aggreguters';
        insert acc1;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;
       
        Contact co1=new Contact();
        co1.firstname='v12';
        co1.lastname='test2';
        co1.Accountid=acc.id;
        co1.Type_of_Contact__c='Technician';
        insert co1;

        Contact co2=new Contact();
        co2.firstname='v123';
        co2.lastname='test3';
        co2.Accountid=acc.id;
        co2.Type_of_Contact__c='Technician';
        insert co2;

        Contact co11=new Contact();
        co11.firstname='v112';
        co11.lastname='test12';
        co11.Accountid=acc.id;
        co11.Type_of_Contact__c='Technician';
        insert co11;

         Contact co3=new Contact();
        co3.firstname='v21';
        co3.lastname='test';
        co3.Accountid=acc1.id;
        co3.Type_of_Contact__c='vendor';
        insert co3;

        user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend';
        us.lastname='qwer';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.Phone='9876543212';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

        user us1=new User();
        us1.Alias='Vt2';
        us1.Email='Vt2.@gmail.com';
        us1.Username='Vt2.@gmail.com';
        us1.IsActive=true;
        us1.Contactid=co1.id;
        us1.Phone='9876543212';
        us1.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us1.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us1.firstname='bony';
        us1.lastname='qwer';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='   ISO-8859-1';
        us1.LanguageLocaleKey='en_US';
        us1.portalrole='Manager';
        insert us1;

         user us11=new User();
        us11.Alias='Vt223';
        us11.Email='Vt221.@gmail.com';
        us11.Username='Vt234.@gmail.com';
        us11.IsActive=true;
        us11.Contactid=co11.id;
        us11.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us11.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us11.firstname='bony3';
        us11.lastname='qwer';
        us11.TimeZoneSidKey='Asia/Kolkata';
        us11.Phone='9876543212';
        us11.LocaleSidKey='en_IN';
        us11.EmailEncodingKey='   ISO-8859-1';
        us11.LanguageLocaleKey='en_US';
        us11.portalrole='Manager';
        insert us11;

        user us2=new User();
        us2.Alias='Vt22';
        us2.Email='Vt21.@gmail.com';
        us2.Username='Vt22.@gmail.com';
        us2.IsActive=true;
        us2.Contactid=co2.id;
        us2.Phone='9876543212';
        us2.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us2.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us2.firstname='bony2';
        us2.lastname='qwerrt';
        us2.TimeZoneSidKey='Asia/Kolkata';
        us2.LocaleSidKey='en_IN';
        us2.EmailEncodingKey='   ISO-8859-1';
        us2.LanguageLocaleKey='en_US';
        us2.portalrole='Manager';
        insert us2;

        user us3=new User();
        us3.Alias='Vt222';
        us3.Email='Vt221.@gmail.com';
        us3.Username='Vt222.@gmail.com';
        us3.IsActive=true;
        us3.Contactid=co3.id;
        us3.ProfileId=pro.id;
       // us1.Accountid=acc.id;
        us3.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend2';
        us3.lastname='qweeer';
        us3.TimeZoneSidKey='Asia/Kolkata';
        us3.LocaleSidKey='en_IN';
        us3.EmailEncodingKey='   ISO-8859-1';
        us3.Phone='9876543212';
        us3.LanguageLocaleKey='en_US';
        us3.portalrole='Manager';
        insert us3;

               zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;
         
        Zone_and_OM_Mapping__c zm1=new Zone_and_OM_Mapping__c();
        zm1.Zone__c=zc.id;
        zm1.isActive__c=true;
        zm1.User__c=us1.id;
        insert zm1;

        Zone_and_OM_Mapping__c zm3=new Zone_and_OM_Mapping__c();
        zm3.Zone__c=zc.id;
        zm3.isActive__c=true;
        zm3.User__c=us3.id;
        insert zm3;

        Zone_and_OM_Mapping__c zm2=new Zone_and_OM_Mapping__c();
        zm2.Zone__c=zc.id;
        zm2.isActive__c=true;
        zm2.User__c=us2.id;
        insert zm2;
        
        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        insert accOw;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.HouseId__c='1423';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.subject='qwertyuyuiop';
        cas.Preferred_Visit_Time__c=System.now().addhours(1);
        cas.Service_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Description='qwertyuioplkjhgfdsaZXCVBNM';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;


        id recordTypeVendor= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Workorder wrk=new Workorder();
          wrk.StartDate=System.now().addHours(6);
          wrk.EndDate=System.now().addHours(7);
          wrk.Tenant__c=cas.Accountid;
          wrk.Tenant_PhoneNo__c='1234567890';
          wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
          wrk.Caseid=cas.id;
          wrk.RecordTypeId=recordTypeVendor;
          wrk.subject=cas.subject;
          wrk.Problem__c=cas.Problem__c;
          wrk.Bill_To__c='Nestaway';
          wrk.Description=cas.Description;
          wrk.House__c=cas.House1__c;
          wrk.HouseID__c=cas.House1__r.HouseId__c;  
          user us22=[select id,name, Contact.Type_of_Contact__c from User where id=:us2.id];
          wrk.ownerID=us2.id;
          insert wrk;

           Holiday__c hol=new Holiday__c();
        hol.User__c=us1.id;
        hol.Start_Date_Time__c=System.now();
        hol.End_Date_Time__c=System.now().addHours(13);
        insert hol;

      ApexPages.StandardController stdController=new ApexPages.StandardController(wrk);
      ChangeAssigneeToWorlkorderExtension myPage =new ChangeAssigneeToWorlkorderExtension(stdController);
              
               myPage.assignTo='Assign To Aggregator';
                myPage.StartTime=System.now().addHours(7);
                myPage.EndTime=System.now().addHours(8);
                myPage.getAssignOptions();
                mypage.Assign();
                myPage.Options();
                //myPage.VenderWrkCount();
                myPage.assintovender=false;
                user ue=[select id,name from user where id=:us2.id];
               myPage.username=ue.name;
                myPage.InsertWorkorder();
               

                  Test.startTest();
               {
                  myPage.assintovender=true;
                user ue1=[select id,name from user where id=:us3.id];
               myPage.vendername=ue1.name;
                myPage.Options();
                myPage.VenderWrkCount();
                myPage.InsertWorkorder();
                 } Test.StopTest();
        
  }
  
  /* public static TestMethod void  Test4ChangeAssigneeToWorlkorderExtensionTest()
  {
    
        Profile pro=[Select id from profile where name='Area Operations manager'];
        user us=[Select id from User where profileId=:pro.id and isActive=true limit 1];
        id recordTypeVendor= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        workorder wrk=[Select id,StartDate,EndDate,Tenant__c,Tenant_PhoneNo__c,Google_Map_Link__c,Caseid,Problem__c,Bill_To__c,House__c,ownerID,RecordTypeId from workorder where RecordTypeId=:recordTypeVendor limit 1];
        System.runAs(us)
        {
           ApexPages.StandardController stdController=new ApexPages.StandardController(wrk);
      ChangeAssigneeToWorlkorderExtension myPage =new ChangeAssigneeToWorlkorderExtension(stdController);
        }
    }*/

}