@isTest
Public Class OpportunityTriggerHandlerTest{
    
    Public Static TestMethod Void doTest(){
        Test.StartTest();

        Account Acc = new Account(Name='Test',PAN_Number__c='AXSWE1234Q');
        insert Acc;

        Opportunity objOpp = new Opportunity();
        objOpp.AccountId = Acc.Id;
        objOpp.Name ='Test Opp';
        objOpp.CloseDate = System.Today();
        objOpp.StageName = 'House Inspection';

        insert objOpp;

        Contract objContract         = new Contract();
        objContract.Opportunity__c   = objOpp.Id;
        objContract.AccountId        = objOpp.AccountId;
        objContract.StartDate        = objOpp.CloseDate;
        objContract.Status           = 'Draft';
        objContract.ContractTerm     = 11;
        objContract.Contact_Email__c = objOpp.Contact_Email__c;
        objContract.Furnishing_Package__c = 'Package A';
        insert objContract;

        Product2 prod = new Product2();
        prod.Name = 'Laptop X200'; 
        prod.Family = 'Package A';
        prod.Product_Type__c = 'Furnishing';
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;

        Quote objQuote = new Quote();
        objQuote.Name = objOpp.Name+'-Quote';
        objQuote.OpportunityId = objOpp.Id;
        objQuote.Status = 'Manually Approved by ZM';
        objQuote.Pricebook2Id = customPB.Id; 
        insert objQuote;  

        QuoteLineItem quoLI = new QuoteLineItem();
        quoLi.QuoteId = objQuote .Id;
        quoLI.Product2Id = prod.Id;
        quoLI.PricebookEntryId = customPrice.Id;

        quoLI.UnitPrice = 40000;
        quoLi.Quantity = 4;
        insert quoLI;
        
        Id dccRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Document Collection Checklist').getRecordTypeId();
        House_Inspection_Checklist__c objDCC = new House_Inspection_Checklist__c();
        objDCC.Name = objOpp.Name + 'DCC';
        objDCC.Opportunity__c = objOpp.Id;
        objDCC.House_layout__c = objOpp.House_layout__c;
        objDCC.Status__c = 'Draft';
        objDCC.RecordTypeId = dccRecordTypeId;
        insert objDCC;

        Id hicRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('House Inspection Checklist').getRecordTypeId();
        House_Inspection_Checklist__c obj = new House_Inspection_Checklist__c();
        obj.Name = objOpp.Name + 'House Inspection Checklist';
        obj.Opportunity__c = objOpp.Id;
        obj.House_layout__c = objOpp.House_layout__c;
        obj.Status__c = 'House Inspection Pending';
        obj.RecordTypeId = hicRecordTypeId;
        insert obj;
 /*
        Room_Inspection__c objRI = New Room_Inspection__c();
        objRI.Name           = 'Room Number 1' ;
        objRI.House_Inspection_Checklist__c = obj.id;    

        insert objRI;    

        Order oRec = new Order();
        oRec.OpportunityId = objOpp.id;
        oRec.AccountId = objOpp.AccountId;
        oRec.Status = 'Submitted';
        oRec.EffectiveDate = System.today();
        oRec.POA_Age__c = objOpp.POA_Age__c;
        oRec.Pricebook2Id = objOpp.Pricebook2Id;
        
        insert oRec;
        
        House__c objHouse = new House__c();
        objHouse.Name           = objOpp.Name;
        objHouse.House_Owner__c = objOpp.AccountId;
        objHouse.Opportunity__c = objOpp.Id;
        objHouse.Stage__c = 'House Draft'; 
        insert objHouse;              
        
        objOpp.StageName = 'Sample Contract';
        update objOpp;
        
        objOpp.StageName = 'House';
        update objOpp;

        objOpp.StageName = 'Quote Creation';
        update objOpp;
        
        /*objOpp.StageName = 'Lost';
        objOpp.Lost_Reason__c='Contract Cancelled';
        update objOpp; */


        Test.StopTest();    
    }
    
    Public Static TestMethod Void doTest1(){
        Test.StartTest();
        
         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Account Acc = new Account(Name='Test');
        insert Acc;

        Opportunity objOpp = new Opportunity();
        objOpp.AccountId = Acc.Id;
        objOpp.Name ='Test Opp';
        objOpp.CloseDate = System.Today();
        objOpp.StageName = 'House Inspection';

        insert objOpp;

        objOpp.StageName = 'Sample Contract';
        update objOpp;

        Contract objContract         = new Contract();
        objContract.Opportunity__c   = objOpp.Id;
        objContract.AccountId        = objOpp.AccountId;
        objContract.StartDate        = objOpp.CloseDate;
        objContract.Status           = 'Draft';
        objContract.ContractTerm     = 11;
        objContract.Contact_Email__c = objOpp.Contact_Email__c;
        objContract.Furnishing_Package__c = 'Package A';
        insert objContract;

        Product2 prod = new Product2();
        prod.Name = 'Laptop X200'; 
        prod.Family = 'Package A';
        prod.Product_Type__c = 'Furnishing';
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;

        Quote objQuote = new Quote();
        objQuote.Name = objOpp.Name+'-Quote';
        objQuote.OpportunityId = objOpp.Id;
        objQuote.Status = 'Manually Approved by ZM';
        objQuote.Pricebook2Id = customPB.Id; 
        insert objQuote;  

        QuoteLineItem quoLI = new QuoteLineItem();
        quoLi.QuoteId = objQuote .Id;
        quoLI.Product2Id = prod.Id;
        quoLI.PricebookEntryId = customPrice.Id;

        quoLI.UnitPrice = 40000;
        quoLi.Quantity = 4;
        insert quoLI;


        Id hicRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('House Inspection Checklist').getRecordTypeId();
        House_Inspection_Checklist__c obj = new House_Inspection_Checklist__c();
        obj.Name = objOpp.Name + 'House Inspection Checklist';
        obj.Opportunity__c = objOpp.Id;
        obj.House_layout__c = objOpp.House_layout__c;
        obj.Status__c = 'House Inspection Pending';
        obj.RecordTypeId = hicRecordTypeId;
        insert obj;

        Room_Inspection__c objRI = New Room_Inspection__c();
        objRI.Name           = 'Room Number 1' ;
        objRI.House_Inspection_Checklist__c = obj.id;    
        insert objRI;    
   /*
      
        objOpp.StageName = 'Sales Order';
        update objOpp;

        objOpp.StageName = 'Final Contract';
        update objOpp;

        objOpp.StageName = 'Docs & Keys Collected';
        update objOpp;
        */

        Test.StopTest();   
        
    }
}