/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for LeadTriggerHandler
**********************************************/
public class LeadTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : on change of New Stage Escalation Level or Open Stage Escalation Level populate the Next Escalation Email
                 and Next Escalation User
**********************************************/
    public static void populateNextEscalationEmail(List<Lead> leadList,Map<id,lead> oldmap){

        System.debug('**populateNextEscalationEmail');

        try{    
                Set<Id> ownerIdSet = new Set<Id>();
                Set<Id> userIdSet = new Set<Id>();
                Map<Id, User> userMap = new Map<Id, User>();

                for(Lead l: leadList){
                    if(l.Escalation_User__c != null){
                        userIdSet.add(l.Escalation_User__c);
                    } else if(string.valueOf(l.OwnerId).startsWith('005')){
                        userIdSet.add(l.OwnerId);
                    }
                }

                if(!userIdSet.isEmpty()){
                    userMap = new Map<Id, User>(UserSelector.getUserDetails(userIdSet));
                }    

                for(Lead l: leadList){

                    if(l.Level_1_Escalation__c==null && 
                        ((l.New_Stage_Escalation_Level__c=='Level 1' && oldmap.get(l.id).New_Stage_Escalation_Level__c!='Level 1') 
                            || (l.Open_Stage_Escalation_Level__c=='Level 1' && oldmap.get(l.id).Open_Stage_Escalation_Level__c!='Level 1'))){

                        if(userMap.get(l.Escalation_User__c).ManagerId != null && userMap.get(l.Escalation_User__c).Manager.Email!=l.Next_Escalation_Email__c){
                           l.Level_1_Escalation__c= l.Next_Escalation_Email__c;
                        }
                    }

                    if(l.Level_2_Escalation__c==null && 
                        ((l.New_Stage_Escalation_Level__c=='Level 2' && oldmap.get(l.id).New_Stage_Escalation_Level__c!='Level 2') 
                            || (l.Open_Stage_Escalation_Level__c=='Level 2' && oldmap.get(l.id).Open_Stage_Escalation_Level__c!='Level 2'))){
                       
                       if(userMap.get(l.Escalation_User__c).ManagerId != null && userMap.get(l.Escalation_User__c).Manager.Email!=l.Next_Escalation_Email__c 
                         &&  userMap.get(l.Escalation_User__c).Manager.Email!=l.Level_1_Escalation__c){
                          l.Level_2_Escalation__c = l.Next_Escalation_Email__c;
                        }
                    }

                    if(l.Level_3_Escalation__c==null && 
                        ((l.New_Stage_Escalation_Level__c=='Level 3' && oldmap.get(l.id).New_Stage_Escalation_Level__c!='Level 3') 
                            || (l.Open_Stage_Escalation_Level__c=='Level 3' && oldmap.get(l.id).Open_Stage_Escalation_Level__c!='Level 3'))){

                       if(userMap.get(l.Escalation_User__c).ManagerId != null && userMap.get(l.Escalation_User__c).Manager.Email!=l.Next_Escalation_Email__c 
                         &&  userMap.get(l.Escalation_User__c).Manager.Email!=l.Level_1_Escalation__c &&  userMap.get(l.Escalation_User__c).Manager.Email!=l.Level_2_Escalation__c){ 
                          l.Level_3_Escalation__c = l.Next_Escalation_Email__c;
                       }
                    }
                    
                    if(l.Escalation_User__c != null && userMap.containsKey(l.Escalation_User__c)){
                        if(userMap.get(l.Escalation_User__c).ManagerId != null){
                            l.Next_Escalation_Email__c = userMap.get(l.Escalation_User__c).Manager.Email;
                            l.Escalation_User__c = userMap.get(l.Escalation_User__c).ManagerId;
                        }
                    } else if(string.valueOf(l.OwnerId).startsWith('005') && userMap.containsKey(l.OwnerId)){
                        if(userMap.get(l.OwnerId).ManagerId != null){
                            l.Next_Escalation_Email__c = userMap.get(l.OwnerId).Manager.Email;
                            l.Escalation_User__c = userMap.get(l.OwnerId).ManagerId;
                        }
                    }               
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - populateNextEscalationEmail');      
            }           
    }   


/**********************************************
    Created By : Mohan
    Purpose    : Update the Next Escalation Email on Insert or On change of Lead Owner
**********************************************/
    public static void populateOwnerEmailId(List<Lead> leadList){
        try{    
                Set<Id> ownerIdSet = new Set<Id>();

                for(Lead l: leadList){
                    ownerIdSet.add(l.OwnerId);
                }

                Map<Id, User> userMap = new Map<Id, User>(UserSelector.getUserDetails(ownerIdSet));

                for(Lead l: leadList){
                    if(string.valueOf(l.OwnerId).startsWith('005')){

                        if(l.Record_Owner_Assigned_Time__c == null){
                            l.Record_Owner_Assigned_Time__c = System.now();
                        }

                        l.Next_Escalation_Email__c = userMap.get(l.OwnerId).Email;
                        l.Escalation_User__c = l.OwnerId;
                    }
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - populating Owner Email Id on Lead');      
            }           
    }   


/**********************************************
    Created By : Mohan
    Purpose    : Create Lead Escalation Tracking Case for Owner Lead
**********************************************/
    public static void createEscalationTrackingCase(List<Lead> leadList){
        try{    
                List<Case> caseList = new List<Case>();
                Set<Id> leadIdSet = new Set<Id>();
                Map<Id, Case> leadIdToCaseMap = new Map<Id, Case>();

                for(Lead l: leadList){
                    leadIdSet.add(l.Id);
                }

                for(Case c: [select Id, Lead__c from Case where Lead__c =: leadIdSet]){
                    leadIdToCaseMap.put(c.Lead__c, c);
                }

                for(Lead l: leadList){
                    if(!leadIdToCaseMap.containsKey(l.Id)){
                        Case c = new Case();
                        c.Lead__c = l.Id;
                        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_LEAD_ESCALATION_TRACKING).getRecordTypeId();
                        c.Lead_Status__c = 'New';
                        caseList.add(c);
                    }
                }

                if(!caseList.isEmpty()){
                    insert caseList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - createEscalationTrackingCase');      
            }           
    }   


/**********************************************
    Created By : Mohan
    Purpose    : Sync the Lead Status with the Escalation Tracking Case
**********************************************/
    public static void syncLeadStatusWithEscalationTrackingCase(List<Lead> leadList){

        System.debug('**syncLeadStatusWithEscalationTrackingCase');
        
        try{    
                Set<Id> leadIdSet = new Set<Id>();
                Map<Id, Case> leadIdToCaseMap = new Map<Id, Case>();
                List<Case> updatedCaseList = new List<Case>();

                for(Lead l: leadList){
                    leadIdSet.add(l.Id);
                }

                List<Case> caseList = [select Id, Lead__c, Lead_Status__c from Case where Lead__c =: leadIdSet];

                for(Case c: caseList){
                    leadIdToCaseMap.put(c.Lead__c, c);
                }

                for(Lead l: leadList){
                    if(leadIdToCaseMap.containsKey(l.Id)){
                        Case c = new Case();
                        c.Id = leadIdToCaseMap.get(l.Id).Id;
                        c.Lead_Status__c = l.Status;
                        if(l.Status == 'Open'){
                            c.Lead_Open_Time__c = System.now();
                        }
                        updatedCaseList.add(c);
                    }
                }

                if(!updatedCaseList.isEmpty()){
                    Update updatedCaseList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - syncLeadStatusWithEscalationTrackingCase');      
            }           
    }           
}