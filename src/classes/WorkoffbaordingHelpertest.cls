@Istest
public class WorkoffbaordingHelpertest {
     
       public Static id recordTypeOffboardingTanent = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT).getRecordTypeId();
    public Static id recordTypeOffboardingUnfurnished = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_UNFURNISHED).getRecordTypeId();
    public Static id recordTypeOffboardingDisconnected = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_DISCONNECTED).getRecordTypeId();
    public Static id recordTypeOffboardingFullandFinal = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_FULL_FINAL_AMT).getRecordTypeId();
    public Static id recordTypeOffboardingFinalInspection = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_FINAL_INSPECTION).getRecordTypeId();
    public Static id recordTypeOffboardingHouseMaintance= Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_MAINTENANCE).getRecordTypeId();
    public Static id unfurnishedOffboardingChecklist = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_OFFBOARDING_UNFURNISHED_CHEKCLIST).getRecordTypeId();
    public Static id recordTypeOuotationformVender = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_QUOTATION_VENDER).getRecordTypeId();
   
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
      Public Static TestMethod Void doTest1(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
         
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        insert hos;
           
         Case c1=  new Case();
         c1.House__c=hos.Id;
        c1.RecordTypeId=caseOccupiedFurnishedRecordtype;
        c1.Active_Tenants__c=3;   
        c1.Status='Open';
        insert c1;
          
           test.starttest();
            List<workorder> wrkList =new List<workorder>(); 
           workOrder wrko=new workOrder();
           wrko.House__c= c1.HouseForOffboarding__c; 
           wrko.Status='Open';
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c1.id;
           wrkList.add(wrko);
           insert wrkList;
           wrko.Status=Constants.WORK_STATUS_CALLED_AND_RETAINED;
           update wrko;
          List<workorder> wdlist= new List<workorder>();
          wdlist.add(wrko);
          Map<id,workorder> newmap= new Map<Id,workorder>();
          newmap.put(wrko.id,wrko);
          WorkOffboardingHelper.OffboardingZAMTaskCreation(wdlist,newmap);
          WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,newmap);
			test.stoptest();
       }
 Public Static TestMethod Void doTest2(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=3;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
        c.Furnishing_Plan__c = Constants.FURNISHING_PLAN_NESTWAY;
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status='Continue Offboarding';
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,oldmap);
			test.stoptest();
       }
     Public Static TestMethod Void doTest3(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=3;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
           wrko_frlist.CaseId=c.id;
     	  wrko_frlist.Status=Constants.WORDK_STATUS_CALLED_AND_NOT_RETAINED;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
             WorkOffboardingHelper.OffboardingZAMTaskCreation(wdlist,oldmap);
			test.stoptest();
       }
    
     Public Static TestMethod Void doTest5(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
        c.Furnishing_Plan__c = Constants.FURNISHING_PLAN_NESTWAY;
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status='Continue Offboarding';
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,oldmap);
			test.stoptest();
       }
    
     Public Static TestMethod Void doTest6(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
       c.Booking_Type__c = Constants.HOUSE_BOOKING_TYPE_SHARED_HOUSE;
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status='Continue Offboarding';
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,oldmap);
			test.stoptest();
       }
  Public Static TestMethod Void doTest7(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=2;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
       c.Furnishing_Plan__c ='NestAway Furnished';
       insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status='Continue Offboarding';
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,oldmap);
			test.stoptest();
       }

     Public Static TestMethod Void doTest8(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
        c.Booking_Type__c = Constants.HOUSE_BOOKING_TYPE_SHARED_HOUSE;
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_CLOSED;
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingTanentWorkOrderUpdate(wdlist,oldmap);
           WorkOffboardingHelper.OffboardingUnfurnishedWorkOrderUpdate(wdlist,oldmap);
           WorkOffboardingHelper.OffboardingHouseDisconnectedWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingFullAndFinalWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingFinalInspectionWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingQuotationforVenderUpdate(wdlist,oldmap);
			test.stoptest();
       }

    
     Public Static TestMethod Void doTest9(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
       c.Furnishing_Plan__c = Constants.FURNISHING_PLAN_NESTWAY;
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_CLOSED;
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
          WorkOffboardingHelper.OffboardingTanentWorkOrderUpdate(wdlist,oldmap);
           WorkOffboardingHelper.OffboardingUnfurnishedWorkOrderUpdate(wdlist,oldmap);
           WorkOffboardingHelper.OffboardingHouseDisconnectedWorkOrderUpdate(wdlist,oldmap);
          WorkOffboardingHelper.OffboardingFullAndFinalWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingFinalInspectionWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingQuotationforVenderUpdate(wdlist,oldmap);
			test.stoptest();
       }

     Public Static TestMethod Void doTest10(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
     
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_CLOSED;
          wrko_frlist.CaseId=c.id;
          wrko_frlist.Maintenance_By__c=Constants.WORKORDER_MAINTENANCE_BY_NESTAWAY;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
         WorkOffboardingHelper.OffboardingQuotationforVenderUpdate(wdlist,oldmap);
       
			test.stoptest();
       }
Public Static TestMethod Void doTest11(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
     
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_CLOSED;
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
    WorkOffboardingHelper.OffboardingTanentWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingUnfurnishedWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingHouseDisconnectedWorkOrderUpdate(wdlist,oldmap);
          WorkOffboardingHelper.OffboardingFullAndFinalWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingFinalInspectionWorkOrderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingQuotationforVenderUpdate(wdlist,oldmap);
         WorkOffboardingHelper.OffboardingZAMStatusUpdate(wdlist,oldmap);
			test.stoptest();
       }
    Public Static TestMethod Void doTest12(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
     
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_INSPECTION_WITH_ISSUE;
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
   
         WorkOffboardingHelper.OffboardingFinalInspectionWorkOrderUpdate(wdlist,oldmap);
     
			test.stoptest();
       }
 Public Static TestMethod Void doTest13(){
       User newUser = Test_library.createStandardUser(1);
       insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c ='Off-Boarded';
        hos.Initiate_Offboarding__c='Yes';
        insert hos;
           
       Case c=  new Case();
       c.House__c=hos.Id;
       c.RecordTypeId=caseOccupiedFurnishedRecordtype;
       c.Active_Tenants__c=0;
       c.HouseForOffboarding__c=hos.id;
       c.Status='Open';
     
        insert c;
          
         
          
           workOrder wrko=new workOrder();
           wrko.Status='Open';
           wrko.House__c=hos.id;
           wrko.RecordTypeId=recordTypeOffboardingTanent;
           wrko.CaseId=c.id;
           insert wrko;
      Map<id,workorder> oldmap= new Map<Id,workorder>();
          oldmap.put(wrko.id,wrko);
       test.starttest();
     List<workorder> wdlist= new List<workorder>();
            //update wrko; 
          workOrder wrko_frlist = new workOrder(id=wrko.id);
          wrko_frlist.House__c=hos.id;
     	  wrko_frlist.Status=Constants.WORKORDER_STATUS_INSPECTION_WITH_ISSUE;
          wrko_frlist.CaseId=c.id;
     	  update wrko_frlist; 
          wdlist.add(wrko_frlist);
       Bathroom__c bc= new Bathroom__c();
        bc.House__c=hos.id;
        bc.Work_Order__c=wrko_frlist.id;
        bc.House__c=hos.id;
        insert bc;
       House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Kitchen_Package__c='Completely Present';
         hic.Work_Order__c=wrko_frlist.id;
        insert hic;
         WorkOffboardingHelper.CreateHicRicBicOnWorkorder(wdlist);
     
			test.stoptest();
       }

}