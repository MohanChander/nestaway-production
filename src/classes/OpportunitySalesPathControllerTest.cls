@isTest
Public Class OpportunitySalesPathControllerTest{

    Public Static TestMethod void doTest(){
          Test.StartTest();
          Opportunity objOpp = new Opportunity();
          objOpp.Name ='Test Opp';
          objOpp.CloseDate = System.Today();
          objOpp.StageName = 'House Inspection';
          
          insert objOpp;
          
          objOpp.StageName = 'Lost';
          objOpp.Lost_Reason__c = 'Wants MG';
          Update objOpp;
          
          ApexPages.StandardController sc = new ApexPages.StandardController(objOpp);
          OpportunitySalesPathController testAccPlan = new OpportunitySalesPathController(sc);
          
          PageReference pageRef = Page.OpportunitySalesPath;
          pageRef.getParameters().put('id', String.valueOf(objOpp.Id));
          Test.setCurrentPage(pageRef);
          
          objOpp.StageName = 'Sample Contract';
          //objOpp.Lost_Reason__c = 'Wants MG';
          Update objOpp;
          
          ApexPages.StandardController sc1 = new ApexPages.StandardController(objOpp);
          OpportunitySalesPathController testAccPlan1 = new OpportunitySalesPathController(sc1);
          
          PageReference pageRef1 = Page.OpportunitySalesPath;
          pageRef1.getParameters().put('id', String.valueOf(objOpp.Id));
          Test.setCurrentPage(pageRef1);
          
          
          Test.StopTest(); 
    } 

}