/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 22/05/2017
    Purpose      : All the Trigger functionality will be handled in the Handler Class */

public class InvoiceTriggerHandler {

    // method to update the House Economics - Executed After Inset, After Delete and After Undelete
    public static void updateHouseEconomics(List<Invoice__c> newInvoices){

        System.debug('updateHouseEconomics is executed: ' + ' Invoices triggered: + ' + newInvoices);
        Set<Id> houseIdSet = new Set<Id>();

        for(Invoice__c inv: newInvoices){
            if(inv.Category__c != null && inv.House__c != null)
                houseIdSet.add(inv.House__c);
        }

        // send the House Id's to the Summation class
        if(!houseIdSet.isEmpty())
            HouseEconomicsSummation.houseEconomicsSummation(houseIdSet);
    } // end of the method - updateHouseEconomics

    // method will be executed after update - Update the House Economics on change of Invoice Amount
    public static void updateHouseEconomicsOnUpdate(Map<Id, Invoice__c> newMap, Map<Id, Invoice__c> oldMap){

        Set<Id> houseIdSet = new Set<Id>();

        for(Invoice__c inv: newMap.values()){
            if(inv.Amount__c != oldMap.get(inv.Id).Amount__c && inv.House__c != null){
                houseIdSet.add(inv.House__c);
            }
        }

        // send the House Id's to the Summation class
        HouseEconomicsSummation.houseEconomicsSummation(houseIdSet);
    }  // end of the method - updateHouseEconomicsOnUpdate


/*******************************************
 * Created By: Mohan
 * Purpose   : after Update Handler
 * *****************************************/ 
    public static void beforeUpdate(Map<Id, Invoice__c> newMap, Map<Id, Invoice__c> oldMap){

        try{
                List<Invoice__c> approvedInvoiceList = new List<Invoice__c>();

                for(Invoice__c inv: newMap.values()){
                    Invoice__c oldInv = oldMap.get(inv.Id);

                    //Once the Invoice is Approved - Invoice should go to finance Queue
                    if(inv.Approval_Status__c == Constants.INVOICE_APPROVAL_STATUS_APPROVED && 
                        inv.Approval_Status__c != oldInv.Approval_Status__c){
                        approvedInvoiceList.add(inv);
                    }
                }

                if(!approvedInvoiceList.isEmpty()){
                    InvoiceTriggerHelper.changeInvoiceOwnerToFinanceQueue(approvedInvoiceList);
                }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'before Update InvoiceTriggerHandler');      

            }           
    }    

    /*******************************************
 * Created By: Mohan
 * Purpose   : after Update Handler
 * *****************************************/ 
    public static void afterUpdate(Map<Id, Invoice__c> newMap, Map<Id, Invoice__c> oldMap){

        try{
                List<Invoice__c> approvedInvoiceList = new List<Invoice__c>();

                for(Invoice__c inv: newMap.values())
                {
                    Invoice__c oldInv = oldMap.get(inv.Id);

                    //Once the Invoice is Approved - Invoice should go to finance Queue
                    if(inv.Approval_Status__c == Constants.INVOICE_APPROVAL_STATUS_APPROVED && 
                        inv.Approval_Status__c != oldInv.Approval_Status__c)
                    {
                        approvedInvoiceList.add(inv);
                    }
                } 
                  if(!approvedInvoiceList.isEmpty())
                  {
                    InvoiceTriggerHelper.CreateInvoiceSettlementCase(approvedInvoiceList);
                  }

          } catch(Exception e)
             {
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e, 'before Update InvoiceTriggerHandler');      
             }  
    }


}