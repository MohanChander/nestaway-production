/*  Created By   : Mohan - WarpDrive Tech works
    Created Date : 28/05/2017
    Purpose      : All the queries related to House Inspection Checklist Object will be here */
public class HouseInspectionCheckListSelector {

    //get list of HIC from the Set of House Id's
    public static List<House_Inspection_Checklist__c> getHICListFromHouseIdSet(Set<Id> houseIdSet){

        Id dccRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Document Collection Checklist').getRecordTypeId();
        return [select Id, House__c, TV__c, TV_Brand__c, TV_Model__c, Tv_Serial_No__c, Warranty_Start_Date_For_TV__c,
                Opportunity__r.AccountId, Kitchen_Package__c, Warranty_Start_Date_For_Fridge__c, Warranty_End_Date_For_Fridge__c,
                Warranty_End_Date_For_TV__c, Fridge__c, Fridge_Model__c, Fridge_Serial_No__c, Fridge_Make__c,
                Washing_Machine__c, Washing_Machine_Brand__c, Washing_Machine_Model__c, Washing_Machine_Serial_No__c
                from House_Inspection_Checklist__c where House__c =: houseIdSet 
                and Type_Of_HIC__c = 'House Inspection Checklist' and House__r.Assets_Created__c = false];
    }

    public static List<House_Inspection_Checklist__c> getHicListWithRICDetails(Set<Id> hicIdSet){
        return [select Id, (select Id from Room_Inspection__r where Has_an_attached_bathroom__c = 'Yes') 
                from House_Inspection_Checklist__c where Id =: hicIdSet];
    }

    //Query for the Checklist records related to Opportunities
    public static List<House_Inspection_Checklist__c> getHICListForOpportunities(Set<Id> oppIdSet){

        return [select Id,Opportunity__c,Service_ZoneCode__c,Onboarding_Zone_Code__c,Acquisition_Zone_code__c, House_Lat_Long_details__c, House_Lat_Long_details__Latitude__s, House_Lat_Long_details__Longitude__s,
                Kitchen_Availability__c, Floor__c, Opportunity__r.Account.PersonEmail from House_Inspection_Checklist__c
                where Opportunity__c =: oppIdSet and RecordType.Name != 'Document Collection Checklist'];
    }

    //Query for the DCC from the Opportunities
    public static List<House_Inspection_Checklist__c> getDCCListForOpportunities(Set<Id> oppIdSet){
        return [select Id, House__c, Opportunity__c, Doc_Reviewer_Approved__c from House_Inspection_Checklist__c 
                where Opportunity__c =: oppIdSet and RecordType.Name =: Constants.CHECKLIST_RT_Document_Collection_Checklist];
    }

    // Added By Baibhav
    // get list of HIC from the Set of House Id's
     public static List<House_Inspection_Checklist__c> getHICchecklistListFromHouseIdSet(Set<Id> houseIdSet){

        Id hicRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('House Inspection Checklist').getRecordTypeId();
        return [select Id,Name, House__c, TV__c, TV_Brand__c, TV_Model__c, Tv_Serial_No__c, Warranty_Start_Date_For_TV__c,Induction_Cooktop__c,
                Kitchen_Package__c, Warranty_Start_Date_For_Fridge__c, Warranty_End_Date_For_Fridge__c,Furnishing_condition__c,Iron_Box__c,
                Warranty_End_Date_For_TV__c, Fridge__c, Fridge_Model__c, Fridge_Serial_No__c, Fridge_Make__c,Microwave__c,Stove__c,Sofa__c,Dining_Table__c,
                Washing_Machine__c, Washing_Machine_Brand__c, Washing_Machine_Model__c, Washing_Machine_Serial_No__c,Air_Conditioner__c,
                Owner_to_fulfill_furniture_through__c, House_layout__c
                from House_Inspection_Checklist__c where House__c =: houseIdSet 
                and (Type_Of_HIC__c = 'House Inspection Checklist' or RecordTypeId =: hicRecordTypeId)];
    }

     public static List<House_Inspection_Checklist__c> getHICListForOpporId(Set<Id> oppIdSet){

        return [select Id,Opportunity__c,Approval_Status__c,RecordTypeId,Status__c,Onboarding_Zone_Code__c,Acquisition_Zone_code__c, House_Lat_Long_details__c, House_Lat_Long_details__Latitude__s, House_Lat_Long_details__Longitude__s,
                Kitchen_Availability__c, Floor__c, Opportunity__r.Account.PersonEmail from House_Inspection_Checklist__c
                where Opportunity__c =: oppIdSet];
            }

      public static List<House_Inspection_Checklist__c> getHICListForAccountId(Set<Id> accIdSet){

        return [select Id,Opportunity__r.AccountId,PAN_Card_Available__c,Opportunity__c,Approval_Status__c,RecordTypeId,Status__c,Onboarding_Zone_Code__c,Acquisition_Zone_code__c, House_Lat_Long_details__c, House_Lat_Long_details__Latitude__s, House_Lat_Long_details__Longitude__s,
                Kitchen_Availability__c, Floor__c, Opportunity__r.Account.PersonEmail,PAN_Card_Number__c from House_Inspection_Checklist__c
                where Opportunity__r.AccountId =: accIdSet];
            }      

}