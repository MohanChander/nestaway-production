@ISTest
public class RicTrigerHandlerTest {
    Public Static Id operationalRtMImoandService= Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(MissingItemsConstants.OPERATIONAL_PROCESS_RT_MIMO_SERVICE).getRecordTypeId();
    Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    Public Static Id moveInRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
    Public Static Id moveOutRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
    Public Static Id mimoRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_TYPE_MIMO_CHECK).getRecordTypeId();
    public Static TestMethod Void doTest(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveInRicRTId;
        bc1.Tenant_Room_Cleaned__c = MissingItemsConstants.TENANT_ROOM_CLEANED_NO;
        bc1.Tube_Light__c=MissingItemsConstants.TUBELIGHT_NOT_PROVIDED;
        bc1.Ceiling_Fan__c=MissingItemsConstants.CEILING_FAN_NOT_PROVIDED;
        bc1.Window_Curtain__c = MissingItemsConstants.WINDOW_CURTAINS_NOT_PROVIDED;
        bc1.Pillow__c=MissingItemsConstants.PILLOW_NOT_PROVIDED;
        bc1.Bed_Sheet__c=MissingItemsConstants.BED_SHEET_NOT_PROVIDED;
        bc1.Cot__c = MissingItemsConstants.COT_NO;
        bc1.Mattress__c=MissingItemsConstants.MATTRESS_No;
        bc1.AC__c=MissingItemsConstants.AC_NO;
        bc1.Chair__c = MissingItemsConstants.CHAIR_NO;
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_NOT_PRESENT;
        bc1.AC__c=MissingItemsConstants.AC_NO;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        
        
        
        system.debug('bc2'+bc2);
    }
    public Static TestMethod Void doTest3(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveInRicRTId;
        
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION;
        bc1.AC__c=MissingItemsConstants.AC_NOT_WORKING;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        
        
        
        system.debug('bc2'+bc2);
    }
    public Static TestMethod Void doTest5(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveInRicRTId;
        
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION;
        bc1.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        
        
        
        system.debug('bc2'+bc2);
    }
    public Static TestMethod Void doTest1(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveOutRicRTId;
        bc1.Tenant_Room_Cleaned__c = MissingItemsConstants.TENANT_ROOM_CLEANED_NO;
        bc1.Tube_Light__c=MissingItemsConstants.TUBELIGHT_NOT_PROVIDED;
        bc1.Ceiling_Fan__c=MissingItemsConstants.CEILING_FAN_NOT_PROVIDED;
        bc1.Window_Curtain__c = MissingItemsConstants.WINDOW_CURTAINS_NOT_PROVIDED;
        bc1.Pillow__c=MissingItemsConstants.PILLOW_NOT_PROVIDED;
        bc1.Bed_Sheet__c=MissingItemsConstants.BED_SHEET_NOT_PROVIDED;
        bc1.Cot__c = MissingItemsConstants.COT_NO;
        bc1.Mattress__c=MissingItemsConstants.MATTRESS_No;
        bc1.AC__c=MissingItemsConstants.AC_NO;
        bc1.Chair__c = MissingItemsConstants.CHAIR_NO;
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_NOT_PRESENT;
        bc1.AC__c=MissingItemsConstants.AC_NO;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        
        
        
        system.debug('bc2'+bc2);
    }
    public Static TestMethod Void doTest2(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveOutRicRTId;
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION;
        bc1.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        
        system.debug('bc2'+bc2);
    }
    public Static TestMethod Void doTest4(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Room_Inspection__c bc= new Room_Inspection__c();
        insert bc;
        Operational_Process__c o = new Operational_Process__c();
        o.recordtypeid=operationalRtMImoandService;
        insert o;
        Room_Inspection__c bc2= new Room_Inspection__c();
        bc2.RecordTypeId=mimoRicRTId;
        bc2.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2.CheckFor__c= bc.id;
        insert bc2;
        Room_Inspection__c bc1= new Room_Inspection__c();
        bc1.CheckFor__c=bc.id;
        bc1.RecordTypeId=moveOutRicRTId;
        bc1.Side_Table__c=MissingItemsConstants.SIDE_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION;
        bc1.AC__c=MissingItemsConstants.AC_NOT_WORKING;
        bc1.CheckList_Verified__c='No';
        insert bc1;
        bc1.CheckList_Verified__c = 'Yes';
        update bc1;
        system.debug('bc2'+bc2);
    }
}