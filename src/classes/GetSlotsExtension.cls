/*************************************(****************************************************
Added by baibhav Kumar
Created date: 16/12/2017
Purpose to Show Avilable Slots
******************************************************************************************/
global  class GetSlotsExtension {

    public id wrkid;
    public Workorder wrk {get;set;}
    public List<SelectOption> dateSelcectList {get;set;}
    public List<SelectOption> slotSelcectList {get;set;}
    public String seldate {get;set;}
    public Map<String,List<SlotTime>> dateToSlotMap;
    public String seletedSlot {get;set;}
    public Map<String,SlotTime> slotTimeMap {get;set;}
    public boolean hide {get;set;}
    public boolean slotavialbe  {get;Set;}
    public FieldServiceSlotsService.JsonResponseBody jsonrepond {get;set;}
    public List<FieldServiceSlotsService.SlotObj> slObjList;
    public Set<id> houseId;

    public GetSlotsExtension(ApexPages.StandardController stdController) {

    wrkid = stdController.getRecord().id;
    slObjList=new List<FieldServiceSlotsService.SlotObj>();
    dateSelcectList=new List<SelectOption>();
    slotSelcectList=new List<SelectOption>();
    houseId=new Set<id>();
    jsonrepond=new FieldServiceSlotsService.JsonResponseBody();
    dateToSlotMap=new Map<String,List<SlotTime>>();
    hide=true;
    slotavialbe=true;

    wrk=[Select id,ownerId,StartDate,EndDate,Work_Start_Time__c,Caseid,case.House1__c,case.House1__r.HouseId__c,case.Problem__r.Name from workorder where id=:wrkid];
    System.debug('***wrk***'+wrk);
    if(wrk.Caseid==null){
         String addErrorMessage='Case Id is not present on workorder';
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
    }
    else{
        if(wrk.Case.house1__c==null){
            String addErrorMessage='House Id is not present on Case';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
        }
        else if(wrk.case.Problem__c==null){
            String addErrorMessage='Problem id is not present on Case';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
        }
        else{
              hide=false;
              houseId.add(wrk.case.house1__c);
            // jsonrepond= FieldServiceSlotsService.getSlots(wrk.case.problem__c, houseId); 
        }

    }
}

  // Method fro reciving json and making select date option
  public void callforJson(){
     
     System.debug('****call*');
     if(!hide){
         jsonrepond = FieldServiceSlotsService.getSlots(wrk.case.problem__c, houseId);
         System.debug('***json****'+jsonrepond);
         if(jsonrepond!=null){
             for(FieldServiceSlotsService.HouseWithSlotsObj houSlot:jsonrepond.slots_for_houses){
                  slObjList=houSlot.slots;
             }
         }
         
     
         /******Test data*********/
     /*    FieldServiceSlotsService.SlotObj so1=new FieldServiceSlotsService.SlotObj();
         so1.from_time=System.now();
         so1.to_time=System.now().addHours(2);
     
         FieldServiceSlotsService.SlotObj so2=new FieldServiceSlotsService.SlotObj();
         so2.from_time=System.now().addHours(4);
         so2.to_time=System.now().addHours(6);

         FieldServiceSlotsService.SlotObj so11=new FieldServiceSlotsService.SlotObj();
         so11.from_time=System.now().adddays(2);
         so11.to_time=System.now().adddays(2).addHours(2);
     
         FieldServiceSlotsService.SlotObj so12=new FieldServiceSlotsService.SlotObj();
         so12.from_time=System.now().adddays(2).addHours(4);
         so12.to_time=System.now().adddays(2).addHours(6);
     
         slObjList.add(so1);
         slObjList.add(so2);
         slObjList.add(so11);
         slObjList.add(so12);*/
        //****************End
         if(slObjList==null || slObjList.isEmpty()){
            slotavialbe=false;
            String addErrorMessage='There are no Slots Availabe';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
           
         }
        else{
                
                // loop for making map of dsate and Slot time
                for(FieldServiceSlotsService.SlotObj slob:slObjList){
                 
                 list<SlotTime> stlist=new list<SlotTime>();
                    Date dt=slob.from_time.Date();
             
                    SlotTime sloT=new SlotTime();
                    sloT.from_time=slob.from_time.Time();
                    sloT.to_time=slob.to_time.Time();
                    slot.strfrom_time=slob.from_time.format('h:mm a');
                    slot.strto_time=slob.to_time.format('h:mm a');
        
             
                    if(dateToSlotMap.containsKey(String.valueOf(dt))){
                         stlist=dateToSlotMap.get(String.valueOf(dt));
                    }
                    stlist.add(sloT);
                    dateToSlotMap.put(String.valueOf(dt),stlist);
             
                } 
                dateSelcectList.add(new SelectOption('-None-','-None-'));
                for(String dt:dateToSlotMap.keySet()){
                      dateSelcectList.add(new SelectOption(dt,dt));
                 }
                  System.debug('****Selctdate*'+dateSelcectList);
              }
         }


}
// Method for Availabe Slots
public void availableslots(){

  slotTimeMap=new Map<String,SlotTime>();   
  slotSelcectList=new List<SelectOption>();
  List<SlotTime> sloTimeList=dateToSlotMap.get(seldate);

  slotSelcectList.add(new SelectOption('-None-','-None-'));
  for(SlotTime slot:sloTimeList){
      slotSelcectList.add(new SelectOption(String.valueOf(slot.from_time+'-'+slot.to_time),slot.strfrom_time+'-'+slot.strto_time));
      slotTimeMap.put(String.valueOf(slot.from_time+'-'+slot.to_time),slot);
  }
     System.debug('****SelctSlot*'+slotTimeMap);

}

//Save Button
 public pagereference Save(){
   
   SlotTime slot = new SlotTime(); 
   slot = slotTimeMap.get(seletedSlot);

   System.debug('*****Slot slected******' + slot);

   Date selectedDate = Date.valueOf(selDate);
   Datetime rescheduledDatetime = Datetime.newInstance(selectedDate, slot.from_time);

   //update the Case Service Visit Time - which will inturn reschedule the Service Appointment
   Case c = new Case();
   c.Id = wrk.CaseId;
   c.Service_Visit_Time__c = rescheduledDatetime;
   update c;

    PageReference parentPage;              
    parentPage = new PageReference('/' + wrk.Id);
    parentPage.setRedirect(true);
    return parentPage;
                
 }

 // Cancel Button
  public pagereference Cancel(){
    
    PageReference parentPage;              
    parentPage = new PageReference('/' + wrk.Id);
    parentPage.setRedirect(true);
    return parentPage;
                
 }
 
   global class SlotTime {
       public Time from_time;
       public Time to_time;
       public String strfrom_time;
       public string strto_time;
   }
}