/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 19/05/2017
    Purpose      : All the queries related to the Case Object are here */

public class CaseSelector {

    //query for the Case records from the Case Id Set 
    public static List<Case> getCasesFromCaseIdSet(Set<Id> caseIdSet){
       return [select Id, Status, House__c, Booked_Object_Type__c,HouseForOffboarding__r.Onboarding_Zone_Code__c,
                ContactId, HouseForOffboarding__r.Name, HouseForOffboarding__r.HouseId__c, 
                HouseForOffboarding__r.ZAM_Email__c, Room_Term__c, Bed__c, Room_Term__r.Room_Inspection__c,
                OwnerId,HouseForOffboarding__r.ZAM__r.Name, Next_Escalation_to__c, RecordTypeId,HouseForOffboarding__c, 
                HouseForOffboarding__r.Reason_For_Initiate_Offboarding__c, HouseForOffboarding__r.ZAM__c,
                HouseForOffboarding__r.HRM__c, Offboarding_Reason__c, Account.PAN_Number__c,
                MoveIn_Slot_Start_Time__c, House1__c, MoveIn_Slot_End_Time__c, MoveIn_Executive__c,
                HouseForOffboarding__r.House_Owner__c, AccountId  from Case where Id =: caseIdSet];
    }

    //query for the no of Open Cases assigned to users - Service Request Cases
    public static List<AggregateResult> getNoOfOpenCasesPerUser(Set<String> caseStatus, Id caseRecordTypeId){
        return [select OwnerId, count(Id) from case where status IN : caseStatus and 
                RecordTypeId =: caseRecordTypeId group by OwnerId ];
    }

    //query for the Case records from the House2 Id Set -  Added by Baibhavx
    public static List<Case> getCasesFromHouseIdSet(Set<Id> houseIdSet){
        return [select Id, Status, House__c, HouseForOffboarding__c, Booked_Object_Type__c, Room_Term__c, Bed__c, Room_Term__r.Room_Inspection__c,
                OwnerId, Next_Escalation_to__c  from Case where HouseForOffboarding__c =: houseIdSet ];
    }

     //query for the Case records from the WordOrder Id Set -  Added by Baibhav
    public static Map<id,Case> getCasesMapFromcaseIdSet(Set<Id> caseIdSet){
        return new Map<id,Case>([select Id, Status,HouseForOffboarding__r.ZAM__c,Active_Tenants__c,
                                Deduction_amount__c,FNF_Amount__c,Booking_Type__c,Furnishing_Plan__c,
                                Furnishing_Type__c,House__c,Stage__c, HouseForOffboarding__c, Booked_Object_Type__c, 
                                Room_Term__c, Bed__c, Room_Term__r.Room_Inspection__c, AccountId,
                                ContactId,
                                HouseForOffboarding__r.Name, HouseForOffboarding__r.HouseId__c, 
                                OwnerId, Next_Escalation_to__c  from Case where id =: caseIdSet ]);
    }   

    //query for No of Agent and Customer Comments
    public static List<AggregateResult> getNoOfCaseComments(Set<Id> caseIdSet){
        return [select Case__c, Account__C, Count(Id) from Case_Comment__c 
                                       where Case__c =: caseIdSet
                                       group by Case__c, Account__c];
    }  

    //Generic Method to give caseLoadMap for Given Case RecordTypes and for Particular Zone Type
    public static Map<String,Map<Id,Integer>> getCaseLoadMap(Set<Id> caseRecordTypeIds, Set<String> caseStatus, Id zoneRecordTypeId){

        Map<String, Map<Id, Integer>> caseLoadMap = new Map<String, Map<Id, Integer>>();
        Map<Id, Integer> openCaseMap = new Map<Id, Integer>();

        for(AggregateResult ar: [select OwnerId, count(Id) from case where status NOT IN : caseStatus and 
                                 RecordTypeId =: caseRecordTypeIds group by OwnerId]){
            openCaseMap.put((Id)ar.get('OwnerId'), (Integer)ar.get('expr0'));            
        }  

        System.debug('***************openCaseMap: ' + openCaseMap + '\n Size of openCaseMap: ' + openCaseMap.size());

        // Populate the CaseLoadMap
        for(Zone_and_OM_Mapping__c zu: ZoneAndUserMappingSelector.getZoneAndUserMappingForParticularZoneType(zoneRecordTypeId)){
            if(caseLoadMap.containsKey(zu.Zone__r.Zone_Code__c)){
                caseLoadMap.get(zu.Zone__r.Zone_Code__c).put(zu.User__c, 
                            openCaseMap.get(zu.User__c) == null ? 0 : openCaseMap.get(zu.User__c));
            } else {
                caseLoadMap.put(zu.Zone__r.Zone_Code__c, 
                                new Map<Id, Integer>{zu.User__c => openCaseMap.get(zu.User__c) == null ? 0 : openCaseMap.get(zu.User__c)});
            }
        }

        return caseLoadMap;
    }  

    //query for the Offbaorded Case
    public static List<Case> getOffboardedCase(Set<Id> houseIdSet){
        return [select Id, Offboarding_Reason__c, HouseForOffboarding__c from Case where HouseForOffboarding__c =: houseIdSet 
                and Stage__c !=: Constants.CASE_STAGE_OWNER_RETAINED];
    }

    //Get cases With Unfurnishing WorkOrder Unfunishing Childs
    public static List<Case> getCasesWithUnfurnishingWorkOrder(Set<Id> caseIdSet){

        Id unfurnishingWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_HOUSE_UNFURNISHED).getRecordTypeId();
        return [select Id, RecordTypeId, (select Id, RecordTypeId from WorkOrders where 
                RecordTypeId =: unfurnishingWorkOrderRTId)
                from Case where Id =: caseIdSet];
    }
    public  static Map<id,Case> getMapCasefromCaseID(Set<Id> caseIdSet)
    {
       return new Map<id,case>([select id,HouseForOffboarding__r.ZAM__c,Active_Tenants__c,TDS_amount_deducted__c, House_City__c, State__c,
                                Recoverable__c, Total_Cost__c, 
                                Deduction_amount__c,FNF_Amount__c,Booking_Type__c,Furnishing_Plan__c,
                                Furnishing_Type__c,House__c,Stage__c, HouseForOffboarding__c, Booked_Object_Type__c, 
                                Room_Term__c, Bed__c, Room_Term__r.Room_Inspection__c, AccountId,
                                ContactId,HouseForOffboarding__r.Name, HouseForOffboarding__r.HouseId__c, 
                                OwnerId, Next_Escalation_to__c,Material_Cost__c,Labour_Cost__c,Express_Service_Cost__c,Service_visit_cost__c,House1__c from case where id=:caseIdSet]);
    }
}