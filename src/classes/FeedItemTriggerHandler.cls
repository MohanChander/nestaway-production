public class FeedItemTriggerHandler {

    public static void afterInsert(Map<Id, FeedItem> newMap) {

        System.debug('**afterInsert FeedItemTriggerHandler');

        try{
                Id serviceRequestRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();

                List<FeedItem> caseFeedItemList = new List<FeedItem>();
                Set<Id> caseIdSet = new Set<Id>();      

                for(FeedItem fi: newMap.values()){

                    System.debug('fi.IsRichText ' + fi.IsRichText);

                    if(string.valueOf(fi.ParentId).startsWith('500') && fi.IsRichText){
                        caseIdSet.add(fi.ParentId);
                    }
                }   

                System.debug('**CaseIdSet size ' + caseIdSet.size());

                Map<Id, Case> caseMap = new Map<Id, Case>([select Id, OwnerId, CaseNumber, Owner.Name, Follower__c from Case 
                                                                   where Id =: caseIdSet and 
                                                                   RecordTypeId =: serviceRequestRtId]); 

                System.debug('**caseMap: ' + caseMap + '\n Size of caseMap: ' + caseMap.size());
                
                for(FeedItem fi: newMap.values()){
                    if(caseMap.containsKey(fi.ParentId) && fi.InsertedById != caseMap.get(fi.ParentId).OwnerId){
                        caseFeedItemList.add(fi);
                    }
                }

                if(!caseFeedItemList.isEmpty()){
                    FeedItemTriggerHelper.sendCaseFeedItemAsEmail(caseFeedItemList, caseMap);
                }
            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'FeedItemTriggerHandler afterInsert');      
            }   
    }
}