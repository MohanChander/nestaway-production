//Added by baibhav

public class AccountTriggerHelper {
	public static void updatePanToHIC(Map<id,account> accMap)
	{
/*		List<House_Inspection_Checklist__c> hicList= HouseInspectionCheckListSelector.getHICListForAccountId(accMap.keySet());
	    List<House_Inspection_Checklist__c> hicUpdate = new List<House_Inspection_Checklist__c>(); 
		for(House_Inspection_Checklist__c hic:hicList)
		{
			if(accMap.get(hic.Opportunity__r.AccountId).PAN_Number__c!=null)
			{
			  hic.PAN_Card_Number__c=accMap.get(hic.Opportunity__r.AccountId).PAN_Number__c;
			  hic.PAN_Card_Available__c='Yes';
			  HouseInspectionTriggHandler.check=true;
			}
			else
			{
		      hic.PAN_Card_Available__c='No';
			}
            hicUpdate.add(hic);
		}
		if(!hicUpdate.isEmpty())
	      Update hicUpdate;	
*/	}


/**********************************************
    Created By : Mohan
    Purpose    : update the Tax Section on Account
**********************************************/
	public static void updateTaxSection(List<Account> accList){

        System.debug('**updateTaxSection');

        try{	
        		Map<String, Tax_Section__c> tsMap = new Map<String, Tax_Section__c>();
        		List<Tax_Section__c> tsList = [select Id, Name, Type__c, TDS_Rates__c from Tax_Section__c];

        		for(Tax_Section__c ts: tsList){
        			if(ts.Type__c == 'HUF/Individuals'){
        				tsMap.put('HUF', ts);
        			} else if(ts.Type__c == 'Others'){
        				tsMap.put('Others', ts);
        			} else {
                        tsMap.put('pan not registerd', ts);
                    }
        		}

        		for(Account acc: accList){

                    if(acc.PAN_Available__c == 'No'){
                        acc.Tax_Section__c = tsMap.get('pan not registerd').Id;                        
                    } else if(acc.PAN_Available__c == 'Yes' && acc.PAN_Number__c != null && 
                        (acc.PAN_Number__c.subString(3,4) == 'P' || acc.PAN_Number__c.subString(4,5) == 'H')){
        				    acc.Tax_Section__c = tsMap.get('HUF').Id;
        			} else {
        				acc.Tax_Section__c = tsMap.get('Others').Id;
        			}
        		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Account - Update Tax Section on Account');      
            }   		
	}	
}