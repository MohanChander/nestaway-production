public with sharing class AssetHelper {
	
	public static void assetAutoIdCreation(List<Asset> asstlist){

        try{    
               //List<Asset> astlist=new List<Asset>();
        		List<Asset> astlist =[Select id,Asset_custom_id__c from Asset where Asset_custom_id__c!=null  order By Asset_Number_from_Asset_Id__c  desc limit 1];
        		//System.debug('***Ast'+ast.Asset_custom_id__c);
        		Integer num=0;
        		
        		if(astlist!=null && !astlist.isEmpty()){
        				String str=astlist[0].Asset_custom_id__c;
        				String[] strA =str.split('-');
        				System.debug(strA[1]);
        				num=Integer.valueof(strA[1]);
        			}
        
        		for(Asset at:asstlist){
        			at.Asset_custom_id__c = 'NA-' + ++num;
        
        		}
        	}  Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'Asset Auto Number');
            }
	}
}