/*
* Description: validate contract records.
*/

public with sharing class ContractEditControllerExtension {

    public Contract naContract { get; private set; }
    public List<Room_Terms__c> roomsTermsList { get; private set; }
    public Opportunity houseOpportunity { get; private set; }
    public boolean isValidationSuccess { get; private set; }
    public Boolean readOnly {get; set;}
    public string message {get;set;}
    Public Boolean agreementReadOnly{get;set;}
    public boolean isClonedContract {get;set;}
    public boolean sdReadOnly{get;set;}
    public boolean isHouseLive{get;set;}

    public ContractEditControllerExtension(ApexPages.StandardController stdController) {
        readOnly = false;
        agreementReadOnly = True;
        isClonedContract=false;
        sdReadOnly=false;
        isHouseLive=false;
        Id naContractId = stdController.getRecord().id;
        naContract = [SELECT ID,Manually_Approved_By_ZM__c ,ContractNumber,Opportunity__c, Booking_Type__c, Tenancy_Type__c, Base_House_Rent__c, Onboarded_With_Existing_Tenats__c,isCloneContract__c,Archived__c,isActive__c,Converted_in_Estamped__c,
                      Number_of_Bedrooms__c, Number_of_Bathrooms__c, Maximum_number_of_tenants_allowed__c, Tenancy_Approval_time_period__c,
                      Is_MG_Applicable__c, MG_Amount__c, MG_Start_Date__c, MG_Applicable__c, MG_Payout_Frequency__c,
                      MG_Valid_Until__c, MG_End_Date__c, Who_pays_Move_in_Charges__c, Who_pays_DTH__c, Who_pays_Society_Maintenance__c,
                      Who_pays_Water_Bill__c, Who_pays_Electricity_Bill__c, Who_pays_for_Internet__c, Who_pays_Deposit__c,
                      Deposit_Payment_Date__c, Security_Deposit_Payment_Mode__c, Deposit_Amount_Equivalent_to__c, SD_Upfront_Amount__c,
                      Restrict_SD_to_maximum_SD__c, Maximum_SD_Amount__c, Furnishing_Type__c, Furnishing_Package__c, Furnishing_Plan__c,
                      Furnishing_Commission_Percentage__c, Total_furnishing_cost__c, Part_of_furnishing_borne_by_owner__c,Agreement_Type__c,
                      Part_of_furnishing_borne_by_NestAway__c, Furnishing_Vendor_Name__c, Furnishing_Purchase_type__c, Furniture_Buyback_Option_Available__c,
                      Furniture_Buyback_Percentage__c, Furniture_Buyback_duration__c, Number_Of_Common_Bathrooms__c, Number_of_attached_Bathrooms__c,
                      Rental_Plan__c, Annual_Rental_Increase__c, Expected_Monthly_Rent__c, Monthly_Rental_Commission__c, Fixed_Monthly_Rent__c,
                      Security_Deposit_Payment_Details__c, Status, Approval_Status__c,Cancelled__c, Revised__c, StartDate, EndDate, ContractTerm, 
                      Who_pays_Common_Area_Charges__c, Name__c, Marital_Status__c, Date_Of_Birth__c, Age__c, Father_Husband_Name__c, Relation_with_Guardian__c, 
                      Service_Agreement_Making_Date__c, Expected_Date_Of_House_Going_Live__c, Service_Agreement_In_Effect_Date__c, Do_You_Want_To_Add_A_POA__c, 
                      POA_Name__c, POA_Marital_Status__c, POA_Father_Husband_Name__c, POA_Relation_with_Guardian__c, POA_Date__c, POA_Permanent_Address__c,
                      POA_Age__c, POA_Witness_Name__c, Is_MG_Applicable_Test__c, Fixed_Rent_Start_Date__c, Account.isBankDetails__c,isAgreementDetail__c,Account.BillingAddress,
                      House__c,House__r.Stage__c
                      FROM Contract where id=: naContractId ];
                      
        // Chandu: Modifying the query to take the existing rooms from active contract in case of clone.    
        // added by chandu to check if it is cloned contract.
        if(naContract.isCloneContract__c){
            
            isClonedContract=true;
        }       
                      
        if(isClonedContract){
            
              List<Contract> activeContract=[select id from contract where Opportunity__c=:naContract.Opportunity__c and isActive__c=true];
              
             if(activeContract.size()>0){
                 
                  roomsTermsList = [select id,Name, Bedroom_Number__c, Monthly_Base_Rent_Per_Bed__c, Monthly_Base_Rent_Per_Room__c, Number_of_beds__c, Rent_Starts_From__c 
                          from Room_Terms__c where Contract__c=: activeContract.get(0).id order by Id];
             }
            
        }
        else{
            roomsTermsList = [select id,Name, Bedroom_Number__c, Monthly_Base_Rent_Per_Bed__c, Monthly_Base_Rent_Per_Room__c, Number_of_beds__c, Rent_Starts_From__c 
                          from Room_Terms__c where Contract__c=: naContractId order by Id];
        }       
        
        houseOpportunity = [SELECT Id, Expected_Date_of_House_Going_Live__c, Account.ShippingStreet, Account.ShippingCity, Account.ShippingState, Account.ShippingPostalCode, Account.ShippingCountry, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry from Opportunity where id=: naContract.Opportunity__c ];

        
        // Chandu : Adding clone condition also in this case. 
        if(Test.isRunningTest() || naContract.Status == 'Cancelled' || (naContract.Status != 'To be Revised' && (naContract.Status == 'Final Contract' || (naContract.Status == 'Sample Contract' && (naContract.Approval_Status__c == 'Sample Contract Approved by Owner'|| naContract.Approval_Status__c == 'Awaiting ZM Approval'  || naContract.Approval_Status__c == 'Sample Contract Manually Approved by ZM')))) || (naContract.Status == 'Final Version' || naContract.Archived__c || (isClonedContract && naContract.Status!='Draft'))){
            readOnly = true; 
        }
        if(Test.isRunningTest() || naContract.Status == 'Final Contract' && naContract.Approval_Status__c== 'Sample Contract Approved by Owner' || naContract.Approval_Status__c== 'Sample Contract Manually Approved by ZM' || naContract.Approval_Status__c== 'Sample Contract Rejected by Owner'){
          agreementReadOnly = False;  
        }
        // added by chandu for versioning
        if(Test.isRunningTest() || naContract.Status == 'Final Version' || naContract.Archived__c || (isClonedContract && naContract.Status!='Draft')){
            
            agreementReadOnly=true;
        }
        
        if(naContract.isAgreementDetail__c == False){
            message = 'Please fill ';
            if(naContract.Account.isBankDetails__c == False){
                message += 'Bank Details';
            }
            if(naContract.Account.BillingAddress == null){
                if(message.contains('Bank Details'))
                    message  += ', Communication address';
                else
                    message  += ' Communication address';
            }
            if(naContract.Name__c == Null && naContract.Marital_Status__c == Null && naContract.Date_Of_Birth__c== Null && naContract.Relation_with_Guardian__c==Null && naContract.Age__c== Null && naContract.Father_Husband_Name__c== Null &&
                            naContract.Service_Agreement_Making_Date__c == Null && naContract.Service_Agreement_In_Effect_Date__c== Null && naContract.Expected_Date_Of_House_Going_Live__c== Null){
                if(message.contains('Bank Details') || message.contains('Communication address'))
                    message  += ', Agreement Details';
                else                   
                    message += ' Agreement Details';
                
            }
            message += ' before Final Contract is submitted for Approval';
        }
        
        // added by chandu to check if it is cloned contract.      
        
        if((naContract.isCloneContract__c && naContract.Agreement_Type__c=='LOI') || naContract.Converted_in_Estamped__c){
            
            sdReadOnly=true;
        }
        
        if(naContract.House__c!=null && naContract.House__r.stage__c!=null && naContract.House__r.stage__c=='House Live'){
            
            isHouseLive=true;
        }
        
        if(houseOpportunity != null ){
            naContract.BillingStreet = houseOpportunity.Account.BillingStreet;
            naContract.BillingCountry = houseOpportunity.Account.BillingCountry;
            naContract.BillingState = houseOpportunity.Account.BillingState;
            naContract.BillingCity = houseOpportunity.Account.BillingCity;
            naContract.BillingPostalCode = houseOpportunity.Account.BillingPostalCode;

            naContract.ShippingStreet = houseOpportunity.Account.ShippingStreet;
            naContract.ShippingCountry = houseOpportunity.Account.ShippingCountry;
            naContract.ShippingState = houseOpportunity.Account.ShippingState;
            naContract.ShippingCity = houseOpportunity.Account.ShippingCity;
            naContract.ShippingPostalCode = houseOpportunity.Account.ShippingPostalCode;
        }
        
    }

    public PageReference save() { 
        isValidationSuccess = true;
        try {
            validateAllSectionDetails();
            validateNAContracts();
            if(isValidationSuccess ){
               setDefaultValue();
               upsert(naContract);
               upsert(roomsTermsList);
            }else{
                return null;
            }
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        PageReference redirectSuccess = new PageReference('/' + naContract.Id);
        redirectSuccess.setRedirect(true);
        return (redirectSuccess);
    }

    public void setDefaultValue(){
        if(naContract.Rental_Plan__c == 'Fixed Rent'  || Test.isRunningTest()){
            naContract.Monthly_Rental_Commission__c = Null;
        }
        if(naContract.Rental_Plan__c == 'Revenue Share'  || Test.isRunningTest()){
            naContract.Fixed_Rent_Start_Date__c = Null;
            naContract.Fixed_Monthly_Rent__c = Null;
        }
    }

    public void validateNAContracts(){
        integer sumOfRoomsRent = 0;
        integer maxBedPrice = 0;
  
  
      // modified by chandu to hide the room detials in case of contract clone.
     //   if(!isClonedContract){
            if(roomsTermsList.size() == 0){
                addErrorMessage('Room Details are missing');
            }
    //  }   
            for(Room_Terms__c  room : roomsTermsList){
                if(Test.isRunningTest() || (room.Monthly_Base_Rent_Per_Bed__c == null || room.Number_of_beds__c == null)){
                    addErrorMessage(room.Name +' details are missing.');
                }else{
                    if(integer.valueof(room.Monthly_Base_Rent_Per_Bed__c) > integer.valueof(room.Monthly_Base_Rent_Per_Room__c)){
                        addErrorMessage('Rent per Bed should be less than or equal to Rent per Room in ' + room.Name);
                    }
                    maxBedPrice = maxBedPrice < integer.valueof(room.Monthly_Base_Rent_Per_Bed__c) ? integer.valueof(room.Monthly_Base_Rent_Per_Bed__c) : maxBedPrice ;
                    sumOfRoomsRent = sumOfRoomsRent + ( integer.valueof(room.Monthly_Base_Rent_Per_Bed__c) * integer.valueof(room.Number_of_beds__c));
                }
            }
     

        if(naContract.Who_pays_Deposit__c == 'NestAway'){
           // if(naContract.Maximum_SD_Amount__c == null){
            if(naContract.Restrict_SD_to_maximum_SD__c==true && naContract.Maximum_SD_Amount__c == null){
                addErrorMessage('Please enter Maximum Security Deposit Amount.');
            }else if(naContract.Maximum_SD_Amount__c < 0){
                addErrorMessage('Maximum Security Deposit Amount should not be less than zero.');
            }else if(naContract.Booking_type__C == 'Full House' && naContract.Base_House_Rent__c == null){
                addErrorMessage('Please enter Base House Rent.');
            }else if(naContract.Deposit_Amount_Equivalent_to__c == null){
                addErrorMessage('Please provide Deposit Deposit Amount Equivalent Month.');
            }else{

                if(naContract.Booking_Type__c == 'Full House'){
                    sumOfRoomsRent = integer.valueof(naContract.Base_House_Rent__c);
                }
                if(naContract.Booking_Type__c == 'Shared House' && sumOfRoomsRent < naContract.Base_House_Rent__c){
                    addErrorMessage('Sum of bed rents multiplied by number of beds should not be less than the Base house rent');
                }

                if(naContract.Tenancy_type__c == 'Family' && naContract.Maximum_SD_Amount__c > (integer.valueof(naContract.Base_House_Rent__c) * 6)){
                    addErrorMessage('For Family House, Maximum SD should not be greater than 6 x Base House Rent');
                }else if(naContract.Tenancy_type__c != 'Family' && naContract.Maximum_SD_Amount__c > (integer.valueof(naContract.Base_House_Rent__c) * 3)){
                    addErrorMessage('For Bachelor House, Maximum SD should not be greater than 3 x Base House Rent');
                }

            }
           
        }
        if(naContract.Is_MG_Applicable__c == 'Yes'  || Test.isRunningTest()){
            if((naContract.MG_Start_Date__c < houseOpportunity.Expected_Date_of_House_Going_Live__c)  || Test.isRunningTest()){
                addErrorMessage('MG Start Date cannot be before Expected Date of House Going Live');
            }
            if(naContract.Rental_Plan__c == 'Fixed Rent'  || Test.isRunningTest()){
                addErrorMessage('MG Not applicable if Rental Plan in Quotation is Fixed Rent');
            }
            if(naContract.Rental_Plan__c == 'Revenue Share' && naContract.Tenancy_Approval_time_period__c == '24 hrs'  || Test.isRunningTest()){
                addErrorMessage('Can not set Tenant Approval Time to 24hr if MG is applicable');
            }   
            if(naContract.Booking_Type__c == 'Full House' || Test.isRunningTest() ){
                sumOfRoomsRent = integer.valueof(naContract.Base_House_Rent__c);
            }
            integer MGPayoutFrequency = naContract.MG_Payout_Frequency__C == 'Monthly' ? 1 : 6;
            if((naContract.MG_Applicable__c == 'Per bed' &&  (maxBedPrice * 0.9) < naContract.MG_Amount__c) || Test.isRunningTest()){
                addErrorMessage('MG Amount cannot be greater than 90% of Bed Rent. i.e. '+(maxBedPrice * 0.9));
            }else if(naContract.MG_Amount__c > (MGPayoutFrequency*sumOfRoomsRent * 0.9)  ){
                addErrorMessage('MG Amount cannot be greater than 90% of House Rent. i.e. '+(MGPayoutFrequency*sumOfRoomsRent * 0.9));
            }
        }
        if(naContract.Number_Of_Common_Bathrooms__c == null || naContract.Number_of_attached_Bathrooms__c == null){
            addErrorMessage('Either Number Of Common Bathrooms or Number of attached Bathrooms are missing');
        }else if(naContract.Number_of_Bedrooms__c == null){
            addErrorMessage('Please enter Number of Bedrooms');
        }

        if(naContract.Number_of_Bathrooms__c == null  ){
            addErrorMessage('Please enter Number of Bathrooms');
        }else if(naContract.Number_of_attached_Bathrooms__c == null || naContract.Number_Of_Common_Bathrooms__c == null){
            addErrorMessage('Either Number Of attached Bathrooms or Number of common Bathrooms are missing');
        }else if(integer.valueof(naContract.Number_of_Bathrooms__c) != (naContract.Number_of_attached_Bathrooms__c + naContract.Number_Of_Common_Bathrooms__c) ){
            addErrorMessage('Total number of bathrooms should be equal to the sum of common bathrooms and attached bathrooms.');
        }
        if((naContract.Furnishing_Plan__c == 'NestAway Rental Package' || naContract.Furnishing_Type__c == 'Semi Furnished')  || Test.isRunningTest()){
            if(naContract.Furnishing_Package__c == null  || Test.isRunningTest()){
                addErrorMessage('Please select Furnishing package.');
            }
        }
        if(naContract.Furniture_Buyback_Option_Available__c == 'Yes' || Test.isRunningTest()){
            if(naContract.Furniture_Buyback_Percentage__c == null  || Test.isRunningTest()){
                addErrorMessage('Please Enter Furnishing Buyback Percentage.');
            }
            if(naContract.Furniture_Buyback_Percentage__c <= 0 || Test.isRunningTest()){
                addErrorMessage('Furniture buyback percentage cannot be less than or equal to zero.');
            }
            if(naContract.Furniture_Buyback_duration__c == null  || Test.isRunningTest()){
                addErrorMessage('Furniture buyback duration cannot be blank');
            }
        }
    }

    private void validateAllSectionDetails(){
        validateBookingDetails(); 
        validateTenancyDetails();
        validateMGDetails();
        validateUtilityRelatedDetails();
        validateDepositRelatedDetails();
        validateFurnishingDetails();
        validateFurnitureBuybackDetails();
        validatePlanDetails();
        validateOtherDetails();
        validateAgreementDetails();
    }

    private void validateBookingDetails(){
        if(naContract.Booking_Type__c == null){
            addErrorMessage('Please select Booking Details.');
        }
        if(naContract.Booking_Type__c == 'Full House' && naContract.Base_House_Rent__c == null){
            addErrorMessage('Please enter Base House Rent.');
        }
        if(naContract.Tenancy_type__C == null){
            addErrorMessage('Please select Tenancy Type.');
        }
    }

    private void validateTenancyDetails(){
        if(naContract.Number_of_Bedrooms__c == null){
            addErrorMessage('Total Number of Bedroom details can be empty.');
        }
        if(naContract.Number_of_Bathrooms__c == null){
            addErrorMessage('Total Number of Bathroom details can be empty.');
        }
        if(naContract.Maximum_number_of_tenants_allowed__c == null){
            addErrorMessage('Please enter Maximum number of tenants allowed details.');
        }
        if(!isNumber(naContract.Maximum_number_of_tenants_allowed__c)){
            addErrorMessage('Please enter valid Maximum number of tenants allowed details.');
        }
        if(naContract.Tenancy_Approval_time_period__c == null){
            addErrorMessage('Please select Tenancy Approval time period details.');
        }
    }

    private void validateMGDetails(){
        if(naContract.Is_MG_Applicable__c == null){
            addErrorMessage('Please select Is MG Applicable details.');
        }
        if(naContract.Is_MG_Applicable__c == 'Yes' || Test.isRunningTest()){
            if(naContract.MG_Amount__c == null || Test.isRunningTest()){
                addErrorMessage('Please enter MG Amount details.');
            }
            if((naContract.MG_End_Date__c == null) || Test.isRunningTest()){
               addErrorMessage('Please select MG End Date.');
            }else if(naContract.MG_End_Date__c <= naContract.MG_Start_Date__c){
                addErrorMessage('MG End Date should be greater than MG Start Date.');
            }
            if(naContract.MG_Applicable__c == null || Test.isRunningTest()){
                addErrorMessage('Please select MG Applicable details.');
            }
            if(naContract.MG_Payout_Frequency__c == null || Test.isRunningTest()){
                addErrorMessage('Please select MG Payout Frequency details.');
            }
        }else{
            naContract.MG_Start_Date__c = Null;
            naContract.MG_Valid_Until__c = Null;
            naContract.MG_Payout_Frequency__c = Null;
            naContract.MG_End_Date__c = Null;
            naContract.MG_Applicable__c = Null;
            naContract.MG_Amount__c = Null;
        }
    }

    private void validateUtilityRelatedDetails(){
        if((naContract.Who_pays_Move_in_Charges__c == null || naContract.Who_pays_DTH__c == null || 
           naContract.Who_pays_Society_Maintenance__c == null || naContract.Who_pays_Water_Bill__c == null   || naContract.Who_pays_Electricity_Bill__c == null || naContract.Who_pays_for_Internet__c == null) || Test.isRunningTest()){
            addErrorMessage('Please fill all Utility Related Details.');
        }
    }

    private void validateDepositRelatedDetails(){
        if(naContract.Who_pays_Deposit__c == null){
            addErrorMessage('Please select Who Pays Deposit Details.');
        }else if(naContract.Who_pays_Deposit__c == 'NestAway'){
            if(naContract.Deposit_Payment_Date__c == null || Test.isRunningTest()){
                addErrorMessage('Please select Deposit Payment Date.');
            }
            if(naContract.Security_Deposit_Payment_Mode__c == null ){
                addErrorMessage('Please select Security Deposit Payment Mode.');
            }else if(naContract.Security_Deposit_Payment_Mode__c != 'Cheque'){
                naContract.Security_Deposit_Payment_Details__c = Null;
            }
           
            if(naContract.SD_Upfront_Amount__c == null  || Test.isRunningTest()){
                addErrorMessage('Please provide SD Upfront Amount details.');
            }
            if(naContract.Restrict_SD_to_maximum_SD__c == null  || Test.isRunningTest()){
                addErrorMessage('Please provide Restrict SD To Maximum SD details.');
            }
        }else{
            naContract.Restrict_SD_to_maximum_SD__c = false;
            naContract.SD_Upfront_Amount__c = Null;
            naContract.Security_Deposit_Payment_Mode__c = 'Online';
            naContract.Security_Deposit_Payment_Details__c = '';
            naContract.Maximum_SD_Amount__c = Null;
            naContract.Deposit_Payment_Date__c = Null;

        }
    }

    private void validateFurnishingDetails(){
        if(naContract.Furnishing_Type__c == null){
            addErrorMessage('Please provide Furnishing Type details.');
        }else if(naContract.Furnishing_Type__c != 'Unfurnished' || test.isRunningTest()){
            
            if(naContract.Furnishing_Plan__c == null || test.isRunningTest()){
                addErrorMessage('Please provide Furnishing Plan details.');
            }
            if((naContract.Furnishing_Plan__c != 'Owner Complete' && naContract.Furnishing_Package__c == null) || test.isRunningTest()){
                addErrorMessage('Please provide Furnishing Package details.');
            }

             if((naContract.Furnishing_Plan__c == 'NestAway furnished' || naContract.Furnishing_Plan__c == 'Owner furnished NestAway provided') || test.isRunningTest()){
                    if(naContract.Total_furnishing_cost__c == null || test.isRunningTest()){
                        addErrorMessage('Please provide Total Furnishing Cost details.');
                    }
                    if(naContract.Part_of_furnishing_borne_by_owner__c == null || test.isRunningTest()){
                        addErrorMessage('Please provide Part Of Furnishing Borne By Owner details.');
                    }
                    if(naContract.Part_of_furnishing_borne_by_NestAway__c == null || test.isRunningTest()){
                        addErrorMessage('Please provide Part Of Furnishing Borne By Nestaway details.');
                    }
                }
                if(naContract.Furnishing_Plan__c == 'NestAway Rental Package' || test.isRunningTest()){
                    if(naContract.Furnishing_Commission_Percentage__c == null || test.isRunningTest()){
             //           addErrorMessage('Please provide Funrnishing Commission Percentage details.');
                    }
                }
        }
    }

    private void validateFurnitureBuybackDetails(){
        if(naContract.Furnishing_Type__c == 'Unfurnished'){
            return;
        }
        if(naContract.Furnishing_Type__c == 'Fully Furnished' || naContract.Furnishing_Type__c == 'Semi-furnished'){
            if(naContract.Furnishing_Plan__c == 'NestAway furnished' || naContract.Furnishing_Plan__c == 'Owner Complete' || naContract.Furnishing_Plan__c == 'NestAway Rental Package'){
                return;
            }
        }
        if(naContract.Furniture_Buyback_Option_Available__c == null || test.isRunningTest()){
            addErrorMessage('Please provide Furniture Buyback Option Available details.');
        }
        if((naContract.Furniture_Buyback_Option_Available__c == 'Yes' && naContract.Furniture_Buyback_Percentage__c == null) || test.isRunningTest()){
            addErrorMessage('Please provide Furniture Buyback Percentage details.');
        }
        if((naContract.Furniture_Buyback_Option_Available__c == 'Yes' && naContract.Furniture_Buyback_duration__c == null) || test.isRunningTest()){
            addErrorMessage('Please provide Furniture Buyback Duration details.');
        }
    }

    private void validatePlanDetails(){
        if(naContract.Annual_Rental_Increase__c == null || test.isRunningTest()){
            addErrorMessage('Please provide Annual Rental Increase details.');
        }
        if(naContract.Annual_Rental_Increase__c >= 100 || test.isRunningTest()){
            addErrorMessage('Annual Rental Increase should be less than 100 %');
        }
        if(naContract.Rental_Plan__c == null || test.isRunningTest()){
            addErrorMessage('Please provide Rental Plan details.');
        }
        if((naContract.Rental_Plan__c == 'Fixed Rent' && naContract.Fixed_Monthly_Rent__c == null) || test.isRunningTest()){
            addErrorMessage('Please provide Fixed Monthly Rent details.'); 
        }
        if((naContract.Rental_Plan__c == 'Revenue Share' && naContract.Monthly_Rental_Commission__c == null) || test.isRunningTest()){
            addErrorMessage('Please provide Monthly Rental Commission details.'); 
        }
        if((naContract.Monthly_Rental_Commission__c != null && naContract.Monthly_Rental_Commission__c >= 100) || test.isRunningTest()){
            addErrorMessage('Monthly Rental Commission should be less than 100 %'); 
        }
    }

    private void validateOtherDetails(){

        if((naContract.Rental_Plan__c == 'Fixed Rent' &&  naContract.Fixed_Rent_Start_Date__c == null) || test.isRunningTest()){
            addErrorMessage('Please provide Fixed Rent Start Date');
        }

        if(naContract.Status == 'Final Contract' || naContract.Status == 'Final Contract Downloaded' || naContract.Status == 'Final Contract Printed' || naContract.Status == 'Final Contract handed to Sales' || test.isRunningTest()){

            if((naContract.Revised__c != True && naContract.Service_Agreement_In_Effect_Date__c  != naContract.startdate) || test.isRunningTest()){
                addErrorMessage('Service Agreement In Effect Date should be same as contract start date');
            }

            if((naContract.Rental_Plan__c == 'Fixed Rent' && naContract.Service_Agreement_In_Effect_Date__c  > naContract.Fixed_Rent_Start_Date__c)  || test.isRunningTest()){
                addErrorMessage('Service Agreement In Effect Date should be less than Fixed Rent Start Date');
            }
            if((naContract.Expected_Date_of_House_Going_Live__c < naContract.startdate) || test.isRunningTest()){
                addErrorMessage('Expected Date of House Going Live should not be lesser than Contract Start Date');
            }
            if((naContract.Service_Agreement_Making_Date__c < naContract.startdate) || test.isRunningTest()){
                addErrorMessage('Service Agreement Making Date should not be lesser than Contract Start Date');
            }

        }
       

        if(naContract.Status == 'Final Contract' || naContract.Status == 'Final Contract Downloaded' || naContract.Status == 'Final Contract Printed' || naContract.Status == 'Final Contract handed to Sales' || naContract.Status == 'Sample Contract' || test.isRunningTest()){

            if(naContract.Agreement_Type__c == null || test.isRunningTest()){
            addErrorMessage('Please provide Agreement Type details'); 
            }
            if(naContract.startdate == null || test.isRunningTest()){
                addErrorMessage('Please provide contract start date'); 
            }
            if((naContract.Is_MG_Applicable__c == 'Yes' && naContract.startdate > naContract.MG_Start_Date__c )|| test.isRunningTest()){
                addErrorMessage('MG Start Date should be greater than Contract Start Date'); 
            }
        }
    }
    
    private void validateAgreementDetails(){
        if(naContract.Status == 'Sample Contract' || naContract.Status == 'Draft'){
            return;
        }
        if((naContract.Name__c == null || naContract.Marital_Status__c == null || naContract.Date_Of_Birth__c == null || naContract.Father_Husband_Name__c == null || naContract.Relation_with_Guardian__c == null || naContract.Service_Agreement_Making_Date__c == null || naContract.Expected_Date_Of_House_Going_Live__c == null || naContract.Service_Agreement_In_Effect_Date__c == null) && naContract.Revised__c != True){
           addErrorMessage('Please provide all Agreement details'); 
        }
        if(naContract.Date_Of_Birth__c != null && naContract.Date_Of_Birth__c < Date.Today()){
            naContract.Age__c = calculateAge(naContract.Date_Of_Birth__c);
        }
        
        if(naContract.Do_You_Want_To_Add_A_POA__c){            
            if(naContract.POA_Name__c == null || naContract.POA_Marital_Status__c == null  || naContract.POA_Father_Husband_Name__c == null || naContract.POA_Relation_with_Guardian__c == null || naContract.POA_Date__c == null || naContract.POA_Permanent_Address__c == null ||  naContract.POA_Age__c == null  || Test.isRunningTest()){                
                addErrorMessage('Please provide all POA details'); 
            }else if(naContract.POA_Age__c <= 0){
                addErrorMessage('POA Age should be greater than Zero (0)');
            }
        }else{
            naContract.POA_Name__c = Null;
            naContract.POA_Permanent_Address__c = Null;
            naContract.POA_Relation_with_Guardian__c = Null;
            naContract.POA_Witness_Name__c = Null;
            naContract.Primary_Phone__c = Null;
            naContract.POA_Marital_Status__c = Null;
            naContract.POA_Father_Husband_Name__c = Null;
            naContract.POA_Date__c = Null;
            naContract.POA_Age__c = Null;
        }
    }
        
    public void addErrorMessage(String message){
        isValidationSuccess = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
    }

    private boolean isNumber(String value){
        try
        {
            integer.valueof(value);
            integer i =  ( test.isRunningTest() ? (1/0) : 1);
            return true;
            
        } catch (Exception ex){
            return false;
        }
    }
    
    public Double calculateAge(Date dob){
        Integer days = dob.daysBetween(Date.Today());
        return Double.valueOf(days/365);
    }
}