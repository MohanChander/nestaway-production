public class BathroomTriggerHandler {

    /*  Created By: Mohan - WarpDrive Tech Works
        Purpose   : When Bathroom is edited it should sync with the webapp system - Make an API call to Web Entity : House 
                    Call to be made only when the House is Live */
    public static void sendWebEntityHouseJson(Map<Id, Bathroom__c> newMap){

        try {
                Set<Id> bathroomIdSet = new Set<Id>();
                Set<Id> houseIdSet = new Set<Id>();

                for(Bathroom__c bathroom: newMap.values()){
                    if(bathroom.House__c != null){
                        bathroomIdSet.add(bathroom.Id);
                    }
                }  

                if(!bathroomIdSet.isEmpty()){
                    List<Bathroom__c> bathroomList = BathroomSelector.getBathroomsWithHouseStatus(bathroomIdSet);

                    for(Bathroom__c bathroom: bathroomList){
                      if(bathroom.House__c != null && bathroom.House__r.Stage__c != Constants.HOUSE_STAGE_HOUSE_DRAFT && bathroom.House__r.Stage__c != Constants.HOUSE_STAGE_ONBOARDING){
                        // commented by chandu to call one time for a house
                       //  House2Json.createJson(bathroom.House__c);
                           houseIdSet.add(bathroom.House__c);
                          
                      }
                    }
                } 


                if(houseIdSet.size()>0){
                    
                    for(Id hId: houseIdSet){
                        
                        House2Json.createJson(hId);
                    }
                }               

            } Catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }         

    }  


    /* Created By: Mohan - WarpDrive Tech Works
        Purpose  : All the Before Insert Actions are under this Method
                 : 1) Associate the Bathroom with the House if Bathroom is created after the House Creation */
    public static void beforeInsert(List<Bathroom__c> bathroomList){

        System.debug('*********************beforeInsert');

        try{
                Set<Id> ricIdSet = new Set<Id>();
                Set<Id> hicIdSet = new Set<Id>();

                for(Bathroom__c bathroom: bathroomList){
                    if(bathroom.Room_Inspection__c != null)
                        ricIdSet.add(bathroom.Room_Inspection__c);
                    else if(bathroom.Checklist__c != null)
                        hicIdSet.add(bathroom.Checklist__c);
                }

                Map<Id, Room_Inspection__c> ricMap = new Map<Id, Room_Inspection__c>(RoomInspectionSelector.getRicWithHouseStatus(ricIdSet));
                Map<Id, House_Inspection_Checklist__c> hicMap = new Map<Id, House_Inspection_Checklist__c>(ChecklistSelector.getHicWithHouseStatus(hicIdSet));

                for(Bathroom__c bathroom: bathroomList){
                    if(bathroom.Room_Inspection__c != null && !ricMap.isEmpty() && ricMap.containsKey(bathroom.Room_Inspection__c) && ricMap.get(bathroom.Room_Inspection__c).House_Inspection_Checklist__r.House__c != null)
                        bathroom.House__c = ricMap.get(bathroom.Room_Inspection__c).House_Inspection_Checklist__r.House__c;
                    if(!ricMap.isEmpty() && ricMap.containsKey(bathroom.Room_Inspection__c) && !ricMap.get(bathroom.Room_Inspection__c).Room_Terms__r.isEmpty())
                        bathroom.Room__c = ricMap.get(bathroom.Room_Inspection__c).Room_Terms__r[0].Id;
                    else if(bathroom.Checklist__c != null && !hicMap.isEmpty() && hicMap.containsKey(bathroom.Checklist__c) && hicMap.get(bathroom.Checklist__c).House__c != null)
                        bathroom.House__c = hicMap.get(bathroom.Checklist__c).House__c;
                }   
            } Catch (Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);                
            }     
    }   
    /**********************************************
    Created By : Mohan
    Purpose    : afterUpdate Handler Method
**********************************************/
    public static void afterUpdate(Map<Id, Bathroom__c> newMap, Map<Id, Bathroom__c> oldMap){

        System.debug('******************afterUpdate');

        try{    
              Id moveInBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
              Id moveOutBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();  
        Id mimoRtId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get('MIMO BIC').getRecordTypeId();
        Map<Id,Bathroom__c> mapofMoveInBathroom = new Map<Id,Bathroom__c>();
                Map<Id,Bathroom__c> mapofMoveOutBathroom = new Map<Id,Bathroom__c>();
                List<Bathroom__c> mimoBicList = new List<Bathroom__c>();

                for(Bathroom__c bic: newMap.values()){

                    //check if the MIMO BIC is updated
                    if(bic.RecordTypeId == mimoRtId){
                        mimoBicList.add(bic);
                    }
                    if(bic.RecordTypeId == moveInBicRTId){
                        mapofMoveInBathroom.put(bic.id,bic);
                    }
                    if(bic.RecordTypeId == moveOutBicRTId){
                        mapofMoveOutBathroom.put(bic.id,bic);
                    }
                }

               /* if(!mimoBicList.isEmpty()){
                    BathroomTriggerHelper.syncMoveInMoveOutChecklistsWithMIMO(mimoBicList);
                }*/
          if(!mapofMoveInBathroom.isEmpty()){
                    BicTriggerHandler.afterUpdateMoveIn(mapofMoveInBathroom,oldmap);
                }
                if(!mapofMoveOutBathroom.isEmpty()){
                    BicTriggerHandler.afterUpdateMoveOut(mapofMoveOutBathroom,oldmap);
                }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'afterUpdate handler Bathroom__c');      
            }   
    }
}