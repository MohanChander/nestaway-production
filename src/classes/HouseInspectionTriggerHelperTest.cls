@IStest
public class HouseInspectionTriggerHelperTest {
    public Static TestMethod void houseTest(){
          
          User newUser = Test_library.createStandardUser(1);
        insert newuser;
           Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
       
        
         Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            opp.accountid=accobj.id;
            insert opp;
       
        
         Test.StartTest();
          House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';        
        hic.Fridge__c='Yes and Working';
        hic.Opportunity__c=opp.id;
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.PAN_Card_Number__c='QWERT2978X';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
        List<House_Inspection_Checklist__c> hicList= new   List<House_Inspection_Checklist__c>();
        hicList.add(hic);
       Map<Id,House_Inspection_Checklist__c> newMap = new   Map<Id,House_Inspection_Checklist__c> ();
       newmap.put(hic.id,hic);
      HouseInspectionTriggerHelper.UpdatePanCardOnAccount(newmap);
        HouseInspectionTriggerHelper.updateMimoHic(HicList);
        HouseInspectionTriggerHelper.updateMimoHicHelper(hic,hic.id);
            Test.stopTest();
    }


}