/***************************************
 * Created By : Mohan
 * Purpose    : WorkOrder Trigger Helper for SnM related functionality
 * ************************************/
public class WorkOrderSnMTriggerHelper {

/****************************************
 * Created By : Mohan
 * Purpose    : Create Case Comment on Work Order Creation or updation
 * **************************************/ 
    public static void generateCaseCommentForVendorWorkOrder(List<WorkOrder> woList, Boolean isInsert){
        
        try{
                Set<Id> userIdSet = new Set<Id>();
                Set<Id> caseIdSet = new Set<Id>();                
                List<Case_Comment__c> ccList = new List<Case_Comment__c>();

                for(WorkOrder wo: woList){
                    userIdSet.add(wo.OwnerId);
                    caseIdSet.add(wo.CaseId);
                }

                Map<Id, User> userMap = new Map<Id, User>([select Id, Name from User where Id =: userIdSet]);
                Map<Id, Case> caseMap = new Map<Id, Case>([select Id, CaseNumber from Case where Id =: caseIdSet]);

                for(WorkOrder wo: woList){
                    Case_Comment__c cc = new Case_Comment__c();
                    String technicianName = userMap.get(wo.OwnerId).Name;
                    Date visitDate = UtilityServiceClass.getDateFromDatetime(wo.StartDate);
                    String visitTime = String.valueOf(wo.StartDate.format('hh:mm:ss a'));
                    Id caseId = wo.CaseId;

                    if(isInsert){
                        cc = CaseCommentGenerator.generateVisitVerifiedComment(technicianName, visitDate, visitTime, caseId);
                    } else {
                        cc = CaseCommentGenerator.generateVisitRescheduledComment(technicianName, visitDate, visitTime, caseId);
                    }
                }                

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Vendor Visit Case Comment Notification');      
            }   
    }    
    
/****************************************
 * Created By : Mohan
 * Purpose    : Validations that will fire before the Vendor can resolve the WO
 *              1) if Material Cost is > 0 then there should be a invoice attached
 * **************************************/ 
    public static void vendorWorkOrderResolutionValidator(List<WorkOrder> woList){

        System.debug('**vendorWorkOrderResolutionValidator');
        
        try{    
                Set<Id> woIdSet = new Set<Id>();
                Map<Id, ContentDocumentLink> contentDocLinkMap = new Map<Id, ContentDocumentLink>();
                Map<Id, Set<Invoice__c>> woWithInvoiceMap = new Map<Id, Set<Invoice__c>>();
                Map<Id, Set<WorkOrder>> woWithInvoiceWoMap = new Map<Id, Set<WorkOrder>>();
                Set<Id> linkedEntityIdSet = new Set<Id>();

                for(WorkOrder wo: woList){
                    woIdSet.add(wo.Id);
                }

                System.debug('**woIdSet: ' + woIdSet + '\n Size of woIdSet: ' + woIdSet.size());

                Map<Id, Invoice__c> invoiceMap = new Map<Id, Invoice__c>([Select Id, Work_Order__c from Invoice__c where RecordType.Name =: Constants.INVOICE_RT_VENDOR_INV and 
                                            Work_Order__c =: woIdSet]);                

                for(Invoice__c inv: invoiceMap.values()){
                    if(woWithInvoiceMap.containsKey(inv.Work_Order__c)){
                        woWithInvoiceMap.get(inv.Work_Order__c).add(inv);
                    } else {
                        woWithInvoiceMap.put(inv.Work_Order__c, new Set<Invoice__c>{inv});
                    }
                    linkedEntityIdSet.add(inv.Id);
                }

                Map<Id, WorkOrder> invoiceWoMap = new Map<Id, WorkOrder>([select Id, Work_Order__c from WorkOrder where RecordType.Name =: WorkOrder_Constants.WORKORDER_RT_INVOICE and 
                                          Work_Order__c =: woIdSet]); 

                for(WorkOrder wo: invoiceWoMap.values()){
                    if(woWithInvoiceWoMap.containsKey(wo.Work_Order__c)){
                        woWithInvoiceWoMap.get(wo.Work_Order__c).add(wo);
                    } else {
                        woWithInvoiceWoMap.put(wo.Work_Order__c, new Set<WorkOrder>{wo});
                    }
                    linkedEntityIdSet.add(wo.Id);
                }
                
                List<ContentDocumentLink> contentDocLinkList = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink 
                                                    WHERE LinkedEntityId in: linkedEntityIdSet]; 

                System.debug('**invoiceMap: ' + invoiceMap + '\n Size of invoiceMap: ' + invoiceMap.size());
                System.debug('**woWithInvoiceMap: ' + woWithInvoiceMap + '\n Size of woWithInvoiceMap: ' + woWithInvoiceMap.size());
                System.debug('**invoiceWoMap: ' + invoiceWoMap + '\n Size of invoiceWoMap: ' + invoiceWoMap.size());
                System.debug('**woWithInvoiceWoMap: ' + woWithInvoiceWoMap + '\n Size of woWithInvoiceWoMap: ' + woWithInvoiceWoMap.size());
                System.debug('**contentDocLinkList: ' + contentDocLinkList + '\n Size of contentDocLinkList: ' + contentDocLinkList.size());
                System.debug('**linkedEntityIdSet: ' + linkedEntityIdSet + '\n Size of linkedEntityIdSet: ' + linkedEntityIdSet.size());                                                    

                for(ContentDocumentLink cdl: contentDocLinkList){
                    contentDocLinkMap.put(cdl.LinkedEntityId, cdl);
                }    

                for(WorkOrder wo: woList){
                    Boolean contanisInvoiceAttachment = false;
                    if(woWithInvoiceMap.containsKey(wo.Id)){
                        for(Invoice__c inv: woWithInvoiceMap.get(wo.Id)){
                            if(contentDocLinkMap.containsKey(inv.Id)){
                                contanisInvoiceAttachment = true;
                            }
                        }
                    }

                    if(woWithInvoiceWoMap.containsKey(wo.Id)){
                        for(WorkOrder invWo: woWithInvoiceWoMap.get(wo.Id)){
                            if(contentDocLinkMap.containsKey(invWo.Id)){
                                contanisInvoiceAttachment = true;
                            }
                        }
                    }

                    if(!contanisInvoiceAttachment){
                        wo.addError('Please upload the Invoice before resolving the Work Order');
                    }
                }                                                                                                                         
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Vendor Work Order Resolution Validation');      

            }   
    }


/****************************************
 * Created By : Mohan
 * Purpose    : Once the WorkOrder is resolved by the Vendor check certain conditions and update the case
 * **************************************/   
    public static void updateCaseOnWorkOrderResolution(List<WorkOrder> woList){
        try{
                Set<Id> caseIdSet = new Set<Id>();
                Set<Id> resolvedCaseIdSet = new Set<Id>();
                List<Case> updatedCaseList = new List<Case>();
                for(WorkOrder wo: woList){
                    caseIdSet.add(wo.CaseId);
                }

                //Query for Cases in which all the work Orders are resolved
                List<Case> caseList = [select Id from Case where Id not in 
                                      (select CaseId from WorkOrder where CaseId =: caseIdSet and isClosed = false)
                                      and Id =: caseIdSet];

                for(Case c: caseList){
                    resolvedCaseIdSet.add(c.Id);
                }    

                if(!resolvedCaseIdSet.isEmpty()){
                    List<AggregateResult> groupedResult = [select Sum(Material_Cost__c) matCost, Sum(Labour_Cost__c) labCost, CaseId cId 
                                                           from WorkOrder
                                                           where CaseId =: resolvedCaseIdSet group by CaseId];
                    
                    for(AggregateResult ar: groupedResult){
                        Case c = new Case();
                        c.Id = (Id)ar.get('cId');
                        c.Material_Cost__c = (Decimal)ar.get('matCost');
                        c.Labour_Cost__c = (Decimal)ar.get('labCost');
                        c.Status = Constants.CASE_STATUS_RESOLVED;
                        updatedCaseList.add(c);
                    }  

                    if(!updatedCaseList.isEmpty()){
                        update updatedCaseList;
                    }                                                         
                }                                  

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Work Order Resolution');      
            }   
    } 

/***************************************************
    Created By : Mohan
    Purpose    : Send an API call when Work Order is created for Vendor
***************************************************/
    @future(callout=true)
    public static void sendVendorDetailsToWebApp(Id woId, Boolean isInsert){

        System.debug('**********************sendVendorDetailsToWebApp');

        try{                 
                WorkOrder wo = [select Id, StartDate, CaseId from WorkOrder where Id =: woId];

                Case c = [select Id, CaseNumber, Preferred_Visit_Time__c from Case where Id =: wo.CaseId]; 

                Contact technicianContact;               

                /*
                WorkOrder queriedWo = [select Id, Onwer.Name, Owner.Contact.Phone, Case.CaseNumber 
                                       from WorkOrder where Id =: wo.Id];  */

                User technician = [select Id, Name, ContactId from User where 
                                   Id in (select OwnerId from WorkOrder where Id =: wo.Id)];

                if(technician.ContactId != null){               
                    technicianContact = [select Phone from Contact where Id =: technician.ContactId];                                       
                }

                List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].SnM_Notification__c +'?auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON vendorDetailsObject = new WBMIMOJSONWrapperClasses.VendorDetailsNotificationJSON();

                /*if Preferred Visit Time and Time Scheduled on the WO send Vendor Notification and if its not the same 
                  rescheduled Notification */                
                if(wo.StartDate == c.Preferred_Visit_Time__c && isInsert){
                    vendorDetailsObject.method = 'send_vendor_details';
                } else {
                    vendorDetailsObject.method = 'reschedule_vendor_visit';                    
                }

                vendorDetailsObject.ticket_number = c.CaseNumber;
                vendorDetailsObject.technician_name = technician.Name;
                if(technicianContact != null){
                    vendorDetailsObject.technician_phone = technicianContact.Phone;
                }                
                vendorDetailsObject.schedule_date_time = wo.StartDate;

                jsonBody = JSON.serialize(vendorDetailsObject);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
        }
        catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Vendor Details API');  
        }                        
    } 
        /*****************************************************************************************************************************************
Added by baibahv
Purpose: to create validation Task
****************************************************************************************************************************************
*/

    public static void CreateTaskToServiceCase(List<workOrder> wrkList)
    {
        try
        {  
            Id genTaskRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_VERIFICATION_TASK).getRecordTypeId();

            System.debug('******Owner*********');
            Set<Id> casidSet= new Set<id>(); 
           for(workOrder wrk : wrkList)
             {
                casidSet.add(wrk.caseID);
             } 
             Map<id,Case> casMap = CaseSelector.getMapCasefromCaseID(casidSet);
            List<task> tskList =new List<task>(); 
            for(workOrder wrk : wrkList)
            {     System.debug('******Owner2*********');
                Task tsk = new Task();
                tsk.whatid=wrk.Caseid;
                tsk.RecordTypeId=genTaskRT;
                tsk.subject='Verification task for WorkOrder :'+wrk.WorkOrderNumber;
                tsk.status='Open';
                tsk.Work_Order__c=wrk.id;
                tsk.OwnerID=casMap.get(wrk.caseId).OwnerId;
                tsk.ActivityDate=System.Today().addDays(1);
                tskList.add(tsk);
            }
            if(!tskList.isEmpty())
             { System.debug('******Owner2*********');
              insert tskList;
             }
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);             
        }
    } 
        /*****************************************************************************************************************************************
Added by baibahv
Purpose: to create Invoice
****************************************************************************************************************************************
*/
public static void createInvoice(List<workorder> wrklist)
{ 
    System.debug('qwertyuiop');
    
    Id vendorInvRt= Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(Constants.INVOICE_RT_VENDOR_INV).getRecordTypeId();
    list<Invoice__c> invlist = new list<Invoice__c>();
    System.debug('qwertyuiop');
    Set<id> usSet=new Set<id>(); 
    for(workorder wrk:wrklist)
    {
        usSet.add(wrk.ownerId);
    }

    Map<id,user> urMap=new Map<id,user>();

    if(!usSet.isEmpty())
    urMap=new Map<id,user>([Select id,Accountid from user where id=:usSet]);

    for(workorder wrk:wrklist)
    {
        Invoice__c inv=new Invoice__c();
        inv.Work_Order__c=wrk.id;
        inv.Amount__c=wrk.Material_Cost__c;
        inv.Vendor__c=urMap.get(wrk.ownerId).Accountid;
        inv.recordTypeId=vendorInvRt;
        invlist.add(inv);
    System.debug('qwertyuiop1234');

    } 
    insert invlist;
}

/****************************************
 * Created By : Mohan
 * Purpose    : Validate the Status Changes for Vendor Work Order
 * **************************************/ 
    public static void vendorWorkOrderStatusValidator(List<WorkOrder> woList, Map<Id, WorkOrder> oldMap){
        
        try{                    
                Set<Id> caseIdSet = new Set<Id>();

                for(WorkOrder wo: woList){
                    caseIdSet.add(wo.CaseId);
                }

                Map<Id, Case> caseMap = new Map<Id, Case>([select Id, Created_Automatically_during_MiMo__c from Case where Id =: caseIdSet]);

                for(WorkOrder wo: woList){
                    WorkOrder oldWo = oldMap.get(wo.Id);

                    if(wo.Status == Constants.WORKORDER_STATUS_WORK_IN_PROGRESS && oldWo.Status != Constants.WORK_STATUS_OPEN){
                        wo.addError('Please enter the OTP to change the Work Order Status to \"Work in Progress\"');
                    } else if(wo.Status == Constants.WORKORDER_STATUS_WORK_IN_PROGRESS && Wo.Enter_OTP__c == null){

                        /*  if the Case is Automatically created during MIMO - no need to send OTP to Tenant
                            Hence the validation to send the OTP to Tenant is disabled */
                        if(caseMap.containsKey(wo.CaseId) && caseMap.get(wo.CaseId).Created_Automatically_during_MiMo__c){

                        } else {
                            wo.addError('Please enter the OTP before you start the Work'); 
                        }
                                                                           
                    } else if(wo.Status == Constants.WORKORDER_STATUS_WORK_DONE && oldWo.Status != Constants.WORKORDER_STATUS_WORK_IN_PROGRESS){
                        wo.addError('You can change the Work Order Status to \"Work Done\" only from \"Work in Progress\" Status');
                    } else if(wo.Status == Constants.WORKORDER_STATUS_RESOLVED && oldWo.Status != Constants.WORKORDER_STATUS_WORK_DONE){
                        wo.addError('You can change the Work Order Status to \"Resolved\" only from \"Work Done\" Status');
                    } else if(wo.Status == Constants.WORKORDER_STATUS_PRECHECK_DONE && oldWo.Status != Constants.WORKORDER_STATUS_WORK_IN_PROGRESS){
                        wo.addError('You can change the Work Order Status to \"Precheck Done\" only from \"Work in Progress\" Status'); 
                    } else if(wo.Status == Constants.WORKORDER_STATUS_DROPPED && oldWo.Status != Constants.WORK_STATUS_OPEN){
                        wo.addError('You cannot change the Work Order Status to \"Dropped\"'); 
                    } else if(wo.Status == Constants.WORKORDER_STATUS_REJECTED_BY_TENANT && 
                        oldWo.Status != Constants.WORK_STATUS_OPEN && oldWo.Status != Constants.WORKORDER_STATUS_WORK_IN_PROGRESS){
                        System.debug('**new Wo Status ' + wo.Status);
                        System.debug('**old Wo Status ' + oldWo.Status);
                        wo.addError('You can change the Work Order Status to \"Rejected by Tenant\" only from \"Open\" or \"Work in Progress\"');  
                    } else if(wo.Status == null && oldWo.Status != null){
                        wo.addError('You cannot change the Work Order Status to \"--None--\"');  
                    } else if(wo.Status == Constants.WORK_STATUS_OPEN && oldWo.Status != null){
                        wo.addError('You cannot change the Work Order Status to \"Open\"');  
                    }                                                                                                                                                           
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Vendor Work Order Status Validator');      
            }   
    }  
  
/*****************************************************************************************************************************************
Added by baibahv
Purpose: to Update Invoice
****************************************************************************************************************************************
*/
    public static void UpdateInvoice(Map<id,workorder> wrkMap)
    {
        try
        {       
            Id vendorInvRt= Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(Constants.INVOICE_RT_VENDOR_INV).getRecordTypeId();
            list<Invoice__c> invlist = [Select id,Amount__c,Work_Order__c from Invoice__c where Work_Order__c=:wrkMap.keySet()];
            list<Invoice__c> updateInvlist = new list<Invoice__c>();
            for(Invoice__c inv:invlist)
            {
                inv.Amount__c=wrkMap.get(inv.Work_Order__c).Material_Cost__c;
                updateInvlist.add(inv);     
            }
            update updateInvlist;
        } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Update Invoice');
                }
}

/***************************
Created By : Mohan
Purpose    : Create Invoice Work Order when the Vendor enters the Amount on the Work Order
****************************/
    public static void createInvoiceWorkOrder(Map<Id, WorkOrder> woMap){
        try {   
                Id invoiceWoRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_INVOICE).getRecordTypeId();    
                
                Map<Id, WorkOrder> invoiceWoMap = new Map<Id, WorkOrder>();
                Set<Id> userIdSet = new Set<Id>();
                List<WorkOrder> woList = new List<WorkOrder>();

                List<WorkOrder> invoiceWoList = [Select Id, Amount__c, Work_Order__c from WorkOrder 
                                                 where RecordType.Name =: WorkOrder_Constants.WORKORDER_RT_INVOICE and 
                                                 Work_Order__c =: woMap.keySet()];

                for(WorkOrder wo: woMap.values()){
                    userIdSet.add(wo.OwnerId);
                } 

                Map<Id, User> userMap = new Map<Id, User>([select Id, AccountId from User where Id =: userIdSet]);                                                
                
                for(WorkOrder invWo: invoiceWoList){
                    invoiceWoMap.put(invWo.Work_Order__c, invWo);
                }   

                for(WorkOrder wo: woMap.values()){
                    if(invoiceWoMap.containsKey(wo.Id)){
                        invoiceWoMap.get(wo.Id).Amount__c = wo.Material_Cost__c;
                        woList.add(invoiceWoMap.get(wo.Id));
                    } else {
                        WorkOrder invWo = new WorkOrder();
                        invWo.Work_Order__c = wo.Id;
                        invWo.Amount__c = wo.Material_Cost__c;
                        invWo.RecordTypeId = invoiceWoRtId;
                        if(userMap.containsKey(wo.OwnerId) && userMap.get(wo.OwnerId).AccountId != null){
                            invWo.AccountId = userMap.get(wo.OwnerId).AccountId;
                        }
                        invWo.WorkTypeId = [select Id from WorkType where Name = 'Invoice'].Id;
                        woList.add(invWo);
                    }
                }  

                if(!woList.isEmpty()){
                    upsert woList;
                }                                            

        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'FSL Module - Invoice Work Order Creation');
        }
    }

/***************************
Created By : Mohan
Purpose    : Update the Service Appointment Status based on the Work Order Status
****************************/
    public static void updateServiceAppointmentStatus(Map<Id, WorkOrder> woMap){

        System.debug('**updateServiceAppointmentStatus');
        
        try {   
                Map<Id, ServiceAppointment> saMap = new Map<Id, ServiceAppointment>();
                List<ServiceAppointment> saList = new List<ServiceAppointment>();

                List<ServiceAppointment> queriedSaList = [SELECT Id, Status, ParentRecordId FROM ServiceAppointment WHERE
                                                   ParentRecordId =: woMap.keySet()];

                System.debug('**queriedSaList: ' + queriedSaList + '\n Size of queriedSaList: ' + queriedSaList.size());

                for(ServiceAppointment sa: queriedSaList){
                    saMap.put(sa.ParentRecordId, sa);
                } 

                for(WorkOrder wo: woMap.values()){
                    if((Wo.Status == WorkOrder_Constants.WORKORDER_STATUS_PRECHECK_DONE || 
                        wo.Status == WorkOrder_Constants.WORKORDER_STATUS_WORK_DONE) && saMap.containsKey(wo.Id)){
                            saMap.get(wo.Id).Status = ServiceAppointmentConstants.STATUS_COMPLETED;
                            saList.add(saMap.get(wo.Id));
                    } else if(wo.Status == WorkOrder_Constants.WORKORDER_STATUS_WORK_IN_PROGRESS && saMap.containsKey(wo.Id)){
                            saMap.get(wo.Id).Status = ServiceAppointmentConstants.STATUS_IN_PROGRESS;
                            saList.add(saMap.get(wo.Id));
                    } else if(wo.Status == WorkOrder_Constants.WORKORDER_STATUS_DROPPED && 
                              saMap.containsKey(wo.Id)){
                            saMap.get(wo.Id).Status = ServiceAppointmentConstants.STATUS_CANCELED;
                            saList.add(saMap.get(wo.Id));
                    } else if(wo.Status == WorkOrder_Constants.WORKORDER_STATUS_REJECTED_BY_TENANT && 
                              saMap.containsKey(wo.Id)){
                            saMap.get(wo.Id).Status = ServiceAppointmentConstants.STATUS_CANNOT_COMPLETE;
                            saList.add(saMap.get(wo.Id));
                    }                    
                } 

                System.debug('**saList ' + saList);

                if(!saList.isEmpty()){
                    update saList;
                }                                 
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'FSL Module - Invoice Work Order Creation');
        }
    }    
          
}