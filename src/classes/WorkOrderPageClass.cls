public class WorkOrderPageClass {
    Public List<WorkOrder> workoderList{get;set;}
    public boolean hic{get;set;}
    public boolean ric{get;set;}
    public boolean bic{get;set;}
    public WorkOrderPageClass (ApexPages.StandardController sController) {
        hic= false;
        ric = false;
        bic = false;
        Integer count1=0;
        Integer count2=0;
        Integer count3=0;
       // Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK).getRecordTypeId();
        //Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK).getRecordTypeId();
        workoderList=[select ID,Case_Status__c,(select id,name,CheckList_Verified__c,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c  from Checklist__r ),
                      (select id,name,CheckList_Verified__c,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c  from Bathrooms__r),
                      (select id,name,CheckList_Verified__c ,Maintenance_Verified__c,Furnishing_Verified__c,Verification_Done__c from Room_Inspection__r)  
                      from WorkOrder WHERE id =: ApexPages.currentPage().getParameters().get('id')];
      //  system.debug('Case_Status__c'+workoderList[0].Case_Status__c);
        system.debug('workoderList'+workoderList);
    
        for(workorder wd:workoderList){
            for(House_Inspection_Checklist__c hc:wd.Checklist__r){
                count1++;
                }
            }
        for(workorder wd:workoderList){
            for(Room_Inspection__c  hc:wd.Room_Inspection__r){
                 count2++;                }
            }
      
        for(workorder wd:workoderList){
            for(Bathroom__c  hc:wd.Bathrooms__r){
                 count3++;
                }
            }
        if(count1 >0){
            hic=true;
        }
         if(count2 >0){
            ric=true;
        }
         if(count3 >0){
           bic=true; 
        }
        }
   
}