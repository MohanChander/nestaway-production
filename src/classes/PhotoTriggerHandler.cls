public class PhotoTriggerHandler {
    
    // added by chandu
    
    public static void beforeUpdate(Map<Id, Photo__c> newMap,Map<Id, Photo__c> oldMap){
        
        for(Photo__c photo: newMap.values()){
            
             if(photo.Image_Url__c!=null && photo.Image_Url__c!=oldMap.get(photo.id).Image_Url__c){
                 
                 photo.Status__c='Resubmitted';
                 photo.Approval_Status__c=null;
                
             }
             
            
              if(photo.Approval_Status__c!=null && photo.Approval_Status__c!=oldMap.get(photo.id).Approval_Status__c){
               
                  
                     if(photo.Approval_Status__c=='Approved'){
                         
                         photo.Status__c='Approved';
                     }
                     else if(photo.Approval_Status__c=='Rejected'){
                          photo.Status__c='Rejected';
                     }
                     else{
                         
                         
                     }
              }
            
        }
        
    }
    
    
    public static void afterInsert(Map<Id, Photo__c> newMap) {
        
        
        Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        
        System.debug('****DocumentRecordTypeID '+DocumentRecordTypeID);
        set<id> tenantIds = new set<id> ();
        list<photo__c> documentsToUpdate = new list <photo__c>();
        
         list<photo__c> photolistFromNewMap = new list<photo__c>([Select id,Status__c,Tenant__c,Document_Submission_Date__c,
                                                                 Approval_Status__c,RecordTypeId,House__c,Is_Document__c,
                                                                 Is_Current_Document__c
                                                                 from photo__c
                                                                 where id in :newMap.keyset() ]);
                                                                 
        for(photo__c each : photolistFromNewMap) {
            if(each.RecordTypeId == DocumentRecordTypeID) {
                if(each.Document_Submission_Date__c == null)
                    each.Document_Submission_Date__c = date.today();
                each.Status__c = 'Submitted';
                each.Is_Document__c = true ;
                each.Is_Current_Document__c = true ;
                documentsToUpdate.add(each);  
                tenantIds.add(each.Tenant__c);
            }
        }
        if(documentsToUpdate != null && documentsToUpdate.size() > 0)
            update documentsToUpdate;
        
     
        updateDocuments(newMap,null);
        
    }
    
    /*  Created By: Mohan - WarpDrive Tech Works
Purpose   : When Bathroom is edited it should sync with the webapp system - Make an API call to Web Entity : House 
Call to be made only when the House is Live */
    public static void sendWebEntityHouseJson(List<Photo__c> photoList){
        
        try {
            Set<Id> houseIdSet = new Set<Id>();
            
            for(Photo__c photo: photoList){
                if(photo.House__c != null){
                    houseIdSet.add(photo.House__c);
                }
            }  
            
            if(!houseIdSet.isEmpty())
                HouseJsonOptimizer.doCallout(houseIdSet);                     
            
        } Catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }         
        
    }  
    
    public static void updateDocuments(Map<Id, Photo__c> newMap,Map<Id, Photo__c> OldMap) { 

        Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        
        System.debug('****DocumentRecordTypeID '+DocumentRecordTypeID);
        set<id> tenantIds = new set<id> ();


        for(photo__c each : newMap.values()) {
            if(each.RecordTypeId == DocumentRecordTypeID && (Trigger.isInsert || (Trigger.isUpdate && each.Status__c!=OldMap.get(each.id).Status__c))) {
              if(each.Tenant__c!=null){
                   tenantIds.add(each.Tenant__c);
              } 
               
            }
        }
        
       if(tenantIds.size()>0){
        map<id,account> accountMapFromDocuments = new map<id,account>([Select id,Documents_Complete__c,Documents_Verified__c,
                                                                       recordtypeId,Employment_Type__c
                                                                       from account where id in : tenantIds
                                                                      ]);
                                                                      
     
        
        List<photo__c> documentList = [Select id,Status__c,Approval_Status__c,Document_Type__c,Document_Is__c,
                                        Tenant__c, Is_Document__c 
                                       from photo__c
                                       where Tenant__c in : accountMapFromDocuments.keySet() AND recordtypeId=:DocumentRecordTypeID];
                                     
                                       
        System.debug('***documentList  -'+documentList);                               
        
        map<id,list<photo__c>> accAndDocumentmap = new map <id,list<photo__c>>() ;
        
        
        if(documentList.size() > 0) {
            for(photo__c each : documentList) {
                if(accAndDocumentmap.containsKey(each.Tenant__c)) {
                    accAndDocumentmap.get(each.Tenant__c).add(each);
                }
                else {
                    list <photo__c> tempDocslist = new list <photo__c>();
                    tempDocslist.add(each);
                    accAndDocumentmap.put(each.Tenant__c,(tempDocslist));
                }
            }
        }
           system.debug('accAndDocumentmap'+accAndDocumentmap);

       Set<Id> tennatIdForReDocAutoApproval = new Set<Id>();

        for(account each : accountMapFromDocuments.values()) {
            
            if(accAndDocumentmap.containsKey(each.id)) {
                if(accAndDocumentmap.get(each.id).size() < 2) {                   
                    each.Documents_Verified__c = false;
                    each.Documents_Complete__c = false;
                }
                else {
                    // For now updating this logic only.
                    //Commenting th i am employed condition
                 //   if(each.Employment_Type__c  != null  &&  each.Employment_Type__c == 'I am employed') {
                        integer noOfDocsPresent = 0;
                        integer documentVerified =0; 
                                            
                        for(photo__c docs : accAndDocumentmap.get(each.id)){
                            
                            if(docs.Document_Type__c == 'Address Proof'){
                                if(docs.Status__c!='Rejected'){
                                    noOfDocsPresent +=1;
                                }
                                if(docs.Status__c=='Approved' || docs.Status__c=='Auto Approved'){
                                    documentVerified+=1;
                                }
                                
                                if(newMap.containsKey(docs.id) && newMap.get(docs.id).Status__c=='Resubmitted' && OldMap.get(docs.id).Status__c!='Resubmitted'){
                                    
                                    tennatIdForReDocAutoApproval.add(each.id);
                                }
                                
                            }
                            else if(docs.Document_Type__c == 'Photo Identification Proof') {
                                 
                                if(docs.Status__c!='Rejected'){
                                    noOfDocsPresent +=1;
                                }
                                if(docs.Status__c=='Approved' || docs.Status__c=='Auto Approved'){
                                    documentVerified+=1;
                                }
                                if(newMap.containsKey(docs.id) && newMap.get(docs.id).Status__c=='Resubmitted' && OldMap.get(docs.id).Status__c!='Resubmitted'){
                                    
                                    tennatIdForReDocAutoApproval.add(each.id);
                                }
                               
                            }
                            else if(docs.Document_Type__c == 'Account Photograph') {
                                   if(docs.Status__c!='Rejected'){
                                       // noOfDocsPresent +=1;
                                    }
                                    if(newMap.containsKey(docs.id) && newMap.get(docs.id).Status__c=='Resubmitted' && OldMap.get(docs.id).Status__c!='Resubmitted'){
                                    
                                       tennatIdForReDocAutoApproval.add(each.id);
                                    }
                                    
                            }                           
                            /*
                            else if(docs.Document_Type__c == 'Employment Proof') {
                                noOfDocsPresent +=1;
                            }*/
                            else{
                                
                            }
                            
                           
                          
                        }
                        if(noOfDocsPresent < 2) {                            
                            each.Documents_Complete__c = false;
                            
                        }
                        else if(noOfDocsPresent >= 2){
                            each.Documents_Complete__c = true;                         
                            
                        }
                        if(documentVerified >= 2) {                            
                            each.Documents_Verified__c = true;
                            
                        }
                        else {
                            each.Documents_Verified__c = false;                         
                            
                        }
                 
                    
                }
                
            } 
            else {
                each.Documents_Complete__c = false;
                each.Documents_Verified__c = false;
                
            }           
        }    
        update accountMapFromDocuments.values();
        
      // chandu: adding code to change the flag for auto doc approval time  
        
        if(tennatIdForReDocAutoApproval.size()>0){
                Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
                 map<id,case> casesMapWhereReDocApproveFire = new map <id,case>([Select id,Stage__c,Documents_Uploaded__c,Document_Reuploaded__c
                                                        from case where recordTypeId =: MoveIn_RecordTypeId  
                                                        AND Tenant__c IN : tennatIdForReDocAutoApproval and isClosed=false and stage__c='Document Verification']);
                List<case> updateCase = new List<case>();                                       
                 for(case each : casesMapWhereReDocApproveFire.values() ) {
                    
                        // change the flag to fire the workflow rule again.
                        if(each.Document_Reuploaded__c==true){
                            each.Document_Reuploaded__c=false;
                        }
                        else if(each.Document_Reuploaded__c==false){
                            each.Document_Reuploaded__c=true;
                        }
                     updateCase.add(each);
                }   

                if(updateCase.size()>0){

                    update updateCase;
                }   
            
        }
        
        
        
       }    
    }
}