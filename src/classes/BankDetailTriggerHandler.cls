public class BankDetailTriggerHandler 
{
  public static void beforeInsert(List<Bank_Detail__c> newlist)
  { 
     //chandu:commenting as we dont need to move this code now. 
     // BankDetailTriggerHelper.bankName(newlist);     
  }
  public static void beforeUpdate(Map<id,Bank_Detail__c> newMap,Map<id,Bank_Detail__c> oldMap)
  {
      
    //chandu:commenting as we dont need to move this code now.      
       /*     
    List<Bank_Detail__c> bkList = new List<Bank_Detail__c>(); 
    for(Bank_Detail__c bk:newMap.values())
    {
      if(bk.Account_Firstname__c!=oldMap.get(bk.id).Account_Firstname__c || bk.Bank_Name__c!=oldMap.get(bk.id).Bank_Name__c || bk.Account_Number__c!=oldMap.get(bk.id).Account_Number__c )
      {
              bkList.add(bk);
      }
    }
      BankDetailTriggerHelper.bankName(bkList);
    */        
  }
  
  // added by chandu to remove the process builder and move the change here
  
  public static void updateAccountForBankDetail(List<Bank_Detail__c> allBankLst){
  
    List<account> updateAccountLst= new List<account>();
    
    for(Bank_Detail__c bk: allBankLst){
    
       if(bk.Related_Account__c!=null){
         account acc = new account();
         acc.id=bk.Related_Account__c;
         acc.isBankDetails__c=true;
         updateAccountLst.add(acc);
        } 
    }
    
    if(updateAccountLst.size()>0){
       update updateAccountLst;
    }
  
  }
  
  
  // added by chandu : to sync bank details with  webapp
    
     public static void syncBank(Map<Id,Bank_Detail__c> newMap){
         
         if(newMap.keySet().size()>0){
             
             syncBanks(newMap.keySet());
         }
     }
    
    @future(callout=true)
     public static void syncBanks(set<Id> bankIds){
         
         for(Id bankid: bankIds){
             SendAPIRequests.bankSync(bankid);
         }
          
     }
    /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: bank detail after update 
 ********************************************************************************************************************************************************************************************************************************************/  

     public static void afterUpdate(map<id,Bank_Detail__c> newMap,map<id,Bank_Detail__c> oldMap)
     {
      list<Bank_Detail__c> bnkList = new list<Bank_Detail__c>(); 
      
       // Chandu : adding code to update account to fix profile issue.
      List<Account> accList = new list<Account>();
      
      for(Bank_Detail__c bd:newMap.values())
      {
        if(bd!=oldmap.get(bd.id))
        {
          bnkList.add(bd);
        }
        if(bd.Related_Account__c!=null && bd.Profile_Verified__c!=null && bd.Profile_Verified__c==false){
            
            accList.add(new account(id=bd.Related_Account__c));
        } 
      }
      if(!bnkList.isEmpty())
      {  
        System.debug('bony-sd-1');
        BankDetailTriggerHelper.updateSDPaymentBank(bnkList);
      }
      
       if(accList.size()>0){
          
           try{
                update accList;
           }
           catch(exception e){
                system.debug('***Error while updating the account details:-'+e.getMessage()+' at line :'+e.getLineNumber());
           }
          
      }

     }
  
}