public class BankDetailTriggerHelper 
{
   // Chandu: Commenting for SD payment change only
   /*
    //Added by baibhav
    // Purpose: to populate Bank name
    public static void bankName(List<Bank_Detail__c> newlist)
    {
        for(Bank_Detail__c bk:newlist)
        {   
            if(bk.Account_Firstname__c!=null)
            {
             bk.name=bk.Account_Firstname__c;
            }
                      
            if(bk.Bank_Name__c!=null)
            {
             bk.name+='-'+bk.Bank_Name__c;
            }
            if(bk.Account_Number__c!=null)
            {
               bk.name+='-'+bk.Account_Number__c.left(4);
            }
        } 
       
      
    }
*/
 /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: bank detail  update on Sd payment 
 ********************************************************************************************************************************************************************************************************************************************/  

    public static void updateSDPaymentBank(List<Bank_Detail__c> bnkList)
       {
        try
        {
            System.debug('bony-sd');
            set<id> bnkIdSet=new set<id>();
            list<workOrder> wrkList=new list<workOrder>();
             
            List<Workorder> updateWrklIst=new List<Workorder>();
            for(Bank_Detail__c bnk:bnkList)
            {
                bnkIdSet.add(bnk.id);
            }
            System.debug('bony-sd-bnk'+bnkIdSet);
            if(!bnkIdSet.isEmpty())
            {
              wrkList=WorkOrderSelector.getSdPaymentWorkOrdersBybnkid(bnkIdSet);
            }
            System.debug('bony-sd-wrk'+wrkList);
           
          
              for(Bank_Detail__c bnk:bnkList)
              {
                if(!wrkList.isEmpty()){
                    for(Workorder wk:wrkList){

                       if(wk.Bank_Detail__c==bnk.id){
                            if(bnk.Account_Number__c!=null)
                            {
                                wk.Account_Number__c=bnk.Account_Number__c;
                            } 
                            if(bnk.Bank_Name__c!=null)
                             {
                                wk.Bank_Name__c=bnk.Bank_Name__c;
                             }
                            if(bnk.IFSC_Code__c!=null)
                             {
                                wk.IFSC_Code__c=bnk.IFSC_Code__c;
                             } 
                            if(bnk.Branch_Name__c!=null)
                             {
                                wk.Branch_Name__c=bnk.Branch_Name__c;
                             } 
                            updateWrklIst.add(wk);
                        }
                    }
                }
                
              }  
            
            if(!updateWrklIst.isEmpty())
            {
                update updateWrklIst;
            }
        }catch(Exception e) {
            System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                         '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + 
                          e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e, 'SD payment'); 
        }
    }
}