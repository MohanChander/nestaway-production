/******************************************
    Created By : Mohan
    Purpose    : Extension for GRN Page
*******************************************/    

public class GenerateGRNPageExtension {

    public Boolean isValidationPassed {get; set;}
    public Order ord {get; set;}
    public List<OrderItem> orderItemList {get; set;}
    public List<Order_Item__c> itemList {get; set;}
    public List<ProductLineItemWrapper> prodWrapperList {get; set;}
    public List<ServiceLineItemWrapper> serviceWrapperList {get; set;}
    Id generalInvoiceRtId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(Constants.INVOICE_RECORD_TYPE_GENERAL_INVOICE).getRecordTypeId();

    public GenerateGRNPageExtension(ApexPages.StandardController stdController) {
        Order orderObj = (Order)stdController.getRecord();
        isValidationPassed = true;

        ord = [select Id, House_PO__c, Vendor__c, OrderNumber, EffectiveDate, Approval_Status__c, 
               Bill_To__c, OwnerId from Order where Id =: orderObj.id];
        System.debug('************order obj ' + ord);

        orderItemList = [select Id, Pricebookentry.Product2.Name, Received_Quantity__c, Quantity, Remaining_Quantity__c, 
                         Pricebookentry.Product2Id, Order.OrderNumber, Order.Vendor__c, Order.House__c, UnitPrice from OrderItem 
                         where OrderId =: ord.Id];

        itemList = [select Id, Item_Name__c, Amount__c, Type__c, Invoice_Generated__c, Ordered_Quantity__c,
                    Received_Quantity__c, Remaining_Quantity__c, Unit_Price__c
                    from Order_Item__c where PO_Number__c =: ord.Id];

        //If the PO is not Approved - Throw an error
        if(ord.Approval_Status__c != 'Approved'){
            isValidationPassed = false;
            String addErrorMessage='Please get the PO Approved before Generating GRN';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
            return;                 
        }                                                        

        prodWrapperList = new List<ProductLineItemWrapper>();
        for(OrderItem oi: orderItemList){
            if(oi.Remaining_Quantity__c > 0){
                ProductLineItemWrapper prodWrapper = new ProductLineItemWrapper();
                prodWrapper.productName = oi.Pricebookentry.Product2.Name;
                prodWrapper.orderedQty = Integer.valueOf(oi.Quantity);
                prodWrapper.remainingQty = Integer.valueOf(oi.Remaining_Quantity__c);
                prodWrapper.ordProduct = oi;
                prodWrapperList.add(prodWrapper);
            }
        }

        for(Order_Item__c oi: itemList){
            if(oi.Type__c == 'Product' && oi.Remaining_Quantity__c > 0){
                ProductLineItemWrapper prodWrapper = new ProductLineItemWrapper();
                prodWrapper.productName = oi.Item_Name__c;
                prodWrapper.orderedQty = Integer.valueOf(oi.Ordered_Quantity__c);
                prodWrapper.remainingQty = Integer.valueOf(oi.Remaining_Quantity__c);
                prodWrapper.ordItem = oi;
                prodWrapperList.add(prodWrapper);                
            }
        }

        serviceWrapperList = new List<ServiceLineItemWrapper>();
            for(Order_Item__c oi: itemList){
                if(oi.Type__c == 'Service' && !oi.Invoice_Generated__c){
                    ServiceLineItemWrapper serviceWrapper = new ServiceLineItemWrapper();
                    serviceWrapper.ordItem = oi;
                    serviceWrapper.serviceName = oi.Item_Name__c;
                    serviceWrapperList.add(serviceWrapper);              
                }
            }
    }

    //method to create Received Products - For Products
    public PageReference createReceivedProducts(){

        List<Received_Product__c> rpList = new List<Received_Product__c>();
        List<Asset> assetList = new List<Asset>(); 

        Boolean generateInvoice = false;

        PageReference pgRef = new PageReference('/' + ord.Id);
        pgRef.setRedirect(true);        

        for(ProductLineItemWrapper prod: prodWrapperList){

            if(prod.receivedQty > prod.remainingQty){
                prod.hasError = true;
                prod.errorMsg = 'Received Quantity cannot be greater than Remaining Quantity';
                return null;
            }

            if(prod.receivedQty != null && prod.receivedQty > 0){
                generateInvoice = true;
            }
        }     

        if(!generateInvoice){
            String addErrorMessage='Please select the products received to generate GRN';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
            return null;              
        }         

        if(!generateInvoice){
            return pgRef;
        }      

        //create Invoice record
        Invoice__c inv = new Invoice__c();
        inv.Order__c = ord.Id;
        inv.Vendor__c = ord.Vendor__c;
        inv.House__c = ord.House_PO__c;
        inv.Invoice_Generation_Date__c = System.today();
        inv.Category__c = 'On-boarding';
        inv.Billed_To__c = ord.Bill_To__c;
        inv.Payment_Type__c = 'Postpaid';
        inv.PO_Owner__c = ord.OwnerId;
        inv.RecordTypeId = generalInvoiceRtId;
        inv.Description__c = 'Invoice generated for Purchase Order ' + ord.OrderNumber;
        insert inv;

        //create GRN record
        GRN__c grn = new GRN__c();
        grn.Order__c = ord.Id;
        grn.Invoice__c = inv.Id;
        grn.House__c = ord.House_PO__c;
        grn.Received_Date__c = System.now();
        insert grn;

        for(ProductLineItemWrapper prod: prodWrapperList){

            if(prod.receivedQty != null && prod.receivedQty > 0){
                Received_Product__c rp = new Received_Product__c();

                if(prod.OrdProduct != null){
                    rp.Product__c = prod.OrdProduct.Pricebookentry.Product2Id;
                    rp.Order_Product__c = prod.ordProduct.Id;
                    rp.Amount__c = prod.OrdProduct.UnitPrice * prod.receivedQty;
                } else if(prod.ordItem != null){
                    rp.Order_Item__c = prod.ordItem.Id;

                    if(prod.ordItem.Unit_Price__c == 0 || prod.ordItem.Unit_Price__c == null){
                        rp.Amount__c = 0;
                    } else{
                        rp.Amount__c = prod.ordItem.Unit_Price__c * prod.receivedQty;
                    }
                }

                rp.Item_Name__c = prod.productName;
                rp.GRN__c = grn.Id;
                rp.Invoice__c = inv.Id;
                rp.Ordered_Quantity__c = prod.orderedQty;
                rp.Received_Quantity__c = prod.receivedQty;
                rp.Remaining_Quantity__c = prod.remainingQty;
                rpList.add(rp);

                for(Integer n=1;n<=prod.receivedQty;n++){
                                Asset a = new Asset();
                
                                if(prod.OrdProduct != null){
                                    a.Product2Id = prod.OrdProduct.Pricebookentry.Product2Id;
                                    a.Price = prod.OrdProduct.UnitPrice;
                                } 
                
                                a.Name = prod.productName;
                                a.Status = 'Shipped';
                                a.PO_Number__c = ord.Id;
                                a.InstallDate = System.today();
                                a.PurchaseDate = ord.EffectiveDate;
                                a.House__c = ord.House_PO__c;
                                assetList.add(a);
                            }
            }
        }

        if(!rpList.isEmpty()){
            insert rpList;
        }

        if(!assetList.isEmpty()){
            insert assetList;
        }   

        return pgRef;
    }

    //Generate Invoice for Services on Click of Generate Invoice Button
    public PageReference onClickGenerateInvoice(){

        List<Received_Product__c> rpList = new List<Received_Product__c>();
        List<Order_Item__c> oiList = new List<Order_Item__c>();

        Boolean generateInvoice = false; 

        PageReference pgRef = new PageReference('/' + ord.Id);
        pgRef.setRedirect(true); 

        for(ServiceLineItemWrapper service: serviceWrapperList){
            if(service.isDone){
                generateInvoice = true;
            }
        }   

        if(!generateInvoice){
            String addErrorMessage='Please select Services to generate Invoice';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
            return null;  
        }

        //create Invoice record
        Invoice__c inv = new Invoice__c();
        inv.Order__c = ord.Id;
        inv.Vendor__c = ord.Vendor__c;
        inv.House__c = ord.House_PO__c;
        inv.Invoice_Generation_Date__c = System.today();
        inv.Category__c = 'On-boarding';
        inv.Billed_To__c = ord.Bill_To__c;
        inv.Payment_Type__c = 'Postpaid';
        inv.RecordTypeId = generalInvoiceRtId;
        inv.Description__c = 'Invoice generated for Purchase Order ' + ord.OrderNumber;
        insert inv;

        for(ServiceLineItemWrapper service: serviceWrapperList){

            Received_Product__c rp = new Received_Product__c();

            if(service.isDone){

                rp.Order_Item__c = service.OrdItem.Id;
                rp.Amount__c = service.OrdItem.Amount__c;
                rp.Item_Name__c = service.serviceName;
                rp.Invoice__c = inv.Id;
                rpList.add(rp);   

                Order_Item__c oi = new Order_Item__c();
                oi.Id = service.OrdItem.Id;
                oi.Invoice_Generated__c = true;
                oiList.add(oi);
            }
        }  

        if(!rpList.isEmpty()){
            insert rpList;
        }

        if(!oiList.isEmpty()){
            update oiList;   
        }

        return pgRef;        
    }

    //wrapper class for Service Line Items
    public class ServiceLineItemWrapper{
        public String serviceName {get; set;}
        public Boolean isDone {get; set;}
        public Order_Item__c ordItem {get; set;}
    }

    //wrapper for Product Line Items
    public class ProductLineItemWrapper{
        public String productName {get; set;}
        public Integer orderedQty {get; set;}
        public Integer remainingQty {get; set;}
        public Integer receivedQty {get; set;}
        public OrderItem ordProduct {get; set;}
        public Order_Item__c ordItem {get; set;}
        public Boolean isSlelected {get; set;}
        public Boolean hasError {get; set;}
        public String errorMsg {get; set;}

        public ProductLineItemWrapper(){
            this.hasError = false;
        }
    }
}