/***************************************
    Created By : Mohan
    Purpose    : Service Class to fetch slots 
****************************************/ 
global class FieldServiceSlotsService {

/***************************************
    Created By : Mohan
    Purpose    : Fetch slots for the Given Problem Id, and Set House Id
****************************************/   
    public static JsonResponseBody getSlots(Id problemId, Set<Id> houseIdSet) {

        //Variable declairation
        Set<String> zoneCodeSet = new Set<String>();
        Set<String> undeployedZoneCodeSet = new Set<String>();
        Map<String, TimeSlot> operatingHoursMap = new Map<String, TimeSlot>();
        Map<String, List<Id>> zoneAndHousesMap = new Map<String, List<Id>>();
        List<HouseWithSlotsObj> housesWithSlotsList = new List<HouseWithSlotsObj>(); 
        set<String> fslDeployedZonesSet = new Set<String>(Label.FSL_Deployed_Zones.split(';'));       

        List<House__c> houseList = [select Id, Service_ZoneCode__c from House__c where Id =: houseIdSet];

        for(House__c house: houseList){
            if(house.Service_ZoneCode__c != null && fslDeployedZonesSet.contains(house.Service_ZoneCode__c)){
                zoneCodeSet.add(house.Service_ZoneCode__c);
            } else {
                undeployedZoneCodeSet.add(house.Service_ZoneCode__c);
            } 

            if(zoneAndHousesMap.containsKey(house.Service_ZoneCode__c)){
                zoneAndHousesMap.get(house.Service_ZoneCode__c).add(house.Id);
            } else {
                zoneAndHousesMap.put(house.Service_ZoneCode__c, new List<Id>{house.Id});
            }        
        }

        System.debug('**zoneCodeSet: ' + zoneCodeSet + '\n Size of zoneCodeSet: ' + zoneCodeSet.size());

        List<ServiceTerritory> stList = [select Id, Zone_Code__c from ServiceTerritory where Zone_Code__c =: zoneCodeSet];

        System.debug('**stList: ' + stList + '\n Size of stList: ' + stList.size());

        List<WorkType> workTypeList = [select Id, EstimatedDuration, Name from WorkType 
                                       where Id in (select Work_Type__c from Problem__c where Id =: problemId)];

        System.debug('**workTypeList ' + 'Size of the List ' + workTypeList.size());                                       

        Integer workDurationHrs = 60;

        if(!workTypeList.isEmpty()){
            workDurationHrs = Integer.valueOf(workTypeList[0].EstimatedDuration);
        }

        //Generate values for Appointment Booking arguments
        OperatingHours abOperatingHours = [SELECT Name, Id, (SELECT EndTime, StartTime, Type, DayOfWeek FROM TimeSlots) FROM 
                                           OperatingHours WHERE Name =: Label.Operating_Hours_for_Scheduling limit 1]; 

        for(TimeSlot ts: abOperatingHours.TimeSlots){
            operatingHoursMap.put(ts.DayOfWeek, ts);
        }     

        Timezone tz = UserInfo.getTimeZone();
        Id schedulingPolicyId = [select id from FSL__Scheduling_Policy__c where Name =: Label.Default_Scheduling_Policy limit 1].Id;        

        //Initialize the Savepoint to roll back the changes once we get the slots
        Savepoint sp = Database.setSavepoint();

        WorkOrder wo = new WorkOrder();
        if(!workTypeList.isEmpty()){
            wo.WorkTypeId = workTypeList[0].Id;
        }        
        insert wo;

        System.debug('**WorkOrder ' + wo);

        List<ServiceAppointment> saList = [select Id from ServiceAppointment where ParentRecordId =: wo.Id];
        System.debug('** Service Appointment List ' + saList.size());  

        ServiceAppointment sa = saList[0];
        sa.DueDate = System.now().addDays(10);

        System.debug('**Service Appointment ' + sa);

        //Fetch slots for the Zones where FSL is deployed
        for(ServiceTerritory st: stList){
            sa.ServiceTerritoryId = st.Id;
            //sa.ArrivalWindowStartTime = System.now();
            sa.EarliestStartTime = Datetime.newInstance(System.now().addDays(1).date(), Time.newInstance(8, 00, 00, 00));

            update sa;

            List<FSL.AppointmentBookingSlot> slots = new List<FSL.AppointmentBookingSlot>();

            slots = FSL.AppointmentBookingService.GetSlots(sa.Id, schedulingPolicyId, 
                                                   abOperatingHours, tz, 
                                                   'SORT_BY_DATE', false);

            List<SlotObj> slotObjList = new List<slotObj>();

            System.debug('**slots ' + slots);

            //System.debug('**slotObjList ' + slotObjList);

            for(FSL.AppointmentBookingSlot slot: slots){
                slotObjList.addAll(createSlots(slot.Interval.Start, slot.Interval.Finish, workDurationHrs, false));

                //System.debug('**Individual Slot ' + slot);
            }            

            HouseWithSlotsObj hwsObj = new HouseWithSlotsObj();
            hwsObj.house_ids = zoneAndHousesMap.get(st.Zone_Code__c);
            hwsObj.slots = slotObjList;
            housesWithSlotsList.add(hwsObj);  
        }

        //Fetch Slots for the Zones where FSL is not deployed
        for(String zone: undeployedZoneCodeSet){

            List<SlotObj> slotObjList = new List<slotObj>();

            for(Datetime startDate = System.today(); startDate < System.today().addDays(11); startDate = startDate.addDays(1)){
                System.debug(startDate);
                Datetime slotStartTime;
                Datetime slotEndTime;    
                if(operatingHoursMap.containsKey(startDate.format('EEEE'))){
                    slotStartTime = Datetime.newInstance(startDate.date(), operatingHoursMap.get(startDate.format('EEEE')).StartTime);        
                }
                
                if(operatingHoursMap.containsKey(startDate.format('EEEE'))){
                    slotEndTime = Datetime.newInstance(startDate.date(), operatingHoursMap.get(startDate.format('EEEE')).EndTime);        
                }    

                if(slotStartTime != null && slotEndTime != null){
                    slotObjList.addAll(createSlots(slotStartTime, slotEndTime, workDurationHrs, true));
                }                            
            }

            HouseWithSlotsObj hwsObj = new HouseWithSlotsObj();
            hwsObj.house_ids = zoneAndHousesMap.get(zone);
            hwsObj.slots = slotObjList;   
            housesWithSlotsList.add(hwsObj);             
        }

        JsonResponseBody responseBody = new JsonResponseBody();
        responseBody.slots_for_houses = housesWithSlotsList;

        Database.rollback(sp);      

        return responseBody;
    }

/*******************************************
 * purpose    : Create Slots for the given time window and 
                retrun the Slots.
 * ****************************************/
    public static List<SlotObj> createSlots(Datetime startTime, Datetime finishTime, Integer duration, Boolean sentInIST){

        System.debug('**createSlots method call');

        List<SlotObj> slotObjList = new List<SlotObj>();

        System.debug('**StartTime ' + startTime);   
        System.debug('**StartTime formatted ' + startTime.format());

        System.debug('**finishTime ' + finishTime);        
        System.debug('**finishTime formatted ' + finishTime.format()); 

        System.debug('**duration ' + duration);    
        if(!sentInIST){
            startTime = startTime.addHours(-5).addMinutes(-30);
            finishTime = finishTime.addHours(-5).addMinutes(-30);   
        }      

        Datetime localFinishTime = startTime.addMinutes(duration);   
        System.debug('**localFinishTime outside for loop' + localFinishTime);

        for(Datetime st = startTime; localFinishTime < finishTime; st = st.addMinutes(duration)){

            System.debug('**System.now() ' + System.now());
            System.debug('**StartTime.addMinutes() ' + st);
            System.debug('**System.now().getTime() ' + System.now().getTime());            
            System.debug('**startTime.addMinutes(duration).getTime() ' + st.getTime());

            if(System.now().getTime() < st.getTime()){
                SlotObj slot = new SlotObj();
                slot.from_time = st;
                slot.to_time = st.addMinutes(duration);
                slotObjList.add(slot);            
            }   

            localFinishTime = localFinishTime.addMinutes(duration);
            System.debug('**localFinishTime inside for loop' + localFinishTime); 
        } 

        System.debug('**slotObjList size ' + slotObjList.size());
        //System.debug('**slotObjList ' + slotObjList);

        return slotObjList;
    }   

    //Object to serialize the response Json
    global class JsonResponseBody {
       public List<HouseWithSlotsObj> slots_for_houses;
    }

    global class HouseWithSlotsObj {
      public List<Id> house_ids;
      public List<slotObj> slots;
    }

    global class SlotObj {
        public Datetime from_time;
        public Datetime to_time;
    }
}