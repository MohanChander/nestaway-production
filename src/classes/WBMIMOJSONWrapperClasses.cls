/**    
*   Class Name     :  WBMIMOJSONWrapperClasses
*
*   Description    :  Webservices JSON Classes.   
*
*    Created By    :  Chandraprakash Jangid   
*
*    Created Date  :  24/07/2017
*
*    Version       :  V1.0 Created

**/


global class WBMIMOJSONWrapperClasses{

public class wrapperZone{
    public String area_type{get;set;}
    public String area_id{get;set;}
    public String area_name{get;set;}
    public String parent_id{get;set;}
    public String serviceable{get;set;}
}

public class SendCaseId {
    
    public String case_id;    
}

public class SendHouseSfID {
    
    public String sf_id ;    
}

public class HouseJsonWhenOffboarded {
    public String sf_id ;
    public String offboarding_reason ;
    public String offboarded_by ;   
    public String offboarded_datetime;
}

// Class to get calendar for Fe's within date range.

global class FECalendarWithinDateRange{
    global String case_id;  //case id
    global String from_date;    //2017-06-17
    global String to_date;  //2017-06-19
    global List<Field_Executives> field_executives;       
    
    
}       
global class Field_Executives {
    global String executive_id; //idsdfsfsdfsdf
    global String executive_email_id;   //cpjangid88@gmail.com
}
// END

// Class to get response of FE with Calendar info

global class FECalendarInfoParent {
    public boolean success;
    public String info;
    public FECalendarInfo data;
}

global class FECalendarInfo{ 
    global String case_Id;  //37483745893
    global String from_date;    //2017-06-17
    global String to_date;  //2017-06-19
    global List<Executives_Calendar> executives_calendar;
    
}
global class Executives_Calendar {
    global String executive_id; //xyz1234567324
    global String executive_Email_Id;   //xyz1234567324
    global List<Calendar> calendar;
}
global class Calendar {
    global String date_value;    //2017-2-18
    global List<Slots> slots;
}
global class Slots {
    global String from_time; //1500463784
    global String to_time;   //1500463784
    global String text; //10PM-10:30PM
    global String note; //text Notes
    global boolean available;
}

//END

// Classe to book Fe Calendar. 
public class BookFECalendar{
    public String case_id;  //case id    
    public String date_value;    //2017-2-23
    public String executive_id; //eqweq
    public String executive_email_id;   //cdjadj@!gmail.com
    public String from_time;    //1500463784
    public String to_time;  //1500463784
    
}

public class BookFECalendarResponse{    
    public boolean success;    //true
    public String data;  
    public String info;  //if have any error
}

//END

// Class to get case or house details from DB so we can give fe of the house.
public class DBMoveInData{
    
    public String case_id;  //iruweoir
    public String house_id; //test
    
}

// Method to get available date for move in.

/* public class MIMODateJsonWrapper {
    public String start_date;   //13-07-2017
    public String end_date; //20-07-2017
    public List<holidays> holidays; //15-07-2017,17-07-2017,17-07-2017
    
}

public class holidays {
    public String date_value;   //2017-07-25
}
*/

public class MIMODateJsonWrapper {
    public boolean success;
    public String info;
    public DataObject data;
}
    
public class DataObject {
    public String start_date;   //2017-08-21
    public String end_date;     //2017-08-30
    public List<holidays>  holidays;
}
public class holidays {
    public String date_value;   //2017-07-25
}


global class SendFEDetailsToWA{
    public String case_id;  //case id
    public String house_id; //test
    public String zone_code; //test
    public List<WA_Field_Executives> field_executives;
}
global class WA_Field_Executives {
    public String executive_id; //idsdfsfsdfsdf
    public String executive_email_id;   //cpjangid88@gmail.com
}
// Send Case Id to get Move in dates
public class MoveInDates {
    public String case_id; 
}

// Send case id in case of auto owner approval confirmation 
public class AutoOwnerApprovalConfirmation {
    public String case_id; 
}


//Send updated Case JSON to NA

public class UpdateCaseToNaForBedrelease {
    public id case_id;
    public string status;         
}

/*bed release response parser */
/* {
   "success":true,
   "info":null,
   "data" : { " deduction_amount" : "deduction_amount",
      "refund_amount": "refund_amount",
       "total_charges": "total_charges"
    }
}*/
public class BedreleaseResponse {
    public boolean success;
    public String info;
    public data data;
}
public class data {
        public String deduction_amount;   //-9558
        public String refund_amount;
        public String total_charges;
}

/************************************************
Created By : Mohan
Purpose    : Wrapper Class for sending OTP to Tenant on Click of Send OTP on WorkOrder
************************************************/
    public class SendOTPRequestJSON{
        public String method;
        public String ticket_number;
        public Integer otp_code;
        public Decimal material_cost;
        public Decimal labour_cost;
        public String technician_name;    
    }

/************************************************
Created By : Mohan
Purpose    : Wrapper Class for sending Invoice details to WebApp
************************************************/
    public class ServiceRequestInvoiceJSON{
        public String ticket_number;
        public Boolean free_visit;
        public Decimal tenant_labour_cost;
        public Decimal service_visit_cost;
        public Decimal express_service_cost;
        public Decimal tenant_material_cost;
        public Decimal owner_labour_cost;
        public Decimal owner_material_cost;
        public Decimal nestaway_labour_cost;
        public Decimal nestaway_material_cost; 
        public AttachmentsObject attachments;
    }

    public class AttachmentsObject{
        public List<fileObj> material_related;
    }

    public class FileObj{
        public String filename;
        public String blob_url;
    }

/************************************************
Created By : Mohan
Purpose    : Wrapper Class for sending Vendor Details to WebApp
************************************************/
    public class VendorDetailsNotificationJSON{
        public String method;
        public String ticket_number;
        public String technician_name;
        public String technician_phone;
        public Datetime schedule_date_time;
    }

/************************************************
Created By : Mohan
Purpose    : Wrapper Class for Owner Inclusion Details to WebApp
************************************************/
    public class OwnerInclusionNotificationJSON{
        public String method;
        public String ticket_number;
        public Datetime deadline_for_owner_approval;
        public Decimal approx_cost;
        public String inclusion_comment;
    }    
}