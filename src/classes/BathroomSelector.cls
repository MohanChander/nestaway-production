/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 31/05/2017
    Purpose      : All the queries related to the Bathroom Object are here */

public class BathroomSelector {

	//Query for all the Opportunities related to a Opportunity
	public static List<Bathroom__c> getBathroomsForOpportunities(Set<Id> oppIdSet){

		return [select Id, Name, Room_Inspection__r.House_Inspection_Checklist__r.Opportunity__c, Room_Inspection__c, Room__c,
				Checklist__r.Opportunity__c, House__c from Bathroom__c where 
				Room_Inspection__r.House_Inspection_Checklist__r.Opportunity__c  =: oppIdSet
				or Checklist__r.Opportunity__c =: oppIdSet];
	}


	//Aggregate Query to get the Bathroom count for House_Inspection_Checklist__c
	public static List<AggregateResult> getBathroomCountForHIC(Set<Id> hicIdSet){

		System.debug('**********hicIdSet ' + hicIdSet);

		return [select Room_Inspection__r.House_Inspection_Checklist__c, count(Id) from Bathroom__c 
				group by Room_Inspection__r.House_Inspection_Checklist__c HAVING Room_Inspection__r.House_Inspection_Checklist__c =: hicIdSet];
	}


	//get the Bathroom List with House Status
	public static List<Bathroom__c> getBathroomsWithHouseStatus(Set<Id> bathroomIdSet){

		return [select Id, House__c, House__r.Stage__c from Bathroom__c where Id =: bathroomIdSet];
	}

	//get Bathroom details for MIMO check
	public static List<Bathroom__c> getBicForMIMOCheck(Set<Id> houseIdSet){
		String typeMimo = Constants.BATHROOM_TYPE_OF_BIC_MIMO_CHECK;
		String typeBic = Constants.BATHROOM_TYPE_OF_BIC_BATHROOM_INSPECTION;
		String filterString = ' from Bathroom__c where (House__c =: houseIdSet or HouseForMIMO__c =: houseIdSet) and ' +  
				'(Type_of_BIC__c =: typeMimo or ' + 
				'Type_of_BIC__c =: typeBic or ' +
				'Type_of_BIC__c = null) ';
		String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_BATHROOM);			
	    List<Bathroom__c> bicList = (List<Bathroom__c>)Database.query(queryString + filterString);
	    return bicList;	
	}

	//get Move in and Move Out Bathroom details for WorkOrders
	public static List<Bathroom__c> getBicForWorkOrders(Set<Id> woIdSet){
		String typeMoveIn = Constants.BATHROOM_TYPE_OF_BIC_MOVE_IN_CHECK;
		String typeMoveOut = Constants.BATHROOM_TYPE_OF_BIC_MOVE_OUT_CHECK;
		String filterString = ', CheckFor__r.Name from Bathroom__c where (Work_Order__c =: woIdSet) and ' +  
				'(Type_of_BIC__c =: typeMoveIn or ' + 
				'Type_of_BIC__c =: typeMoveOut)';
		String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_BATHROOM);			
	    List<Bathroom__c> bicList = (List<Bathroom__c>)Database.query(queryString + filterString);
	    return bicList;	
	}
	
	/****************************************************************
	    Added By Baibhav
*******************************************************************/	

	public static List<Bathroom__c> getBicformHouseidset(Set<Id> houseIdset)
	{ 
		Id bathRtType=Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_Furnishing).getRecordTypeId();
		return [SELECT Bucket_Mug__c,Cracks_Seepages__c,Health_Faucet__c,House__c,Mirror__c,
		Name,No_of_Non_Functional_Lights__c,No_of_Non_Functional_Taps__c,Toilet_Brush__c,
		Toilet_Type__c,Water_Heater_Type__c,Water_Heater__c,Work_Order__c FROM Bathroom__c where house__c=:houseIdSet and  (Type_of_BIC__c='Bathroom Inspection' or Type_of_BIC__c=null)];
	}	
	
}