public class OpportunityTriggerHelper 
{
/**********************************************************************************************************************************************
    Added by baibhav
    Purpose to Add error when Address on Opportunity isChanged when house is there 
********************************************************************************************************************************************/
    public static void OpportunityAddressErroe(Map<id,Opportunity> oppMap)
    {
        try
        {
            List<House__c> houList=HouseSelector.getHouseListFromOppidSet(oppMap.keySet());
            Map<id,List<House__c>> houMapOpp =new Map<id,List<House__c>>();
            for(House__c ho:houList)
            {
                List<House__c> hlist =new List<House__c>();
                if(houMapOpp.containsKey(ho.Opportunity__c))
                {
                    hlist=houMapOpp.get(ho.Opportunity__c);
                } 
                hlist.add(ho);
                houMapOpp.put(ho.Opportunity__c,hlist);
            }
            for(Opportunity op:oppMap.values())
            {
                if(houMapOpp.containskey(op.id) && houMapOpp.get(op.id).size()>0)
                op.adderror('Address cannot be changed on Opportunity');
            }
            } Catch(Exception e) {
                 System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                 UtilityClass.insertGenericErrorLog(e);
            }
     
    }
    
}