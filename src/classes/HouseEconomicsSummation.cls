/*  Created By   : Mohan - WarpDrive Tech Works
	Created Date : 22/05/2017
	Purpose      : The House Economics Section With Onboarding, OPM, Offboarding will be updated based on the 
				   House Id set Provided */
public class HouseEconomicsSummation {

	// Method to perform the summation of the Invoice Amounts based on the Category 
	public Static void houseEconomicsSummation(Set<Id> houseIdSet){

		System.debug('houseEconomicsSummation method is called');

		Map<Id, Map<String, Decimal>> houseMap = new Map<Id, Map<String, Decimal>>();
		List<House__c> updatedHouseList = new List<House__c>();
		List<AggregateResult> houseSummationResult = new List<AggregateResult>();

		System.debug('House Id set: ' + houseIdSet);

		//query for the List of Houses with child Invoices
		if(!houseIdSet.isEmpty())
			houseSummationResult = InvoiceSelector.getSummatedAmountOfInvoices(houseIdSet);

		System.debug('AggregateResult: ' + houseSummationResult);

		for(AggregateResult ar: houseSummationResult){
			if(houseMap.containsKey((Id)ar.get('House__c'))){
				houseMap.get((Id)ar.get('House__c')).put((String)ar.get('Category__c'), (Decimal)ar.get('total'));
			} else {
				houseMap.put((Id)ar.get('House__c'), new Map<String, Decimal>{(String)ar.get('Category__c') => (Decimal)ar.get('total')});
			}
		}

		//if there are no Invoices associated with the House update the totals to 0
		for(Id houseId: houseIdSet){
			if(houseMap.isEmpty() || !houseMap.containsKey(houseId)){
				House__c h = new House__c(  Id = houseId,
											Onboarding__c = 0,
											OPM__c = 0,
											Offboarding__c = 0
										  );	
				updatedHouseList.add(h);			
			}
		}

		System.debug('House Map: ' + houseMap);

		for(Id houseId: houseMap.keySet()){
			if(houseId != null){
				House__c h = new House__c(  Id = houseId,
											Onboarding__c = houseMap.get(houseId).get('On-boarding'),
											OPM__c = houseMap.get(houseId).get('OPM'),
											Offboarding__c = houseMap.get(houseId).get('Off-boarding')
										  );
				updatedHouseList.add(h);
			}
		}

		if(!updatedHouseList.isEmpty())
			update updatedHouseList;
	} // end of the method

}