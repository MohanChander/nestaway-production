public class CaseExcelToUpdateSettlementCase {
    public String parseFile_var {get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public list<settlementCaseWrapper> setllementCasTList {get;set;}
    public set<string> caseNumbers;
    public map<string,settlementCaseWrapper> caseNumberAndDescMap = new map<string,settlementCaseWrapper>();
    public map<id,case> caseMapToUpdate = new map<id,case>();
    set<Id> successIds = new set<id>();
    String[] filelines = new String[]{};
        List<Account> accstoupload;
    
    class settlementCaseWrapper {
        String transactionId;
        Date transactionDate;
        String caseNumber;
    }
    
    public CaseExcelToUpdateSettlementCase(ApexPages.StandardSetController controller){
    }

    
    public CaseExcelToUpdateSettlementCase(ApexPages.StandardController controller){
    }
    
    public Pagereference ReadFile()
    {
        String error_message = '';
      if(contentFile!=null){    
        nameFile= getStringBody(contentFile);
        List<List<String>> parsedFields = parseCSV(nameFile,true);
        if(parsedFields != null ){
            try{
                
                //nameFile=contentFile.toString();
                //nameFile= EncodingUtil.base64Encode(contentFile);
                //nameFile = EncodingUtil.urlEncode(nameFile, 'UTF-8');
                List<Id> idList =  new List<Id>();
                Map<String,string> map1 = new Map<string,string>();

                //system.debug('**Total number of rows in the uploaded csv ' + Integer.valueOf(parsedFields.size()) + '\n *****'+parsedFields);

                setllementCasTList = new list<settlementCaseWrapper>(); 
                caseNumbers = new set<String>();
                caseNumberAndDescMap = new map<string,settlementCaseWrapper>(); 
                integer k=0;

                //Loop through each line of the CSV. e1 represents a single row in the uploaded csv file
                //parsedFields represents all the row uploaded in the CSV
                for(List<String> e1: parsedFields) {

                    if(k > 7 && e1 != null && e1.size()>3 && e1[2].contains('SF')) {
                        
                        //System.debug('**** e1 '+e1);
                        settlementCaseWrapper setllmentObj = new settlementCaseWrapper();
                        // dateTime dt = new dateTime(e1[1]);
                        // setllmentObj.transactionDate = dt;
                        setllmentObj.transactionId = e1[3];
                        setllmentObj.caseNumber = (e1[2]).split(' ')[0];
                        if(setllmentObj.caseNumber!=null){
                            setllmentObj.caseNumber=setllmentObj.caseNumber.trim();
                        }                       
                        setllementCasTList.add(setllmentObj);
                    }
                    k++;
                     //System.debug('***** e1'+e1) ;
                }

                //System.debug('***************setllementCasTList: ' + setllementCasTList + '\n Size of setllementCasTList: ' + setllementCasTList.size());

                //populate the caseNumber Set - CSV has been parsed and CaseNumbers have been extracted
                for(settlementCaseWrapper each : setllementCasTList) {
                    caseNumbers.add(each.caseNumber);
                    caseNumberAndDescMap.put(each.caseNumber,each);
                    System.debug('***** transactionId'+ each.transactionId) ;
                    System.debug('***** transactionDate'+ each.transactionDate) ;
                    System.debug('***** caseNumber'+ each.caseNumber) ;
                }

                //System.debug('**caseNumberAndDescMap : ' + caseNumberAndDescMap + '\n Size of caseNumberAndDescMap: ' + caseNumberAndDescMap.size());

                if(caseNumbers != null && caseNumbers.size() > 0) {
                    list<case> caseList = new list<case>();
                    Id moveOutSettlementRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();
                    Id nestawaySettlementRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT ).getRecordTypeId();
                    Id vendorSettlementRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_VENDOR_SETTLEMENT ).getRecordTypeId();


                    //query for the list of Cases that have been uploaded in the input CSV file
                    caseList = [Select id,kotak_Case_Number__c,Settlement_Case_Updated__c,Transaction_Date__c,
                                Dashboard_Update_Required__c,
                                Dashboard_Update_Status__c,Status,CaseNumber,Transaction_ID__c 
                                from case where kotak_Case_Number__c IN: caseNumbers 
                                and (recordtypeId =: moveOutSettlementRecTypeId  or 
                                     recordtypeId =: nestawaySettlementRecTypeId or
                                     recordtypeId =: vendorSettlementRecTypeId )];

                    //System.debug('***************caseList: ' + caseList + '\n Size of caseList: ' + caseList.size());

                    for(case each : caseList) {
                        if(caseNumberAndDescMap.containsKey(each.kotak_Case_Number__c)) {
                            case c  = new case();
                            c.id = each.id;
                            if(caseNumberAndDescMap.get(each.kotak_Case_Number__c) != null && caseNumberAndDescMap.get(each.kotak_Case_Number__c).transactionId != null){
                                c.Transaction_ID__c = caseNumberAndDescMap.get(each.kotak_Case_Number__c).transactionId;
                            }
                            c.Status = 'Paid';
                            c.Transaction_Date__c = System.now();
                            c.Dashboard_Update_Required__c = 'Yes';
                            c.Dashboard_Update_Status__c = 'Updated Successfully';
                            c.Settlement_Case_Updated__c='true';
                            c.Settlement_Case_Updated_Datetime__c=system.now();                         
                            caseMapToUpdate.put(c.id,c);
                        }
                    }
                    
                    if(caseMapToUpdate != null && caseMapToUpdate.size() > 0) {
                        
                        System.debug('**caseMapToUpdate: ' + caseMapToUpdate + '\n Size of caseMapToUpdate: ' + caseMapToUpdate.size());

                        List<Database.SaveResult> results = Database.update(caseMapToUpdate.values(), false);
                        //string errorString='Error occured in updating records: ';
                        for (Database.SaveResult result : results) {
                            if (!result.isSuccess()){
                                
                                for (Database.Error err : result.getErrors()){
                                    System.debug('Error: '+ err.getStatusCode() + ' ' + err.getMessage());
                                
                                    error_message+=err.getStatusCode() + '-' + err.getMessage()+' <BR> <BR>';
                                }
                            }
                            else{
                                successIds.add(result.getId());
                            }
                        }
                        //update caseMapToUpdate.values();
                    }
                }

                //when the error message is null - all the records have been successfully updated
                if(error_message == ''){
                    ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.INFO,'File Uploaded Successfully. Number of cases updated Successfully: '+caseMapToUpdate.values().size());
                    ApexPages.addMessage(myMsg2);    
                    return null;
                }
                else{
                     string errorCaseNumbers='Error in case number: ';
                     Integer i=0;
                     List<case> caseLst=[select id,casenumber,Settlement_Case_Updated__c from case where id IN:caseMapToUpdate.keySet()];
                      System.debug('***caseLst '+caseLst);
                     boolean hasCase=false; 
                    for(case cs: caseLst){
                        
                          if(!successIds.contains(cs.id)){
                              
                               errorCaseNumbers +=cs.caseNumber+',';
                               hasCase=true;
                               i++;
                          }
                        
                    }
                     errorCaseNumbers=errorCaseNumbers.removeEnd(',');
                     if(hasCase){
                         errorCaseNumbers=errorCaseNumbers+'.';
                     }
                    string error_message1='File Uploaded Successfully. <BR> Number of cases updated Successfully: ' + 
                                           successIds.size()+ ' <Br> Number of cases failed: ' +
                                           i + '  <Br> '+errorCaseNumbers+' <BR> <BR>'+' Errors : <BR>'+error_message;
                    system.debug('****error_message1'+error_message1);
                    ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.INFO,error_message1);
                    ApexPages.addMessage(myMsg2);    
                    return null; 
                }
            }
            catch(Exception er) {
                System.debug('***The following exception has occurred: ' + er.getMessage()+'******\n'+er+er.getLineNumber());
                PageReference pageRef1 = new PageReference('/apex/Case_ParseExcelToUpdateSettlementCase');
                ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,er.getMessage());
                ApexPages.addMessage(myMsg1);
                //pageRef1.setRedirect(true);
                return null;
            }
        }
        else{
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR,'File is not of correct format or file is empty');
            ApexPages.addMessage(myMsg2);  
           
            return null;
        }
      }
      else{
        
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR,'File is not uploaded.');
            ApexPages.addMessage(myMsg2);  
          
            return null;
      }   
    }  
    
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }

    //method to convert Blob to String
    public string getStringBody(Blob body){
        
        string csvBody='';
        try{
            csvBody=body.toString();
        }
        catch(exception e){
            system.debug('***exception in converting to string'+e.getMessage());
            csvBody='';
        }
        if(csvBody==''){                      
            csvBody= blobToString(body,'ISO-8859-1'); 
        }         
        
        return csvBody;
    }
    /*****************************************************************************************************
This function convers the input CSV file in BLOB format into a string
@param input    Blob data representing correct string in @inCharset encoding
@param inCharset    encoding of the Blob data (for example 'ISO 8859-1')
*****************************************************************************************************/
    public static String blobToString(Blob input, String inCharset){

        //convert the blob to Hexadecimal
        String hex = EncodingUtil.convertToHex(input);

        // System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    } 
    

}