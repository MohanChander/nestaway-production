public without sharing class ContractTermChangesController  {
   public Contract newCon {get; set;}
   public Contract activeCon {get; set;} 
   public List<Contract> newConLst {get; set;}
   public List<Contract> activeConLst {get; set;}      
   public Map<String,Operational_Process__c> fieldNamesMap {get; set;}
   public Set<String> uniqueFieldNames {get; set;}
   public boolean showDetails{get;set;}
   Public string tokenId{get;set;}
   public List<Operational_Process__c> showContChangesFieldData {get; set;}
   

   public ContractTermChangesController(){
     
      newCon = new Contract();
      activeCon = new Contract();     
      showDetails=false;
      showContChangesFieldData = new List<Operational_Process__c>();
      fieldNamesMap = new Map<String,Operational_Process__c>();
     try{
      tokenId = ApexPages.currentPage().getParameters().get('id');    
      
      uniqueFieldNames = new Set<String>();
      Id ContProcessRecordTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(Constants.OPERATIONAL_PROCESS_RT_CONT_OWNER_VER_FIELDS).getRecordTypeId();
      List<Operational_Process__c> ContProcess=[select id,Label_Name__c,Field_API_Name__c from Operational_Process__c where recordtypeId=:ContProcessRecordTypeId and Field_API_Name__c!=null and Label_Name__c!=null order by Order__c ASC];
      system.debug('***ContProcess'+ContProcess);
      String baseSOQL = UtilityServiceClass.getQueryString('Contract');    
      
      for(Operational_Process__c op : ContProcess){
           system.debug('***fieldName'+op.Field_API_Name__c);
           string sfField=op.Field_API_Name__c.toUpperCase();
           uniqueFieldNames.add(sfField);
           fieldNamesMap.put(sfField,op);
      }     
      
          
       
      string newConSOQL = baseSOQL + ' From Contract where Agreement_Approval_Code__c = :tokenId and isCloneContract__c=true';     
      newConLst=Database.Query(newConSOQL);
     
     
      
      if(newConLst.size()>0){
          newCon=newConLst.get(0);
          
          if(newCon.Opportunity__c!=null){
            //  string oppId=newCon.Opportunity__c;
            //  string activeConSoql = baseSOQL + ' from contract where Opportunity__c=:oppId and isActive__c=true order by createddate desc';
              string parentContId=newCon.Parent_Contract__c;
              string activeConSoql = baseSOQL + ' from contract where id=:parentContId';
              activeConLst=Database.Query(activeConSoql);
              
              if(activeConLst.size()>0){
                  
                  activeCon=activeConLst.get(0);
                  
                    
                    
                    // For each field
                    for(String field : fieldNamesMap.keyset()){
                        
                        // Check whether new value != than old value for the same record

                        if (newCon.get(field) != activeCon.get(field)){                     
                            
                            showContChangesFieldData.add(fieldNamesMap.get(field));
                            showDetails=true; 
                        }
                    }
                    if(!showDetails){
                          
                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,''));
                      }                 
                  
                               
                  
              }
               else{
          
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Contract not found.'));
      }
              
          }
          
           else{
          
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Contract not found.'));
      }
          
      }  
      
      else{
          
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Contract not found.'));
      }
    }
    catch(exception e){
        
        system.debug('***Exception in generating contract version changes :'+e.getMessage()+' at line :'+e.getLineNumber());
    }
     
      
   }


}