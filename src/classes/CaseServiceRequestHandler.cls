/*  Created By   : Deepak - WarpDrive Tech Works
    Created Date : 18/05/2017
    */

public class CaseServiceRequestHandler {
    
    /*  Created By : Mohan 
        Purpose    : When a Service Request Case is Created/ Updated with Problem - Problem related info to be populated 
        on the Case */
    public static void UpdateProblemRelatedInfo(List<Case> caseList, Boolean isInsert){
        try{
            Map<Id, Problem__c> problemMap = new Map<Id, Problem__c>(ProblemSelector.getProblemList());
            
            for(Case c: caseList){
                c.Queue__c = problemMap.get(c.Problem__c).Queue__c;
                c.Require_visit__c = problemMap.get(c.Problem__c).Scheduled_Visit_Required__c;
                c.Priority = problemMap.get(c.Problem__c).Priority__c;
                c.Subject = problemMap.get(c.Problem__c).Name;
                c.Subject_for_Escalation_Email__c = problemMap.get(c.Problem__c).Name.replace('➢','-->');              
            }
        } catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);      
        }
    }
    
    
    /*  Created By : Mohan
        Pupose     : On change of Escalation Level update the Next Escalation to field on the Case */  
    @future 
    public static void onCaseEsclation(Set<Id> caseIdSet){
        
        try{
            set<Id> userIdSet = new Set<Id>();
            Map<Id, User> managerMap = new Map<Id, User>();
            List<Case> caseList = CaseSelector.getCasesFromCaseIdSet(caseIdSet);
            
            for(Case c: caseList){
                if(c.Next_Escalation_to__c != null)
                    userIdSet.add(c.Next_Escalation_to__c);
                else if(c.OwnerId.getSObjectType() == User.SObjectType){
                    c.Next_Escalation_to__c = c.OwnerId;
                    userIdSet.add(c.OwnerId);
                }
            }
            
            for(User u: UserSelector.getUserDetails(userIdSet)){
                managerMap.put(u.Id, u);
            }
            
            for(Case c: caseList){
                if(c.Next_Escalation_to__c != null){
                    c.Next_Escalation_Email__c = !managerMap.isEmpty() && managerMap.containsKey(c.Next_Escalation_to__c) && managerMap.get(c.Next_Escalation_to__c) != null ? managerMap.get(c.Next_Escalation_to__c).Manager.Email : c.Next_Escalation_Email__c;                    
                    c.Next_Escalation_to__c  = !managerMap.isEmpty() && managerMap.containsKey(c.Next_Escalation_to__c) && managerMap.get(c.Next_Escalation_to__c) != null ? managerMap.get(c.Next_Escalation_to__c).ManagerId : c.Next_Escalation_to__c;
                } 
            }
            
            //auto completion of the Milestones
            completeMilestones(caseIdSet);
            
            //switch off the Case Trigger before updation
            if(!caseList.isEmpty()){
                StopRecursion.CASE_SWITCH = false;
                update caseList;
                StopRecursion.CASE_SWITCH = true;
            }
        } catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e);
        }
    } 
    
    /*  Created by : Mohan
        Purpose    : Complete the Milestones for the given CaseIdSet */
    public static void completeMilestones(Set<Id> caseIdSet){
        
        List<CaseMilestone> cmList = [select Id, CompletionDate, CaseId from CaseMilestone where CaseId =: caseIdSet and CompletionDate = null];
        
        for(CaseMilestone cm: cmList){
            cm.CompletionDate = System.now();
        }
        
        update cmList;
    }

    /*  Created by : Mohan
        Purpose    : Populate the Next Email to field when Owner is changed
                   : Manager to Whome SLA voilation mail has to go */
    public static void populateNextEmailTofield(List<Case> caseList){

        try{
                Set<Id> userIdSet = new Set<Id>();

                for(Case c: caseList){
                    if(c.OwnerId.getSObjectType() == User.SObjectType)
                        userIdSet.add(c.OwnerId);
                }

                Map<Id, User> userMap = new Map<Id, User>(UserSelector.getUserDetails(userIdSet));

                for(Case c: caseList){
                    if(c.OwnerId.getSObjectType() == User.SObjectType){
                        c.Next_Escalation_Email__c = userMap.get(c.OwnerId).Manager.Email;
                        c.Next_Escalation_to__c = userMap.get(c.OwnerId).ManagerId;
                    }
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }

/**********************************************
    Created By : Mohan
    Purpose    : Update the Follower on the Case when the Queue is changed
**********************************************/
    public static void updateFollowerOnCase(List<Case> caseList){
        try{

                Map<String, Operational_Process__c> queueToFollowerMap = new Map<String, Operational_Process__c>();

                List<Operational_Process__c> opList = [select Id, Case_Queue__c, User__c, User__r.Email from Operational_Process__c 
                                                        where RecordType.Name = 'Service Request Case Comment Notification Configuration'];

                for(Operational_Process__c op: opList){
                    queueToFollowerMap.put(op.Case_Queue__c, op);
                }

                for(Case c: caseList){
                    if(queueToFollowerMap.containsKey(c.Queue__c) && queueToFollowerMap.get(c.Queue__c).User__c != null){
                        c.Follower__c = queueToFollowerMap.get(c.Queue__c).User__c;
                        c.Follower_Email__c = queueToFollowerMap.get(c.Queue__c).User__r.Email;
                    }
                }                                                        

            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                                 e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() 
                                 + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'updateFollowerOnCase');      
            }           
    }

/**********************************************
    Created By : Mohan
    Purpose    : create Vendor Work Order when the Tenant selects the slot for Visit on the Dashboard
                 (FSL Module Implementation)
**********************************************/
    public static void createVendorWorkOrder(List<Case> caseList){

        System.debug('**createVendorWorkOrder');

        try{
                Id vendorWoRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_VENDER).getRecordTypeId();            
                List<WorkOrder> vendorWorkOrderList = new List<WorkOrder>();
                Set<Id> problemIdSet = new Set<Id>();
                Set<String> zoneCodeSet = new Set<String>();
                Set<Id> caseIdSet = new Set<Id>();
                Map<String, ServiceTerritory> stMap = new Map<String, ServiceTerritory>();

                for(Case c: caseList){
                    problemIdSet.add(c.Problem__c);
                    zoneCodeSet.add(c.Zone_Code__c); 
                    caseIdSet.add(c.Id);                 
                }

                Map<Id, Problem__c> problemMap = new Map<Id, Problem__c>([select Id, Work_Type__c from Problem__c where Id =: problemIdSet]);                             
                Map<Id, Case> caseMap = new Map<Id, Case>([select Id, House1__r.Door_Number__c, House1__r.Street__c, House1__r.Locality_city__c,
                                                           House1__c, House1__r.HouseId__c, Service_Visit_Time__c, House_City__c, House_Id__c,
                                                           House1__r.Pincode__c, House1__r.State__c, House1__r.Country__c from Case where 
                                                           Id =: caseIdSet ]);
                List<ServiceTerritory> stList = [select Id, Name, Zone_Code__c from ServiceTerritory where Zone_Code__c =: zoneCodeSet]; 

                for(ServiceTerritory st: stList){
                    stMap.put(st.Zone_Code__c, st);
                }

                for(Case c: caseList){
                    WorkOrder wo = new WorkOrder();
                    wo.CaseId = c.Id;
                    //wo.Bill_To__c = 
                    wo.Tenant__c = c.AccountId;
                    wo.Subject = c.Subject;
                    wo.Description = c.Description;
                    wo.AccountId = c.AccountId;
                    wo.ContactId = c.ContactId;
                    wo.House__c = c.House1__c;
                    wo.HouseId__c = c.House_Id__c;
                    wo.WorkTypeId = problemMap.get(c.Problem__c).Work_Type__c;
                    wo.RecordTypeId = vendorWoRtId;
                    if(stMap.containsKey(c.Zone_Code__c)){
                        wo.ServiceTerritoryId = stMap.get(c.Zone_Code__c).Id;
                    }                    
                    wo.Street = caseMap.get(c.Id).House1__r.Street__c;
                    wo.City = caseMap.get(c.Id).House_City__c;  
                    wo.State = caseMap.get(c.Id).House1__r.State__c;
                    wo.Country = caseMap.get(c.Id).House1__r.Country__c; 
                    wo.PostalCode = caseMap.get(c.Id).House1__r.Pincode__c;   
                    vendorWorkOrderList.add(wo);
                }  

                if(!vendorWorkOrderList.isEmpty()){
                    insert vendorWorkOrderList;
                }                                                  
            } catch(Exception e){
                System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                             e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() 
                             + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'createVendorWorkOrder - FLS Module');      
            }           
    }    

/**********************************************
    Created By : Mohan
    Purpose    : To reschedule the Service Appointments when the Service Visit Time is changed on the Case
**********************************************/
    public static void rescheduleVendorWorkOrder(Map<Id, Case> newMap){

        System.debug('**rescheduleVendorWorkOrder');

        try{
                Id vendorWoRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_VENDER).getRecordTypeId();            
                
                Map<Id, WorkOrder> woMap = new Map<Id, WorkOrder>([select Id, CaseId, Street, City, State, Country, PostalCode 
                                                                   from WorkOrder where RecordTypeId =: vendorWoRtId and
                                                                   CaseId =: newMap.keySet() and isClosed = false]);
                if(!woMap.isEmpty()){
                    WorkOrderFSLService.unScheduleServiceAppointmentsForWorkOrders(woMap);
                }                                                                                               
            } catch(Exception e){
                System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                             e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() 
                             + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'createVendorWorkOrder - FLS Module');      
            }           
    } 


/**********************************************
    Created By : Mohan
    Purpose    : Update Vendor Work Order Status for any Changes on the Case
**********************************************/
    public static void updateVendorWorkOrderForCaseChanges(Map<Id, Case> newMap){

        System.debug('**updateVendorWorkOrderForCaseChanges');

        try{
                Id vendorWoRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_VENDER).getRecordTypeId();            
                
                List<WorkOrder> woList = new List<WorkOrder>();

                Map<Id, WorkOrder> woMap = new Map<Id, WorkOrder>([select Id, CaseId from WorkOrder where RecordTypeId =: vendorWoRtId and
                                                                   CaseId =: newMap.keySet() and isClosed = false]);
                for(WorkOrder wo: woMap.values()){

                    if(newMap.containsKey(wo.CaseId) && (newMap.get(wo.CaseId).Status == CaseConstants.CASE_STATUS_CASE_AUTOCLOSED ||
                        newMap.get(wo.CaseId).Status == CaseConstants.CASE_STATUS_CANCELLED)){
                            wo.Status = WorkOrder_Constants.WORKORDER_STATUS_DROPPED;
                            woList.add(wo);
                    }
                } 

                System.debug('**woList: ' + woList + '\n Size of woList: ' + woList.size());

                if(!woList.isEmpty()){
                    update woList;
                }                                                                                          
            } catch(Exception e){
                System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                             e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() 
                             + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'createVendorWorkOrder - FLS Module');      
            }           
    }            
}