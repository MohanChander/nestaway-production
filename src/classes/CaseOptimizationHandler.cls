/**********************************************************************************
Added by Baibhav
Purpose: For Hnadling Onboading Case
**********************************************************************************/
public class CaseOptimizationHandler{

    public static id keyhandlingWrk=Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_KEY_HANDLIND).getRecordTypeId();
    public static id onBoardingWrk=Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_ONBOADING).getRecordTypeId();
    public static id onBoardingChecklistWrk=Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_ONBOADING_CHECKLIST).getRecordTypeId();
    public static id furnishCaseOnboarding=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
    public static id unfurnishCaseOnboarding=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();

    public static void AfterInsert(Map<id,case> newMap){
        
        List<Case> casList = new List<Case>();
        for(Case ca:newMap.values()){
            if((ca.RecordtypeId==furnishCaseOnboarding || ca.RecordtypeId==unfurnishCaseOnboarding) && ca.status==Constants.CASE_STATUS_KEY_HANDLING){
              casList.add(ca);
            }
        }
        if(!casList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casList,keyhandlingWrk,'Key Handling');
        }
        
    }

    public static void AfterUpdate(Map<id,case> newMap,Map<id,case> oldMap){
        
        List<Case> casMainList = new List<Case>();
        List<Case> casVerList = new List<Case>();
        List<Case> casPhotoList = new List<Case>();
        List<Case> casFurnList = new List<Case>();
        List<Case> casMakeList = new List<Case>();
        Map<id,Case> casDropMap = new Map<id,Case>();


        for(Case ca:newMap.values()){
            if(ca.RecordtypeId==furnishCaseOnboarding || ca.RecordtypeId==unfurnishCaseOnboarding){

                if(ca.status==Constants.CASE_STATUS_MAINTENANCE && ca.status!=oldmap.get(ca.id).status){
                  casMainList.add(ca);
                }
                if(ca.status==Constants.CASE_STATUS_VERIFICATION && ca.status!=oldmap.get(ca.id).status){
                  casVerList.add(ca);
                }
                if(ca.status==Constants.CASE_STATUS_PHOTOGRAPHY && ca.status!=oldmap.get(ca.id).status){
                  casPhotoList.add(ca);
                }
                
                if(ca.status==Constants.CASE_STATUS_MAKE_HOUSE_LIVE && ca.status!=oldmap.get(ca.id).status){
                  casMakeList.add(ca);
                }
                if(ca.status==Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT && ca.status!=oldmap.get(ca.id).status){
                  casFurnList.add(ca);
                }

                if(ca.status==Constants.CASE_STATUS_DROPPED && ca.status!=oldmap.get(ca.id).status){
                   casDropMap.put(ca.id,ca);
                }
            }                                   
        }
        if(!casMainList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casMainList,onBoardingChecklistWrk,'Maintenance');
        }
        if(!casVerList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casVerList,onBoardingChecklistWrk,'Verification');
        }
        if(!casPhotoList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casPhotoList,onBoardingWrk,'Photography');
        }
        if(!casFurnList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casFurnList,onBoardingChecklistWrk,'Furnishing');
           CaseOnboardingHelper.OnboadringWorkorder(casFurnList,onBoardingWrk,'DTH');
           CaseOnboardingHelper.OnboadringWorkorder(casFurnList,onBoardingWrk,'Wifi');
        }
        if(!casMakeList.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorder(casMakeList,onBoardingWrk,'Make House Live');
        }
         if(!casDropMap.isEmpty()){
           CaseOnboardingHelper.OnboadringWorkorderDrop(casDropMap);
        }
        
    }
}