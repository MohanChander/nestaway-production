public class WorkOrderOnboardingController {
    Public List<WorkOrder> workoderList{get;set;}
    Public List<Room_Inspection__c > RoomInspectionList{get;set;}
    Public List<House_Inspection_Checklist__c> HouseInspectionList{get;set;}
    public String  StringHouseInspectionID;
    public String RoomInspectionID;
    Public String workorderid;
    public String StringHouseID;
    public WorkOrderOnboardingController (ApexPages.StandardController sController) {
        
        workoderList=[select ID,House__c,(select id,name  from Checklist__r ),(select id,name  from Room_Inspection__r)  from WorkOrder WHERE id =: ApexPages.currentPage().getParameters().get('id')];
        workorderid=workoderList[0].id;
        StringHouseID=workoderList[0].House__c;
        if(StringHouseID!=null){
        HouseInspectionList=[Select id,name from House_Inspection_Checklist__c where house__c=:StringHouseID and Type_Of_HIC__c =  'House Inspection Checklist' ];
        StringHouseInspectionID=HouseInspectionList[0].id;
        }
        if(StringHouseInspectionID!=null){
        system.debug('HouseInspectionList'+HouseInspectionList);     
        RoomInspectionList=[select id,Name,Has_an_attached_bathroom__c   from Room_Inspection__c where House_Inspection_Checklist__c=:StringHouseInspectionID];
        
        system.debug('workoderList'+workoderList); 
        }
    } 
    
}