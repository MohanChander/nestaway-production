/**********************************************
Created By : Deepak
Purpose    : Property Manager Assignment Logic for Onboarding, Offboarding WorkOrder
Invocation : During the Onboarding, Offboarding WorkOrder Creation
**********************************************/
public class WorkOrderAPMAssignment {
    public static  Id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Move In  WorkOrder').getRecordTypeId();
    public static void assignmentOfOwners(List<WorkOrder> woList){
        Set<Id> SetofHouse = new Set<Id>();
        Set<Id> SetofHouse1 = new Set<Id>(); 
        Map<Id,Id> mapOfHouseAPM = new  Map<Id,Id>();
        Map<Id, Integer> openWorkOrderMap = new Map<Id, Integer>();
        Map<Id,Id> mapOfHouseProperotyManager = new  Map<Id,Id>();
        Map<Id,String> mapOfHOuseandZone = new  Map<Id,String>();
        //Map of Zone and the related Property Managers
        Set<id> houseSetToBeUpdated = new Set<id>();
        Map<String, Set<Id>> zoneAndAPmMap = new Map<String, Set<Id>>();
        List<HOuse__C> listOfHOuseToBeUPdated = new List<House__C>();
        Map<String,Id> cityToHeadId = new Map<String,Id>();
        Map<Id,House__C> mapOfHouse = new Map<ID,House__c>();
        List<House__C> houseListToBeUpdated = new  List<House__C>();
        List<WorkOrder> workOrderListToUPdated = new List<WorkOrder>();
        try{
            for(WorkOrder each: woList){
                SetofHouse.add(each.House__C);
            }
            if(!SetofHouse.isEmpty()){
                List<House__C> houseList = [Select id,Property_Manager__c,PM_Zone_Code__c,Onboarding_Zone_Code__c ,APM__c,city__c  from House__C where id in:SetofHouse];
                for(House__c each:HouseList){
                    mapOfHouse.put(each.id,each);
                }
            }
            
            
            List<City__c> cityList= [ Select id,name, Operation_Head__c from City__c where Operation_Head__c!=null];
            for(City__c c: cityList){
                cityToHeadId.put(c.name,c.Operation_Head__c);
            }
            
            
            
            
            for(WorkOrder each:woList){
                if(each.recordtypeid !=moveInWorkOrderRTId){
                    if(mapOfHouse.get(each.house__C).apm__c !=null ){
                        each.OwnerId  = mapOfHouse.get(each.house__C).APM__c;
                    }
                    else{
                        workOrderListToUPdated.add(each);
                        houseSetToBeUpdated.add(each.House__C);
                    }
                }       
            }
            if(!houseSetToBeUpdated.isEmpty()){
                houseListToBeUpdated= [Select Id, Onboarding_Zone_Code__c, Property_Manager__c, APM__c
                                        from house__c where id in:houseSetToBeUpdated];
            }
            if(!houseListToBeUpdated.isEmpty()){
                HouseTriggerHelper.populatePmOnHouse(houseListToBeUpdated,False);
            }
            if(!workOrderListToUPdated.isEmpty()){
                for(WorkOrder each: workOrderListToUPdated){
                    SetofHouse1.add(each.House__C);
                }
            }
            
            if(!SetofHouse1.isEmpty()){
                List<House__C> houseList = [Select id,Property_Manager__c,PM_Zone_Code__c, APM__c,city__c,Onboarding_Zone_Code__c  from House__C where id in:SetofHouse1];
                for(House__c each:HouseList){
                    mapOfHouse.put(each.id,each);
                }
            }
            
            system.debug('houseListToBeUpdated'+houseListToBeUpdated);
            if(!workOrderListToUPdated.isEmpty()){
                for(WorkOrder each:workOrderListToUPdated){
                    if(each.recordtypeid !=moveInWorkOrderRTId){
                        if(mapOfHouse.get(each.house__C).APM__c !=null ){
                            each.OwnerId  =mapOfHouse.get(each.house__C).APM__c;
                        }
                        else if(mapOfHouse.get(each.house__C).Property_Manager__c !=null){
                            each.OwnerId  = mapOfHouse.get(each.house__C).Property_Manager__c;
                        }
                        else if(mapOfHouse.containsKey(each.house__C) && mapOfHouse.get(each.house__c).city__c != null && cityToHeadId.containsKey(mapOfHouse.get(each.house__c).city__c)){
                            each.ownerId=cityToHeadId.get(mapOfHouse.get(each.house__c).city__c);
                        }
                        else
                        {}
                    }       
                }
            }
        }
        Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);              
        }
    }
}