public  class ChangeAssigneeToWorlkorderExtension {

    public id wrkid;
    public DateTime StartTime {get;set;}
    public DateTime EndTime {get;set;}
    public List<SelectOption> userOption1 {get;set;}
    public id ownerId;
    public Boolean vendor {get;set;}
    public Boolean techni {get;set;}
    public List<User> vendorUsrList;
    public String techniName {get;set;}
    public String username {get;set;}
    public String vendername {get;set;}
    public String assignTo {get;set;}
    public Workorder wrk {get;set;}
    public List<SelectOption> userOptions {get;set;}  
    public List<SelectOption> venderOptions {get;set;}
    public Integer wrkCount {get;set;}
    public String addErrorMessage;
    public boolean assintovender {get;set;}
    public boolean assintoaggre {get;set;}
    public boolean hide {get;set;}
    public boolean snm {get;set;}
    public Boolean isStartTime {get; set;}
    public User use;

    public ChangeAssigneeToWorlkorderExtension(ApexPages.StandardController stdController) 
    {
        wrkid = stdController.getRecord().id;
        wrk=[Select id,ownerId,StartDate,EndDate,Work_Start_Time__c,case.Zone_Code__c,Problem__r.Skills__c,Problem__c from workorder where id=:wrkid];
        StartTime = wrk.StartDate;
        EndTime = wrk.EndDate;
        ownerId=wrk.ownerId;
        isStartTime = true;
        List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        userOptions = new List<SelectOption>();
        venderOptions = new List<SelectOption>();
        Set<string> prof=new Set<String>(label.Snm_profile_access.split(';'));
        Map<id,Profile> admin=new Map<id,Profile>([Select id,name from profile where name='System Administrator' or name=:prof]);
        User currUsr=[Select id,Type_of_Contact__c from user where id=:UserInfo.getUserId()];
         if((currUsr.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_VENDOR || admin.get(UserInfo.getProfileID()).name=='System Administrator') && wrk.Work_Start_Time__c==null)
         {
            vendor=true;
            techni=true;
            System.debug('**Inside the admin block'+ admin.get(UserInfo.getProfileID()).name);
            hide=true;
            snm=false;
         }
         else if(label.Snm_profile_access.contains(admin.get(UserInfo.getProfileID()).name))
         {
            vendor=false;
            techni=false;
            hide=false;
            snm=true;
            System.debug(hide+'**Inside the Snm block'+ admin.get(UserInfo.getProfileID()).name);
         }
         else if(currUsr.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_TECHNICIAN && wrk.Work_Start_Time__c==null)
         {
            vendor=false;
            techni=true;
            hide=true;
         }
         else if(wrk.Work_Start_Time__c==null)
         {  System.debug('***bony***');
            vendor=false;
            techni=false;
            hide=true;
            String addErrorMessage='You are neither Vendor nor Technician';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
         }
         else if(wrk.Work_Start_Time__c!=null)
         {  System.debug('***bony***');
            vendor=false;
            techni=false;
            String addErrorMessage='Once the Work is Started you cannot reassign or change time slot';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
         }
         use=[Select id,Account.Vendor_Type__c,Vendor_Account_name__c,name,Type_of_Contact__c from User  where id=:wrk.Ownerid];
        if(((use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_INHOUSE) ||(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY)) && use.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_TECHNICIAN)
        {
           userOptions.add(new SelectOption(use.name,use.name+' - ('+use.Vendor_Account_name__c+')'));
           venderOptions.add(new SelectOption('-None-','-None-'));
        }
        else if(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS && use.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_VENDOR)
        {
           venderOptions.add(new SelectOption(use.name,use.name));
           userOptions.add(new SelectOption('-None-','-None-'));
        }

       Option1();
       Options();
    }
    /**************************************************************************************************************************************/        
    public void Option1()
    {   
        userOption1=new List<SelectOption>();
        User owner =[Select id,name,AccountID,Vendor_Type__c,contactId from User where id=:ownerId];
        System.debug('******AccountID****'+owner.AccountId);
        if(owner.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_INHOUSE && owner.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY)
        {
                if(owner.AccountId!=null)
                {
                    Map<id,User> useMap = new Map<id,User>([Select id,name from user where AccountID=:owner.AccountID and Contact.Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN]);
                    List<Holiday__c> holiList = [select User__c,End_Date_Time__c,Start_Date_Time__c from Holiday__c where User__c=:useMap.keySet() and ((( Start_Date_Time__c < :StartTime)  and (End_Date_Time__c > :StartTime ))or ((Start_Date_Time__c < :EndTime) and (End_Date_Time__c > :EndTime)))];
                    Map<id,List<Holiday__c>> holiMapUsrId = new Map<id,List<Holiday__c>>();
                    List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:useMap.keySet() and ((( StartDate <= :StartTime)  and (EndDate > :StartTime ))or ((StartDate < :EndTime) and (EndDate >= :EndTime)))];
                    Map<id,List<Workorder>> wrkMapUsrId = new Map<id,List<Workorder>>();
                    for(id us:useMap.keySet())
                    {
                       List<Holiday__c> holi=new List<Holiday__c>();
                       List<Workorder> wrk=new List<Workorder>();
                       if(holiList!=null)
                       {           
                           for(Holiday__c ho:holiList)
                           {
                             if(ho.User__c==us)
                               holi.add(ho);
                           }   
                           holiMapUsrId.put(us,holi);
                       }
                         if(wrkList!=null)
                       {           
                           for(Workorder wo:wrkList)
                           {
                             if(wo.OwnerId==us)
                               wrk.add(wo);
                           }   
                           wrkMapUsrId.put(us,wrk);
                       }
                    }
                    Set<id> userIdToPass = new set<Id>();
                    for(id ui:useMap.keySet())
                    {
                        if((holiMapUsrId.get(ui)==null || holiMapUsrId.get(ui).size()==0) && (wrkMapUsrId.get(ui)==null || wrkMapUsrId.get(ui).size()==0))
                        {
                           userIdToPass.add(ui); 
                        }
                    }
                    vendorUsrList=[Select id,name from user where id=:userIdToPass];
            }
        }
        else
        {
            if(owner.AccountId!=null)
            {
                vendorUsrList=[Select id,name from user where AccountID=:owner.AccountID and Contact.Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN];
            }

        }

        userOption1.add(new SelectOption('-None-','-None-'));
        if(!vendorUsrList.isEmpty())
        {
            for(User us:vendorUsrList)
            {
                userOption1.add(new SelectOption(us.name,us.name));
            }
        }
    }
    /******************************************************************************************************************************************/        
    public pagereference UpdateWrk()
    {    

        System.debug('**UpdateWrk');
        System.debug('*********values' + vendor + techni + techniName);

         if(vendor && techni && techniName != '-None-')
         {
            User ur=[Select id from user where name=:techniName];
            wrk.ownerid=ur.id;
            System.debug('**Inside the User selection block');
         }

         wrk.StartDate=StartTime;
         wrk.EndDate=EndTime;
         Update wrk;
         System.debug('wrok'+wrk);
         PageReference parentPage;
         if(wrk!=null){
            parentPage = new PageReference('/' + wrk.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        return null;
    }
    /**************************************************************************************************************************************************************/
    public pagereference Cancel()
    {

         PageReference parentPage;
         if(wrk!=null){
            parentPage = new PageReference('/' + wrk.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        return null;

    }
/***************************************************************************************************************************************************************************/
 public List<SelectOption> getAssignOptions()
    {
        List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        return assignOptions;
    }
     public void Assign()
     {
     if(assignTo=='Assign To Aggregator') 
     {
        assintovender=true;
        assintoaggre=false;
     } 
     else if(assignTo=='Assign To Technician') 
     {
        assintovender=false;
        assintoaggre=true;
     } 

    }
    /********************************************************************************************************************************************************************************************/
       public void Options()
       { 
        if(startTime != null && isStartTime){
            endTime = startTime.addHours(1);

            System.debug(startTime);
            System.debug(endTime);
        }
        userOptions = new List<SelectOption>();
        venderOptions = new List<SelectOption>();
        if(((use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_INHOUSE) ||(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY)) && use.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_TECHNICIAN)
        {
           userOptions.add(new SelectOption(use.name,use.name+' - ('+use.Vendor_Account_name__c+')'));
           venderOptions.add(new SelectOption('-None-','-None-'));
        }
        else if(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS && use.Type_of_Contact__c==Constants.TYPE_OF_CONTACT_VENDOR)
        {
           venderOptions.add(new SelectOption(use.name,use.name));
           userOptions.add(new SelectOption('-None-','-None-'));
        }
            
        System.debug('*******'+startTime);
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:wrk.case.Zone_Code__c and isActive__c=true and (Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN or Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_VENDOR)];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<Holiday__c> holiList = [select User__c,End_Date_Time__c,Start_Date_Time__c from Holiday__c where User__c=:usrIDSet and ((( Start_Date_Time__c < :startTime)  and (End_Date_Time__c > :startTime ))or ((Start_Date_Time__c < :endTime) and (End_Date_Time__c > :endTime)))];
        Map<id,List<Holiday__c>> holiMapUsrId = new Map<id,List<Holiday__c>>();
        List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:usrIDSet and status!=:Constants.CASE_STATUS_RESOLVED and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
        Map<id,List<Workorder>> wrkMapUsrId = new Map<id,List<Workorder>>();
        for(id us:usrIDSet)
        {
           List<Holiday__c> holi=new List<Holiday__c>();
           List<Workorder> wrk=new List<Workorder>();
           if(holiList!=null)
           {           
               for(Holiday__c ho:holiList)
               {
                 if(ho.User__c==us)
                   holi.add(ho);
               }   
               holiMapUsrId.put(us,holi);
           }
             if(wrkList!=null)
           {           
               for(Workorder wo:wrkList)
               {
                 if(wo.OwnerId==us)
                   wrk.add(wo);
               }   
               wrkMapUsrId.put(us,wrk);
           }
        }

        Set<id> userIdToPass = new set<Id>();
        for(id ui:usrIDSet)
        {
            if((holiMapUsrId.get(ui)==null || holiMapUsrId.get(ui).size()==0) && (wrkMapUsrId.get(ui)==null || wrkMapUsrId.get(ui).size()==0))
            {
               userIdToPass.add(ui); 
            }
        }
        List<User> usrList=[select id,username,name,Vendor_Account_name__c from User where id=:userIdToPass and ((Vendor_Capability__c Includes(:wrk.Problem__r.Skills__c)) OR (Vendor_Capability__c Includes('Generalist'))) and ((Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_INHOUSE) or (Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY))];
        System.debug('******USERLIST****'+usrList);
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                userOptions.add(new SelectOption(us.name,us.name+' - ('+us.Vendor_Account_name__c+')'));
            }
        }
        Vender();
    }
/********************************************************************************************************************************************************************************************/
     public void Vender() 
    {
        
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:wrk.case.Zone_Code__c and isActive__c=true and Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_VENDOR];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<User> usrList=[select id,username,name from User where id=:usrIDSet and (Vendor_Capability__c Includes(:wrk.Problem__r.Skills__c) OR Vendor_Capability__c Includes('Generalist')) and (Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS)];
        System.debug('*******Vender USer***'+usrList);
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                venderOptions.add(new SelectOption(us.name,us.name));
            }
         }   
    }
/********************************************************************************************************************************************************************************************/
      public void VenderWrkCount()
     {  if(vendername!=null)
        {  System.debug('*****Count*******');
                    User us = [select id,name from user where name=:vendername];
                    List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:us.id and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
                    
                    if(wrkList!=null || !wrkList.isEmpty())
                    {
                      wrkCount=wrkList.size();
                    }
                    else
                    {   
                       wrkCount=0;
                    }
        }
        else
        wrkCount=0;

        System.debug(vendername+'**********count*****'+wrkCount);
     }
     /*************************************************************************************************************************************************************************************************************************************/
   public pagereference InsertWorkorder()
    { 
        try
        {     
            Case cas=[select id,Service_Visit_Time__c from Case where id=:wrk.Caseid];
              User assignTO =new User();
               if(startTime==null || endTime==null)
                {
                   addErrorMessage='StartTime and EndTime Cannot be bkank.'; 
                }
                 if(startTime > endTime)
                {
                   addErrorMessage='StartTime should be smaller then EndTime.'; 
                }

               wrk.StartDate=startTime;
               wrk.EndDate=endTime;
                if(assintovender==false)
                {
                    assignTO=[Select id from user where name=:username];
                    wrk.ownerid=assignTO.id;
                }
                else 
                {
                    assignTO=[Select id from user where name=:vendername];
                    wrk.ownerid=assignTO.id;
                }
                 System.debug('******Bony1******'+assignTO);
                 Update wrk;
                 cas.Service_Visit_Time__c=startTime;
                 update cas;
                 PageReference parentPage;
                if(wrk!=null)
                {
                    parentPage = new PageReference('/' + wrk.Id);
                    parentPage.setRedirect(true);
                    return parentPage;
                } 
        }
        catch (Exception e)
        {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
        }
        return null;
    }
}