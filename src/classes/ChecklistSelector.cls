/*  Created By   : Mohan - WarpDrive Tech Works
    Created Date : 19/05/2017
    Purpose      : All the queries related to the Checklist Object are here */

public class ChecklistSelector {

    //get List of Checklist records with Room Inspection and Detail child records from Opportunity Id Set
    public Static List<House_Inspection_Checklist__c> getChecklistWithRoomsAndKeys(Set<Id> oppIdSet){
        return [select Id, Opportunity__c, (select Id, House__c from Room_Inspection__r), (select Id, House__c from Detail__r)
                from House_Inspection_Checklist__c where Opportunity__c =: oppIdSet and 
                Type_Of_HIC__c =:Constants.CHECKLIST_RT_House_Inspection_Checklist];
    }


    //get Hic List with House Status
    public static List<House_Inspection_Checklist__c> getHicWithHouseStatus(Set<Id> hicIdSet){

        return [select Id, House__c, House__r.Stage__c from House_Inspection_Checklist__c 
                where Id =: hicIdSet and House__c != null];
    }

    //get Checklist details for Move in Check and Move out Check
    public static List<House_Inspection_Checklist__c> getHicForMIMOCheck(Set<Id> houseIdSet){

        String typeHIC = Constants.CHECKLIST_TYPE_HIC;
        String typeMimo = Constants.CHECKLIST_TYPE_MIMO_CHECK;
        String recordTypeHic = Constants.CHECKLIST_RT_House_Inspection_Checklist;

        String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_CHECKLIST);
        String filterString = ' from House_Inspection_Checklist__c where (House__c =: houseIdSet ' +  
                  'or HouseForMIMO__c =: houseIdSet) and (Type_Of_HIC__c =: typeHIC or ' +
                  'Type_Of_HIC__c =: typeMimo ' +
                  'or (Type_Of_HIC__c = null and RecordType.Name =: recordTypeHic)) ';
        List<House_Inspection_Checklist__c> hicList = (List<House_Inspection_Checklist__c>)Database.query(queryString + filterString);
        return hicList;
    }

    //get Move In - Move Out Checklist details related to the House
    public static List<House_Inspection_Checklist__c> getMoveInMoveOutChecklistRelatedToHouse(Set<Id> houseIdSet){
        return [select Id, Name, Type_Of_HIC__c, HouseForMIMO__c from House_Inspection_Checklist__c where HouseForMIMO__c =: houseIdSet 
                order by CreatedDate desc];
    }

    //get Move in and Move Out checklist details related to the WorkOrder
    public static List<House_Inspection_Checklist__c> getHicForWorkOrders(Set<Id> woIdSet){

        String typeMoveIn = Constants.CHECKLIST_TYPE_MOVE_IN_CHECK;
        String typeMoveOut = Constants.CHECKLIST_TYPE_MOVE_OUT_CHECK;       
        String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_CHECKLIST);
        String filterString = ', HouseForMIMO__r.Name from House_Inspection_Checklist__c where Work_Order__c =: woIdSet ' +  
                  'and (Type_Of_HIC__c =: typeMoveIn or ' +
                  'Type_Of_HIC__c =: typeMoveOut) ';
        List<House_Inspection_Checklist__c> hicList = (List<House_Inspection_Checklist__c>)Database.query(queryString + filterString);
        return hicList;               
    }

    //Query for All open Move In & Move Out HIC checklists unverified
    public static List<House_Inspection_Checklist__c> getUnverifiedChecklists(Set<Id> houseIdSet){

        String typeMoveIn = Constants.CHECKLIST_TYPE_MOVE_IN_CHECK;
        String typeMoveOut = Constants.CHECKLIST_TYPE_MOVE_OUT_CHECK;   
        Id moveInRtId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Move in Check').getRecordTypeId();
        Id moveOutRtId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Move Out Check').getRecordTypeId();
        string stringNo = 'No';

        String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_CHECKLIST);
        String filterString = ' from House_Inspection_Checklist__c where HouseForMIMO__c =: houseIdSet ' +  
                  'and ((Type_Of_HIC__c =: typeMoveIn and RecordTypeId =: moveInRtId) or ' +
                  '(Type_Of_HIC__c =: typeMoveOut and RecordTypeId =: moveInRtId)) and ' +
                  '(CheckList_Verified__c =: stringNo or CheckList_Verified__c = null)';  
        List<House_Inspection_Checklist__c> hicList = (List<House_Inspection_Checklist__c>)Database.query(queryString + filterString);
        return hicList; 
    }       
}