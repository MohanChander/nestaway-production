/*
* Description: It's Contract trigger handler class and updates the contract and opportunity records based on few condition.
*/

Public Class ContractTriggerHandler{
    
    Public Static Void AfterUpdate(map<Id,Contract> newMap,map<Id,Contract> oldMap){
        list<Opportunity> lstOppUpdate = new list<Opportunity>();
        list<Quote> lstQuoteToUdpdate = new list<Quote>();
        set<Id> OppIdSet = new set<Id>();
        set<Id> setOppId = new set<Id>();
        set<Id> OppId = new set<Id>();
    //    Set<Id> setOfContract = new Set<Id>();
        map<id,Decimal> sdamount = new map<id,Decimal>();
        map<Id,Contract> createSDUpfrontWO = new map<Id,Contract>();
        map<Id,Contract> versioningCreateSDUpfrontWO = new map<Id,Contract>();
        map<Id,Contract> cancelSDUpfrontWO = new map<Id,Contract>();
        map<Id,Contract> updateSDUpfrontAmountInWO = new map<Id,Contract>();
        List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();
        List<Contract> contListForInsurance = new List<Contract>();                    

                                
        for(Contract each : newMap.values()){
            
            if(each.Agreement_Type__c != null && oldMap.get(each.Id).Agreement_Type__c  == Constants.CONTRACT_AGREEMENT_TYPE_E_LOI  && (each.Agreement_Type__c ==Constants.CONTRACT_AGREEMENT_TYPE_E_STAMPED  || each.Agreement_Type__c == Constants.CONTRACT_AGREEMENT_TYPE_E_NOTARIZED ) ){
                contListForInsurance.add(each);
            }
            
            /* Chandu - Commented - 16/11/2017 - Versioning Deployment : no need to take this now.
             if(each.Approval_Status__c  !=null && each.Approval_Status__c  != oldMap.get(each.Id).Approval_Status__c   && each.isCloneContract__c && each.Parent_Contract__c!=null && each.Approval_Status__c =='Approved by Central Team' ){
             
                 setOfContract.add(each.id);
            } 
            */
            if((each.Furnishing_Plan__c!=null && oldMap.get(each.Id).Furnishing_Plan__c == null && each.Furnishing_Plan__c=='Owner furnished NestAway provided') || (oldMap.get(each.Id).Furnishing_Plan__c!='Owner furnished NestAway provided' && each.Furnishing_Plan__c=='Owner furnished NestAway provided')){ //each.Agreement_Type__c != 'LOI' && 
                Opportunity opp = new Opportunity(Id=each.Opportunity__c,StageName='Quote Creation');
                lstOppUpdate.add(opp);
            }
            if(!each.isCloneContract__c && each.Approval_Status__c=='Approved By Central Team' && each.Approval_Status__c != oldMap.get(each.Id).Approval_Status__c){  // && each.Agreement_Type__c != 'LOI'
                    //Calling JSON Method
                    HouseJson.createJSONContract(each.Id);
             }
             
             if(each.isCloneContract__c && (each.Approval_Status__c==Constants.CONTRATC_APPROVAL_STATUS_APPROVED_BY_OWNER || each.Approval_Status__c=='Cloned Contract Manually Approved by ZM') && each.Approval_Status__c != oldMap.get(each.Id).Approval_Status__c){
                 
                  HouseJson.createJSONContract(each.Id);
                  
                  if(each.Create_SD_WO_IF_Tenant_To_Nestaway__c){
                      
                      versioningCreateSDUpfrontWO.put(each.id,each);
                  }
             }
             
             
             
          /*  else if(each.Agreement_Type__c == 'LOI' && each.Status != 'Cancelled' && each.Approval_Status__c != oldMap.get(each.Id).Approval_Status__c && ((each.Approval_Status__c=='Approved By Central Team' && each.Monthly_Rental_Commission__c < 0.075) || (each.Approval_Status__c=='Approved by HRM'&& each.Monthly_Rental_Commission__c >= 0.075))){
               Opportunity opp = new Opportunity(Id=each.Opportunity__c,StageName='House');
               lstOppUpdate.add(opp);
            } */
            if(each.Status=='Final Contract' && each.Status != oldMap.get(each.Id).Status ){  //&& each.Agreement_Type__c != 'LOI'
                
                
                OppIdSet.add(each.Opportunity__c);
                Opportunity opp = new Opportunity(Id=each.Opportunity__c,StageName='Final Contract');
                lstOppUpdate.add(opp);
            }
            if(each.Status=='Final Contract'  && each.Approval_Status__c=='Approved by Central Team' && each.Approval_Status__c != oldMap.get(each.Id).Approval_Status__c ){               
               
                Opportunity opp = new Opportunity(Id=each.Opportunity__c,StageName='Docs Collected');
                lstOppUpdate.add(opp);
            }
            
            else if (each.Status=='Cancelled' && each.Status != oldMap.get(each.Id).Status){
                Opportunity opp = new Opportunity(Id=each.Opportunity__c,StageName='Lost',Lost_Reason__c='Contract Cancelled');
                lstOppUpdate.add(opp);
            }else if(each.Status=='To be Revised' && each.Status != oldMap.get(each.Id).Status){
                   setOppId.add(each.Opportunity__c); 
            }
            if(each.SD_Upfront_Amount__c != oldMap.get(each.Id).SD_Upfront_Amount__c && each.Status=='To be Revised'){
                oppid.add(each.Opportunity__c); 
                sdamount.put(each.Opportunity__c,each.SD_Upfront_Amount__c);
            }
            // added by chandu: to check the SD upfront case
            if(each.Who_pays_Deposit__c!=oldMap.get(each.Id).Who_pays_Deposit__c){
                
                
                if(each.Who_pays_Deposit__c==null || each.Who_pays_Deposit__c=='Tenant'){
                    
                    cancelSDUpfrontWO.put(each.id,each);
                }
                else if(each.Who_pays_Deposit__c=='NestAway'){
                    
                    
                      if(each.isCloneContract__c){
                         // versioningCreateSDUpfrontWO.put(each.id,each);
                         // this will create after approval.
                      }
                      else{
                          createSDUpfrontWO.put(each.id,each);
                      }
                      
                    
                }
                
            }
            if(each.SD_Upfront_Amount__c!=oldMap.get(each.Id).SD_Upfront_Amount__c){
                
                 updateSDUpfrontAmountInWO.put(each.id,each);
            }      
            
            
            
        }
        
        /* Chandu - Commented - 16/11/2017 - Versioning Deployment : no need to take this now.
        // Added by deepak 
        // to insert case for house once approval status chnange to Approved by Central Team  
        if(!setOfContract.isEmpty()){
            system.debug('**setOfContract***'+setOfContract);
            CaseHouseOnBoardingHelper.insertCaseOnBoarding(setOfContract);
        }
        */
        
        if(oppid != NULL){
                list<House_Inspection_Checklist__c> dcc = [select id, Status__c,Opportunity__c from House_Inspection_Checklist__c
                                                             where Opportunity__c In: oppId and 
                                                             recordtype.name =: 'Document Collection Checklist'];
                if(dcc.size() > 0){
                    for(House_Inspection_Checklist__c hic : dcc){
                        hic.status__c = 'Draft';
                        hic.SD_Upfront_amount__c = sdamount.get(hic.Opportunity__c);
                    }
                }
                
                if(dcc.size() > 0){                                             
                    try{
                        update dcc;
                    }
                    catch(Exception ex){
                        system.debug('Error while updating DCC');
                    }    
                }    
        }
        
        if(setOppId.size()>0){
            RevisedQuote_SalesOrder(setOppId);
        }
        if(OppIdSet.size()>0){
           lstQuoteToUdpdate = [SELECT id,Status,OpportunityId FROM Quote WHERE OpportunityId IN: OppIdSet AND Status='To be Revised']; 
           if(lstQuoteToUdpdate.size()>0){
               for(Quote each : lstQuoteToUdpdate){
                   each.Status='Draft';
               }
               try{
                   update lstQuoteToUdpdate;
               }catch(Exception ex){
                   System.debug('==exception while Updating Quote to Draft in ContractTriggerHandler=='+ex);
               }
           }
        }
        
        if(lstOppUpdate.size()>0){
            try{
                update lstOppUpdate;
            }catch(exception ex){
                System.Debug('==Exception while updating Opp in ContractHandler=='+ex);
            }
        }
        
        // Chandu: adding method for SD upfront validation.
         system.debug('*****cancelSDUpfrontWO'+cancelSDUpfrontWO);
        if(!cancelSDUpfrontWO.isEmpty()){
            
                Id ownerSdPayment = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();      
                Id ownerSdPaymentReadOnly = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT_READ_ONLY).getRecordTypeId();  
             List<House__c> allHousesLst=[select id,Contract__c,(select id,Status,Upfront_amount__c from Work_Orders__r where (recordtypeId=:ownerSdPayment OR recordtypeId=:ownerSdPaymentReadOnly) limit 1) from house__c where Contract__c IN:cancelSDUpfrontWO.keySet()];
            List<Workorder> updateWolst= new List<Workorder>();
            for(House__c house: allHousesLst){
                
                
                   if(house.Work_Orders__r!=null && house.Work_Orders__r.size()==1){
                       
                       workorder wo=house.Work_Orders__r.get(0);
                       
                       if(wo.Status=='Verified' || wo.Status=='Completed' || wo.Status=='Submitted'){
                           
                           cancelSDUpfrontWO.get(house.Contract__c).addError('You can not change the Who pays Deposit.');
                       }
                       else{
                           
                           wo.status='Canceled';
                           updateWolst.add(wo);
                       }
                       
                   }
            }
            system.debug('*****updateWolst'+updateWolst);
            if(updateWolst.size()>0){
                
                update updateWolst;
            }
            
        }
        
        if(!createSDUpfrontWO.isEmpty()){
            
             Id ownerSdPayment = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();      
             Id ownerSdPaymentReadOnly = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT_READ_ONLY).getRecordTypeId();  
             List<House__c> allHousesLst=[select id,Contract__c,(select id,Status,Upfront_amount__c from Work_Orders__r where (recordtypeId=:ownerSdPayment OR recordtypeId=:ownerSdPaymentReadOnly) limit 1) from house__c where Contract__c IN:createSDUpfrontWO.keySet()];
            List<Workorder> updateWolst= new List<Workorder>();
            for(House__c house: allHousesLst){
                
                Contract con=createSDUpfrontWO.get(house.Contract__c);
                
                   if(house.Work_Orders__r!=null && house.Work_Orders__r.size()==1){
                       
                       workorder wo=house.Work_Orders__r.get(0);
                       
                       if(wo.Status=='Verified' || wo.Status=='Completed' || wo.Status=='Submitted'){
                           
                           createSDUpfrontWO.get(house.Contract__c).addError('You can not change the Who pays Deposit.');
                       }
                       else{
                           
                           wo.status='Open';
                           wo.Upfront_amount__c=con.SD_Upfront_Amount__c;
                           updateWolst.add(wo);
                       }
                       
                   }
                   else{
                          
                          if(con.SD_Upfront_Amount__c!=null && con.SD_Upfront_Amount__c>0){

                                Workorder wrk = new Workorder();
                                wrk.House__c=house.id;                          
                                if(con.AccountId!=null){
                                wrk.AccountId=con.AccountId;
                                }


                                if(con.Name__c!=null)
                                wrk.Agreement_Name__c=con.Name__c;

                                if(con.Security_Deposit_Payment_Mode__c!=null)
                                wrk.Payment_mode__c=con.Security_Deposit_Payment_Mode__c;

                                wrk.RecordTypeId=ownerSdPayment;
                                wrk.subject='Upfront SD';
                                wrk.Upfront_amount__c=con.SD_Upfront_Amount__c;
                                wrk.Security_Deposit_Payment_Details__c=con.Security_Deposit_Payment_Details__c; 
                                if(con.Opportunity__c!=null && con.Opportunity__r.OwnerId!=null && string.ValueOf(con.Opportunity__r.OwnerId).startsWith('005')){   
                                wrk.ownerId=con.Opportunity__r.OwnerId;
                                }

                                updateWolst.add(wrk);
                            }
                }
            }
            
            if(updateWolst.size()>0){
                
                upsert updateWolst;
            }

        }
        if(!updateSDUpfrontAmountInWO.isEmpty()){
            
                Id ownerSdPayment = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();      
                Id ownerSdPaymentReadOnly = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT_READ_ONLY).getRecordTypeId();  
                 List<House__c> allHousesLst=[select id,Contract__c,(select id,Status,Upfront_amount__c from Work_Orders__r where (recordtypeId=:ownerSdPayment OR recordtypeId=:ownerSdPaymentReadOnly) limit 1) from house__c where Contract__c IN:updateSDUpfrontAmountInWO.keySet()];
                List<Workorder> updateWolst= new List<Workorder>();
                for(House__c house: allHousesLst){
                    
                    
                       if(house.Work_Orders__r!=null && house.Work_Orders__r.size()==1){
                           
                           workorder wo=house.Work_Orders__r.get(0);
                           
                           if(wo.Status=='Verified' || wo.Status=='Completed' || wo.Status=='Submitted'){
                               
                               updateSDUpfrontAmountInWO.get(house.Contract__c).addError('You can not edit upfornt amount for this contract.');
                           }
                           else{
                               
                                   // wo.status='Open';
                                    wo.Upfront_amount__c=updateSDUpfrontAmountInWO.get(house.Contract__c).SD_Upfront_Amount__c;
                                    updateWolst.add(wo);
                           }
                           
                       }
                }

                if(updateWolst.size()>0){
                    
                    update updateWolst;
                }
            
            
        }
        
        // for versioning create sd workorder
          if(!versioningCreateSDUpfrontWO.isEmpty()){
              
              Id ownerSdPayment = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId(); 
              List<Workorder> updateWolst= new List<Workorder>();
              for(contract con : versioningCreateSDUpfrontWO.values()){
                  
                    if(con.SD_Upfront_Amount__c!=null && con.house__c!=null && con.SD_Upfront_Amount__c>0){

                                Workorder wrk = new Workorder();
                                wrk.House__c=con.house__c;
                                if(con.AccountId!=null){
                                wrk.AccountId=con.AccountId;
                                }


                                if(con.Name__c!=null)
                                wrk.Agreement_Name__c=con.Name__c;

                                if(con.Security_Deposit_Payment_Mode__c!=null)
                                wrk.Payment_mode__c=con.Security_Deposit_Payment_Mode__c;

                                wrk.RecordTypeId=ownerSdPayment;
                                wrk.subject='Upfront SD';
                                wrk.Upfront_amount__c=con.SD_Upfront_Amount__c;
                                wrk.Security_Deposit_Payment_Details__c=con.Security_Deposit_Payment_Details__c; 
                                if(con.Opportunity__c!=null && con.Opportunity__r.OwnerId!=null && string.ValueOf(con.Opportunity__r.OwnerId).startsWith('005')){   
                                wrk.ownerId=con.Opportunity__r.OwnerId;
                                }

                                updateWolst.add(wrk);
                    }
                  
              }

              if(updateWolst.size()>0){
                  
                  insert updateWolst;
              }
            

          }
           if(!contListForInsurance.IsEmpty()){
            ContractTriggerHelper.sendApiCallForInsurance(contListForInsurance);
        } 
    }
    Public Static Void BeforeUpdate(map<Id,Contract> newMap, map<Id,Contract> oldMap){
        
        /* Chandu - Commented - 16/11/2017 - Versioning Deployment 
         Set<Id> accSet = new Set<Id>();
        for(Contract con:newMap.values())
        {
             if(con.AccountId!=null){   
                 accSet.add(con.AccountId);
             }
          
        }
        */
        ContractTriggerHelper.ContarctAddressFormAccount(newMap.values());
    /* Chandu - Commented - 16/11/2017 - Versioning Deployment. 
       // List<Account> accList = AccountSelector.getAccountDetailsFromSet(accSet);
       // List<Bank_Detail__c> bankList = BankDetailSelector.getBankDetailByAccount(accSet);
     */        
       User currUsr=[Select id,City_Manager__c from User where id=:UserInfo.getUserID()];  
      /* Chandu - Commented - 16/11/2017 - Versioning Deployment.      
        Map<Id,account> accMap=new Map<Id,account>([select id,Api_Success__c,(select id,Api_Success__c,IFSC_Api_Sucess__c from Bank_Details__r order by LastModifiedDate desc limit 1) from account where id IN:accSet]);
        Map<Id,Bank_Detail__c> accToBankDetail=new Map<Id,Bank_Detail__c>();
         */     
        
        Map<Id,Id> conToOppMap = new Map<Id,Id>();
        Set<Id> parentConIds = new Set<Id>();
        Set<Id> checkBedStatusForContractIds = new Set<Id>();
        Set<Id> houseIdsForBedStatus = new Set<Id>();
        Map<Id,Id> contractToHouseMap = new Map<Id,Id>();
    
     // added by baibhav
        List<contract> contList=new List<contract>(); 
         
     /* Chandu - Commented - 16/11/2017 - Versioning Deployment.       
        for(account acc: accMap.values()){
            
            if(acc.Bank_Details__r!=null && acc.Bank_Details__r.size()>0){
                accToBankDetail.put(acc.id,acc.Bank_Details__r.get(0));
            }   
        }
      */ 
        for(Contract each : newMap.values() ){
            if(each.Cancelled__c && !oldMap.get(each.Id).Cancelled__c){
                 each.Status='Cancelled';   
            }
            
            if(each.Revised__c && !oldMap.get(each.Id).Revised__c){
                 each.Status='To be Revised'; 
                 each.Manually_Approved_By_ZM__c = False;  
            }
            
            if(!each.isCloneContract__c && each.Manually_Approved_By_ZM__c && !oldMap.get(each.Id).Manually_Approved_By_ZM__c ){  //&& each.Agreement_Type__c != 'LOI'
               
                 each.Approval_Status__c='Sample Contract Manually Approved by ZM';  
                 each.Status='Final Contract'; 
            }
         /* Chandu - Commented - 16/11/2017 - Versioning Deployment : no need to take this now.   
            If(each.Approval_Status__c=='Awaiting HRM Approval')
            {
               if(each.AccountId!=null && accMap.containsKey(each.AccountId)){
                   account acc=accMap.get(each.AccountId);
                   system.debug('****acc'+acc);
                   if(acc.Api_Success__c==null || acc.Api_Success__c!='true'){
                       
                       each.adderror(Label.Account_Sync_API_Fail_Msg);
                   }                   
                   
               }
               else{
                  // need to see if the account is missing we have to show error or not 
                //  each.adderror(Label.Account_Sync_API_Fail_Msg); 
               }
               
               if(each.AccountId!=null && accToBankDetail.containsKey(each.AccountId)){
                       
                   if(accToBankDetail.get(each.AccountId).Api_Success__c==null || accToBankDetail.get(each.AccountId).Api_Success__c!='true'){
                           each.adderror(Label.Bank_Sync_API_Fail_Msg);
                    }

                   if(accToBankDetail.get(each.AccountId).IFSC_Api_Sucess__c==null || !accToBankDetail.get(each.AccountId).IFSC_Api_Sucess__c.Contains('True')){
                           each.adderror('Bank IFSC code is not valid');
                    }

                }
                
            }
           */
           
            // added by chandu:- for sample contract approval.
            if(each.Agreement_Approval_Code__c==null && each.Approval_Status__c=='Awaiting ZM Approval'){
                
                    String hashString = each.ContractNumber+ String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
                    Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
                    each.Agreement_Approval_Code__c = string.valueOf(each.id).RIGHT(6)+EncodingUtil.convertToHex(hash);
                    
            }
            // added by chandu:- for sample contract approval.
            if(each.Approval_Status__c!=null && each.Approval_Status__c=='Awaiting ZM Approval' && each.Approval_Status__c!=oldMap.get(each.Id).Approval_Status__c){
                
                each.Owner_Approval_Status__c='';
                each.City_Manager__c=currUsr.City_Manager__c ; 
            } 
            // added by chandu : to check if the commercial fields are changed on cloned contract.
            if(each.Approval_Status__c!=null && each.Approval_Status__c!= oldMap.get(each.Id).Approval_Status__c && each.Approval_Status__c=='Awaiting ZM Approval' && each.isCloneContract__c && each.Opportunity__c!=null && each.Parent_Contract__c!=null){
                
                conToOppMap.put(each.id,each.Opportunity__c);
                parentConIds.add(each.Parent_Contract__c);
            } 
            
            if(each.isCloneContract__c && each.House__c!=null && (each.Booking_Type__c!= oldMap.get(each.Id).Booking_Type__c || each.Tenancy_Type__c!= oldMap.get(each.Id).Tenancy_Type__c)){
                
               checkBedStatusForContractIds.add(each.id);
               houseIdsForBedStatus.add(each.House__c);
               contractToHouseMap.put(each.id,each.House__c);
            }
            
            // added by deepak : to check  furnishng type
             if(each.Furnishing_Type__c !=null && each.Furnishing_Type__c != oldMap.get(each.Id).Furnishing_Type__c  && each.isCloneContract__c && each.Parent_Contract__c!=null && each.Furnishing_Type__c =='Semi-furnished' && oldMap.get(each.Id).Furnishing_Type__c =='Fully Furnished'){
               newmap.get(each.id).adderror('Furnishing Type can not be changed from Fully Furnished to Semi-furnished.');
            } 
            if(each.Furnishing_Type__c !=null && each.Furnishing_Type__c != oldMap.get(each.Id).Furnishing_Type__c  && each.isCloneContract__c && each.Parent_Contract__c!=null && each.Furnishing_Type__c =='Unfurnished' && oldMap.get(each.Id).Furnishing_Type__c =='Fully Furnished'){
               newmap.get(each.id).adderror('Furnishing Type can not be changed from Fully Furnished to Unfurnished.');
            } 
             if(each.Furnishing_Type__c !=null && each.Furnishing_Type__c != oldMap.get(each.Id).Furnishing_Type__c  && each.isCloneContract__c && each.Parent_Contract__c!=null && each.Furnishing_Type__c =='Unfurnished' && oldMap.get(each.Id).Furnishing_Type__c =='Semi-furnished'){
               newmap.get(each.id).adderror('Furnishing Type can not be changed from Semi-furnished to Unfurnished.');
            } 
              
            // added by chandu: to check if the contract is converted from LOI to E stamped
            if(each.Agreement_Type__c !=null && each.Agreement_Type__c != oldMap.get(each.Id).Agreement_Type__c &&  each.Agreement_Type__c=='E-Stamped' && oldMap.get(each.Id).Agreement_Type__c=='LOI'){
                
                each.Converted_in_Estamped__c=true;
            }            
            if(each.isCloneContract__c && each.Is_Commercial_Field_Changed__c &&  each.Approval_Status__c!= oldMap.get(each.Id).Approval_Status__c && each.Approval_Status__c=='Approved by ZM'){
                
                each.Approval_Status__c='Awaiting City Head Approval';
            }
            
            // validation to not go SD from nestaway to tenant.
            if(each.isCloneContract__c && each.Who_pays_Deposit__c=='Tenant' &&  each.Who_pays_Deposit__c!= oldMap.get(each.Id).Who_pays_Deposit__c){
                
                 newmap.get(each.id).adderror('You can not change Who pays Deposit to Tenant in new version.');
            }
            
            if(each.isCloneContract__c && each.Who_pays_Deposit__c=='Nestaway' &&  each.Who_pays_Deposit__c!= oldMap.get(each.Id).Who_pays_Deposit__c){
                
                 each.Create_SD_WO_IF_Tenant_To_Nestaway__c=true;
            }
            
            if(each.Approval_Status__c=='Awaiting Owner Approval' && each.Manually_Approved_By_ZM__c && each.Manually_Approved_By_ZM__c !=oldMap.get(each.Id).Manually_Approved_By_ZM__c){
                
                each.Approval_Status__c='Cloned Contract Manually Approved by ZM';
                
            } 
           
            if(each.House_Term_Change_URL__c==null && each.isCloneContract__c && each.Approval_Status__c=='Awaiting ZM Approval'){
                          each.House_Term_Change_URL__c=url.getsalesforcebaseurl().toexternalform()+'/apex/ContractTermChanges?id='+each.Agreement_Approval_Code__c;
             }    
             // added by baibhav 
             if(each.Status!=oldmap.get(each.id).Status && each.Status==Contract_Constants.CONTRACT_STAGE_FINAL){
                contList.add(each);
             }       
            
        } 
    
    // added by baibhav
        if(!contList.isEmpty()){
          System.debug('****HRM');
            ContractTriggerHelper.HrmforApproval(contList);
          }
        
       // added by chandu for versioning of contract change.    
        if(!conToOppMap.isEmpty()){
                
          try{  
            Id operationalProcessRecTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(Constants.OPERATIONAL_PROCESS_RT_CONT_COMM_VER_FIELDS).getRecordTypeId();
            List<Operational_Process__c> opertionalProcessLst=[select id,Field_API_Name__c from Operational_Process__c where recordtypeid=:operationalProcessRecTypeId and Field_API_Name__c!=null];
            List <String> versioningAPIFieldNames = new List <String>();
            for(Operational_Process__c op: opertionalProcessLst){
                
                versioningAPIFieldNames.add(op.Field_API_Name__c);
            }
            
          //   String filterString = ' from contract where Opportunity__c IN:oppIds and isActive__c=true order by createddate desc';
            String filterString = ' from contract where id IN:parentConIds';
            String queryString = UtilityServiceClass.getQueryString('Contract');    
            Map<Id,Contract> allActiveContracts=new Map<Id, contract>((List<contract>)Database.query(queryString + filterString));
            
            if(!allActiveContracts.isEmpty()){       
                
                for (Id  conId : conToOppMap.keySet())
                {
                    Contract newCon=newMap.get(conId);                  
                    Contract activeCon;
                     if(allActiveContracts.containsKey(newCon.Parent_Contract__c)){
                         
                         activeCon=allActiveContracts.get(newCon.Parent_Contract__c);
                         
                           // For each field
                            for(String field : versioningAPIFieldNames)
                            {
                                // Check whether new value != than old value for the same record
                                
                                
                                if (newCon.get(field) != activeCon.get(field))
                                {
                                    newMap.get(conId).Is_Commercial_Field_Changed__c=true;
                                    
                                }
                            }
                         
                         
                     }       
                }
            }
          }
          catch(exception e){
              
              system.debug('***Exception in comparing fields '+e.getMessage()+' at line '+e.getLineNumber());
          }       
            
        }
        
        if(checkBedStatusForContractIds.size()>0){
            
            Set<Id> issueHouseIds= new Set<Id>();
            List<House__c> allHouseLst=[select id,(select id,Status__c from Beds__r where Status__c='Sold Out') from house__c where id IN:houseIdsForBedStatus];
            for(House__c house: allHouseLst){
                
                if(house.Beds__r!=null && house.Beds__r.size()>0){
                    
                    issueHouseIds.add(house.id);
                }
            }
            
            for(Id conId : checkBedStatusForContractIds){
                
                 if(contractToHouseMap.containsKey(conId)){
                     
                      string houseId=contractToHouseMap.get(conId);
                      
                      if(issueHouseIds.contains(houseId)){
                          
                          newMap.get(conId).addError('You can not change Booking Type or Tenancy Type for this contract.');
                      }
                 } 
                 
                  
                
            }
        }
        
    }
    
    Public Static Void RevisedQuote_SalesOrder(set<Id> setOppId){
          
          list<Quote> lstQuoteToUpdate          = new list<Quote>();
          list<Order> lstOrderToUpdate          = new list<Order>();  
          
          lstQuoteToUpdate=[SELECT id,Status,OpportunityId FROM Quote WHERE OpportunityId IN : setOppId];
          lstOrderToUpdate=[SELECT id,Status,OpportunityId FROM Order WHERE OpportunityId IN : setOppId];
          
          if(lstQuoteToUpdate.size()>0){
              for(Quote each : lstQuoteToUpdate){
                  each.Status='To be Revised';
              }
              try{
                  update lstQuoteToUpdate;     
              }catch(exception ex){
                  System.debug('==exception while Updating Quote in COntractTriggerHandler=='+ex);
              }
          }
          
          if(lstOrderToUpdate.size()>0){
              for(order each : lstOrderToUpdate){
                  each.Status='To be Revised';
              }
              try{
                  update lstOrderToUpdate;     
              }catch(exception ex){
                  System.debug('==exception while Updating Order in COntractTriggerHandler=='+ex);
              }
          }
    }

     /*************************************************************************************
     * Created By:   Sanjeev Shukla- (KVP Business Solutions)
     * Created Date: 26/May/2017
     * Description:  This method share the parent account record with Opportunity Owner's Manager, City HRM, Document Reviewer, Item Manager, City Manager
     *************************************************************************************/

    public static void shareAccountAndContract(Map<Id,Contract> newMap, Map<Id, Contract> oldMap){
      
      List<AccountShare> accShareList = new List<AccountShare>();
      Map<Id,Id> contractIds = new Map<Id,Id>();                    // use this variable to keep contracrt is as well as opportunity Id
      for(Contract con : newMap.values()){
        if (con.Approval_Status__c != oldMap.get(con.Id).Approval_Status__c && (con.Approval_Status__c == 'Awaiting ZM Approval' || con.Approval_Status__c == 'Awaiting HRM Approval') && con.Opportunity__c != NULL) {
            contractIds.put(con.Id, con.Opportunity__c);
        }
      }

      if(!contractIds.isEmpty()){
        Set<Id> oppOwnerIdSet = new Set<Id>();
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, OwnerId FROM Opportunity WHERE ID IN : contractIds.values()]);
        for(Opportunity opp : oppMap.values()){
          oppOwnerIdSet.add(opp.OwnerId);
        }
        if (!oppOwnerIdSet.isEmpty()) {
          Map<Id,User> userMap = new Map<Id, User>([SELECT Id, ManagerId, City_HRM__c, Document_Reviewer__c, Item_Manager__c, City_Manager__c FROM User WHERE Id IN : oppOwnerIdSet]);
          for(Id conId : contractIds.keySet()){
               /* Share the contract record with Manager*/
              if(userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).ManagerId != NULL){
              AccountShare accShare       = new AccountShare();
              accShare.AccountId          = newMap.get(conId).AccountId;
              accShare.UserOrGroupId      = userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).ManagerId ;
              accShare.AccountAccessLevel = 'Edit';
              accShare.OpportunityAccessLevel = 'Read';
              accShareList.add(accShare);
            }
          
            /* Share the contract record with City HRM*/
            if(userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).City_HRM__c != NULL){
              AccountShare accShare       = new AccountShare();
              accShare.AccountId          = newMap.get(conId).AccountId;
              accShare.UserOrGroupId      = userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).City_HRM__c;
              accShare.AccountAccessLevel = 'Edit';
              accShare.OpportunityAccessLevel = 'Read';
              accShareList.add(accShare);
            }

            if(userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).Document_Reviewer__c != NULL){
              AccountShare accShare       = new AccountShare();
              accShare.AccountId          = newMap.get(conId).AccountId;
              accShare.UserOrGroupId      = userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).Document_Reviewer__c;
              accShare.AccountAccessLevel = 'Edit';
              accShare.OpportunityAccessLevel = 'Read';
              accShareList.add(accShare);
            }

            if(userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).Item_Manager__c != NULL){
              AccountShare accShare       = new AccountShare();
              accShare.AccountId          = newMap.get(conId).AccountId;
              accShare.UserOrGroupId      = userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).Item_Manager__c;
              accShare.AccountAccessLevel = 'Edit';
              accShare.OpportunityAccessLevel = 'Read';
              accShareList.add(accShare);
            }

            if(userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).City_Manager__c != NULL){
              AccountShare accShare       = new AccountShare();
              accShare.AccountId          = newMap.get(conId).AccountId;
              accShare.UserOrGroupId      = userMap.get(oppMap.get(newMap.get(conId).Opportunity__c).OwnerId).City_Manager__c;
              accShare.AccountAccessLevel = 'Edit';
              accShare.OpportunityAccessLevel = 'Read';
              accShareList.add(accShare);
            }
          }
          if (!accShareList.isEmpty()) {
              Database.SaveResult[] accShareInsertResult = Database.insert(accShareList,false);
          }
        }
      }
    }  
    
    
     /*  Created By: Mohan - WarpDrive Tech Works
        purpose   : Make a callout to the webapp system with Web Entity=House on update of Contract associated with a House
        Execution : after Update */
    public static void sendWebEntityHouseJson(Map<Id, Contract> newMap){
        
        try {

              Set<Id> contractIdSet = new Set<Id>();

              for(Contract con: newMap.values()){
                if(con.House__c != null){
                  contractIdSet.add(con.Id);
                }
              }

              if(!contractIdSet.isEmpty()){
                List<Contract> contractList = ContractSelector.getContractsWithHouseStatus(contractIdSet);
                  //call the House2Json class to make the callout to WebEntity=House
                  for(Contract con: contractList){
                      if(con.House__c != null && con.House__r.Stage__c != Constants.HOUSE_STAGE_HOUSE_DRAFT && con.House__r.Stage__c != Constants.HOUSE_STAGE_ONBOARDING){
                        House2Json.createJson(con.House__c);
                      }
                  }
              }
            } Catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }     
    } // end of the method - sendWebEntityHouseJson  

    // Added  by Baibhav 
     Public Static Void BeforeInsert(List<Contract> newList){
       ContractTriggerHelper.ContarctAddressFormAccount(newList);
     }
}