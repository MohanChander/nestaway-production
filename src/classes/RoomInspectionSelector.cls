/*  Created By   : Mohan - WarpDrive Tech Works
	Created Date : 19/05/2017
	Purpose      : All the queries related to the Case Object are here */

public class RoomInspectionSelector {

	//query for the Room Inspection records with Attached Bathrooms from HIC Id set
	public static List<AggregateResult> getRoomInspectionAggregateResult(Set<Id> hicIdSet){
		List<AggregateResult> groupedResult = [select House_Inspection_Checklist__c, count(Id) from Room_Inspection__c where
											   Has_an_attached_bathroom__c = 'Yes' and House_Inspection_Checklist__c =: hicIdSet
											   group by House_Inspection_Checklist__c];
		return groupedResult;
	}


	//get Room Inspection List with House Status
	public static List<Room_Inspection__c> getRicWithHouseStatus(Set<Id> ricIdSet){

		return [select Id, House__c, House__r.Stage__c, House_Inspection_Checklist__r.House__c, House_Inspection_Checklist__r.House__r.Stage__c,
				(select Id, Name from Room_Terms__r) from Room_Inspection__c where Id =: ricIdSet];
	}

	//get Room Inspection details for MIMO check
	public static List<Room_Inspection__c> getRicForMIMOCheck(Set<Id> houseIdSet){

		String typeMimo = Constants.ROOM_INSPECTION_TYPE_OF_RIC_MIMO_CHECK;
		String typeRIC = Constants.ROOM_INSPECTION_TYPE_OF_RIC_ROOM_INSPECTION;
		String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_ROOMINSPECTION);
		String filterString = ', House_Inspection_Checklist__r.House__c from Room_Inspection__c ' +
				'where (House_Inspection_Checklist__r.House__c =: houseIdSet or HouseForMIMO__c =: houseIdSet) and ' +
				'(Type_of_RIC__c =: typeMimo or ' +
				 'Type_of_RIC__c =: typeRIC or ' +
				 'Type_of_RIC__c = null)';
	    List<Room_Inspection__c> ricList = (List<Room_Inspection__c>)Database.query(queryString + filterString);
	    return ricList;
	}

	//get Move in and Move Out Room Inspection details related to WorkOrder
	public static List<Room_Inspection__c> getRicForWorkOrders(Set<Id> woIdSet){

		String typeMoveIn = Constants.ROOM_INSPECTION_TYPE_OF_RIC_MOVE_IN_CHECK;
		String typeMoveOut = Constants.ROOM_INSPECTION_TYPE_OF_RIC_MOVE_OUT_CHECK;
		String queryString = UtilityServiceClass.getQueryString(Constants.OBJECT_ROOMINSPECTION);
		String filterString = ', CheckFor__r.Name from Room_Inspection__c ' +
				'where (Work_Order__c =: woIdSet) and ' +
				'(Type_of_RIC__c =: typeMoveIn or ' +
				 'Type_of_RIC__c =: typeMoveOut)'; 
	    List<Room_Inspection__c> ricList = (List<Room_Inspection__c>)Database.query(queryString + filterString);
	    return ricList;
	}	
	// Added By baibhav
	public static List<Room_Inspection__c> getRicFormHouseIdSet(Set<id> houseIdSet)
	{
		return [SELECT AC__c,Bedroom_Number__c,Bed_Sheet__c,Bed_side_table__c,Book_rack__c,Ceiling_Fan__c,Chair__c,
		CheckFor__c,CheckList_Verified__c,Clothes_Stand__c,Comment__c,Cot_Type__c,Cot__c,Cracks_Seepages__c,CreatedById,
		CreatedDate,Cupboards__c,Dining_Table_Chair__c,Drawer_chest__c,Dust_Bin__c,Furnishing_Comment__c,Furnishing_Verified__c,
		House__c,LastActivityDate,LastModifiedById,LastModifiedDate,Maintenance_Comment__c,Maintenance_Verified__c,Matress__c,
		Mattress__c,MIMO_Comment__c,Name,Nestaway_Id__c,Nestaway_Table__c,Night_Lamp__c,No_of_Cots__c,No_of_Damaged_Cots__c,
		No_of_Damaged_Matress__c,No_of_Matress__c,No_of_Non_Functional_Fans__c,No_of_Non_Functional_Plugpoins__c,No_of_Non_Functional_Tubelights__c,
		No_of_Non_Functioning_Room_Keys__c,No_of_Wardrobes__c,Number_of_Non_Functioning_Wardrobe_Keys__c,
		Number_of_working_ceiling_fans__c,Number_of_working_tubelights_lamps__c,OwnerId,Own_Cupboard__c,Painting__c,Pillow__c,
		RecordTypeId,Room_Verified__c,Side_Table__c,Study_Table__c,Tube_Light__c,Verification_Comment__c,Verification_Done__c,Wardrobe__c,
		Window_Curtain__c, House_Inspection_Checklist__r.House__C, House_Inspection_Checklist__c,
		Work_Order__c FROM Room_Inspection__c where 
		House_Inspection_Checklist__r.house__c=:houseIdSet and 
		(Type_of_RIC__c ='Room Inspection' or Type_of_RIC__c=null)];
	}
}