@isTest(SeeAllData=true)
Public CLass LeadSalesPathControllerTest {
    
    Public Static TestMethod void doTest(){
        Test.StartTest();
        
        /*Zing_API_URL__c zing = new Zing_API_URL__c();
        zing.Name          = 'Zing URL';
        zing.Auth_Token__c = 'ftLKMp_z_e3xX9Yiax-q';
        zing.Email__c      = 'unknownadmin@nestaway.com';
        zing.Tag__c        = 'OwnerAcquisition';
        zing.URL__c        = 'http://40.122.207.71/admin/zinc/lat_long?';
        
        insert zing;*/
        
        
        Lead objLead = new Lead();
        objLead.FirstName                  = 'Test';
        objLead.LastName                   = 'Test';
        objLead.Phone                      = '9066955369';
        objLead.MobilePhone                = '9406695578';      
        objLead.Email                      = 'Test@test.com';
        objLead.Source_Type__c             = 'Assisted';
        objLead.Area_Code__c               = '7';         
        objLead.LeadSource                 = 'City Marketing';
        objLead.City_Marketing_Activity__c = 'Roadshow';
        objLead.Street                     = 'Test';
        objLead.City                       = 'Bangalore';
        objLead.State                      = 'Karnataka';
        objLead.Status                     = 'New';
        objLead.PostalCode                 = '560078';
        objLead.Country                    = 'India';
        insert objLead;
        
        objLead.Status                     = 'Open';
        Update objLead;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(objLead);
        LeadSalesPathController testAccPlan1 = new LeadSalesPathController(sc1);
        objLead.Status                     = 'Disqualified';
        objLead.Reason_for_closing__c      = 'Duplicate';
        Update objLead;
        LeadSalesPathController testAccPlan = new LeadSalesPathController(sc1);
        Test.StopTest();
    }
}