@isTest
public Class QuoteTriggerHandlerTest{
    
    public Static TestMethod void quoteTest(){
        Test.StartTest();
            Pricebook2 pb = new Pricebook2 ();
    pb.name='NestAway Standard Price Book';
    insert pb;
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        Quote quoObj = new Quote();
        quoObj.Name = 'Quote1';
        quoObj.Status = 'Draft';
        quoObj.OpportunityId = opp.Id;
        quoObj.approval_status__c = 'Awaiting Items Manager Approval';
        
        insert quoObj;
        
        quoObj.approval_status__c = 'Manually Approved by ZM';
        quoObj.Status             = 'To be Revised';
        //quoObj.Rounded_Total__c = 45; // it's formula field you can't assign value to this field. 
        update quoObj;
        quoObj.Status           = 'Cancelled';
        update quoObj;
        quoObj.Status           = 'Draft';
        update quoObj;
        opp.StageName           = 'Sample Contract';
        update opp;
        
        /*quoObj.Rounded_Total__c = 19; // it's formula field you can't assign value to this field. 
update quoObj;

quoObj.Rounded_Total__c = 100; // it's formula field you can't assign value to this field. 
update quoObj;

quoObj.Rounded_Total__c = 250; // it's formula field you can't assign value to this field. 
update quoObj;

quoObj.Rounded_Total__c = 1250; // it's formula field you can't assign value to this field. 
update quoObj; */
        
        Test.StopTest();
    }
}