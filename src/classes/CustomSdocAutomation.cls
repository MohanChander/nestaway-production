// Chandu : This class is used to auto generate the document by using the sdoc job method.

public class CustomSdocAutomation{


 public static SDOC__SDJob__c createSdocJobRecord(string recordId,string objectApiName,string doclistId,string sendAnEmail,boolean startJob,string runAsUser){


          SDOC__SDJob__c job = new SDOC__SDJob__c();
          job.SDOC__Oid__c=recordId;
          job.SDOC__ObjApiName__c=objectApiName;
          job.SDOC__Doclist__c=doclistId;
          job.SDOC__SendEmail__c=sendAnEmail;
          job.SDOC__Start__c=startJob;
          if(runAsUser!=null){
              job.SDOC__RunAs__c=runAsUser;
          }
          insert job;
          
          return job;
  
 }

}