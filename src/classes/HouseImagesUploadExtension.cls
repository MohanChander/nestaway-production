global class HouseImagesUploadExtension {

    public static id houseidForRemote{get;set;}
    public id houseid {get;set;}
    public static id houseId_static {get;set;}
    map<String,String> optionlist {get;set;}
    public String selectedValue {get;set;}
    public String secondrySelectedValue {get;set;}
    public String tertiarySelectedValue {get;set;}
    
    public boolean renderSecPicklist {get;set;}
    public boolean renderThirdPicklist {get;set;}

    public List<SelectOption> secondarySelectList{ get; set; }
    
    public List<SelectOption> secondarySelectListTemp{ get; set; }
    
    public map<String,list<String>> houseSublistMap {get;Set;}
    public map<String,list<String>> roomSublistMap {get;Set;}

    public list<bathroom__c> bth_house {get;set;}
    
    public HouseImagesUploadExtension(ApexPages.StandardController controller) {
        try{
            renderSecPicklist = true;
            renderThirdPicklist = true;
            houseid = ApexPages.currentPage().getParameters().get('id');
            houseidForRemote = ApexPages.currentPage().getParameters().get('id');
            houseId_static =  ApexPages.currentPage().getParameters().get('id');
            System.debug('***houseid '+houseid);
            System.debug('***houseId_static '+houseId_static);
            house__c h = new house__c();
            h = [Select name from house__c where id = : houseid];
            list<room_terms__c> rt = [Select id,name from room_terms__c where house__c = : houseid ];

            bth_house = [Select id,name from bathroom__c where house__c = : houseid AND Room__c =: null ];


            optionlist = new map<String,String>();
            optionlist.put('-Select-','-Select-');
            optionlist.put(houseid,'House-'+h.name);
            for(room_terms__c r : rt){
                optionlist.put(r.id,'Room-'+r.name);
            }

            list<string> templist = new list <String>();
            templist.add('Living');
            templist.add('Balcony');
            templist.add('Bathroom');
            templist.add('Kitchen');
            templist.add('Others');
            houseSublistMap = new map<String,list<String>>();
            houseSublistMap.put(h.id,templist);
            
            
            
            
            
            
        }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
        }
    }
    
    public void getSecondaryPickVals(){
        System.debug('selectedValue'+selectedValue);
        
        secondarySelectList = new List<selectOption>();
        secondarySelectList.add(new SelectOption('-Select-','-Select-'));
        
        System.debug('houseSublistMap.get(selectedValue)'+houseSublistMap.get(selectedValue));
        if(selectedValue != '-Select-'){
            for(String s : houseSublistMap.get(selectedValue))
                secondarySelectList.add(new SelectOption(s, s));
        }
    }
    public void setTruefalseForPicklist(){
        try{
            System.debug('***selectedValue Ameed '+selectedValue+'\n ***'+selectedValue.substring(0,3));
            if(selectedValue == null){
                if(selectedValue.substring(0,3) == 'a05' ){
                    System.debug('1st picklist true');
                    renderSecPicklist = true;
                }
                else if(selectedValue.substring(0,3) == 'a08' ){
                    System.debug('2nd picklist true');
                    renderThirdPicklist = true;
                }
                else{
                    System.debug('none true');
                }
             }
            else{
                renderSecPicklist = false;
                renderThirdPicklist = false;
            }
         }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
        }
    }
    public list<SelectOption> getSecondList(){
        List<SelectOption> options = new List<SelectOption>();
        try{
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption(houseid,'Balcony'));
            options.add(new SelectOption(houseid,'Kitchen'));
            options.add(new SelectOption(houseid,'Living'));
            options.add(new SelectOption(houseid,'Dining'));
            //options.add(new SelectOption(houseid,'Bathroom'));
            options.add(new SelectOption(houseid,'Others')); 
            
            for(bathroom__c b : bth_house){
                options.add(new SelectOption(houseid+'_'+b.id,b.name));
            }
            
               
        }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
        }
        return options;
            
        
    }
    
    public list<SelectOption> getThirdList(){
        List<SelectOption> options = new List<SelectOption>();
        try{
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption('Bathroom','Bathroom'));
            options.add(new SelectOption('Bedroom','Bedroom'));
        }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
        }
        return options;
        
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        try{
            //List<SelectOption> options = new List<SelectOption>();
            for(String ids :optionlist.keySet()){
                options.add(new SelectOption(ids,optionlist.get(ids)));    
            }
        }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
        }
        return options;
    }
    /* Resync house to the webapp - Commented By - Mohan
    
    public static void resyncHouse(){
        if( houseId_static != null)  {
              System.debug('***Resync House Id '+houseId_static);
              HouseJson.createJSON(houseId_static);
        } else {
            houseId_static = ApexPages.currentPage().getParameters().get('id');
            HouseJson.createJSON(houseId_static);
         }
    } */

    public static void resyncHouse(){
        if( houseId_static != null)  {
              System.debug('***Resync House Id '+houseId_static);
              HouseJson.createJSON(houseId_static);
        } else {
            houseId_static = ApexPages.currentPage().getParameters().get('id');
            HouseJson.createJSON(houseId_static);
         }
    }    
    
    @RemoteAction
    global static list<photo__C> onLoadPhotoUploadPg(String houseIds){
        System.debug('***onLoadPhotoUploadPg house Id '+'\n -- '+houseIds);
        list<photo__c> photoList = new list<photo__c>();
        try{
            photoList = [select id,name,Is_Visible__c,createddate, Image_Tag__c ,Image_Url__c,bathroom__c,bathroom__r.name,room_term__c,room_term__r.name from photo__c where House__c =: id.valueOf(houseIds)  ORDER BY CreatedDate DESC];
            System.debug('*******PhotoList' + photoList);
            if(photoList.size() > 0)
                return photolist;
            else
                return null;
        }
        catch(exception e){
            System.debug('****exception ee '+e.getLineNumber()+e.getMessage()+e);
            return null;
        }
    }
    
    @remoteAction
    global static Boolean updatePhotosToserverMarkAsInvisible(list <String> photoIds){
    	list <photo__c> photoListTosend = new list <Photo__c>();
        photoListTosend = [Select id,Is_Visible__c from photo__c where id in : photoIds];
       
        list<photo__c> pListToUpate =  new list<photo__c>();
         try{
            for(photo__c ph : photoListTosend){
                ph.Is_Visible__c = false;
                pListToUpate.add(ph);
            }
            
            if(pListToUpate.size() > 0){
                update pListToUpate;
                return true;
            }
            else
            return false;
        }
        catch(Exception e){
            return false;
        }
    }
    
    @remoteAction
    global static Boolean updatePhotosToserver(list <String> photoIds){
       
        System.debug('inputPhotoIds'+photoIds);
        list <photo__c> photoListTosend = new list <Photo__c>();
        photoListTosend = [Select id,Is_Visible__c from photo__c where id in : photoIds];
       
        list<photo__c> pListToUpate =  new list<photo__c>();
         
        System.debug('photoListTosend '+photoListTosend);
        try{
            for(photo__c ph : photoListTosend){
                ph.Is_Visible__c = true;
                pListToUpate.add(ph);
            }
            
            if(pListToUpate.size() > 0){
                update pListToUpate;
                return true;
            }
            else
            return false;
        }
        catch(Exception e){
            return false;
        }
        
        
    }
    
    @RemoteAction 
    global static String callApexToCreatePhotos(String enitity1Id,String Enttity2Id,String entityName1,String entityName2, String Url){
        System.debug('###entityName1  '+entityName1+'---\n ---  '+'entityName2 -- '+entityName2+'url---'+URL);
        System.debug('###enitity1Id -- '+enitity1Id+'---  '+'Enttity2Id -- '+Enttity2Id);
        //System.debug('selectedValue '+selectedValue);
        try{

            Id RecordTypeId = Schema.SObjectType.photo__c.getRecordTypeInfosByName().get('House Images').getRecordTypeId();


            String message = '-';
            Photo__c ph  = new Photo__c ();
            ph.Image_Url__c  = Url;

            if(RecordTypeId != null) {
                ph.recordtypeid = RecordTypeId;
            }
        
            System.debug('***entityName1'+entityName1);
        
            if(entityName1 != null && entityName1 !=' ' && entityName1.split('-')[0] == 'Room'){
                System.debug('***1st if');
                room_terms__c rt = [Select id,house__c,name from room_terms__c where id = : id.valueOf(enitity1Id)];
                ph.Name = 'Image for room '+rt.name;
                ph.House__c = rt.house__c;
                ph.Image_For__c  = 'Room';
                ph.Room_Term__c = rt.id;
                ph.Is_Visible__c = false;
                if(entityName2 != null && entityName1 !=' '){
                    if(entityName2 == 'Bathroom'){
                        //bathroom__c bth = new bathroom__C();
                        list<bathroom__c> bth = [Select id ,name from bathroom__c where Room__c =:rt.id];
                        if(bth != null && bth.size()>0)
                        {
                            ph.Bathroom__c = bth[0].id;
                            ph.Image_Tag__c = 'Bathroom' ;
                        }
                        else{
                            ph.Image_Tag__c = 'Bathroom' ;
                        }
                    }
                    if(entityName2 == 'Bedroom'){
                        ph.Image_Tag__c = 'Bedroom' ;
                        //ph.Room_Term__c = null;
                    }
                }
            }
            else if(entityName1 != null && entityName1 !=' ' && entityName1.split('-')[0] == 'House'){
                /*a050l000000HysmAAC_a0O0l000000DNLEEA4*/
                System.debug('***2nd if');
                ph.House__c = id.valueOf(enitity1Id);
                ph.Name = 'Image for House '+entityName1.split('-')[1];
                ph.Image_For__c  = 'House';
                
                ph.Is_Visible__c = false;
                
                if(entityName2 != null && entityName2 !=' '){
                    if(Enttity2Id.contains('_')){
                        ph.Image_Tag__c = 'Bathroom';
                        ph.Bathroom__c = Enttity2Id.split('_')[1];
                    }
                    else{
                        ph.Image_Tag__c = entityName2;
                    }
                }
            
            }
            System.debug('PHOTO CREATED - '+ ph);
            if(ph != null && ph.Image_For__c == 'House'){
                insert ph;
                return 'Record inserted sucessfully';
            }
            else if(ph != null && ph.Image_For__c == 'Room' && ph.Image_Tag__c == 'Bedroom'){
                insert ph; 
                return 'Record inserted sucessfully';
            }  
            else if(ph != null && ph.Image_For__c == 'Room' && ph.Image_Tag__c == 'Bathroom' && ph.Bathroom__c !=null){
                insert ph; 
                return 'Record inserted sucessfully';
            }  
            else if(ph != null && ph.Image_For__c == 'Room' && ph.Image_Tag__c == 'Bathroom' && ph.Bathroom__c == null){
                return 'No attached bathroom present in the selected room so image is not uploaded. ';
            }
            else 
                return 'Record inserted sucessfully';
            
            
        }
        catch(exception e){
            System.debug('****exception ee '+e.getLineNumber()+e.getMessage()+e);
            return e.getLineNumber()+' '+e.getMessage();
        }
            
        
        
         
    } 
    
}