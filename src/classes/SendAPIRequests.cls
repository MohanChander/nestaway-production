Public class SendAPIRequests{
    
     public static string resp;
public class InsuranceDetailsWrapeerClass{
        public String Insurance_case_sf_id{get;set;}
        public String House_sf_id{get;set;}
        
    }
    public class InsuranceDetailsJsonRespWrapeerClass{        
        public string success;
        public string info;
        public insuranceDataWrapper data;
        
    }
    public class insuranceDataWrapper{
        
        public string status{get;set;}  
        public string message{get;set;} 
        public string nestawayid{get;set;}  
        public string ownername{get;set;} 
        public string owneraddr{get;set;}  
        public string propertyaddr{get;set;} 
        public string mobile{get;set;}  
        public Integer si_bldg{get;set;} 
        public string bld_bnf{get;set;}  
        public Integer si_content{get;set;} 
        public string cont_bnf{get;set;} 
        public Integer si_mbd{get;set;} 
        public string mbd_bnf{get;set;} 
        public string email{get;set;}
        
        
    }
    @future(callout=true)
    public static void sendInsuranceDetailsToWebAppFuture(Id insId){    
        system.debug('sendInsuranceDetailsToWebApp1'+insId);
        
        sendInsuranceDetailsToWebApp(insId);
    }
    
    public static void sendInsuranceDetailsToWebApp(Id insId){   
        system.debug('sendInsuranceDetailsToWebApp'+insId);
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            Insurance__c  ins = [select id,API_Success__c,API_Info__c,Last_API_Sync_Time__c,house__c,Insurance_API_message__c,Status__c, Policy_Link_1__c, Policy_Link_2__c,Policy_Link_3__c, Policy_Start_Date__c,  
                                 Si_Bldg__c ,Si_Content__c ,Si_Mbd__c ,Owner_Phone_Number__c,Owner_Address__c,Owner_Email__c , ownername__c     from Insurance__c    where id=:insId];    
            InsuranceDetailsWrapeerClass rp = new InsuranceDetailsWrapeerClass();
            rp.House_sf_id=ins.house__c;
            rp.Insurance_case_sf_id=ins.id;
            string strEndPoint= nestURL[0].House_Insurance_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
            HttpResponse res;
            string jsonBody; 
            jsonBody=JSON.serialize(rp);
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            respBody= res.getBody();
            resp=res.getBody();
            system.debug('***RespBody'+respBody);               
            InsuranceDetailsJsonRespWrapeerClass dataJson= new InsuranceDetailsJsonRespWrapeerClass();
            dataJson = (InsuranceDetailsJsonRespWrapeerClass)JSON.deserialize(respBody,InsuranceDetailsJsonRespWrapeerClass.Class);              
            if(dataJson!=null){
                if(res.getStatusCode()==200){
                    ins.API_Success__c = dataJson.success;
                    ins.API_Info__c   = dataJson.info;      
                    ins.Last_API_Sync_Time__c = System.now();  
                    if(dataJson.data != null){
                        if(dataJson.data.status!=null){
                            ins.Status__c=dataJson.data.status;
                        }
                        if(dataJson.data.message  !=null){
                            ins.Insurance_API_message__c  =dataJson.data.message;
                        }
                        if(dataJson.data.nestawayid !=null){
                            ins.Nestaway_Id__c =dataJson.data.nestawayid ;
                        }
                        if(dataJson.data.ownername !=null){
                            ins.ownername__c=dataJson.data.ownername ;
                        }
                        if(dataJson.data.owneraddr !=null){
                            ins.Owner_Address__c=dataJson.data.owneraddr ;
                        }
                        if(dataJson.data.propertyaddr !=null){
                            ins.Property_Address__c =dataJson.data.propertyaddr ;
                        }
                        if(dataJson.data.mobile != null){
                            ins.Owner_Phone_Number__c =dataJson.data.mobile ;
                        }
                        if(dataJson.data.email !=null){
                            ins.Owner_Email__c =dataJson.data.email;
                        }
                        if(dataJson.data.si_bldg  !=null){
                            ins.Si_Bldg__c  =dataJson.data.si_bldg ;
                        }
                        if(dataJson.data.bld_bnf !=null){
                            ins.Bld_Bnf__c  =dataJson.data.bld_bnf ;
                        }
                        if(dataJson.data.si_content  != null){
                            ins.Si_Content__c  =dataJson.data.si_content  ;
                        }
                        if(dataJson.data.cont_bnf   !=null){
                            ins.Cont_Bnf__c   =dataJson.data.cont_bnf   ;
                        }
                        if(dataJson.data.si_mbd !=null){
                            ins.Si_Mbd__c =dataJson.data.si_mbd ;
                        }
                        if(dataJson.data.mbd_bnf  !=null){
                            ins.Mbd_Bnf__c   =dataJson.data.mbd_bnf  ;
                        }
                    }
                }  
                else
                {
                    ins.API_Success__c = dataJson.success;
                    ins.API_Info__c   = dataJson.info;      
                    ins.Last_API_Sync_Time__c = System.now();                
                    
                    if(dataJson.data.status!=null){
                        ins.Status__c=dataJson.data.status;
                    }
                    if(dataJson.data.message!=null){
                        ins.Insurance_API_message__c =dataJson.data.message;
                    }
                }
            }
            
            if(ins.id!=null){         
                Update ins;         
            }
        }
        catch(exception e){
                Insurance__c ins1 = new Insurance__c(id = insId);
                    ins1.API_Success__c = 'False';
                    ins1.API_Info__c   = resp;     
                    ins1.Last_API_Sync_Time__c = System.now(); 
            update ins1;
            UtilityClass.insertErrorLog('Webservices','Insurance Api failed :'+insId+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
        }                   
          
    }
    public class RPDetailsWrapeerClass{
        
        public Decimal amount{get;set;}
        public String house_sf_id{get;set;}
        public String work_order_id{get;set;}
        public String reason{get;set;}
    }
    public class RPDetailsJsonRespWrapeerClass{        
        public string success;
        public string info;
        public dataWrapper data;
        
    }
    public class dataWrapper{
        
        public boolean sd_exists{get;set;}      
    }
    
    @future(callout=true)
    public static void sendRPDetailsToWebAppFuture(Id workOrderId){    
        
        sendRPDetailsToWebApp(workOrderId);
    }
    
    public static void sendRPDetailsToWebApp(Id workOrderId){   
        
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            Workorder wo = [select id,API_Success__c,API_Info__c,Last_API_Sync_Time__c,house__c,Upfront_amount__c from workorder where id=:workOrderId];    
            RPDetailsWrapeerClass rp = new RPDetailsWrapeerClass();
            rp.amount=wo.Upfront_amount__c;
            rp.house_sf_id=wo.house__c;
            rp.work_order_id=wo.id;
            rp.reason='upfront';                
            string strEndPoint= nestURL[0].WO_Send_RP_Details_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
            HttpResponse res;
            string jsonBody; 
            jsonBody=JSON.serialize(rp);
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            respBody= res.getBody();
            system.debug('***RespBody'+respBody);               
            RPDetailsJsonRespWrapeerClass dataJson= new RPDetailsJsonRespWrapeerClass();
            dataJson = (RPDetailsJsonRespWrapeerClass)JSON.deserialize(respBody,RPDetailsJsonRespWrapeerClass.Class);              
            if(res.getStatusCode()==200 && dataJson!=null){
                wo.API_Success__c = dataJson.success;
                wo.API_Info__c   = dataJson.info;      
                wo.Last_API_Sync_Time__c = System.now();                
                wo.RP_Sent_to_webapp__c=true;
                if(dataJson.data!=null && dataJson.data.sd_exists!=null && dataJson.data.sd_exists==true){
                    wo.Description='Sd is already exists for this house.';
                    wo.Status='Canceled';
                }
            }
            else{
                
                if(dataJson!=null){
                    wo.API_Success__c = dataJson.success;
                    wo.API_Info__c   = dataJson.info;          
                }
                else{
                    
                    wo.API_Success__c ='false';
                    wo.API_Info__c   = 'RP API Failed.';
                    
                }              
                
                wo.Last_API_Sync_Time__c = System.now(); 
            }
            
            if(wo.id!=null){                    
                StopRecursion.DisabledWorkOrderTrigger= true;                        
                Update wo;         
            }
        }
        catch(exception e){
            
            UtilityClass.insertErrorLog('Webservices','API Failed for RP send to Webapp :'+workOrderId+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
        }               
        
        
        
    }
    
    public static void accountSync(Id accountIds){             
        
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            account acc = new account();
            acc.id=accountIds;
            string strAccountJson;
            strAccountJson        = GetObjectJson('Account', accountIds);           
            strAccountJson        = strAccountJson;
            string strEndPoint= nestURL[0].Account_Sync_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
            HttpResponse res;
            system.debug('***jsonBody'+strAccountJson); 
            res=RestUtilities.httpRequest('POST',strEndPoint,strAccountJson,null);
            respBody = res.getBody();                    
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(respBody);
            system.debug('***RespBody'+respBody); 
            acc.API_Success__c = String.ValueOf(m.get('success'));
            acc.API_Info__c   = String.ValueOf(m.get('info'));      
            acc.Last_API_Sync_Time__c = System.now();              
            if(acc.id!=null){                    
                StopRecursion.DisabledAccountTrigger  = true;                        
                Update acc;         
            }
        }
        catch(exception e){
            
            UtilityClass.insertErrorLog('Webservices','API Failed for account sync:'+accountIds+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+'**RespBody'+respBody); 
        }               
        
        
        
        
    }
    
    
    public static void bankSync(Id bankIds){
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            Bank_Detail__c bank = new Bank_Detail__c();
            bank.id=bankIds;
            string strAccountJson;
            strAccountJson        = GetObjectJson('Bank_Detail__c', bankIds);           
            strAccountJson        = strAccountJson;
            string strEndPoint= nestURL[0].Bank_Sync_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
            HttpResponse res;
            system.debug('***jsonBody'+strAccountJson); 
            res=RestUtilities.httpRequest('POST',strEndPoint,strAccountJson,null);
            respBody = res.getBody();                    
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(respBody);
            system.debug('***RespBody'+respBody); 
            bank.API_Success__c = String.ValueOf(m.get('success'));
            bank.API_Info__c   = String.ValueOf(m.get('info'));      
            bank.Last_API_Sync_Time__c = System.now();              
            if(bank.id!=null){                    
                StopRecursion.DisabledBankTrigger   = true;                        
                Update bank;         
            }
        }
        catch(exception e){
            
            UtilityClass.insertErrorLog('Webservices','API Failed for bank account sync :'+bankIds+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+'**RespBody'+respBody); 
        }            
        
        
    }
    
    
    
    //Chandu: Method to send the API request to web to create RP in case of move out with issue.
    //Start
    // JSON Classes : Response parser class
     public class MoveoutWithIssueRPSettlementAPIWrapper{
         
        public string   reason{get;set;}
        public decimal  amount {get;set;}
        public string   notes {get;set;}
        public string   case_id {get;set;}
         
     }
     
    Public class MoveoutWithIssueRPSettlementRespWrapper{
        public string success;
        public string info;
        public RPDataWrapper data;      
        public string respCode;
        public string jsonRespBody;
    }   
        
    public class RPDataWrapper {            
            public Integer id;  //1951427
            public String salesforce_mappable_id;   //TLX6LDGZYJRIC0
            
    }
    
    public static MoveoutWithIssueRPSettlementRespWrapper createMoveOutSettlementRPINWebApp(Id parentCaseId,Decimal amount,string notes){   
        
        MoveoutWithIssueRPSettlementRespWrapper apiResp= new MoveoutWithIssueRPSettlementRespWrapper();
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();
            Org_Param__c param = Org_Param__c.getInstance();
            if(param!=null && param.Move_Out_RP_Reason__c!=null && nestURL[0].Move_Out_Settlement_RP_API__c!=null){             
                        
                string strEndPoint= nestURL[0].Move_Out_Settlement_RP_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
                HttpResponse res;
                string jsonBody; 
                string respBody;
                
                MoveoutWithIssueRPSettlementAPIWrapper moveOutRp= new MoveoutWithIssueRPSettlementAPIWrapper();
                moveOutRp.reason=param.Move_Out_RP_Reason__c;
                moveOutRp.amount=amount;
                moveOutRp.notes=notes;
                moveOutRp.case_id=parentCaseId;
                
                jsonBody=JSON.serialize(moveOutRp);
                res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
                respBody= res.getBody();
                system.debug('***RespBody'+respBody);               
                
                apiResp = (MoveoutWithIssueRPSettlementRespWrapper)JSON.deserialize(respBody,MoveoutWithIssueRPSettlementRespWrapper.Class);              
                if(apiResp!=null){
                    apiResp.respCode=string.valueOf(res.getStatusCode());
                    apiResp.jsonRespBody=respBody;
                }
                        
                
            }   
        }
        catch(exception e){
            
             UtilityClass.insertErrorLog('Webservices','API Failed for move out RP creation in Webapp for case id :'+parentCaseId+' error:'+e.getMessage()+' at line:'+e.getLineNumber()); 
             apiResp=null;
        }               
        
        return apiResp;
        
    }
    
    //END
    Public Static String GetObjectJson(String SobjectName , Id recId){
        String SobjectApiName = SobjectName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String strFields = '';
        
        for(Schema.SObjectField fieldName : fieldMap.Values() ){
            if(strFields == null || strFields == '')
            {
                strFields = String.ValueOf(fieldName);
            }else{
                strFields = strFields + ' , ' + String.ValueOf(fieldName);
            }
        }
        
        JSONGenerator gen_opp = JSON.createGenerator(true);
        gen_opp.writeStartObject();
        if(recId!=null){
            String query ='';
            
            query = 'SELECT ' + strFields + ' FROM ' + SobjectApiName + ' WHERE ID=:recId Limit 1';
            
            
            Sobject objOpp = Database.query(query);        
            
            if(objOpp!=null){
                for(Schema.SObjectField eachField : fieldMap.Values()){
                    
                    String fieldName = String.ValueOf(eachField);
                    Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                    
                    
                    if(fielddataType==Schema.DisplayType.Boolean){
                        gen_opp.writeBooleanField(fieldName,Boolean.valueOf(objOpp.get(fieldName)));
                    }else if(fielddataType==Schema.DisplayType.Date){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeDateField(fieldName,Date.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.DateTime){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeDateTimeField(fieldName,DateTime.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.Double){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.Percent){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.Currency){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Double.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.Integer){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeNumberField(fieldName,Integer.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }else if(fielddataType==Schema.DisplayType.Email  || fielddataType==Schema.DisplayType.URL || fielddataType==Schema.DisplayType.ID || fielddataType==Schema.DisplayType.MultiPicklist ||  fielddataType==Schema.DisplayType.Picklist || fielddataType==Schema.DisplayType.String || fielddataType==Schema.DisplayType.TextArea || fielddataType==Schema.DisplayType.REFERENCE){
                        if(objOpp.get(fieldName)!=null){
                            gen_opp.writeStringField(fieldName,String.ValueOf(objOpp.get(fieldName)));
                        }else{
                            gen_opp.writeNullField(fieldName);
                        }
                    }                                
                }
            }            
            gen_opp.writeEndObject();
            
            //System.debug('==gen_opp=='+gen_opp.getAsString());
        }
        return gen_opp.getAsString();
    }
    

    
    
}