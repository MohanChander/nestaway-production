@isTest
public class MilestoneTimeCalculatorTest {

	@isTest
	public static void MilestoneTimeCalculatorTest() {

    	Id ServiceRequestRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();

        // Select an existing milestone type to test with
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        // Create case data.
        // Typically, the milestone type is related to the case, 
        // but for simplicity, the case is created separately for this test.
        Case c = new Case(priority = 'High', Escalation_Level__c = 'Level 1', RecordTypeId = ServiceRequestRecordTypeId);
        insert c;

		MilestoneTimeCalculator calculator = new MilestoneTimeCalculator();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);        
        
        c.Escalation_Level__c = 'Level 2';
        update c;

        c.Escalation_Level__c = 'Level 3';
        update c;	
	}
}