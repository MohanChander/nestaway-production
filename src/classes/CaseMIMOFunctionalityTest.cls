@Istest
public class CaseMIMOFunctionalityTest {
    public static   Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static  Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
    public static  Id internaltRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_INTERNAL_TRANSFER ).getRecordTypeId();
    public static Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
    public static    Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK_READ_ONLY).getRecordTypeId();
    public static   Id mimohicTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
    public static  Id mimoricRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
    public static Id mimobicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
    public static   Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
    public static    Id caseSettlement_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();
    
    public static  Id contractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get(Constants.tenantContract_RecordType).getRecordTypeId();
    Public Static TestMethod Void doTest(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  createmoveincase();
        c.Stage__c = 'Moved-in all OK';
            update c;
        workorder wo = new workorder();
        wo.CaseId = c.id;
        insert wo;
        Map<id,case> newmap = new Map<id,case>();
        newmap.put(c.id,c);
        CaseMIMOFunctionality.createTaskMoveOutSchedule(newmap);
        CaseMIMOFunctionality.triggerAutoOwnerApprovalConfirmationInWebApp(c.id);
    }
    Public Static TestMethod Void doTest1(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  createmoveout();
        workorder wo = new workorder();
        wo.CaseId = c.id;
        insert wo;
        Map<Id,Case> newMap = new  Map<Id,Case> ();
        newMap.put(c.id,c);
        set <id> massClosedParentIds= new Set<id>();
        massClosedParentIds.add(c.id);
        Map<Id,Id> parentToCaseIdMap = new  Map<Id,Id>();
        parentToCaseIdMap.put(c.id,c.id);
        CaseMIMOFunctionality.closeParentAndDeleteRelated(massClosedParentIds);
        CaseMIMOFunctionality.tenantCancelledMoveOut(c.id,'new');
        CaseMIMOFunctionality.bedReleaseOnInspectionCOmplete(c.id,'new');
        CaseMIMOFunctionality.createMoveOutWorkOrderFOrCAllCentre(newmap);
        CaseMIMOFunctionality.checkMoveInDateValidation(newmap,parentToCaseIdMap,MoveOutRecordTypeId);
    }
    Public Static TestMethod Void doTest2(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  creatSettlement();
         workorder wo = new workorder();
        wo.CaseId = c.id;
        insert wo;
    }
     Public Static TestMethod Void doTest90(){
       User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        case cint= new case();
        cint.RecordTypeId=internaltRecordTypeId;
        cint.Tenant__c=accobj.id;
        cint.AccountId=accobj.id;
        cint.status='Progress';
        cint.type='Internal Transfer';
        cint.Tenant_Type__c='boys';
        cint.SD_Status__c='pending';
        cint.From_House__c=hos.id;
        cint.Origin='web';
        insert cint;
        
        test.starttest();
        Case c=  new Case();
        c.parentid=cint.id;
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Move_in_date__c=System.today()-2;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='SD Default';
        c.Booked_Object_Type__c='House';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MoveOut';
        
        c.Tenant__c=accObj.id;
        
        c.Contract_End_Date__c=system.today();
        insert c;
         Map<id,case> newmap = new Map<id,case>();
        newmap.put(c.id,c);
         map<id,account> tenantAccountMap = new map<id,account>();
         tenantAccountMap.put(accObj.id,accObj);
         map<id,house__c> houseMap = new  map<id,house__c>();
         houseMap.put(hos.id,hos);
         CaseMIMOFunctionality.createTasks(newmap,tenantAccountMap,houseMap);
        CaseMIMOFunctionality.createTaskMoveOutSchedule(newmap);
        CaseMIMOFunctionality.triggerAutoOwnerApprovalConfirmationInWebApp(c.id);
    }
    public static CAse createmoveincase()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.House_Owner__c=accObj.id;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.id;
        c.ownerId=newuser.Id;
        c.AccountId=accobj.id;
        c.MoveIn_Slot_End_Time__c=system.today();
        c.MoveIn_Slot_Start_Time__c=system.today();
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='House';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOvein';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_start_Date__c=system.today();
        c.Move_in_date__c=system.today();
        c.License_Start_Date__c=system.today();
        c.Requires_Owner_Approval__c=true;
        c.Owner_approval_status__c='Pending';
        c.Contract_End_Date__c=system.today();
        c.Documents_Verified__c=true;
        c.Tenant_Profile_Verified__c=true;
        c.Documents_Uploaded__c=true;
        insert c;
        return c;
        
    }
    public static CAse createmoveout()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        case cint= new case();
        cint.RecordTypeId=internaltRecordTypeId;
        cint.Tenant__c=accobj.id;
        cint.AccountId=accobj.id;
        cint.status='Progress';
        cint.type='Internal Transfer';
        cint.Tenant_Type__c='boys';
        cint.SD_Status__c='pending';
        cint.From_House__c=hos.id;
        cint.Origin='web';
        insert cint;
        
        test.starttest();
        Case c=  new Case();
        c.parentid=cint.id;
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Move_in_date__c=System.today()-2;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='SD Default';
        c.Booked_Object_Type__c='House';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MoveOut';
        
        c.Tenant__c=accObj.id;
        
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;
        
    }
    
    public static CAse creatSettlement()
    {    
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        Set<id> setoftennant = new Set<id>();
        setoftennant.add(accObj.id);
        Photo__c ph= new Photo__c();
        insert ph;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.house__c=hos.id;
        insert b;
        Test.startTest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= caseSettlement_RecordTypeId;
        c.Booking_Id__c='389769';
        c.Dashboard_Update_Required__c='Yes';
        c.Dashboard_Update_Status__c='Updated Successfully';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Net_Amount_to_Be_Refunded__c=82;
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;        
    }
    
    
}