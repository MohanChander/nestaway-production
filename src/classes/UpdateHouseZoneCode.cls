global class UpdateHouseZoneCode implements Database.Batchable<sObject>,Database.AllowsCallouts{

   global final String Query;
  

   global UpdateHouseZoneCode (string inputQuery){

    if(inputQuery==null){    
    
      Query='SELECT id,Onboarding_Zone_Code__c,Onboarding_Zone__c,House_Lattitude__c,House_Longitude__c FROM House__c where Onboarding_Zone_Code__c=null and House_Longitude__c !=null  AND House_Lattitude__c != null and (stage__c!=\'House Live\' and stage__c!=\'Off-Boarded\')';
    }
    else{
        Query=inputQuery;
    }
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      
            Date dateToday = Date.today();
             List<wrapper> wrpList = new List<wrapper>();
            String sMonth = String.valueof(dateToday.month());
            String sDay = String.valueof(dateToday.day());
            if(sMonth.length()==1){
                sMonth = '0' + sMonth;
            }
            if(sDay.length()==1){
                sDay = '0' + sDay;
            }
            String sToday = String.valueof(dateToday.year()) +'-'+sMonth +'-'+ sDay ;
            list <Zing_API_URL__c> zingapiurl = [SELECT id, URL__c  FROM Zing_API_URL__C];
            
            Map<string,Id> zoneMapping = new Map<string,Id>();
            List<Zone__c> zoneLst=[select id,name,Zone_code__c from Zone__c];
            for(Zone__c z: zoneLst){
                
                zoneMapping.put(z.Zone_code__c,z.id);
            }
            
            List<House__c> hsLst=(List<House__c> )scope;
            List<House__c> updateHsLst = new List<House__c>();
            for(House__c h: hsLst){
                
                
                 String ApiEndpoint='';
                
                string lat = string.valueOf(h.House_Lattitude__c);       // '12.9201963' ;  
                string lon = string.valueOf(h.House_Longitude__c);      //'77.6481260' ;
                
                String timestamp = sToday;//'2017-05-23';
                if(zingapiurl.size() > 0){
                    ApiEndpoint = zingapiurl[0].URL__c;
                }
               
                ApiEndpoint+='tag=HouseOnboarding&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    //ApiEndpoint = 'https://staging.nestaway.xyz/admin/zinc/lat_long?tag=OwnerAcquisition&long=77.6481260&lat=12.9201963&timestamp=2017-04-21';
                    
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(ApiEndpoint);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000
                    
                    try {
                        res = http.send(req);
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<Wrapper>)JSON.deserialize(str,List<wrapper>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ){
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);
                                    h.Onboarding_Zone_code__c = wrpList[0].area_id ;
                                    if(zoneMapping.containsKey(h.Onboarding_Zone_code__c)){
                                        h.Onboarding_Zone__c=zoneMapping.get(h.Onboarding_Zone_code__c);
                                    }
                                    updateHsLst.add(h);
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                                
                                
                            }
                            
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                    }
                    
                    
                    
                }
          
            if(updateHsLst.size()>0){
                 Database.update(updateHsLst);
            }
            
           
      
      
      
    }

   global void finish(Database.BatchableContext BC){
   }
   
    public class wrapper{
        public String area_type{get;set;}
        public String area_id{get;set;}
        public String area_name{get;set;}
        public String parent_id{get;set;}
        public String serviceable{get;set;}
    }
}