/****************************************************************
Added by baibhav
***************************************************************/
global class CityHelper {

    //Added by baibhav
    //To give responce of valid city
    public static LIst<Sobject> ValidCity(LIst<Sobject> sobj){
    try{
        List<City__c> ctList=[Select id,name,NewCity__c,NewCity__r.name from City__c];
        Set<String> city=new Set<String>();
        Map<String,String> validCityMap=new  Map<String,String>();
        List<Sobject> passObject=new List<Sobject>();
        for(City__c c:ctList){
            if(c.NewCity__c==null){
              city.add(c.name.toUpperCase());  
            }
            else {
               validCityMap.put(c.name.toUpperCase(),c.NewCity__r.name) ;
            }
        }
        System.debug('***MapCity'+validCityMap);
        System.debug('***MapCitySet'+validCityMap.keySet());
        System.debug('***City'+city);
    
        for(Sobject so:sobj){
               if(String.valueOf(so.getSObjectType())=='Lead'){
                     lead ld=(lead)so;
                     Boolean val=false;
    
                     if(ld.city!=null){
    
                              if(!city.isEmpty()){
                                   val=city.contains(ld.City.toUpperCase());
                                   System.debug('****val'+val);
                                }
                              if(val==false){
                                  if(validCityMap.containsKey(ld.city.toUpperCase())){
                                      system.debug('**valdCity'+validCityMap.get(ld.city.toUpperCase()));
                                      ld.city=validCityMap.get(ld.city.toUpperCase());
                                  }
                                  else{
                                    //ld.addError(label.Error_msg_for_Invalid_city);  
                                  }
                              }
                              passObject.add(ld);
                        }
                }
    
                else if(String.valueOf(so.getSObjectType())=='Opportunity'){
                    Opportunity opp=(Opportunity)so;
                       Boolean val=false;
    
                    if(opp.city__c!=null){  
    
                        if(!city.isEmpty()){
                             val=city.contains(opp.city__c.toUpperCase());
                         }
                         if(val==false){
                             if(validCityMap.containsKey(opp.city__c.toUpperCase())){
                            opp.city__c=validCityMap.get(opp.city__c.toUpperCase());
                            }
                            else{
                              opp.addError(label.Error_msg_for_Invalid_city);      
                            }
                         }
                         passObject.add(opp);
                    }
                }
    
                else if(String.valueOf(so.getSObjectType())=='House__c'){
                    House__c hou=(House__c)so;
                       Boolean val=false;
                       Boolean val1=false;
    
    
                    if(hou.city__c!=null){  
    
                          if(!city.isEmpty()){  
                              val=city.contains(hou.city__c.toUpperCase());
                           }
                          if(val==false){
                              if(validCityMap.containsKey(hou.city__c.toUpperCase())){
                             hou.city__c=validCityMap.get(hou.city__c.toUpperCase());
                             }
                              else{
                               hou.addError(label.Error_msg_for_Invalid_city);    
                             }
                          }
                          passObject.add(hou);
                      }
                      if(hou.Locality_city__c!=null){  
    
                          if(!city.isEmpty()){  
                               val1=city.contains(hou.Locality_city__c.toUpperCase());
                           }
                          if(val1==false){
                              if(validCityMap.containsKey(hou.Locality_city__c.toUpperCase())){
                             hou.Locality_city__c=validCityMap.get(hou.Locality_city__c.toUpperCase());
                             }
                              else{
                               hou.addError(label.Error_msg_for_Invalid_city);   
                             }
                          }
                          passObject.add(hou);
                      }
                }
          
         }
        return passObject;
      }  catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
            UtilityClass.insertGenericErrorLog(e,'Validate city');
            return null;
        }
  }    


}