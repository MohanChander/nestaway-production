/*  Created By : Mohan
    Purpose    : Assignment Logic for House Offboarding Cases and WorkOrders */
public class CaseHouseOffboaringAssignment {

    /*  Created By : Mohan
        Purpose    : Logic for Case Assignment */
    public static void houseOffboardingCaseAssignment(List<Case> caseList){

        try{
                Id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
                Id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
                Id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
                Id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
                Id OffboardingZoneUserRTId =  Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_RECORD_TYPE_OFFBOARDING).getRecordTypeId();
                Id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
                
                Map<String, City__c> cityMap = new Map<String, City__c>();
                Set<Id> caseRecordTypeIdSet = new Set<Id>{
                    caseOccupiedFurnishedRecordtype,
                    caseOccupiedUnfurnishedRecordtype,
                    caseUnoccupiedFurnishedRecordtype,
                    caseUnoccupiedUnfurnishedRecordtype
                };
                set<String> closedStatus = new Set<String>(Label.Move_Out_Case_Closed_Status.split(';'));
                Map<String, Map<Id, Integer>> caseLoadMap = CaseSelector.getCaseLoadMap(caseRecordTypeIdSet, closedStatus, OffboardingZoneUserRTId);
                Set<Id> houseIdSet = new Set<Id>();
                Map<String, Zone__c> zoneMap = new Map<String, Zone__c>();

                for(Zone__c zone: ZoneAndUserMappingSelector.getZoneRecordsForRecordTypeId(OffboardingZoneRTId)){
                    zoneMap.put(zone.Zone_code__c, zone);
                }


                for(City__c city: CitySelector.getCityDetails()){
                    cityMap.put(city.Name.toUpperCase(), city);
                }   

                System.debug('***************caseList: ' + caseList + '\n Size of caseList: ' + caseList.size());

                for(Case c: caseList){
                    houseIdSet.add(c.HouseForOffboarding__c);
                }

                System.debug('***************houseIdSet: ' + houseIdSet + '\n Size of houseIdSet: ' + houseIdSet.size());

                Map<Id, House__c> houseMap = new Map<Id, House__c>(HouseSelector.getHouseListFromIdSet(houseIdSet));
                Id unassignedHouseOffboardingCaseQueueId = GroupAndGroupMemberSelector.getIdfromName(Constants.QUEUE_UNASSIGNED_HOUSE_OFFBOARDING_CASE_QUEUE);

                System.debug('***************houseMap: ' + houseMap + '\n Size of houseMap: ' + houseMap.size());
                System.debug('***************caseLoadMap: ' + caseLoadMap + '\n Size of caseLoadMap: ' + caseLoadMap.size());

                for(Case c: caseList){
                    String houseCity = !houseMap.containsKey(c.HouseForOffboarding__c) || houseMap.get(c.HouseForOffboarding__c).City__c == null ? null : houseMap.get(c.HouseForOffboarding__c).City__c.toUpperCase();
                    if(c.Offboarding_Zone_Code__c != null && !caseLoadMap.isEmpty() && caseLoadMap.containsKey(c.Offboarding_Zone_Code__c) && 
                        !caseLoadMap.get(c.Offboarding_Zone_Code__c).isEmpty()){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: caseLoadMap.get(c.Offboarding_Zone_Code__c).keySet()){
                            awList.add(new AssignmentWrapper(userId, caseLoadMap.get(c.Offboarding_Zone_Code__c).get(userId)));
                        }  
                        awList.sort();
                        c.OwnerId = awList[0].UserId;
                        Integer noOfCases = caseLoadMap.get(c.Offboarding_Zone_Code__c).get(awList[0].UserId);
                        caseLoadMap.get(c.Offboarding_Zone_Code__c).put(awList[0].UserId, noOfCases+1);
                    } 
                    //Assign the case to the ZOM 
                    else if(c.Offboarding_Zone_Code__c != null && zoneMap.containsKey(c.Offboarding_Zone_Code__c) &&
                            zoneMap.get(c.Offboarding_Zone_Code__c).ZOM__c != null){
                        c.OwnerId = zoneMap.get(c.Offboarding_Zone_Code__c).ZOM__c;
                    } 
                    // Assign the Case to City Head
                    else if(houseCity != null && cityMap.containsKey(houseCity) && cityMap.get(houseCity).ROM__c != null){
                        c.OwnerId = cityMap.get(houseCity).ROM__c;
                    }
                    // Assign the Case to the Queue
                    else
                        c.OwnerId = unassignedHouseOffboardingCaseQueueId;  
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);              
            }
    }

    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }

        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }   
}