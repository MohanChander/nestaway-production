global class GenerateInvoiceButtonPageExtension {

    global Case c {get; set;}
    global boolean stop {get; set;}

    global GenerateInvoiceButtonPageExtension(ApexPages.StandardController stdController){

        try{
                this.c = (Case)stdController.getRecord();
                Id genTaskRT = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_VERIFICATION_TASK).getRecordTypeId();
                list<task> tskList=[Select id,status from task where whatId=:this.c.id and RecordTypeId=:genTaskRT 
                                    and status!=:Constants.TASK_STATUS_VERIFED];

                Case caseObj = [select Id, Material_Cost__c, Labour_Cost__c, Tenant_Material_Cost__c, Owner_Material_Cost__c, 
                                NestAway_Material_Cost__c, Tenant_Labor_Cost__c, Owner_Labor_Cost__c, NestAway_Labor_Cost__c,
                                Recoverable_From__c, Zone_Code__c, Invoice_Generated__c, CaseNumber, Status
                                from Case where Id =: c.Id];

                this.stop = false;

                /* Commented By : Mohan - SnM Deployed in all the Zones - Hence code commented
                //Allow Generate Invoice buttton to only certain Zones
                set<String> deployedZones = new Set<String>(Label.SnM_Deployed_Zones.split(';'));
                if(!deployedZones.contains(caseObj.Zone_Code__c)){
                    String addErrorMessage='Generate Invoice button is not current available in ' + caseObj.Zone_Code__c + '. Please check with Admin';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                    return;             
                }   End of comment */         

                //Invoice cannot be generated more than once
                if(caseObj.Invoice_Generated__c){
                    String addErrorMessage='Invoice is already generated for Case ' + caseObj.CaseNumber;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                    return;                 
                }

                //Check if there are any Vendor Work Orders that got created and then validate if there are any Open WorkOrders
                List<WorkOrder> woList = [select Id, isClosed, WorkOrderNumber from WorkOrder where CaseId =: caseObj.Id 
                                          and RecordType.Name =: Constants.WORKORDER_RT_VENDER];
                if(woList.isEmpty()){
                    String addErrorMessage='There are no Vendor Work Orders associated with the Case ' + caseObj.CaseNumber;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                    return;                     
                } 

                for(WorkOrder wo: woList){
                    if(!wo.isClosed){
                        String addErrorMessage='The Work Order ' + wo.WorkOrderNumber + ' related to the Case is still open.';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                        return; 
                    }
                }    

                //Check if there is any Verfication thats in Open State
                List<Task> taskList = [select Id, Status, IsClosed from Task where whatId =: caseObj.Id
                                       and RecordType.Name =: Constants.TASK_RT_VERIFICATION_TASK];
                for(Task t: taskList){
                    if(!t.isClosed){
                        String addErrorMessage='Please close the Verification Task before generating the Invoice';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
                        return;                         
                    }
                }                                                 

                //Check if the Material Cost is split accurately
                if(caseObj.Material_Cost__c != caseObj.Owner_Material_Cost__c + caseObj.Tenant_Material_Cost__c + caseObj.NestAway_Material_Cost__c){
                    this.stop=false;
                    String addErrorMessage='Please check the Material Cost split. It does not match the Material Cost ' + caseObj.Material_Cost__c;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));
                    return; 
                }

                //Check if the Labour Cost is split accurately
                if(caseObj.Labour_Cost__c != caseObj.Owner_Labor_Cost__c + caseObj.Tenant_Labor_Cost__c + caseObj.NestAway_Labor_Cost__c){
                    this.stop=false;
                    String addErrorMessage='Please check the Labour Cost split. It does not match the Labour Cost ' + caseObj.Labour_Cost__c;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
                    return; 
                }       

                //Check for Split
                if(caseObj.Recoverable_From__c == 'Tenant'){
                    if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Tenant';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
                        return;                 
                    }

                    if(caseObj.Owner_Labor_Cost__c > 0 || caseObj.Owner_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Tenant';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
                        return;                 
                    }           
                }

                if(caseObj.Recoverable_From__c == 'Owner'){
                    if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Owner';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage)); 
                        return;                 
                    }

                    if(caseObj.Tenant_Labor_Cost__c > 0 || caseObj.Tenant_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Owner';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));    
                        return;             
                    }           
                }   

                if(caseObj.Recoverable_From__c == 'Tenant & Owner'){
                    if(caseObj.NestAway_Labor_Cost__c > 0 || caseObj.NestAway_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with NestAway when Recoverable From is Tenant & Owner';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));    
                        return;             
                    }       
                }

                if(caseObj.Recoverable_From__c == 'Tenant & NestAway'){
                    if(caseObj.Owner_Labor_Cost__c > 0 || caseObj.Owner_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with Owner when Recoverable From is Tenant & NestAway';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));        
                        return;         
                    }       
                }

                if(caseObj.Recoverable_From__c == 'Owner & NestAway'){
                    if(caseObj.Tenant_Labor_Cost__c > 0 || caseObj.Tenant_Material_Cost__c > 0){
                        this.stop=false;
                        String addErrorMessage='You cannot Split the Share with Tenant when Recoverable From is Owner & NestAway';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));    
                        return;             
                    }       
                }                                   

                if(tskList!=null && tskList.size()!=0)
                {   
                    this.stop=false;
                    String addErrorMessage='Please ensure that Verification Task is Verified before generating the Invoice';
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));   
                }

            } catch(Exception e){
                    System.debug('****Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString()); 
                    this.stop=false;
                    String addErrorMessage='There is an issue. Please contact your admin';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,addErrorMessage));   
            }     

            this.stop = true;                   
    }

    @RemoteAction
    global static Boolean sendInvoiceDetailsToWebApp(Id caseId, boolean Stop){

        System.debug('**********************sendInvoiceDetailsToWebApp');
        Case updatedCase = new Case();
        updatedCase.Id = caseId; 

        try{
            if(stop)
                {
                    Set<Id> cdIdSet = new Set<Id>();
                    Set<Id> contentDocIdSet = new Set<Id>();
                    Set<id> invSet =new Set<id>();
                    Set<Id> invWoSet = new Set<Id>();
                    Map<id,ContentDocumentLink> cdMap = new Map<Id, ContentDocumentLink>();
                    List<WBMIMOJSONWrapperClasses.FileObj> fileObjList = new List<WBMIMOJSONWrapperClasses.FileObj>();
                    set<String> fslDeployedZonesSet = new Set<String>(Label.FSL_Deployed_Zones.split(';'));

                    Case c = [select Id, CaseNumber, Free_Visit__c, Tenant_Labor_Cost__c, Service_visit_cost__c,
                              Express_Service_Cost__c, Tenant_Material_Cost__c, Owner_Labor_Cost__c, Owner_Material_Cost__c,
                              NestAway_Labor_Cost__c, NestAway_Material_Cost__c, Zone_Code__c from Case where Id =: caseId];

                    List<String> imageURLList = new List<String>();
                    HttpRequest req = new HttpRequest();
                    
                    List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                    String authToken=nestURL[0].Webapp_Auth__c;
                    String EndPoint = nestURL[0].Invoice_Generation_API__c +'&auth='+authToken;
                    
                    System.debug('***EndPoint  '+EndPoint);

                    List<WorkOrder> woList = [select Id, (SELECT Id, Name FROM Invoices__r),
                                              (SELECT Id from Work_Orders__r) 
                                              from WorkOrder where CaseId =: c.Id and Status =: Constants.WORKORDER_STATUS_RESOLVED
                                              limit 1];
                                              
                       for(WorkOrder wo: woList){
                        for(Invoice__c inv: wo.Invoices__r){
                           invSet.add(inv.id);
                        }

                        for(WorkOrder invWo: wo.Work_Orders__r){
                            invWoSet.add(invWo.Id);
                        }
                       } 

                    if(!invSet.isEmpty() && !fslDeployedZonesSet.contains(c.Zone_Code__c)){
                        cdMap = new Map<id,ContentDocumentLink>([SELECT id,ContentDocumentId, LinkedEntityId 
                                                                                           FROM ContentDocumentLink 
                                                                                           WHERE LinkedEntityId in :invSet]); 
                    }

                    if(!invWoSet.isEmpty() && fslDeployedZonesSet.contains(c.Zone_Code__c)){
                        cdMap = new Map<id,ContentDocumentLink>([SELECT id,ContentDocumentId, LinkedEntityId 
                                                                                           FROM ContentDocumentLink 
                                                                                           WHERE LinkedEntityId in :invWoSet]); 
                    }                    

                        for(ContentDocumentLink cdl: cdMap.values()){
                            contentDocIdSet.add(cdl.ContentDocumentId);
                        }                                                                                                       

                        List<ContentVersion> cvList = [select Id, Title, FileExtension from ContentVersion where 
                                                       ContentDocumentId =: contentDocIdSet 
                                                       and isLatest = true];                                                                               

                        for(ContentVersion cv: cvList){
                            WBMIMOJSONWrapperClasses.FileObj fileObj = new WBMIMOJSONWrapperClasses.FileObj();
                            fileObj.filename = cv.Title + '.' + cv.FileExtension;
                            fileObj.blob_url = '/services/data/v41.0/sobjects/ContentVersion/' +
                                               cv.Id + '/VersionData';
                            fileObjList.add(fileObj);
                        }
                    

                    System.debug('***************fileObjList: ' + fileObjList + '\n Size of fileObjList: ' + fileObjList.size());
                    
                    string jsonBody='';
                    WBMIMOJSONWrapperClasses.ServiceRequestInvoiceJSON invoiceObject = new WBMIMOJSONWrapperClasses.ServiceRequestInvoiceJSON();
                    invoiceObject.ticket_number = c.CaseNumber;
                    invoiceObject.free_visit = c.Free_Visit__c;
                    invoiceObject.tenant_labour_cost = c.Tenant_Labor_Cost__c;
                    invoiceObject.service_visit_cost = c.Service_visit_cost__c;
                    invoiceObject.express_service_cost = c.Express_Service_Cost__c;
                    invoiceObject.tenant_material_cost = c.Tenant_Material_Cost__c;
                    invoiceObject.owner_labour_cost = c.Owner_Labor_Cost__c;
                    invoiceObject.owner_material_cost = c.Owner_Material_Cost__c;
                    invoiceObject.nestaway_labour_cost = c.NestAway_Labor_Cost__c;
                    invoiceObject.nestaway_material_cost = c.NestAway_Material_Cost__c;      

                    WBMIMOJSONWrapperClasses.AttachmentsObject attObj = new WBMIMOJSONWrapperClasses.AttachmentsObject();
                    attObj.material_related = fileObjList;

                    invoiceObject.attachments = attObj;

                    jsonBody = JSON.serialize(invoiceObject);
                    System.debug('***jsonBody  '+jsonBody);
                    
                    HttpResponse res;
                    res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                    
                    String respBody = res.getBody();
                    system.debug('****respBody'+ respBody);
                    System.debug('**resStatusCode' + res.getStatusCode());

                        
                    if(res.getStatusCode() == 200) {  

                        //allow Case closure for a non admin user by setting the flag to true
                        CaseServiceRequestTriggerHandler.canCloseCase = true;

                        updatedCase.Invoice_Generated__c = true;
                        updatedCase.Status = 'Closed';
                        updatedCase.Generate_Invoice_API_Success__c = res.getStatus();
                        updatedCase.Generate_Invoice_API_Info__c = res.getBody();
                        update updatedCase;
                        return true;
                    } else{
                        updatedCase.Generate_Invoice_API_Success__c = res.getStatus();
                        updatedCase.Generate_Invoice_API_Info__c = res.getBody();
                        update updatedCase;                     
                        return false;
                    }
                }
                else
                {
                  return false; 
                }
        }
        catch (Exception e){
            System.debug('*******************There is an exception');
            UtilityClass.insertGenericErrorLog(e, 'Invoice Generation API'); 
            updatedCase.Generate_Invoice_API_Success__c = 'Exception';
            updatedCase.Generate_Invoice_API_Info__c = 'Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                                                        e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() +
                                                        '\n Cause: ' + e.getCause() + '\nStack Trace ' + 
                                                        e.getStackTraceString();
            update updatedCase;             
            return false;   
        }               
    }       
}