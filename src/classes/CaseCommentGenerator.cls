/********************************************
 * Created By : Mohan
 * Purpose    : To Generate Case Comments for different events.
 * ******************************************/
public class CaseCommentGenerator {
    
    public static Case_Comment__c generateVisitVerifiedComment(String technicianName, Date visitDate, String visitTime, Id caseId){
        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = caseId;
        cc.System_Generated_Comment__c = true;
        cc.Comment__c = 'Hello, \n \n' + 
                        'Our technician, ' + technicianName + ', will visit your house on ' + visitDate + ' at ' + visitTime + 'to fix this.\n' + 
                        ' \n' +
                        'Thank you for your patience.\n' +
                        'Note: This is an auto-generated message.';
        return cc;
    }
    
    public static Case_Comment__c generateVisitRescheduledComment(String technicianName, Date visitDate, String visitTime, Id caseId){
        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = caseId;
        cc.Comment__c = 'Hello, \n \n' +
                        'There has been a change in the visit to sort this. We\'re sorry as all our technicians have been caught up.\n \n' + 
                        'We have rescheduled the visit to ' + visitDate + ' at ' + visitTime + '. Our technician, ' + technicianName + ', will attend to this.\n' + 
                        'We appreciate your patience.\n \n' +
                        'Note: This is an auto-generated message.';
        cc.System_Generated_Comment__c = true;
        return cc;
    }

    public static Case_Comment__c generateOwnerInclusionUpdatedSLAComment(String expectedResolutionTime, Id caseId){
        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = caseId;
        cc.Comment__c = 'Hello, \n \n' +
                        'This service request requires the owner\'s approval of the quotation. We have already notified them about this and are awaiting their reply. \n \n' +
                        'Once we have their approval, we will begin the work right away. We are expecting to resolve this by ' + expectedResolutionTime + '.'; 
        cc.System_Generated_Comment__c = true;
        return cc;        
    }

    public static Case_Comment__c generateOwnerApprovalComment(Id caseId){
        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = caseId;
        cc.Comment__c = 'Hello, \n \n' +
                        'The owner has approved the quotation for the work required to fix this issue. We have already started the process to solve this. You will hear from us soon.';
        cc.System_Generated_Comment__c = true;
        return cc;          
    }

    public static Case_Comment__c generateOwnerRejectionComment(Id caseId){
        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = caseId;
        cc.Comment__c = 'Hello, \n \n' +
                        'Unfortunately, the owner has declined our request to start work on this. We will need more time than expected as we discuss this issue with them personally. \n \n' +
                        'Thank you for being patience.';

        cc.System_Generated_Comment__c = true;
        return cc;          
    }    
}