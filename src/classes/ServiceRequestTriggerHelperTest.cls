@IsTest
public class ServiceRequestTriggerHelperTest {
   
    public static Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get('Property Management Zone').getRecordTypeId();
    public static Id zuApmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Assistance Property Manager').getRecordTypeId();
    public static Id zuPmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Property Manager').getRecordTypeId();
     public static Id serviceRequestWO = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_Service_Request_WorkOrder).getRecordTypeId();
    static Id serviceRequestMappingRTId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();

    public Static TestMethod Void  TestMethod1(){
     List<Case>  cList =  new List<Case>();

        Test.startTest() ;
       zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;       
      
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
       
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = serviceRequestMappingRTId;
        zu1.isActive__c =true;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
		 problem__c p= new problem__c();        
        House__c hos= new House__c();
        hos.name='house1';
        hos.apm__c = newuser.id;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
        
         Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        Case c=  new Case();
        c.Status='Waiting On Tenant Approval';
        c.House__c=hos.Id;
        c.Problem__c=p.id;
        c.RecordTypeId=devRecordTypeId;
        c.ownerId=newuser.Id;
        c.Subject='test';
        c.Next_Escalation_to__c= newuser.id;
        insert c;
        cList.add(c);
        
		WorkOrder wo = new WorkOrder();
		wo.House__c=hos.id;
        wo.caseid = c.id;
        wo.status = 'Open';
        wo.Queue__c='Finance';
        wo.RecordTypeId=serviceRequestWO;
		insert wo;  
        WorkOrder wo1 = new WorkOrder();
		wo1.House__c=hos.id;
        wo1.caseid = c.id;
        wo1.status = 'Closed';
        wo1.Queue__c='Property Manager';
        wo1.RecordTypeId=serviceRequestWO;
		insert wo1;
       
             WorkOrder wo2 = new WorkOrder();
		wo2.House__c=hos.id;
        wo2.caseid = c.id;
        wo2.status = 'Closed';
        wo2.Queue__c='Services Electrical';
        wo2.RecordTypeId=serviceRequestWO;
        wo2.Service_Zone_Code__c='359';
		insert wo2;
         Test.stopTest();
        List<WorkOrder> woList =  new List<WorkOrder>();
        woList.add(wo);
        woList.add(wo1);
        woList.add(wo2);
        ServiceRequestTriggerHelper.workOrderAssignment(woList);
        ServiceRequestTriggerHelper.afterInsert(woList);
            ServiceRequestTriggerHelper.afterupdate(woList);
            ServiceRequestTriggerHelper.beforeinsert(woList);
            ServiceRequestTriggerHelper.changeOwnerEmailOnWorkOrder(cList);
    }
     

}