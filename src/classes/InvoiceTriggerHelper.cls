/*******************************************
 * Created By: Mohan
 * Purpose   : Helper Class for Invoice Trigger
 * *****************************************/ 
public class InvoiceTriggerHelper {

/*******************************************
 * Created By: Mohan
 * Purpose   : Change the Invoice Owner to Finance Queue when Approved
 * *****************************************/   
  public static void changeInvoiceOwnerToFinanceQueue(List<Invoice__c> invList){
        try{  
            Id financeQueueId = GroupAndGroupMemberSelector.getIdfromName('Finance');

            for(Invoice__c inv: invList){
              inv.OwnerId = financeQueueId;
            }
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                           '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() +
                           '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'changeInvoiceOwnerToFinanceQueue');      
            }   
  }


/**********************************************************************************************************************************************
    Added by baibahv
    Purpose for creating Invoice settlement case
            
**********************************************************************************************************************************************/
    public static void CreateInvoiceSettlementCase(List<Invoice__c> invList)
    {  System.debug('Bony2');
        try
           {            Id invSettlement =Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_INVOICE_SETTLEMENT).getRecordTypeId();  
                        List<Case> newCase =new List<Case>();
                        Set<id> accid =new Set<Id>();
                        Map<Id, Account> accountMap=new Map<Id, Account>();
                        List<Bank_Detail__c> bnkList =new List<Bank_Detail__c>(); 
                        Group grp=[SELECT Id,Name,Type FROM Group where name='Finance'];
                        for(Invoice__c inv:invList)
                        {
                          accid.add(inv.Vendor__c);
                        }
                        if(!accid.isEmpty())
                        {
                            accountMap = new Map<Id, Account>([select Id, Name, Tax_Section__r.TDS_Rates__c, TIN_No__c,
                                                               City__r.Name, BillingState, Tax_Section__r.Name, Tax_Section__r.Type__c,
                                                               GST_NO__c,GST_Registered__c,PAN_Available__c,PAN_Number__c from 
                                                               Account where Id =: accid]);
                            

                         
                            bnkList =BankDetailSelector.getBankDetailByAccount(accid);
                        }
                        System.debug('Bony2'+bnkList);
                       Map<id,Bank_Detail__c> bnkMapToAccID =new Map<id,Bank_Detail__c>();
                      for(Bank_Detail__c bk:bnkList)
                         {
                           bnkMapToAccID.put(bk.Related_Account__c,bk);
                         }
                         System.debug('Bony3');
                         for(Invoice__c inv:invList)
                         {
                           
                            System.debug('Bony3-1');

                             

                              Case ca=new Case();
                              ca.RecordTypeId=invSettlement;
                              ca.Billed_To__c = inv.Billed_To__c;
                              ca.Recoverable__c = inv.Recoverable__c;
                              ca.subject='Invoice Settlement Case';
                              if(inv.Vendor__c!=null)
                              {
                                ca.AccountId = inv.Vendor__c; 
                                if(accountMap.containsKey(inv.Vendor__c)){

                                  if(accountMap.get(inv.Vendor__c).GST_Registered__c!=null){
                                    ca.GST_Registered__c=accountMap.get(inv.Vendor__c).GST_Registered__c;
                                  }
                                  if(accountMap.get(inv.Vendor__c).GST_NO__c!=null){
                                    ca.GST_NO__c=accountMap.get(inv.Vendor__c).GST_NO__c;
                                  }
                                  if(accountMap.get(inv.Vendor__c).PAN_Number__c!=null){
                                    ca.PAN__c=accountMap.get(inv.Vendor__c).PAN_Number__c;
                                  }
                                   if(accountMap.get(inv.Vendor__c).Tax_Section__c!=null){
                                    ca.TDS_section__c=accountMap.get(inv.Vendor__c).Tax_Section__r.Name;
                                  }
                                   if(accountMap.get(inv.Vendor__c).Tax_Section__r.Type__c!=null){
                                    ca.TDS_type__c=accountMap.get(inv.Vendor__c).Tax_Section__r.Type__c;
                                  }
                                }                             
                              }
                              else
                              {
                                inv.addError('Vendor field is Blank. Vender should be Selected');
                              }
                              if(inv.House__c!=null)
                              {
                               ca.House1__c=inv.House__c;
                               ca.house__c=inv.house__c;
                              }
                              else
                              {
                                 inv.addError('House field is Blank.');
                              }
                              ca.Invoice__c=inv.id; 
                              ca.Settlement_Amount_Calculated_Via_System__c=inv.Amount__c ;
                              ca.status='New';                       
                              if(grp!=null)
                              {
                                ca.ownerId=grp.id;
                              }
                              System.debug('Bony3-1'+bnkMapToAccID.get(inv.Vendor__c));

                              if(bnkMapToAccID.containsKey(inv.Vendor__c))
                              {
                                if(bnkMapToAccID.get(inv.Vendor__c).name!=null)
                                {
                                  ca.A_c_Holder_Name_Tenant__c=bnkMapToAccID.get(inv.Vendor__c).name;
                                }
                                if(bnkMapToAccID.get(inv.Vendor__c).Account_Number__c!=null)
                                {
                                  ca.Tenant_bank_A_c_No__c=bnkMapToAccID.get(inv.Vendor__c).Account_Number__c;
                                }
                                if(bnkMapToAccID.get(inv.Vendor__c).IFSC_Code__c!=null)
                                {
                                  ca.Bank_IFSC_code__c=bnkMapToAccID.get(inv.Vendor__c).IFSC_Code__c;
                                }
                                if(bnkMapToAccID.get(inv.Vendor__c).Bank_Name__c!=null)
                                {
                                   ca.Tenant_Bank_Name__c=bnkMapToAccID.get(inv.Vendor__c).Bank_Name__c;
                                }
                                if(bnkMapToAccID.get(inv.Vendor__c).Branch_Name__c!=null)
                                {
                                   ca.Tenant_Bank_Branch_Name__c=bnkMapToAccID.get(inv.Vendor__c).Branch_Name__c;
                                }
                              }
                              newCase.add(ca);
                            
                         } 
                         if(!newCase.isEmpty())
                         {System.debug('Bony4'+newCase);
                           insert newCase;
                         }

           }catch (Exception e)
           {
              System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                            e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + 
                            '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
              UtilityClass.insertGenericErrorLog(e, 'Vendor Settlement Case Creation');
           }
    }
}