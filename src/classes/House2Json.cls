public class House2Json {

    @future(CallOut=True)
    public static void createJson(Id houseId){
         
          createJsonMethod(houseId);
    }
    
    public static void createJsonMethod(Id houseId){    

       // System.debug('**************createJson Method Invoked');

        try{
                House__c house = HouseSelector.getHouseDetailsFromHouseId(houseId);
                
            boolean runTriggers=true;    
            NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values();   
            if(checkMigration.Disable_Data_Migration_Triggers__c && house.Data_Migration__c){

                runTriggers=false;

            }
            if(runTriggers){
                Id accId;
                Id oppId;
                Id contractId; 

                if(house.Opportunity__r.AccountId != null){
                    accId = house.Opportunity__r.AccountId;
                }

                if(house.Opportunity__c != null){
                    oppId = house.Opportunity__c;
                }

                if(house.Contract__c != null){
                    contractId = house.Contract__c;
                }
                // Added by chandu:- adding current timestamp and unique SF id in json.
                JSONGenerator gen_opp = JSON.createGenerator(true);
                gen_opp.writeStartObject();
                gen_opp.writeFieldName('HouseJsonParam');
                gen_opp.writeStartArray();
                gen_opp.writeStartObject();
                gen_opp.writeStringField('Id',String.ValueOf(house.Id));
                gen_opp.writeStringField('TimeStamp',String.ValueOf(DateTime.now().getTime()));
                gen_opp.writeEndObject();
                gen_opp.writeEndArray();
                gen_opp.writeEndObject();
                string houseParam   = gen_opp.getAsString();
                houseParam    = houseParam.substring(0,houseParam.length()-1);
                houseParam        = houseParam.substring(1,houseParam.length());

                
                

                String strHouseJson = HouseJson.GetObjectJson('House__c', houseId);
                String strOppJson;
                String strHIC_Json;
                String strContractJson;
                String strAccountJSON;
                String strBathroomJSON;
                String strPhotosJSON;
                String strFinalJSON;

              //  System.debug(strHouseJson);

                strHouseJson    = strHouseJson.substring(0,strHouseJson.length()-1);
                
                strOppJson        = HouseJson.GetObjectJson('Opportunity', oppId);
                strOppJson        = strOppJson.substring(0,strOppJson.length()-1);
                strOppJson        = strOppJson.substring(1,strOppJson.length());
                
                strHIC_Json       = HouseJson.GetObjectJson('House_Inspection_Checklist__c', oppId);
                strHIC_Json       = strHIC_Json.substring(0,strHIC_Json.length()-1);
                strHIC_Json       = strHIC_Json.substring(1,strHIC_Json.length());
                
                // modified by chandu: for contract now we have multiple contract so take the latest one updated on house.
                // strContractJson   = HouseJson.GetObjectJson('Contract', oppId);  
                strContractJson   = HouseJson.GetObjectJson('Contract', ContractId);
                strContractJson   = strContractJson.substring(0,strContractJson.length()-1);
                strContractJson   = strContractJson.substring(1,strContractJson.length());
                
                strAccountJSON    = HouseJson.GetObjectJson('Account', accId);
                strAccountJSON    = strAccountJSON.substring(0,strAccountJSON.length()-1);
                strAccountJSON    = strAccountJSON.substring(1,strAccountJSON.length());        

                String strRoomTermJson = HouseJson.createRoomJson('Room_Terms__c',houseId);
                strRoomTermJson   = strRoomTermJson.substring(0,strRoomTermJson.length()-1);
                strRoomTermJson   = strRoomTermJson.substring(1,strRoomTermJson.length());  
              //  System.debug(strRoomTermJson);      

                strBathroomJSON   = HouseJson.createRoomJson('Bathroom__c',houseId);
                strBathroomJSON   = strBathroomJSON.substring(0,strBathroomJSON.length()-1);
                strBathroomJSON   = strBathroomJSON.substring(1,strBathroomJSON.length());

                strPhotosJSON   = HouseJson.createRoomJson('Photo__c',houseId);
                strPhotosJSON   = strPhotosJSON.substring(1,strPhotosJSON.length());    
                
               // strFinalJSON = strHouseJson +','+strAccountJSON +',' +strOppJson + ','+strHIC_Json +','+strContractJson + ',' + strRoomTermJson + 
                 strFinalJSON = strHouseJson+','+houseParam+','+strAccountJSON +',' +strOppJson + ','+strHIC_Json +','+strContractJson + ',' + strRoomTermJson + 
                               ','+ strBathroomJSON +',' + strPhotosJSON;

               System.debug('***********FinalJson' + strFinalJSON.replaceall('\r\n',''));
                
                HttpRequest req = new HttpRequest();

                List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();
                System.debug('*********** '+nestURL[0].Webentity_House_Url__c);         

                String strEndPoint = nestURL[0].Webentity_House_Url__c;
                
                req.setEndPoint(strEndPoint);
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/json');
                req.setMethod('POST');
                string jsonString = strFinalJSON.replaceall('\r\n','');
                req.setBody(jsonString);
                req.setTimeout(120000);
               // System.debug('==req=='+req);
                System.debug('==req=='+req.getBody());
                Http http = new Http();
                HttpResponse res;
                
                if(!Test.isRunningTest()){
                 
                  res = http.send(req);
                //  System.debug('===res=='+res);
                  system.debug('==Body=='+res.getBody());
                  String str = res.getBody();
                  Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(str);
                  System.debug('==m=='+m);
                  
                    if(res.getStatusCode()==200){
                        system.debug('*****************response' + res + '\n Body: ' + str); 
                        house.Api_House_Success__c = String.ValueOf(m.get('success'));
                        house.Api_House_Info__c = String.ValueOf(m.get('info'));                        
                    } else { 
                        house.Api_House_Success__c = String.ValueOf(m.get('success'));
                        house.Api_House_Info__c = String.ValueOf(m.get('info'));                               
                    }  
                    house.Last_API_Sync_Time__c = System.now();
                } 

                String houseStage = [select Id, Stage__c from House__c where Id =: house.Id].Stage__c;

                //Update the Room, Bathroom Names when the House Id is populated on the House
                List<House__c> updatedHouseList = new List<House__c>();
                if(!house.Updated_Child_Name_with_House_Id__c && house.HouseId__c != null){
                    updatedHouseList.add(house);
                }

               


                if(house != null && houseStage != Constants.HOUSE_STAGE_OFFBOARDED){                    
                    StopRecursion.HouseSwitch = false;

                    if(!updatedHouseList.isEmpty()){
                 //       HouseTriggerHelper.updateRoomAndBedAndRIC(updatedHouseList);
                    }

                   

                    update house;
                    StopRecursion.HouseSwitch = true;
                }
            }
            } catch (Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
                UtilityClass.insertGenericErrorLog(e, 'Exception in House2Json');      
            }
    }   
}