@IsTest
public class MoveInScheduleRescheduleControllerTest {
      Public Static TestMethod Void doTest(){
        
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get('Property Management Zone').getRecordTypeId();
        Id zuApmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Assistance Property Manager').getRecordTypeId();
        Id zuPmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Property Manager').getRecordTypeId(); 
           User newUser = Test_library.createStandardUser(1);
        insert newuser;
            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            cusSet.MIMO_Calendar_Date_API__c='MoveInDateAPI';
            cusSet.Moveout_Calendar_Date_API__c='MoveInDateAPI';
            cusSet.MIMO_FE_Calendar_API__c='MoveInFECalAPI';
            cusSet.MIMO_Calendar_Schedule_API__c='MoveInFESchAPI';
            insert cusSet;
          Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        City__c ct=new City__c();
        ct.name='Bangalore';
        insert ct;
        
        House__c hos= new House__c();
        hos.name='house1';
          hos.Onboarding_Zone_Code__c='32';
        hos.House_Owner__c=accObj.Id;
        hos.Assets_Created__c=false;
        hos.Name ='Test';
        hos.City__c='Bangalore';
        insert hos;
      
        zone__c zc= new zone__c();
        zc.Zone_code__c ='32';
        zc.Name='Test';
        zc.City__c=ct.id;
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;  
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;

        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2; 
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*
        Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;
        */
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
          b.House__c=hos.id;
        insert b;
        
        List<Case> csLst= new List<Case>();
        Case c=  new Case();
        c.Status=Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.AccountId=accObj.Id;
         // c.House_Id__c=hos.id;
        
        c.MoveIn_Slot_End_Time__c=System.today();
        c.MoveIn_Slot_Start_Time__c=System.today();
       c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='Room';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOve out';
        c.Move_in_date__c=system.today();
        c.License_End_Date__c=system.today();
        c.License_Start_Date__c=system.today();
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        csLst.add(c);
     //    insert c;
        case c1 = new case();
        c1=c.clone();
        c1.RecordTypeId=MoveOutRecordTypeId;
        csLst.add(c1);
        insert csLst;
        //insert c1;  
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MoveInScheduleRescheduleContCalloutMock());
            Apexpages.currentPage().getParameters().put('Id', c.id);
            ApexPages.StandardController sC = new ApexPages.standardController(c);
            ApexPages.StandardController sC1 = new ApexPages.standardController(c1);
            MoveInScheduleRescheduleController mo= new MoveInScheduleRescheduleController(sc);
            mo.getDatesFromNAApp(c.id);
            mo.allUsersoption();
            mo.populateFesWithSelecteddate();
            mo.getslotoption();
            mo.selectedUsers=newUser.id;
            mo.selectedDate='2017-08-21';           
            mo.populateTimeSlotsWithFEs();
            mo.getdateOptions();
            mo.showCityUsers();
            mo.showZoneUsers();
            mo.cancel();
            mo.SelectedSlot='0';            
            mo.saveMIMOFESlot();
            mo.CityFromHouse='Delhi';
            mo.allFe_user();
            mo.CityFromHouse='Noida';
            mo.allFe_user();
            mo.CityFromHouse='Gurugram';
            mo.allFe_user();
            mo.CityFromHouse='Navi Mumbai';
            mo.allFe_user();            
            MoveInScheduleRescheduleController.fromJSONWrapper wrap= new MoveInScheduleRescheduleController.fromJSONWrapper();
            wrap.start_date='13-07-2017';
            wrap.end_date='13-07-2017';
            wrap.holidays=new Map<String,String>();
            Apexpages.currentPage().getParameters().put('Id', c1.id);           
            MoveInScheduleRescheduleController mo1= new MoveInScheduleRescheduleController(sc1);
            mo1.getDatesFromNAApp(c1.id);
            mo1.allUsersoption();
          test.stoptest();
      }
}