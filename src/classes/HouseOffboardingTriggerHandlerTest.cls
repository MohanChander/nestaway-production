/******************************************************************
Added By baibhav
date: 14/11/2017
****************************************************************/
@Istest
public class HouseOffboardingTriggerHandlerTest {
	public static TestMethod void  Test1(){
        
         Account Acc = new Account(Name='Test',PAN_Number__c='123456');
         acc.PAN_Number__c='CAVPK1234V';
         insert Acc;

		Opportunity objOpp = new Opportunity();
        objOpp.AccountId = Acc.Id;
        objOpp.Name ='Test Opp';
        objOpp.CloseDate = System.Today();
        objOpp.StageName = 'House Inspection';

        insert objOpp;

		Contract objContract         = new Contract();
        objContract.Opportunity__c   = objOpp.Id;
        objContract.AccountId        = objOpp.AccountId;
        objContract.StartDate        = objOpp.CloseDate;
        objContract.Status           = 'Draft';
        objContract.ContractTerm     = 11;
        objContract.Contact_Email__c = objOpp.Contact_Email__c;
        objContract.Furnishing_Package__c = 'Package A';
        insert objContract;

        Profile pro=[select id from Profile where name='RM'];

         user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend';
        us.lastname='qwer';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.Phone='9876543212';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

 	    House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.Opportunity__c=objOpp.id;
        hos.Contract__c=objContract.id;
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        hos.HRM__c=us.id;
        hos.ZOM__c=us.id;
        hos.ZAM__c=us.id;
        insert hos;

        Room_Terms__c rc= new Room_Terms__c();
        rc.House__c=hos.id;
        insert rc;

        bed__c bedc= new bed__c();
        bedc.House__c=hos.id;
        bedc.Room_Terms__c=rc.id;
        bedc.status__c=Constants.BED_STATUS_SOLD_OUT;
        insert bedc;
       

        hos.Initiate_Offboarding__c='Yes';
        hos.Offboarding_Reason__c='Owner has sold the Property';
        update hos;
    }

}