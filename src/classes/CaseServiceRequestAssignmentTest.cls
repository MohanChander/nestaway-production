@isTest
public class CaseServiceRequestAssignmentTest {
    Public Static TestMethod Void doTest(){
        List<Case>  cList =  new List<Case>();
        List<Case>  updatedcList =  new List<Case>();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
         problem__c p= new problem__c();
        insert p;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='811';
        zc.Name='Test';
        insert zc;
        
       
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.Id;
        hos.Onboarding_Zone_Code__c='811';
        insert hos; 
        
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        Case c=  new Case();
        c.Status='Waiting On Tenant Approval';
        c.House__c =hos.Id;
        c.House1__c =hos.Id;
        c.Problem__c=p.id;
        c.RecordTypeId=devRecordTypeId;
        c.ownerId=newuser.Id;
        c.Subject='test';
        c.Queue__c='Ops Home Services';
      //  c.Sub_Problem_Type__c='SP1';
       // c.Sub_Sub_Problem_Type__c='SSP1';
       // c.Problem_Type__c='P1';
        insert c;
        
        update c;
        cList.add(c);
        CaseServiceRequestAssignment.serviceRequestCaseAssignment(cList);
    }
}