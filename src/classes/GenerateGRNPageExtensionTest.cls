// Addded by baibhav
@IsTest
public with sharing class GenerateGRNPageExtensionTest 
{
    public static TestMethod void GenerateGRNPageExtensionTest1()
    {
        Product2 pu=new Product2();
        pu.Name='34 pcs Dinner Set';
        pu.Family='Package B';
        pu.ProductCode='II-000-00-KA-34D';
        pu.Product_Type__c='Furnishing';
        pu.Service_Tax__c=14;
        insert pu;

        Product2 pu1=new Product2();
        pu1.Name='34 pcs Dinner Set';
        pu1.Family='Package B';
        pu1.ProductCode='II-000-00-KA-34D';
        pu1.Product_Type__c='Furnishing';
        pu1.Service_Tax__c=14;
        insert pu1;

        Pricebook2 pb=new Pricebook2();
        pb.name='Test price';
        pb.isActive=true;
        insert pb;

        Id stdPbId = Test.getStandardPricebookId();

        PricebookEntry pe1=new PricebookEntry();
        pe1.IsActive=true;
        pe1.Pricebook2Id=stdPbId;
        pe1.Product2Id=pu.id;
        pe1.UnitPrice=1000;
        pe1.UseStandardPrice=false;
        insert pe1;

        PricebookEntry pe=new PricebookEntry();
        pe.IsActive=true;
        pe.Pricebook2Id=pb.id;
        pe.Product2Id=pu.id;
        pe.UnitPrice=1000;
        pe.UseStandardPrice=false;
        insert pe;

        PricebookEntry pe21=new PricebookEntry();
        pe21.IsActive=true;
        pe21.Pricebook2Id=stdPbId;
        pe21.Product2Id=pu1.id;
        pe21.UnitPrice=1000;
        pe21.UseStandardPrice=false;
        insert pe21;

        PricebookEntry pe2=new PricebookEntry();
        pe2.IsActive=true;
        pe2.Pricebook2Id=pb.id;
        pe2.Product2Id=pu1.id;
        pe2.UnitPrice=1000;
        pe2.UseStandardPrice=false;
        insert pe2;


        Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
        acc.Vendor_Type__c='Inhouse';
        acc.Active__c=true;
        insert acc;

        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.HRM__c=newuser.id;
        hos.ZAM__c=newuser.id;
        hos.Assets_Created__c=false;
        insert  hos;

        Id orderRecordTypeID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        Order od=new Order();
        od.AccountId=acc.id;
        od.EffectiveDate=System.Today();
        od.Expected_Arrival_Date__c=System.Today().addDays(1);
        od.Category__c='Furnishing';
        od.RecordTypeId=orderRecordTypeID;
        od.Status='PDF Generated';
        od.House_PO__c=hos.id;
        od.Vendor__c=acc.id;
        od.Amount__c=3456;
        od.Approval_Status__c='Approved';
        od.Bill_To__c='Owner';
        od.No_of_Items__c=7;
        od.Pricebook2Id =pb.id;
        insert od;

        orderItem odt=new OrderItem();
        odt.OrderId=od.id;
        odt.PricebookEntryId = pe.id;
        odt.isCreatedFromSalesOrder__c = true;
        odt.Quantity=2;
        odt.Items_VAT_rate_inclusive_of_surcharge__c=10;
        odt.UnitPrice =800;
        insert odt;

      /*  orderItem odt1=new OrderItem();
        odt1.OrderId=od.id;
        odt1.Quantity=2;
        odt1.Items_VAT_rate_inclusive_of_surcharge__c=10;
        odt1.PricebookEntryId = pe1.id;
        odt1.UnitPrice=800;
        insert odt1;*/

        Order_Item__c oi=new Order_Item__c();
        oi.PO_Number__c=od.id;
        oi.Item_Name__c='Item 1';
        oi.Amount__c=1000;
        oi.Type__c='product';
        oi.Ordered_Quantity__c=10;
        insert oi;

        Order_Item__c oi1=new Order_Item__c();
        oi1.PO_Number__c=od.id;
        oi1.Item_Name__c='Item 1';
        oi1.Amount__c=1000;
        oi1.Type__c='product';
        oi1.Ordered_Quantity__c=10;
        insert oi1;

      
      ApexPages.StandardController stdController=new ApexPages.StandardController(od);
      GenerateGRNPageExtension gRn =new GenerateGRNPageExtension(stdController); 
        
    }
        public static TestMethod void GenerateGRNPageExtensionTest2()
    {
        Product2 pu=new Product2();
        pu.Name='34 pcs Dinner Set';
        pu.Family='Package B';
        pu.ProductCode='II-000-00-KA-34D';
        pu.Product_Type__c='Furnishing';
        pu.Service_Tax__c=14;
        insert pu;

        Product2 pu1=new Product2();
        pu1.Name='34 pcs Dinner Set';
        pu1.Family='Package B';
        pu1.ProductCode='II-000-00-KA-34D';
        pu1.Product_Type__c='Furnishing';
        pu1.Service_Tax__c=14;
        insert pu1;

        Pricebook2 pb=new Pricebook2();
        pb.name='Test price';
        pb.isActive=true;
        insert pb;

        Id stdPbId = Test.getStandardPricebookId();

        PricebookEntry pe1=new PricebookEntry();
        pe1.IsActive=true;
        pe1.Pricebook2Id=stdPbId;
        pe1.Product2Id=pu.id;
        pe1.UnitPrice=1000;
        pe1.UseStandardPrice=false;
        insert pe1;

        PricebookEntry pe=new PricebookEntry();
        pe.IsActive=true;
        pe.Pricebook2Id=pb.id;
        pe.Product2Id=pu.id;
        pe.UnitPrice=1000;
        pe.UseStandardPrice=false;
        insert pe;

        PricebookEntry pe21=new PricebookEntry();
        pe21.IsActive=true;
        pe21.Pricebook2Id=stdPbId;
        pe21.Product2Id=pu1.id;
        pe21.UnitPrice=1000;
        pe21.UseStandardPrice=false;
        insert pe21;

        PricebookEntry pe2=new PricebookEntry();
        pe2.IsActive=true;
        pe2.Pricebook2Id=pb.id;
        pe2.Product2Id=pu1.id;
        pe2.UnitPrice=1000;
        pe2.UseStandardPrice=false;
        insert pe2;


        Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
        acc.Vendor_Type__c='Inhouse';
        acc.Active__c=true;
        insert acc;

        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.HRM__c=newuser.id;
        hos.ZAM__c=newuser.id;
        hos.Assets_Created__c=false;
        insert  hos;

        Id orderRecordTypeID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        Order od=new Order();
        od.AccountId=acc.id;
        od.EffectiveDate=System.Today();
        od.Expected_Arrival_Date__c=System.Today().addDays(1);
        od.Category__c='Furnishing';
        od.RecordTypeId=orderRecordTypeID;
        od.Status='PDF Generated';
        od.House_PO__c=hos.id;
        od.Vendor__c=acc.id;
        od.Amount__c=3456;
        od.Approval_Status__c='Approved';
        od.Bill_To__c='Owner';
        od.No_of_Items__c=7;
        od.Pricebook2Id =pb.id;
        insert od;

        orderItem odt=new OrderItem();
        odt.OrderId=od.id;
        odt.PricebookEntryId = pe.id;
        odt.isCreatedFromSalesOrder__c = true;
        odt.Quantity=2;
        odt.Items_VAT_rate_inclusive_of_surcharge__c=10;
        odt.UnitPrice =800;
        insert odt;

      /*  orderItem odt1=new OrderItem();
        odt1.OrderId=od.id;
        odt1.Quantity=2;
        odt1.Items_VAT_rate_inclusive_of_surcharge__c=10;
        odt1.PricebookEntryId = pe1.id;
        odt1.UnitPrice=800;
        insert odt1;*/

        Order_Item__c oi=new Order_Item__c();
        oi.PO_Number__c=od.id;
        oi.Item_Name__c='Item 1';
        oi.Amount__c=1000;
        oi.Type__c='Service';
        oi.Invoice_Generated__c=true;
        oi.Ordered_Quantity__c=10;
        insert oi;

        Order_Item__c oi1=new Order_Item__c();
        oi1.PO_Number__c=od.id;
        oi1.Item_Name__c='Item 1';
        oi1.Amount__c=1000;
        oi1.Type__c='Service';
        oi1.Invoice_Generated__c=false;
        oi1.Ordered_Quantity__c=10;
        insert oi1;

      
      ApexPages.StandardController stdController=new ApexPages.StandardController(od);
      GenerateGRNPageExtension gRn =new GenerateGRNPageExtension(stdController); 

      gRn.prodWrapperList[0].receivedQty=2;
      gRn.prodWrapperList[0].OrdProduct=odt;

      grn.createReceivedProducts();

      grn.serviceWrapperList[0].ordItem=oi;
      grn.serviceWrapperList[0].isDone=false;
      grn.onClickGenerateInvoice();

      grn.serviceWrapperList[0].isDone=true;
      grn.onClickGenerateInvoice();

        
    }
}