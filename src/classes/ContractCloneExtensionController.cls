public  with sharing class ContractCloneExtensionController {


    // empty constructor
    public ContractCloneExtensionController (ApexPages.StandardController controller) {

    }    
   
    
    @RemoteAction
    public static CloneContractReturnWrapper cloneContract(String conId) {
       
             cloneContractReturnWrapper returnWrap = new cloneContractReturnWrapper();
             
             
             try{
                  // adding the validation rules to clone contract.
                  
                  List<Contract> conLst=[select id,House__c,House__r.stage__c,Opportunity__c,Status,Approval_Status__c,isActive__c,Archived__c from contract where id=:conId];
               
                  contract con = new contract();
                  if(conLst.size()>0 && conLst.get(0).Opportunity__c!=null){
                      con=conLst.get(0);
                      
                       Id cloneContractRT = Schema.SObjectType.Contract.getRecordTypeInfosByName().get(Constants.CONTRACT_RT_CLONE_CONTRACT).getRecordTypeId();
                      List<Contract> otherContractsLst=[select id from contract where id!=:con.id and Opportunity__c=:con.Opportunity__c and isActive__c=false and isCloneContract__c=true and Approval_Status__c!='Rejected by Owner' and Approval_Status__c!='Rejected by ZM' and Approval_Status__c!='Rejected by City Head' and status!='Archived'];
                      
                     Integer defaultDay=2; 
                     
                    // check if user can press this button today or not
                     Org_Param__c param = Org_Param__c.getInstance();
                     if(param.Contract_Clone_stop_Days__c!=null){
                         defaultDay=Integer.valueOf(param.Contract_Clone_stop_Days__c);
                         
                     }                   
                    Date currentDate=date.today();
                    Integer numberOfDays = Date.daysInMonth(currentDate.year(), currentDate.month());
                    Date lastDayOfMonth = Date.newInstance(currentDate.year(), currentDate.month(), numberOfDays);
                    
                    // Check the user profile if he can press the button.
                    
                    Set<string> profileNames = new Set<string>();
                    string defaultProfile='System Administrator';
                    profileNames.add(defaultProfile.toUpperCase());
                    
                    if(param.Profile_Which_Can_Clone_Contract__c!=null){
                        
                        List<string> profileSplit=param.Profile_Which_Can_Clone_Contract__c.split(',');
                        for(string pf: profileSplit){
                            
                            profileNames.add(pf.toUpperCase());
                        }
                    }
                    
                    Map<Id,Profile> allOrgProfile=new Map<Id,profile>([select id,name from profile]);
                    string currentUserProfileName;
                    if(allOrgProfile.containsKey(UserInfo.getProfileId())){
                        
                        currentUserProfileName=allOrgProfile.get(UserInfo.getProfileId()).name.toUpperCase();
                    }
                    
                    if(currentUserProfileName==null || !profileNames.contains(currentUserProfileName)){
                        
                        returnWrap.isSuccess=false;
                        returnWrap.contID=conId;
                        returnWrap.message='You dont have permission to create contract vesion.';
                    }
                                        
                    else if(currentDate >= (lastDayOfMonth.addDays(-defaultDay)) && currentDate <= (lastDayOfMonth.addDays(defaultDay))){
                        
                        returnWrap.isSuccess=false;
                        returnWrap.contID=conId;
                        returnWrap.message='Contract can not be clone in month end period.';
                    }
                     
                    else if(con.isActive__c==null || con.isActive__c==false || con.House__c==null || con.House__r.stage__c==null || con.House__r.stage__c!='House Live'){
                          
                            returnWrap.isSuccess=false;
                            returnWrap.contID=conId;
                            returnWrap.message='You can create version of active contract after house become live.';
                          
                      } 
                    
                    else if(otherContractsLst.size()>0){
                        
                            returnWrap.isSuccess=false;
                            returnWrap.contID=conId;
                            returnWrap.message='In Progrees version contract is presnet on this contract. Please work on the same.';
                        
                    }                
                      else{
                      
                        Contract originalContract = new Contract(id=conId);
                        sObject originalSObject = (sObject) originalContract;

                        List<sObject> originalSObjects = new List<sObject>{originalSObject};

                        List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                              originalSobjects,
                              originalSobject.getsObjectType());
                             
                        Contract clonedContract = (Contract)clonedSObjects.get(0);                
                        system.debug('*****clonedContract'+clonedContract);
                        clonedContract.Parent_Contract__c=conId;
                        clonedContract.recordtypeId=cloneContractRT;
                        clonedContract.isCloneContract__c=true;
                        clonedContract.Status='Draft';
                        clonedContract.Approval_Status__c='Draft';
                        clonedContract.API_Info__c=null;
                        clonedContract.API_Sucess__c=null;
                        clonedContract.Agreement_Approval_Code__c=null;
                        clonedContract.Owner_Approval_Status__c=null;
                        clonedContract.Owner_Approved_By_API__c=false;      
                        clonedContract.Manually_Approved_By_ZM__c=false;
                        clonedContract.Is_Commercial_Field_Changed__c=false;
                        clonedContract.Archived__c=false;
                        clonedContract.City_Manager__c=null;
                        clonedContract.isActive__c=false;
                        clonedContract.Sync_Failed_Number__c=null;
                        clonedContract.House_Term_Change_URL__c=null;                       
                        insert clonedContract;
                        
                        returnWrap.isSuccess=true;
                        returnWrap.contID=clonedContract.id;
                        returnWrap.message='Contract Cloned Successfully.';
                      }
                   }
                   else{
                       
                        returnWrap.isSuccess=false;
                        returnWrap.contID=conId;
                        returnWrap.message='Invalid contract id.';
                   }
             }
            catch(Exception e){
                
                system.debug('****exception in remote clone mehtod '+e.getMessage()+' at line'+e.getLineNumber());
                returnWrap.isSuccess=false;
                returnWrap.contID=conId;
                returnWrap.message='An internal error has occurred. Please contact your system administrator.';
            }            
               
       
        return returnWrap;
    }
    
    public class CloneContractReturnWrapper{
        
        public boolean isSuccess {get;set;}
        public string contID {get;set;}
        public string message {get;set;}
        
    }

}