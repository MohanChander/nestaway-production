@IsTest
public class CasePropertyManagerAssignmentTest {
    public static Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get('Property Management Zone').getRecordTypeId();
    public static Id zuApmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Assistance Property Manager').getRecordTypeId();
    public static Id zuPmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Property Manager').getRecordTypeId();   Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
    public static Id IT_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_INTERNAL_TRANSFER).getRecordTypeId(); 
    
    public Static TestMethod Void  TestMethod1(){
        Test.startTest() ;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;
        Test.stopTest();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
         City__c ct=new City__c();
        ct.name='Bangalore';
        insert ct;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City__c=ct.name;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
        
        CAse c =new CAse();
        c.house__C= hos.id;
        insert c;
        
        CAse c1 =new CAse();
        c1.HouseForOffboarding__c= hos.id;
        c.RecordTypeId=caseUnoccupiedFurnishedRecordtype;
        insert c1;
        CAse c2 =new CAse();
        c2.From_House__c= hos.id;
        insert c2;
        List<Case> caseList =  new List<Case>();
        caseList.add(c1);
        caseList.add(c);
        caseList.add(c2);
        CasePropertyManagerAssignment.assignmentOfOwners(caseList, 'House Onboarding');
    }
 public Static TestMethod Void  TestMethod2(){
        Test.startTest() ;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;
        Test.stopTest();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
         City__c ct=new City__c();
        ct.name='Bangalore';
        insert ct;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City__c=ct.name;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
        
        CAse c =new CAse();
        c.house__C= hos.id;
        insert c;
        
        CAse c1 =new CAse();
        c1.HouseForOffboarding__c= hos.id;
        insert c1;
        CAse c2 =new CAse();
        c2.From_House__c= hos.id;
        insert c2;
        List<Case> caseList =  new List<Case>();
        caseList.add(c1);
        caseList.add(c);
        caseList.add(c2);
        CasePropertyManagerAssignment.assignmentOfOwners(caseList, 'Move In');
    }
 public Static TestMethod Void  TestMethod3(){
        Test.startTest() ;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;
        Test.stopTest();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
         City__c ct=new City__c();
        ct.name='Bangalore';
        insert ct;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City__c=ct.name;
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '123';
        insert hos;
        
        
        CAse c =new CAse();
        c.house__C= hos.id;
        insert c;
        
        CAse c1 =new CAse();
        c1.HouseForOffboarding__c= hos.id;
        insert c1;
        CAse c2 =new CAse();
        c2.From_House__c= hos.id;
        insert c2;
        List<Case> caseList =  new List<Case>();
        caseList.add(c1);
        caseList.add(c);
        caseList.add(c2);
        CasePropertyManagerAssignment.assignmentOfOwners(caseList, 'Move out');
    }
     
}