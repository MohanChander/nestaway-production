global without sharing class OwnerSDPaymentClass {
    public OwnerSDPaymentClass(ApexPages.StandardController controller) {
    }
    public  pageReference OwnerSdPayment(){
        try{
            String sourceId= ApexPages.currentPage().getParameters().get('sourceId');
            WorkOrder wd= [Select id,status,Api_Success__c   from WorkOrder where id =:sourceId];
            system.debug('***'+wd.Api_Success__c);
            if(wd.Api_Success__c!= null && wd.Api_Success__c.contains('True')){
                wd.adderror(' Status is not verified or Api was True.');
                return null;
            }
            else{
                if( (wd.Api_Success__c == null || wd.Api_Success__c.contains('False') ) && wd.status=='Verified')
                {
                    System.debug('Coming');
                    SendAPIRequests.sendRPDetailsToWebApp(wd.id);
                }
            }
            pageReference ref = new PageReference('/'+wd.id); 
            ref.setRedirect(true); 
            return ref; 
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
            return null;
        }
    }
}