@isTest
public Class TaskTriggerHandlerTest{
    
    public static id casfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
        public static id casunfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
 public static  Id keyHandlingRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Key Handling').getRecordTypeId();
        public static   Id operationTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Operation Task').getRecordTypeId();

    public Static TestMethod void TaskTest(){
        Test.StartTest();
          NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.House_Lattitude__c=null;
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Open';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
     
        Task t = new Task();
        t.House__c=hos.id;
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.WhatId        = houseIC.Id;
        t.Number_of_Keys_for_Balcony__c='1';
            t.Number_of_Keys_for_BedRoom__c='1';
            t.Number_of_Keys_for_Cupboards__c='1';
            t.Number_of_Keys_for_Kitchen__c='1';
            t.Number_of_Keys_for_Main_Door__c='1';
            t.Number_of_Keys_for_Servant_Room__c='1';
            t.Number_of_Keys_for_Storeroom__c='1';
        insert t;
         
     
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
         Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Subject       = 'House Visit';
        t1.Status        = 'Open';
        t1.Priority      = 'Normal';
        t1.whatid       = houseIC.Id;
        t.House__c=hos.id;
        t1.Number_of_Keys_for_Balcony__c='2';
            t1.Number_of_Keys_for_BedRoom__c='2';
            t1.Number_of_Keys_for_Cupboards__c='2';
            t1.Number_of_Keys_for_Kitchen__c='2';
            t1.Number_of_Keys_for_Main_Door__c='2';
            t1.Number_of_Keys_for_Servant_Room__c='2';
            t1.Number_of_Keys_for_Storeroom__c='2';
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        insert dc;
          map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
           List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandler.AfterUpdate(newmap,oldmap);
        TaskTriggerHandler.afterInsertTask(TaskList);
        TaskTriggerHandler.updateCaseStatus(c);
        TaskTriggerHandler.createInvoiceRecordsOnTaskClosure(newmap,oldmap);
        TaskTriggerHandler.newAfterUpdate(newmap,oldmap);
        Test.StopTest();
        
    }
    
    public Static TestMethod void TaskTest1(){
        Test.StartTest();
          NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Lattitude__c=2.2;
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Open';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
     
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.Current_Latitude__c=2.389;
        t.Current_Longitude__c=2.11;
        t.WhatId        = c.Id;
        t.Number_of_Keys_for_Balcony__c='1';
            t.Number_of_Keys_for_BedRoom__c='1';
            t.Number_of_Keys_for_Cupboards__c='1';
            t.Number_of_Keys_for_Kitchen__c='1';
            t.Number_of_Keys_for_Main_Door__c='1';
            t.Number_of_Keys_for_Servant_Room__c='1';
            t.Number_of_Keys_for_Storeroom__c='1';
           t.RecordTypeId=keyHandlingRecordTypeId;
        insert t;
         
     
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
         Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Current_Latitude__c=27.389;
        t1.Current_Longitude__c=22.11;
        t1.Subject       = 'House Visit';
        t1.Status        =  Constants.TASK_STATUS_COMPLETED;
        t1.Priority      = 'Normal';
        t1.WhatId        =c.Id;
        t1.Cost__c = 10;
        t1.Number_of_Keys_for_Balcony__c='3';
            t1.Number_of_Keys_for_BedRoom__c='3';
            t1.Number_of_Keys_for_Cupboards__c='3';
            t1.Number_of_Keys_for_Kitchen__c='3';
            t1.Number_of_Keys_for_Main_Door__c='3';
            t1.Number_of_Keys_for_Servant_Room__c='3';
            t1.Number_of_Keys_for_Storeroom__c='3';
        t1.RecordTypeId=keyHandlingRecordTypeId;
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        dc.Keys_Collected_For__c = 'Balcony';
        dc.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Balcony__c;
        insert dc;
          Detail__c dc1= new Detail__c();
        dc1.Case__c=c.id;
        dc1.Task_Id__c=t1.id;
        dc1.Keys_Collected_For__c = 'Bedroom';
        dc1.Number_of_Keys_collected__c = t1.Number_of_Keys_for_BedRoom__c;
        insert dc1;
         Detail__c dc2= new Detail__c();
        dc2.Case__c=c.id;
        dc2.Task_Id__c=t1.id;
        dc2.Keys_Collected_For__c = 'Cupboards';
        dc2.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Cupboards__c;
        insert dc2;
         Detail__c dc3= new Detail__c();
        dc3.Case__c=c.id;
        dc3.Task_Id__c=t1.id;
        dc3.Keys_Collected_For__c = 'Bedroom';
        dc3.Number_of_Keys_collected__c = t1.Number_of_Keys_for_BedRoom__c;
        insert dc3;
         Detail__c dc4= new Detail__c();
        dc4.Case__c=c.id;
        dc4.Task_Id__c=t1.id;
        dc4.Keys_Collected_For__c = 'Kitchen';
        dc4.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Kitchen__c;
        insert dc4;
          Detail__c dc5= new Detail__c();
        dc5.Case__c=c.id;
        dc5.Task_Id__c=t1.id;
        dc5.Keys_Collected_For__c = 'Main Door';
        dc5.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Main_Door__c;
        insert dc5;
          Detail__c dc6= new Detail__c();
        dc6.Case__c=c.id;
        dc6.Task_Id__c=t1.id;
        dc6.Keys_Collected_For__c = 'Servant Room';
        dc6.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Servant_Room__c;
        insert dc6;
          Detail__c dc7= new Detail__c();
        dc7.Case__c=c.id;
        dc7.Task_Id__c=t1.id;
        dc7.Keys_Collected_For__c = 'Store Room';
        dc7.Number_of_Keys_collected__c = t1.Number_of_Keys_for_Storeroom__c;
        insert dc7;
          map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
           List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandler.AfterUpdate(newmap,oldmap);
        TaskTriggerHandler.afterInsertTask(TaskList);
        TaskTriggerHandler.updateCaseStatus(c);
        TaskTriggerHandler.createInvoiceRecordsOnTaskClosure(newmap,oldmap);
        TaskTriggerHandler.newAfterUpdate(newmap,oldmap);
        Test.StopTest();
        
    }
     public Static TestMethod void TaskTest2(){
        Test.StartTest();
          NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
       // hos.House_Lattitude__c=2.2;
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Open';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
     
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.Current_Latitude__c=2.389;
        t.Current_Longitude__c=2.11;
        t.WhatId        = c.Id;
        t.Number_of_Keys_for_Balcony__c='1';
            t.Number_of_Keys_for_BedRoom__c='1';
            t.Number_of_Keys_for_Cupboards__c='1';
            t.Number_of_Keys_for_Kitchen__c='1';
            t.Number_of_Keys_for_Main_Door__c='1';
            t.Number_of_Keys_for_Servant_Room__c='1';
            t.Number_of_Keys_for_Storeroom__c='1';
           t.RecordTypeId=keyHandlingRecordTypeId;
            t.Subject='Photography';
        insert t;
         
     
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
         Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Current_Latitude__c=27.389;
        t1.Current_Longitude__c=22.11;
        t1.Subject       = 'House Visit';
        t1.Status        =  Constants.TASK_STATUS_COMPLETED;
        t1.Priority      = 'Normal';
        t1.WhatId        =c.Id;
         t1.RecordTypeId=operationTaskRecordTypeId;
        t1.Cost__c = 10;
         t1.HIC__c=houseIC.id;
         t1.Task_Related_To__c='HIC';
        t1.HO_Type__c='Furnishing and DTH / Wifi Connection';
        t1.Task_Type__c='House Onboarding';
        t1.Number_of_Keys_for_Balcony__c='3';
            t1.Number_of_Keys_for_BedRoom__c='3';
            t1.Number_of_Keys_for_Cupboards__c='3';
            t1.Number_of_Keys_for_Kitchen__c='3';
            t1.Number_of_Keys_for_Main_Door__c='3';
            t1.Number_of_Keys_for_Servant_Room__c='3';
            t1.Number_of_Keys_for_Storeroom__c='3';
         t1.Subject='Photography';
        
        update t1;
      
          map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
           List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandler.AfterUpdate(newmap,oldmap);
        TaskTriggerHandler.afterInsertTask(TaskList);
        TaskTriggerHandler.updateCaseStatus(c);
        TaskTriggerHandler.createInvoiceRecordsOnTaskClosure(newmap,oldmap);
        TaskTriggerHandler.newAfterUpdate(newmap,oldmap);
        Test.StopTest();
        
    }
}