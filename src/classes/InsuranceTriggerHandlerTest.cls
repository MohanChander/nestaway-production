@isTest
public class InsuranceTriggerHandlerTest {
    
    public Static TestMethod void accTest(){
        Test.StartTest();
         User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Insurance__c  inc = new Insurance__c ();
        inc.Status__c='new';
        inc.House__c=hos.id;
        insert inc;
        inc.Status__c='To Be Queued';
        update inc;
        Test.StopTest();
}
    public Static TestMethod void accTest1(){
        Test.StartTest();
         User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Insurance__c  inc = new Insurance__c ();
        inc.Status__c='Renewed';
        inc.House__c=hos.id;
        insert inc;
        inc.Status__c='To Be Queued';
        update inc;
        Test.StopTest();
}
}