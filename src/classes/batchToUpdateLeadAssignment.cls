global class batchToUpdateLeadAssignment implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    
    public batchToUpdateLeadAssignment(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
      
       // String query = 'Select id from lead';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Lead> leadList) {
    
            Set<Id> setLeadId = new Set<Id>();
            for(Lead allLead: leadList){
                setLeadId.add(allLead.id);
            }
            
            List<NA_TerittoryMappingSite.wrapper>  wrpList = new List<NA_TerittoryMappingSite.Wrapper>();    
            Id LeadUnCLassifiedRecId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Unclassified Lead').getRecordTypeId();
            System.debug('==LeadUnCLassifiedRecId=='+LeadUnCLassifiedRecId);
            
            Set<Id> setLeadRecordId = new set<Id>();
            setLeadRecordId.add(LeadUnCLassifiedRecId);
            
            List<Lead> lstNewLead = new List<Lead>();
            //lstNewLead = [SELECT id,Name,Email,OwnerId,Latitude,longitude,Area_Code__c,Owner.UserRoleId,Owner.userRole.Name,NonServiceable_Locality__c,NonServiceable_City__c,Street,City,State,Country,PostalCode FROM Lead WHERE Id IN: setLeadId AND RecordtypeId !=: LeadUnCLassifiedRecId];
            lstNewLead = [SELECT id,Name,Email,OwnerId,Latitude,longitude,Area_Code__c,Owner.UserRoleId,Owner.userRole.Name,NonServiceable_Locality__c,NonServiceable_City__c,Street,City,State,Country,PostalCode FROM Lead WHERE Id IN: setLeadId AND RecordtypeId NOT IN : setLeadRecordId];
            system.debug('==lstNewLead=='+lstNewLead);
            Map<String, Zing_API_URL__c> allParam = Zing_API_URL__c.getAll();
            System.debug('==allParam=='+allParam);
            Zing_API_URL__c zingAPI = allParam.values();
            String strURL;
            String strAuth;
            String strTag;
            String strEmail;
            String strAreaId;
            String strTimeStamp;
            strTimeStamp=System.Now().year()+'-'+System.Now().month()+'-'+System.Now().day()+' '+System.Now().hour()+':'+System.Now().minute()+':'+System.Now().second();
            system.debug('==strTimeStamp=='+strTimeStamp);
            
            
            if(zingAPI!=Null){
                strURL   = zingAPI.URL__c;
                strAuth  = zingAPI.Auth_Token__c;
                strTag   = zingAPI.Tag__c;
                strEmail = zingAPI.Email__c;      
            }
            system.debug('==strURL=='+strURL);
            system.debug('==strAuth=='+strAuth);
            system.debug('==strTag=='+strTag);
            system.debug('==strEmail=='+strEmail);
                
            
            for(Lead each : lstNewLead){ 
                  
              string address='';
  
              if(each.Street!=null){
                address += each.Street+',';
              }
              if(each.City !=null){
                address += each.City+',';
              }
              if(each.State!=null){
                address +=each.State+',';
              }
              if(each.Country!=null){
                address += each.Country+',';
              }
              if(each.PostalCode!=null){
                address += each.PostalCode+',';
              }
              /* if address is not blank then get the latitude and longitude through google api */
              if(address !=''){
                HttpRequest req = new HttpRequest();
                /* Encode address */
                address = EncodingUtil.urlEncode(address,'UTF-8');
                req.setEndPoint('https://maps.googleapis.com/maps/api/geocode/xml?address='+address+'&sensor=true');
                req.setMethod('GET');
                Http http = new Http();
                HttpResponse res;
                if(!Test.isRunningTest()){
                  res = http.send(req);
                }else{
                  /* create sample data for test method */
                  String resString = '<GeocodeResponse><status>OK</status><result><geometry><location><lat>37.4217550</lat> <lng>-122.0846330</lng></location>';
                  resString +='</geometry> </result> </GeocodeResponse>';
                  res = new HttpResponse();
                  res.setBody(resString);
                  res.setStatusCode(200);
                  
                }
                
                Dom.Document doc = res.getBodyDocument();   
                /* Get the root of xml response */
                Dom.XMLNode geocodeResponse = doc.getRootElement();
                if(geocodeResponse!=null){
                  /* Get the result tag of xml response */
                  Dom.XMLNode result = geocodeResponse.getChildElement('result',null);
                  if(result!=null){
                    /* Get the geometry tag  of xml response */
                    Dom.XMLNode geometry = result.getChildElement('geometry',null);
                    if(geometry!=null){
                      /* Get the location tag  of xml response */
                      Dom.XMLNode location = geometry.getChildElement('location',null);
                      if(location!=null){
                        /* Get the lat and lng tag  of xml response */
                        String lat = location.getChildElement('lat', null).getText();
                        String lng = location.getChildElement('lng', null).getText();
                        try{
                          if(Decimal.valueof(lat) != null)  
                              each.Latitude =Decimal.valueof(lat);
                          if(Decimal.valueof(lng) != null)
                              each.Longitude =Decimal.valueof(lng);
                          
                        }catch(Exception ex){
                          system.debug('Exception '+ex.getMessage());
                        }
                      }
                    }
                  }
                }
              }
               if(each.Latitude!= null && each.Longitude!=null){
                    String strLat  = String.ValueOf(each.Latitude);
                    String strLong = String.ValueOf(each.Longitude); 
                    Http pro = new Http();
                    HttpRequest Req = new HttpRequest();
                    HttpResponse Res = new HttpResponse();
                    String endPoint = strURL+'auth_token='+strAuth +'&tag=' +strTag ;
                    strTimeStamp = EncodingUtil.urlEncode(strTimeStamp, 'UTF-8');
                    endPoint = endpoint+'&lat='+ strLat +'&long='+ strLong +'&timestamp='+ strTimeStamp +'&email='+strEmail+'&password='+'admin123'; 
                    
                    System.debug('==endPoint=='+endPoint);
                    Req.setEndpoint(endPoint);
                    Req.setMethod('GET');
                    if(!Test.isRunningTest()){
                        Res = pro.send(req);
                    }
                    else{
                        /* create sample data for test method */
                          String resString = strURL+'auth_token='+strAuth +'&tag=' +strTag;
                          resString += '&lat=37.4217550&long=-122.0846330&timestamp='+ strTimeStamp +'&email='+strEmail+'&password='+'admin123' ;
                          system.debug('---resString---'+resString);
                          res = new HttpResponse();
                          res.setBody(resString);
                       //   res.setStatusCode(200);
                    }
                    System.debug('STatus code='+Res.getStatusCode());
                    if(Res.getStatusCode()==200){
                        each.Area_Code__c='';
                        System.debug('==each.Area_Code=='+each.Area_Code__c);
                        wrpList = (List<NA_TerittoryMappingSite.Wrapper>)JSON.deserialize(Res.getBody(),List<NA_TerittoryMappingSite.wrapper>.class);
                        System.debug('wrapper list='+wrpList);
                        for(NA_TerittoryMappingSite.Wrapper eachWrap: wrpList){
                            if(String.isBLANK(each.Area_Code__c)){
                                each.Area_Code__c = eachWrap.area_id; System.debug('==each.Area_Code inside=='+each.Area_Code__c);
                            }
                            if(eachWrap.serviceable=='f'){
                                each.NonServiceable_Locality__c= TRUE;
                            }
                        }
                    }
                }
            }
            NA_TerittoryMappingSite.LeadAssignment(lstNewLead);

    
    }
    
     global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
    
}