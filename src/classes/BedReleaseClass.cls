global without sharing class BedReleaseClass {
    public BedReleaseClass(ApexPages.StandardController controller) {
    }
    public  pageReference BedRelease(){
        try{
            String sourceId= ApexPages.currentPage().getParameters().get('sourceId');
            Case cs= [Select id,status,Bed_Released_API_Status__c  from case where id =:sourceId];
            if(cs.Bed_Released_API_Status__c != null && cs.Bed_Released_API_Status__c.contains('True')){
                cs.adderror('Bed has been already released.');
                return null;
                
            }
            else
            {
                if(cs.status == CONSTANTS.CASE_STATUS_MOVEDOUTALLOK ){
                    CaseMIMOFunctionality.bedReleaseOnInspectionCOmplete(cs.id,CONSTANTS.CASE_STATUS_MOVEDOUTALLOK);
                }
                else if(cs.status == CONSTANTS.CASE_STATUS_MOVEDOUTWITHISSUE ){
                    CaseMIMOFunctionality.bedReleaseOnInspectionCOmplete(cs.id,CONSTANTS.CASE_STATUS_MOVEDOUTWITHISSUE);
                }
            }
            pageReference ref = new PageReference('/'+cs.id); 
            ref.setRedirect(true); 
            return ref; 
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
            return null;
        }
    }
}