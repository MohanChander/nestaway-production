global class batchupdateHouse implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    
    public batchupdateHouse(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query = 'Select id,Onboarding_Zone_code__c,House_Lat_Long_details__Latitude__s,'+
                        'House_Lat_Long_details__Longitude__s , Acquisition_Zone_code__c, House__c'+
                        'FROM House_Inspection_Checklist__c'+
                        'where (Acquisition_Zone_code__c = NULL OR Onboarding_Zone_code__c = NULL)'+
                    'AND (House_Lat_Long_details__Longitude__s != NULL AND House_Lat_Long_details__Latitude__s != NULL )';
        */
       // String query = 'Select id,Acquisition_Zone_Code__c,Onboarding_Zone_Code__c,House_Lattitude__c,House_Longitude__c,city__c,HRM__c,ZAM__c from house__c';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> newHouses) {
      
        try{
                Set<Id> oppIdSet = new Set<Id>();
                Set<Id> houseIds = new Set<id>();

                for(House__c house: newHouses){
                    if(house.Opportunity__c!=null){   
                        oppIdSet.add(house.Opportunity__c);
                    }
                }

                //Query for the Opportunity records with the childs
                Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(OpportunitySelector.getOpportunitiesWithChilds(oppIdSet));
                Map<Id, List<Room_Inspection__c>> oppRoomInspectionMap = new Map<Id, List<Room_Inspection__c>>();
                Map<Id, List<Detail__c>> oppKeyDetailsMap = new Map<Id, List<Detail__c>>();
                Map<Id, List<Room_Terms__c>> roomTermMap = new Map<Id, List<Room_Terms__c>>();
                Map<Id, List<Bed__c>> bedMap = new Map<Id, List<Bed__c>>();
                Map<Id, List<Bathroom__c>> bathroomMap = new Map<Id, List<Bathroom__c>>();
                Set<Id> roomInspectionIdSet = new Set<Id>();           
                Map<Id, Id> RoomTermWithRoomInspectionMap = new Map<Id, Id>();    //Contains RoomTermId and RoomInspectionId   

                List<House_Inspection_Checklist__c> inspCheckList = ChecklistSelector.getChecklistWithRoomsAndKeys(oppIdSet);

                for(House_Inspection_Checklist__c checklist: inspCheckList){
                    
                    List<Detail__c> keyDetailsListTemp = new List<Detail__c>();
                    
                    for(Detail__c keyDetail: checklist.Detail__r){
                        keyDetailsListTemp.add(keyDetail);
                    }
                    
                    oppKeyDetailsMap.put(checklist.Opportunity__c, keyDetailsListTemp);  
                }

                //query for the Bed Records and populate the bedMap
                for(Bed__c bed: BedSelector.getBedsForOpportunities(oppIdSet)){
                    Id oppId = bed.Room_Terms__r.Contract__r.Opportunity__c;
                    
                    if(bedMap.containsKey(oppId)){
                        bedMap.get(oppId).add(bed);
                    } else {
                        bedMap.put(oppId, new List<Bed__c>{bed});
                    }
                }

                //query for the Bathroom Records and populate bathroomMap
                for(Bathroom__c bathroom: BathroomSelector.getBathroomsForOpportunities(oppIdSet)){
                    Id oppId;
                    if(bathroom.Room_Inspection__r.House_Inspection_Checklist__r.Opportunity__c != null){
                        oppId = bathroom.Room_Inspection__r.House_Inspection_Checklist__r.Opportunity__c;
                    } else if (bathroom.Checklist__r.Opportunity__c != null){
                        oppId = bathroom.Checklist__r.Opportunity__c;
                    }
                    
                    if(bathroomMap.containsKey(oppId)){
                        bathroomMap.get(oppId).add(bathroom);
                    } else {
                        bathroomMap.put(oppId, new List<Bathroom__c>{bathroom});
                    }
                }

                //query for Room Term records and populate the roomTermMap 
                for(Room_Terms__c roomTerm: RoomTermsSelector.getRoomTermsForOpportunities(oppIdSet)){
                    Id oppId = roomTerm.Contract__r.Opportunity__c;
                    if(roomTermMap.containsKey(oppId)){
                        roomTermMap.get(oppId).add(roomTerm);
                    } else {
                        roomTermMap.put(oppId, new List<Room_Terms__c>{roomTerm});
                    }

                    //populate RoomTermWithRoomInspectionMap 
                    if(roomTerm.Room_Inspection__c != null)
                        RoomTermWithRoomInspectionMap.put(roomTerm.Room_Inspection__c, roomTerm.Id);                
                }

                List<House_Inspection_Checklist__c> inspectionList = new List<House_Inspection_Checklist__c>();  //to be updated
                List<Order> ordList = new List<Order>();                                                         //to be updated
                List<Contract> contractList = new List<Contract>();                                              //to be updated
                List<Room_Terms__c> roomsToBeUpdated = new List<Room_Terms__c>();
                List<Detail__c> keyDetailsToBeUpdated = new List<Detail__c>();
                List<Bed__c> bedList = new List<Bed__c>();
                List<Bathroom__c> bathroomList = new List<Bathroom__c>();
                Map<Id,string> houseToZoneCode = new Map<Id,string>();

                //get the HIC RecordTypeId 
                Id HICRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_House_Inspection_Checklist).getRecordTypeId();
                Id dccRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Document_Collection_Checklist).getRecordTypeId();

                //Associate the Opportunity childs with the House 
                for(House__c house: newHouses){
                    house.Temp_Done__c=true;
                    houseIds.add(house.id);
                    
                    if(house.Opportunity__c!=null && oppMap.containsKey(house.Opportunity__c)){
                        
                        Opportunity opty = oppMap.get(house.Opportunity__c);
                        
                        for(House_Inspection_Checklist__c checklist: opty.House_Inspection_Checklist__r){
                            if(checklist.House__c==null){
                                checklist.House__c = house.Id;          
                                inspectionList.add(checklist);  
                            }               
                            
                        }
                        /*
                        for(Order ord: opty.Orders){
                          if(ord.House__c==null){   
                            ord.House__c = house.Id;
                            ordList.add(ord);
                          }
                        }
                        
                        for(Contract con: opty.Contracts__r){
                           if(con.House__c==null){      
                            con.House__c = house.Id;
                            contractList.add(con);
                           }    
                        }
                        */
                    } 
                    
                    if(!bedMap.isEmpty() && bedMap.containsKey(house.Opportunity__c) && !bedMap.get(house.Opportunity__c).isEmpty()){
                        for(Bed__c bed: bedMap.get(house.Opportunity__c)){
                            if(bed.House__c==null){     
                                bed.House__c = house.Id;
                                bedList.add(bed);
                            }
                        }
                    }
                    
                    if(!bathroomMap.isEmpty() && bathroomMap.containsKey(house.Opportunity__c) && !bathroomMap.get(house.Opportunity__c).isEmpty()){
                        for(Bathroom__c bathroom: bathroomMap.get(house.Opportunity__c)){

                            //update RoomTerm on Bathroom
                            if(bathroom.Room_Inspection__c != null)
                                bathroom.Room__c = (!RoomTermWithRoomInspectionMap.isEmpty() && RoomTermWithRoomInspectionMap.containsKey(bathroom.Room_Inspection__c)) ? RoomTermWithRoomInspectionMap.get(bathroom.Room_Inspection__c) : null; 
                          if(bathroom.House__c==null){          
                            bathroom.House__c = house.Id;
                            bathroomList.add(bathroom);
                          } 
                        }
                    }                
                    
                    if(!roomTermMap.isEmpty() && roomTermMap.containsKey(house.Opportunity__c) && !roomTermMap.get(house.Opportunity__c).isEmpty()){
                        for(Room_Terms__c room: roomTermMap.get(house.Opportunity__c)){
                            if(room.House__c==null){    
                                room.House__c = house.Id;
                                roomsToBeUpdated.add(room);
                            }   
                        }
                    }
                    
                    if(oppKeyDetailsMap.containsKey(house.Opportunity__c) && !oppKeyDetailsMap.get(house.Opportunity__c).isEmpty()){        
                        for(Detail__c keyDetail: oppKeyDetailsMap.get(house.Opportunity__c)){
                         if(keyDetail.House__c==null){  
                            keyDetail.House__c = house.Id;
                            keyDetailsToBeUpdated.add(keyDetail);
                         }  
                        }
                    }
                    
                    // if(house.Onboarding_Zone_Code__c!=null){
                    //      houseToZoneCode.put(house.id,house.Onboarding_Zone_Code__c);
                    //  }
                    
                } // end of for loop

                //updation block
                if(!inspectionList.isEmpty()){
                    try{
                        update inspectionList;
                    } catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);
                    }
                    
                }

                if(!ordList.isEmpty()){
                    try{
                        //update ordList;
                    } catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                    
                    }
                    
                }

                if(!contractList.isEmpty()){
                    try{
                        //update contractList;
                    } catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                    
                    }
                    
                }   

                if(!roomsToBeUpdated.isEmpty()){
                    try {
                        update roomsToBeUpdated;
                    } catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                    
                    }
                    
                }  

                if(!bedList.isEmpty()){
                    try {
                        update bedList;
                    } Catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                
                    }
                }

                if(!bathroomList.isEmpty()){
                    try {
                        update bathroomList;
                    } Catch (Exception e){
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                    
                    }
                }            

                if(!keyDetailsToBeUpdated.isEmpty()){
                    try {
                        update keyDetailsToBeUpdated;
                    } catch (Exception e) {
                        System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                        UtilityClass.insertGenericErrorLog(e);                    
                    }
                    
                }

                update newHouses;  


        
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151'); 
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
}