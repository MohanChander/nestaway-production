/***************************************
    Created By : Mohan
    Purpose    : Helper methods for Field Service Lightning Module.
                 Module related service methods are present in this class
****************************************/        
public class FieldServiceHelper {

/***************************************
    Created By : Mohan
    Purpose    : 1) Schedule the Service Appointments based on the Scheduling Policy
                 2) Change the Owner of the Vendor Work Order to the Assigned Resource
****************************************/   
    public static void scheduleServiceAppointment(List<ServiceAppointment> saList){

        System.debug('**scheduleServiceAppointment');

        try{    
                List<ServiceAppointment> scheduledSaList = new List<ServiceAppointment>();
                Map<Id, ServiceAppointment> saMap = new Map<Id, ServiceAppointment>();
                Map<Id, AssignedResource> assignedResourceMap = new Map<Id, AssignedResource>();
                List<WorkOrder> vendorWorkOrderList = new List<WorkOrder>();
                Set<Id> unscheduledSaSet = new Set<Id>();

                Id schedulingPolicyId = [select id from FSL__Scheduling_Policy__c where Name =: Label.Default_Scheduling_Policy limit 1].Id; 

                for(ServiceAppointment sa: saList){
                    FSL.ScheduleResult scheduleResult = FSL.ScheduleService.Schedule(schedulingPolicyId, sa.Id);
                    if (scheduleResult != null)
                    {   
                        scheduledSaList.add(sa);
                        saMap.put(sa.Id, sa);
                    }
                    else
                    {
                        unscheduledSaSet.add(sa.Id);
                    }
                }  

                //update the Owner of the Work Order for the Scheduled Appointments
                List<AssignedResource> arList = [select Id, ServiceAppointmentId, ServiceAppointment.ParentRecordId, ServiceResourceId, 
                                                 ServiceResource.RelatedRecordId from AssignedResource where ServiceAppointmentId =: saMap.keySet()];

                for(AssignedResource ar: arList){
                    assignedResourceMap.put(ar.ServiceAppointmentId, ar);
                }   

                for(ServiceAppointment sa: saMap.values()){
                    WorkOrder wo = new WorkOrder();
                    wo.Id = sa.ParentRecordId;
                    if(assignedResourceMap.containsKey(sa.Id)){
                        wo.OwnerId = assignedResourceMap.get(sa.Id).ServiceResource.RelatedRecordId;
                    }
                    vendorWorkOrderList.add(wo);                    
                } 
                
                System.debug('**vendorWorkOrderList' + vendorWorkOrderList); 

                if(!vendorWorkOrderList.isEmpty()){
                    update vendorWorkOrderList;
                }    

                if(!unscheduledSaSet.isEmpty()){
                    UtilityClass.insertErrorLog('FSL Module - scheduleServiceAppointment', String.valueOf(unscheduledSaSet));
                }                                    

            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'FSL Module - scheduleServiceAppointment');      
            }   
    } 
}