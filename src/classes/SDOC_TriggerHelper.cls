public class SDOC_TriggerHelper {
    public static void afterUpdate(Map<id,SDOC__SDoc__c > newMap,Map<id,SDOC__SDoc__c > oldMap){
        Id RecordTypeIdcont = Schema.SObjectType.contract.getRecordTypeInfosByName().get(Constants.tenantContractReadonly_RecordType).getRecordTypeId();
        Set<id> setofsdId= new Set<ID>();
        Set<id> setOfContract= new Set<ID>();
        List<Contract> ContList = new List<Contract>();
        Map<Id,String> mapOfSdoc= new  Map<Id,String>();
        for(SDOC__SDoc__c SDOC: newMap.values()){
            if(SDOC.SDOC__GD_Link__c!=null){
                setofsdId.add(SDOC.id);
            }
        }
      
        system.debug('newMap'+newMap.values());
        if(!setofsdId.isEmpty()){
        List<SDOC__SDoc__c> sdocList=[Select id,SDOC__GD_Link__c,(Select id,SDOC__Contract__c from SDOC__SDoc__r ) from SDOC__SDoc__c where id in:setofsdId and SDOC__SDTemplate__r.SDOC__Template_Format__c ='Pdf'];
        for(SDOC__SDoc__c s:sdocList){
            for(SDOC__SDRelationship__c sd:s.SDOC__SDoc__r){
                
                mapOfSdoc.put(sd.id,s.SDOC__GD_Link__c);
                setOfContract.add(sd.SDOC__Contract__c);
            }
        }
        }
        system.debug('mapOfSdoc'+mapOfSdoc);
        List<Contract> ListtoUpdatecontract = new List<Contract>();
        if(!setOfContract.isEmpty()){
       ContList=[Select id,Status,Tenant_Contract_URL__c,Agreement_Generation_Date__c,Send_Contract_To_Web__c,(select id from SDOC__SDoc_Relationships__r where SDOC__SDoc__r.SDOC__SDTemplate__r.SDOC__Template_Format__c ='Pdf'order by createddate desc limit 1) from  Contract where id in:setOfContract];
        }
        if(!ContList.isEmpty()){
        for(Contract c:ContList){
            for(SDOC__SDRelationship__c s:c.SDOC__SDoc_Relationships__r){              
                if(Label.sDoc_URL != null){
                    c.Tenant_Contract_URL__c = Label.sDoc_URL+mapOfSdoc.get(s.id);
                }
                c.Send_Contract_To_Web__c =true;
                c.RecordTypeId=RecordTypeIdcont;
                c.Status='Final Contract';
                c.Agreement_Generation_Date__c =system.now();
                ListtoUpdatecontract.add(c);
            }
        }
        }
        system.debug('ListtoUpdatecontract'+ListtoUpdatecontract);
        if(!ListtoUpdatecontract.isEmpty()){
            update ListtoUpdatecontract;
        }
    }
}