@isTest
public class OrderItemTriggerTest{
    
    @isTest
    public static void orderItemTriggerTestMethod(){
        
           /* Account accObj = new Account();
            accObj.Name = 'TestAcc';
            insert accObj;
                       
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Product2 testProduct = new Product2(Name='Test', IsActive=true);
            insert testProduct;
            Pricebook2 testPriceBook = new Pricebook2(Name='Test', IsActive=true);
            insert testPriceBook;
            
            
            Order orderObj = new Order();
            orderObj.Name =  'TestOrder';
            orderObj.Status = 'Draft';
            orderObj.OpportunityId = opp.Id;
            orderObj.AccountId = accObj.Id;
            orderObj.EffectiveDate = system.today();
            orderObj.pricebook2Id=Test.getStandardPricebookId();
            insert orderObj;

           
            PricebookEntry[] testPricebookEntries = new PricebookEntry[0];
            testPricebookEntries.add(new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=4.99, Product2Id=testProduct.Id));
            testPricebookEntries.add(new PricebookEntry(Pricebook2Id=testPriceBook.Id, UnitPrice=4.99, UseStandardPrice=false, Product2Id=testProduct.Id));
            insert testPricebookEntries;

            OrderItem oi = new OrderItem(OrderId=orderObj.id,Quantity=decimal.valueof('1'),PricebookEntryId=testPricebookEntries[0].Id);
            insert oi;     
            
            oi.Quantity = 2;
            update oi;*/
            
              //insert dummy Vendor account
        Account vendoracc = new account(name ='TestVendor',Contact_Email__c='ameed@warpdrivetech.in',TIN_No__c='tin12345',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091') ;
        vendoracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendoracc.Active__c=true;
        insert(vendoracc);
        Contact cont = new Contact ();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Email='ameed@warpdrivetech.in';
        cont.phone='1234567890';
        cont.accountId = vendoracc.id;
        insert cont;
        
        /*insert dummy Owner account
        Account owneracc = new account(firstname='Test',lastname='test',Contact_Email__c='owner@warpdrivetech.in',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091') ;
        owneracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert(owneracc); */
        
        Account owneracc = new Account();
        owneracc.FirstName = 'Mohan';
        owneracc.LastName = 'Demo';
        insert owneracc;
        
        //insert dummy city
        City__c c = new City__c();
        c.name='New Delhi';
        insert c;
        //insert dummy house
        house__c h = new house__c(Name='white house', House_Layout__c='1 BHK', House_Owner__c=owneracc.id, City_Master__c = c.id);
        insert h;
        //insert dummy product
        
        Id pricebookId = Test.getStandardPricebookId();
      
           Product2 prod = new Product2(Name='TestProd', IsActive=true);  
           insert(prod);  
       
           PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pricebookId, Product2Id=prod.Id, IsActive=true, UnitPrice=10);    
           insert(pbe);
        
           Account acc = new Account(name='test');
           insert(acc);      
                     
           Opportunity opp = new Opportunity(Name='test', StageName='Open', CloseDate=System.today(), AccountId=acc.Id, Pricebook2Id=pricebookId);      
           insert(opp);
        
        //insert dummy Order
        order o = new order(accountId=owneracc.id,vendor__c =vendoracc.id, EffectiveDate = System.today(), Status ='Draft' ,house__C=h.id, pricebook2Id=pricebookId );
        o.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        insert o;
        

        OrderItem oi = new OrderItem(OrderId=o.id, UnitPrice=100, Quantity=decimal.valueof('1'), PricebookEntryId=pbe.Id);
        insert oi;     
        
        oi.Quantity = 2;
        update oi;      
        
        
            
            InvoiceUtility.addTest();
            InvoiceUtility.addTest1();
            InvoiceUtility.addTest2();
            InvoiceUtility.addTest3();
            InvoiceUtility.addTest4();
            InvoiceUtility.addTest6();
            InvoiceUtility.addTest7();
            InvoiceUtility.addTest8();
            InvoiceUtility.addTest9();
            InvoiceUtility.addTest10();
            InvoiceUtility.addTest11();
            InvoiceUtility.addTest12();
            InvoiceUtility.addTest13();
            InvoiceUtility.addTest14();
            InvoiceUtility.addTest15();
            
             tempclass.addTest();
            tempclass.addTest1();
            tempclass.addTest2();
            tempclass.addTest3();
            tempclass.addTest4();
            tempclass.addTest6();
            tempclass.addTest16();
            tempclass.addTest17();
            tempclass.addTest18();
            tempclass.addTest19();
            tempclass.addTest20();
            tempclass.addTest21();
            
      }      
      
}