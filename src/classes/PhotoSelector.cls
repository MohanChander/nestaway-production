public class PhotoSelector {

	//get Photo List with house Status
	public static List<Photo__c> getPhotosWithHouseStatus(Set<Id> photoIdSet){

		return [select Id, House__c, House__r.Stage__c from Photo__c where Id =: photoIdSet];
	}
}