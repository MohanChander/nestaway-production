public class ContractTriggerHelper
{
    public static void ContarctAddressFormAccount(List<contract> contlist)
    {
        // Chandu: adding condition to update only in owner contract.
        
          Id ownerConRecordTypeId=Schema.SObjectType.Contract.getRecordTypeInfosByName().get(Constants.CONTRACT_RT_OWNER).getRecordTypeId();
        
        //End
        
        
        Set<id> accIdSet=new Set<id>();
        Map<id,Account> accMap=new Map<id,Account>();
        for(contract ct:contlist)
            {
                accIdSet.add(ct.Accountid);
            }
        if(!accIdSet.isEmpty())
            {
             accMap=AccountSelector.getAccountMapfromAccId(accIdSet);
            }
        for(contract ct:contlist)
            {
              // chandu : adding condition to update only in owner contract.
              
            if(ct.recordtypeId==null || ct.recordtypeId==ownerConRecordTypeId){       
                if(accMap.containsKey(ct.accountid))
                {
                    if(accMap.get(ct.accountid).BillingCity!=null)
                    {
                        ct.BillingCity=accMap.get(ct.accountid).BillingCity;
                    }
                    if(accMap.get(ct.accountid).BillingCountry!=null)
                    {
                    ct.BillingCountry=accMap.get(ct.accountid).BillingCountry;
                    }
                    if(accMap.get(ct.accountid).BillingCountryCode!=null)
                    {
                        ct.BillingCountryCode=accMap.get(ct.accountid).BillingCountryCode;
                    }
                    if(accMap.get(ct.accountid).BillingPostalCode!=null)
                    {
                        ct.BillingPostalCode=accMap.get(ct.accountid).BillingPostalCode;
                    }
                    if(accMap.get(ct.accountid).BillingState!=null)
                    {
                       ct.BillingState=accMap.get(ct.accountid).BillingState;
                    }
                    if(accMap.get(ct.accountid).BillingStateCode!=null)
                    {
                      ct.BillingStateCode=accMap.get(ct.accountid).BillingStateCode;
                    }
                    if(accMap.get(ct.accountid).BillingStreet!=null)
                    {
                        ct.BillingStreet=accMap.get(ct.accountid).BillingStreet;
                    }
                    if(accMap.get(ct.accountid).BillingGeocodeAccuracy!=null)
                    {
                        ct.BillingGeocodeAccuracy=accMap.get(ct.accountid).BillingGeocodeAccuracy;
                    }
                    if(accMap.get(ct.accountid).ShippingCity!=null)
                    {
                        ct.ShippingCity=accMap.get(ct.accountid).ShippingCity;
                    }
                    if(accMap.get(ct.accountid).ShippingCountry!=null)
                    {
                       ct.ShippingCountry=accMap.get(ct.accountid).ShippingCountry;
                    }
                    if(accMap.get(ct.accountid).ShippingCountryCode!=null)
                    {
                       ct.ShippingCountryCode=accMap.get(ct.accountid).ShippingCountryCode;
                    }
                    if(accMap.get(ct.accountid).ShippingGeocodeAccuracy!=null)
                    {
                       ct.ShippingGeocodeAccuracy=accMap.get(ct.accountid).ShippingGeocodeAccuracy;
                    }
                    if(accMap.get(ct.accountid).ShippingPostalCode!=null)
                    {
                        ct.ShippingPostalCode=accMap.get(ct.accountid).ShippingPostalCode;
                    }
                    if(accMap.get(ct.accountid).ShippingState!=null)
                    {
                        ct.ShippingState=accMap.get(ct.accountid).ShippingState;
                    }
                    if(accMap.get(ct.accountid).ShippingStateCode!=null)
                    {
                        ct.ShippingStateCode=accMap.get(ct.accountid).ShippingStateCode;
                    }
                    if(accMap.get(ct.accountid).ShippingStreet!=null)
                    {
                        ct.ShippingStreet=accMap.get(ct.accountid).ShippingStreet;
                    }
               
                }
              } 
            }
          
    }
     // Added by deepak 
    // To update insurance to send a api
    public static void sendApiCallForInsurance(List<Contract> contList){
        Set<ID> setofHouse = new Set<ID>();
        List<House__c> houseList = new List<House__C>();
        List<Insurance__C> insList = new List<Insurance__C>();
        try{
        for(Contract each : ContList){
             if(each.house__c != null){
            setofHouse.add(each.House__c);
        }
        }
        if(!setofHouse.isEmpty()){
            houseList = [Select id,(Select id,status__c from Insurances__r where status__C ='new' and LOI__c =  false ) from House__C where id in:setofHouse];
         if(!houseList.isEmpty()){
            for(House__C each : houseList){
                for(Insurance__c  ins :  each.Insurances__r){
                    ins.status__c = Constants.INSURANCE_STATUS_TYPE_TO_BE_QUEUED; 
                    ins.LOI__c = true;
                    insList.add(ins);
                }
            }
        }
            if(!insList.isEmpty()){
            update insList;
        }
        }
        }Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'insurance from contract');
        }
    }
     /****************************************************************************************
     Added by Baibhav Kumar
     Purpose : for Populating Hrm on contract as Approver for Final  contract Approving
     Created on: 11/7/2017

    *****************************************************************************************/
    public static void HrmforApproval(list<Contract> newList){
        try{
                List<GroupMember> grpmbList=new List<GroupMember>();
                
                Map<id,Integer> usrMapWithCount=new Map<id,Integer>();
        
                grpmbList =[SELECT GroupId,Id,UserOrGroupId,group.name FROM GroupMember where group.name=:label.Contract_HRM_Approval_Queue_name];
        
                if(!grpmbList.isEmpty()){
                    for(GroupMember grp:grpmbList){
                        usrMapWithCount.put(grp.UserOrGroupId,0);
                    }
                }
        
                System.debug('***User'+usrMapWithCount);
                List<Contract> contlist=new List<Contract>(); 
                List<AggregateResult>  agrresult=new List<AggregateResult>();
        
                if(!usrMapWithCount.keySet().isEmpty()){
                  agrresult=[Select hrm__c, Count(id) from contract where HRM__c=:usrMapWithCount.keySet() and  Status='Final Contract' 
                            and (Approval_Status__c=:Contract_Constants.CONTRACT_APP_STATUS_AW_HRM_APP or Approval_Status__c=:Contract_Constants.CONTRACT_APP_STATUS_MANULLY_APP_ZM 
                            or Approval_Status__c=:Contract_Constants.CONTRACT_APP_STATUS_SAM_CONT_APP_OWNER) and RecordType.name='Owner Contract' group by HRM__c];
                 }
        
                System.debug('***aggreatmap'+agrresult);
        
                for(AggregateResult ar:agrresult){
                     Integer noOfCont = 0;
                        if((Integer)ar.get('expr0') == null){
                            noOfCont = 0;
                        } else {
                            noOfCont = (Integer)ar.get('expr0');
                        }
                        usrMapWithCount.put((Id)ar.get('HRM__c'),noOfCont);
                }
        
                System.debug('***User1map'+usrMapWithCount);
                
                
                for(Contract ct:newList){
        
                    if(!usrMapWithCount.keySet().isEmpty()){
        
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: usrMapWithCount.keySet()){
                           awList.add(new AssignmentWrapper(userId, usrMapWithCount.get(userId)));
                        }
        
                        awList.sort();
                        ct.HRM__c  = awList[0].UserId; 
        
                        Integer noOfCont = usrMapWithCount.get(awList[0].UserId);
                        usrMapWithCount.put(ct.HRM__c, ++noOfCont);
                    }
        
                }
            } Catch(Exception e) {
               System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e,'HRM Assiignment for contract Approval');
              }

      
    }
     /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }

        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }   
}