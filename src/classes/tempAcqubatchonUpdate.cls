global class tempAcqubatchonUpdate implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    
    public tempAcqubatchonUpdate(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query = 'Select id,Onboarding_Zone_code__c,House_Lat_Long_details__Latitude__s,'+
                        'House_Lat_Long_details__Longitude__s , Acquisition_Zone_code__c, House__c'+
                        'FROM House_Inspection_Checklist__c'+
                        'where (Acquisition_Zone_code__c = NULL OR Onboarding_Zone_code__c = NULL)'+
                    'AND (House_Lat_Long_details__Longitude__s != NULL AND House_Lat_Long_details__Latitude__s != NULL )';
        */
       // String query = 'Select id,Acquisition_Zone_Code__c,Onboarding_Zone_Code__c,House_Lattitude__c,House_Longitude__c,city__c,HRM__c,ZAM__c from house__c';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> houseList) {
      
        StopRecursion.HouseSwitch = false;       
        list <house__c> houseListToUpdate = new list <house__c> ();
        list <Zing_API_URL__c> zingapiurl = [SELECT id, URL__c  FROM Zing_API_URL__C];
        List<WBMIMOJSONWrapperClasses.wrapperZone > wrpList = new List< WBMIMOJSONWrapperClasses.wrapperZone >(); 
        try { 
        Date dateToday = Date.today();
            
        String sMonth = String.valueof(dateToday.month());
        String sDay = String.valueof(dateToday.day());
        if(sMonth.length()==1){
            sMonth = '0' + sMonth;
        }
        if(sDay.length()==1){
            sDay = '0' + sDay;
        }
        String sToday = String.valueof(dateToday.year()) +'-'+sMonth +'-'+ sDay ;
        for(house__c hic : houseList) {
               
                String ApiEndpoint='';
                String APIEndpoint_acquisition = '';             
                string lat = String.valueOf(hic.House_Lattitude__c) ;       // '12.9201963' ;  
                string lon = String.valueOf(hic.House_Longitude__c) ;      //'77.6481260' ;
                
                String timestamp = sToday;//'2017-05-23';
                if(zingapiurl.size() > 0) {
                    ApiEndpoint = zingapiurl[0].URL__c ;
                    APIEndpoint_acquisition = zingapiurl[0].URL__c ;
                }
               

                if( APIEndpoint_acquisition != null && APIEndpoint_acquisition != '' && lat!= null ) {
                    APIEndpoint_acquisition+='tag=OwnerAcquisition&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(APIEndpoint_acquisition);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000

                    try {
                        System.debug('try1');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ) {
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);

                                    if(wrpList[0].area_id != null) {
                                        hic.Temp_Acquisition_Zone_Code__c   = wrpList[0].area_id ;
                                   /*     //hic_to_Update.add(hic);
                                        if(house.id != null ) {
                                            house.Acquisition_Zone_Code__c = wrpList[0].area_id ;
                                            //houseListToUpdate.add(house);
                                            System.debug('\n***'+houseListToUpdate);
                                        }
                                        
                                        System.debug('****'+hic_to_Update);
                                     */ 
                                    }
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                            }
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                      
                    }
                }
              // hic_to_Update.add(hic);

              
              
              
                houseListToUpdate.add(hic);

            } 
        
            if(houseListToUpdate.size() > 0) {
                
                 StopRecursion.HouseSwitch = false;
                     update houseListToUpdate;
                

            }
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
}