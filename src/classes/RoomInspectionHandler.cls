/*
* Description: It's trigger handler class of Room Inspection object's trigger, to update & delete Room Term object records based on few conditions
*/


Public Class RoomInspectionHandler{
    Public Static Void AfterUpdate(Map<Id,Room_Inspection__c> oldMap, Map<Id,Room_Inspection__c> newMap){
      Id moveInRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
      Id mimoRtId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get('MIMO RIC').getRecordTypeId();
      Id moveOutRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
        
        Map<Id,String> mapId_RoomNumber = new Map<Id,String>();
        List<Room_Inspection__c> mimoRicList = new List<Room_Inspection__c>();
        Map<Id,Room_Inspection__c> mapofMoveInRoom = new Map<Id,Room_Inspection__c>();
        Map<Id,Room_Inspection__c> mapofMoveOutRoom = new Map<Id,Room_Inspection__c>();
    
        for(Room_Inspection__c each : newMap.values()){
            if(each.Name != Null && oldMap.get(each.Id).Name!=null && each.Name !=  oldMap.get(each.Id).Name){
                mapId_RoomNumber.put(each.Id,each.Name);
            }
             if(each.RecordTypeId == mimoRtId){
                mimoRicList.add(each);
            }
            if(each.RecordTypeId == moveInRicRTId){
                mapofMoveInRoom.put(each.id,each);
            }
            if(each.RecordTypeId == moveOutRicRTId){
                mapofMoveOutRoom.put(each.id,each);
            }
        }
        //added by : Mohan - Sync Move In and Move Out Checklists
       /* if(!mimoRicList.isEmpty()){
            RoomInspectionTriggerHelper.syncMoveInMoveOutChecklistsWithMIMO(mimoRicList);
        }*/
        if(!mapofMoveInRoom.isEmpty()){
            RicTrigerHandler.afterUpdateMoveIn(oldmap,mapofMoveInRoom);
        }
        if(!mapofMoveOutRoom.isEmpty()){
            RicTrigerHandler.afterUpdateMoveOut(oldmap,mapofMoveOutRoom);
        }
        list<Room_Terms__c>lstRoomTermsToUpdate = new list<Room_Terms__c>();
        if(!mapId_RoomNumber.isEmpty()){
            lstRoomTermsToUpdate = [SELECT Id,Name,Bedroom_Number__c,Room_Inspection__c FROM Room_Terms__c WHERE Room_Inspection__c IN:mapId_RoomNumber.keyset()];
            for(Room_Terms__c each: lstRoomTermsToUpdate){
                if(mapId_RoomNumber.containsKey(each.Room_Inspection__c)){
                    each.Name = mapId_RoomNumber.get(each.Room_Inspection__c);
                }
            }
        }
        if(lstRoomTermsToUpdate.size()>0){
            try{
                Update lstRoomTermsToUpdate;
            }catch(exception ex){
                System.debug('==exception while RoomInspection Update=='+ex);
            }
        }   
        // RicTrigerHandler.AfterUpdate(oldmap,newmap);
    }
    
    Public Static Void AfterDelete(Map<Id,Room_Inspection__c> oldMap){
        list<Room_Terms__c> lstRoomTermsToDelete = new list<Room_Terms__c>();
        lstRoomTermsToDelete = [SELECT id,Room_Inspection__c FROM Room_Terms__c WHERE Room_Inspection__c IN: oldMap.keyset() ];
        if(lstRoomTermsToDelete.size()>0){
            try{
                Delete lstRoomTermsToDelete;
            }catch(exception ex){
                system.debug('==exception while deleting Room Terms in Room Inspection Trigger=='+ex);
            }
        }
    }

    /*  Created By: Mohan - WarpDrive Tech Works
        Purpose   : When RIC is edited it should sync with the webapp system - Make an API call to Web Entity : House 
                    Call to be made only when the House is Live */
    public static void sendWebEntityHouseJson(Map<Id, Room_Inspection__c> newMap){

        try {
                Set<Id> ricIdSet = new Set<Id>();
                Set<Id> houseIdSet = new Set<Id>();

                for(Room_Inspection__c ric: newMap.values()){
                    if(ric.House__c != null){
                        ricIdSet.add(ric.Id);
                    }
                }  

                if(!ricIdSet.isEmpty()){
                    List<Room_Inspection__c> ricList = RoomInspectionSelector.getRicWithHouseStatus(ricIdSet);

                    for(Room_Inspection__c ric: ricList){
                      if(ric.House_Inspection_Checklist__r.House__c != null && ric.House_Inspection_Checklist__r.House__r.Stage__c != Constants.HOUSE_STAGE_HOUSE_DRAFT && ric.House__r.Stage__c != Constants.HOUSE_STAGE_ONBOARDING){
                         // House2Json.createJson(ric.House__c);
                         houseIdSet.add(ric.House__c);
                      }
                    }
                }  
                
                if(houseIdSet.size()>0){
                  for(id hId:houseIdSet){
                      House2Json.createJson(hId);
                  }
                }    

            } Catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }         

    }  

    
     // Method created by chandu
    // method to create bathroom record
    
    public static void createBathrooms(Map<Id,Room_Inspection__c > newMap,Map<Id,Room_Inspection__c> oldMap){
        
        try{
          List<Bathroom__c> allBathroomLst=[select id,Room_Inspection__c from Bathroom__c where Room_Inspection__c IN:newMap.keyset()];
          Map<Id,List<Bathroom__c>> batchRoomMap = new Map<Id,List<Bathroom__c>>();
          Set<Id> hicIdSet = new Set<Id>();
          Map<Id, Integer> noOfBathroomsAtHicMap = new Map<Id, Integer>();
          Map<Id, Integer> actualAttachedBathroomsMap = new Map<Id, Integer>();

          for(Room_Inspection__c ric: [select Id, House_Inspection_Checklist__r.Number_of_attached_bathrooms__c, 
                                       House_Inspection_Checklist__c from Room_Inspection__c where Id =: newMap.keyset()]){
              noOfBathroomsAtHicMap.put(ric.House_Inspection_Checklist__c, Integer.valueOf(ric.House_Inspection_Checklist__r.Number_of_attached_bathrooms__c));
          }

          for(Room_Inspection__c ric: newMap.values()){
            if(ric.House_Inspection_Checklist__c != null)
              hicIdSet.add(ric.House_Inspection_Checklist__c);
          }

          List<AggregateResult> groupedResult = BathroomSelector.getBathroomCountForHIC(hicIdSet);

          for(AggregateResult ar: BathroomSelector.getBathroomCountForHIC(hicIdSet)){
            actualAttachedBathroomsMap.put((Id)ar.get('House_Inspection_Checklist__c'), (Integer)ar.get('expr0'));
          }    

          System.debug('**********actualAttachedBathroomsMap' + actualAttachedBathroomsMap); 
          System.debug('**********BathRoomSelector Query' + BathroomSelector.getBathroomCountForHIC(hicIdSet));
          System.debug('**********groupedResult' + groupedResult);                      

          for(Bathroom__c bathroom: allBathroomLst){
              
             if(!batchRoomMap.containsKey(bathroom.Room_Inspection__c)) 
              batchRoomMap.put(bathroom.Room_Inspection__c, new List<Bathroom__c>{bathroom});
             else{
                 batchRoomMap.get(bathroom.Room_Inspection__c).add(bathroom);
             }
          }
          
          list<Bathroom__c> insertBathroom = new List<Bathroom__c>();
          list<Bathroom__c> deleteBathroom = new List<Bathroom__c>();
          
          for(Room_Inspection__c ric: newMap.values()){        
                                   
                       if(ric.Has_an_attached_bathroom__c == 'Yes' && !batchRoomMap.containsKey(ric.id)){      

                              //validation check - Added by Mohan 
                              // chandu: by pass this condition for WO page.
                              if(!StopRecursion.DisabledBathRoomValidationOnWOPage && (noOfBathroomsAtHicMap.get(ric.House_Inspection_Checklist__c) == 0 || actualAttachedBathroomsMap.get(ric.House_Inspection_Checklist__c) >= noOfBathroomsAtHicMap.get(ric.House_Inspection_Checklist__c))){
                                ric.addError('No of Attached Bathrooms specified at House Inspection Checklist is ' + noOfBathroomsAtHicMap.get(ric.House_Inspection_Checklist__c) + '. You cannot add more Attached Bathrooms than specified.');
                              }           
                              
                               Bathroom__c batchRoom= new Bathroom__c();
                               batchRoom.Room_Inspection__c=ric.id;
                               batchRoom.Type__c='Attached';
                               if(ric.name.contains('Room Number')){
                                  batchRoom.name=ric.name.replace('Room Number','Bathroom');
                               } 
                                else{
                                     batchRoom.name='Bathroom';
                                }
                                
                               insertBathroom.add(batchRoom);
                           }
                           
                           
                       
                       else{
                           
                          
                          if(ric.Has_an_attached_bathroom__c == 'No' && batchRoomMap.containsKey(ric.id)){    
                           
                             deleteBathroom.addAll(batchRoomMap.get(ric.id));
                           
                          }
                          
                       }       
              
          }
         
          if(insertBathroom.size()>0){
              
              insert insertBathroom;
          }
          if(deleteBathroom.size()>0){
              
              delete deleteBathroom;
          } 
        }  
        catch(exception e){
              system.debug('****Exception in batchroom insert or delete  :'+e.getMessage()+' at line:'+e.getLineNumber());
        }
          
    }


   

}