public with sharing class GenrateQRExtension {
    public final house__c hou {get;set;}
    public List<WraperRoom> wrapRoomList {get;set;}
    public List<WraperBed> wrapBedList {get;set;}
    public List<WraperAsset> wrapAsstList {get;set;}
    public List<WraperQR> wrapQRtList {get;set;}

    public boolean houselect {get;set;}
    public boolean warpAssTrue {get;set;}
    public boolean warpbedTrue {get;set;}
    public boolean warproomTrue {get;set;}



    public GenrateQRExtension(ApexPages.StandardController stdController) {
        hou=[SELECT id,HouseId__c,QR_code__c from house__c where id=:stdController.getRecord().id];
        List<Room_Terms__c> romList=[Select id,name,QR_code__c  from Room_Terms__c where House__c=:hou.id];
        List<Bed__c> bedList=[Select id,name,Room_Terms__r.name,QR_code__c from Bed__c where House__c=:hou.id];
        List<Asset> asstList=[Select id,name,QR_code__c from Asset where House__c=:hou.id];
        houselect=false;
        wrapRoomList=new List<WraperRoom>();
        wrapBedList=new List<WraperBed>();
        wrapAsstList=new List<WraperAsset>();
        wrapQRtList=new List<WraperQR>();
        warpAssTrue=false;
        warproomTrue=false;
        warpbedTrue=false;
        for(Room_Terms__c rt:romList){
            WraperRoom wr=new WraperRoom();
            wr.sel=false;
            wr.room=rt;
            wrapRoomList.add(wr);
        }
        for(Bed__c bd:bedList){
            WraperBed wb=new WraperBed();
            wb.sel=false;
            wb.bed=bd;
            wrapBedList.add(wb);
        }
        for(Asset at:asstList){
            WraperAsset wa=new WraperAsset();
            wa.sel=false;
            wa.asst=at;
            wrapAsstList.add(wa);
        }
        if(wrapRoomList.size()>0){
            warproomTrue=true;
        }
        if(wrapBedList.size()>0){
            warpbedTrue=true;
        }
        if(wrapAsstList.size()>0){
            warpAssTrue=true;
        }
        System.debug('***be'+wrapBedList);


    }
    public class WraperRoom{
        public boolean sel {get;set;}
        public Room_Terms__c room {get;set;} 
    }
    public class WraperBed{
        public boolean sel {get;set;}
        public Bed__c bed {get;set;} 
    }
    public class WraperAsset{
        public boolean sel {get;set;}
        public Asset asst {get;set;} 
    }
     public class WraperQR{
        public String name {get;set;}
        public id objId {get;set;} 
    }
    public PageReference Genrate(){
        System.debug('*****QR***');

         WraperQR qr1=new WraperQR();
         qr1.name='House: '+hou.HouseId__c;
         qr1.objId=hou.id;
         wrapQRtList.add(qr1);
        
        if(!wrapRoomList.isEmpty()){
            for(WraperRoom wr:wrapRoomList){
                if(wr.sel==true){
                    WraperQR qr=new WraperQR();
                    qr.name=hou.HouseId__c+'\n'+wr.room.name;
                    qr.objId=wr.room.id;
                    wrapQRtList.add(qr);
                }
           }
        }
         if(!wrapBedList.isEmpty()){
            for(WraperBed wb:wrapBedList){
                if(wb.sel==true){
                    WraperQR qr=new WraperQR();
                    qr.name=hou.HouseId__c+'\n'+wb.bed.Room_Terms__r.name+'\n'+wb.bed.name;
                    qr.objId=wb.bed.id;
                    wrapQRtList.add(qr);
                }
           }
        }
         if(!wrapAsstList.isEmpty()){
            for(WraperAsset wa:wrapAsstList){
                if(wa.sel==true){
                    WraperQR qr=new WraperQR();
                    qr.name=hou.HouseId__c+'\n Asset: '+wa.asst.name;
                    qr.objId=wa.asst.id;
                    wrapQRtList.add(qr);
                }
           }
        }

        PageReference pr = new PageReference('/apex/QRcodePage?id='+hou.id);
        //pr.setRedirect(true);
        return pr;

    }
      public pagereference cancel(){
       
        PageReference parentPage;
        if(hou!=null){
            parentPage = new PageReference('/' + hou.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        return null;
    }
}