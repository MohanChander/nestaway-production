/**********************************************
    Created By : Mohan
    Purpose    : Service class for Work Order related functionality (FSL Module)
**********************************************/
public class WorkOrderFSLService {

/**********************************************
    Created By : Mohan
    Purpose    : Update the ArrivalStartWindow and ArrivalFinishWindow of the Service Appointments
                 Call the ScheduleService so that the Service Appointments are Scheduled.
**********************************************/ 
    public static void scheduleServiceAppointmentsForWorkOrders(Map<Id, WorkOrder> woMap){

        System.debug('**scheduleServiceAppointmentsForWorkOrders');

        try{
                Map<Id, Case> caseMap = new Map<Id, Case>([select Id, Service_Visit_Time__c, Problem__r.Work_Type__r.EstimatedDuration
                                                           from Case where 
                                       Id in (Select CaseId from WorkOrder where Id =: woMap.keySet())]);

                List<ServiceAppointment> saList = [Select Id, ParentRecordId, ArrivalWindowStartTime, ArrivalWindowEndTime
                                                   from ServiceAppointment where 
                                                   ParentRecordId =: woMap.keySet() and 
                                                   (Status =: ServiceAppointmentConstants.STATUS_NONE or
                                                    Status =: ServiceAppointmentConstants.STATUS_SCHEDULED)];

                for(ServiceAppointment sa: saList){
                    if(woMap.containsKey(sa.ParentRecordId) && caseMap.containsKey(woMap.get(sa.ParentRecordId).CaseId)){
                        sa.ArrivalWindowStartTime = caseMap.get(woMap.get(sa.ParentRecordId).CaseId).Service_Visit_Time__c;
                        if(caseMap.get(woMap.get(sa.ParentRecordId).CaseId).Problem__r.Work_Type__r.EstimatedDuration != null){
                            sa.ArrivalWindowEndTime = sa.ArrivalWindowStartTime.addMinutes(Integer.valueOf(caseMap.get(woMap.get(sa.ParentRecordId).CaseId).Problem__r.Work_Type__r.EstimatedDuration));
                        } else {
                            sa.ArrivalWindowEndTime = sa.ArrivalWindowStartTime.addMinutes(60);
                        }                    
                        sa.Street = woMap.get(sa.ParentRecordId).Street;
                        sa.City = woMap.get(sa.ParentRecordId).City;  
                        sa.State = woMap.get(sa.ParentRecordId).State;
                        sa.Country = woMap.get(sa.ParentRecordId).Country; 
                        sa.PostalCode = woMap.get(sa.ParentRecordId).PostalCode;    
                    }                  
                }   

                if(!saList.isEmpty()){
                    Update saList;
                    FieldServiceHelper.scheduleServiceAppointment(saList);
                }                                                                                  

            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'FSL Module - scheduleServiceAppointmentsForWorkOrders', String.valueOf(woMap.keySet()));      
            }              
    }


/**********************************************
    Created By : Mohan
    Purpose    : Unschedule Service Appointments when the Work Order and reschedule the Wo
**********************************************/   
    public static void unScheduleServiceAppointmentsForWorkOrders(Map<Id, WorkOrder> woMap){

        System.debug('**unScheduleServiceAppointmentsForWorkOrders');

        try{
                List<ServiceAppointment> saList = [Select Id, ParentRecordId, ArrivalWindowStartTime, ArrivalWindowEndTime
                                                   from ServiceAppointment where 
                                                   ParentRecordId =: woMap.keySet()];

                for(ServiceAppointment sa: saList){
                    sa.Status = ServiceAppointmentConstants.STATUS_NONE;                             
                }   

                if(!saList.isEmpty()){
                    Update saList;
                    scheduleServiceAppointmentsForWorkOrders(woMap);                  
                }                                                                                  

            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'FSL Module - unScheduleServiceAppointmentsForWorkOrders');      
            }              
    }  
}