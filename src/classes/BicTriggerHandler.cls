public class BicTriggerHandler {
    Public Static Id operationalRtMImoandService= Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(MissingItemsConstants.OPERATIONAL_PROCESS_RT_MIMO_SERVICE).getRecordTypeId();
    Public Static Id moveOutBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();  
    Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    Public Static Id moveInBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
    Public Static Id mimoBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK).getRecordTypeId();    
    Public Static Void afterUpdateMoveIn(Map<Id,Bathroom__c > newMap,Map<Id,Bathroom__c > oldmap){
        Set<Id> SetOfBic = new Set<Id>();
        Set<Id> SetOfBath = new Set<Id>();
        Set<Id> SetOfcasebath = new Set<Id>();
        List<Case> caseList = new List<Case>();
        List<Case> dupcaseList = new List<Case>();
        Set<Id> setofCaseTobeUpdated = new Set<Id>();
        List<Case> listOfCaseToBeInsert = new List<Case>();
        List<Case> listOfCaseToBeInsert1 = new List<Case>();
        List<Case> listOfCaseToBeUpdate = new List<Case>();
        List<Case> ListOfcaseupdate = new List<Case>();
        Map<Id,Case> mapOfCaseInsertedBefore = new  Map<Id,Case>();
        Map<Id,List<Case>> mapOfHouseandListOfdupliactecase = new Map<id,List<Case>>();
        List<Case>  caseListInsertedBefore = new List<Case>();
        List<Bathroom__c > BicListMimo = new List<Bathroom__c >();
        List<Bathroom__c > BicListMoveIn = new List<Bathroom__c >();
        Map<id,Bathroom__c > mimoMapBic = new  Map<id,Bathroom__c >();
        Map<String,List<Case>> mapofBathroomandListOfcase = new Map<String,List<Case>>();
        List<Operational_Process__c> opList = [Select id,Problem_Name__c ,Field_API_Name__c ,New_Field_Value__c ,Previous_Field_Value__c,Problem__c   from Operational_Process__c];
        for(Bathroom__c  	 each:newmap.values()){
            if(each.RecordTypeId == moveInBicRTId){
                if(each.CheckList_Verified__c=='Yes' && oldMap.get(each.id).CheckList_Verified__c=='No'){
                    SetOfBath.add(each.CheckFor__c);
                    SetOfBic.add(each.id); 
                }
            }
        }
        system.debug('SetOfBath'+SetOfBath);
        system.debug('SetOfBic'+SetOfBic);
        if(!SetOfBath.isEmpty()){
            BicListMimo=[Select id,HouseForMIMO__c,Mirror__c,Bucket_Mug__c,CheckFor__c,Toilet_Brush__c,Tenant_Bathroom_Cleaned__c,Water_Heater__c    from Bathroom__c  where recordtypeid =:mimoBicRTId and  CheckFor__c in:SetOfBath];
        }
        if(!SetOfBic.isEmpty()){
            BicListMoveIn=[Select id,HouseForMIMO__c,Mirror__c,Bucket_Mug__c,CheckFor__c,Toilet_Brush__c,Tenant_Bathroom_Cleaned__c ,Water_Heater__c ,Work_Order__r.caseid,Work_Order__r.Tenant__c from Bathroom__c  where recordtypeid =:moveInBicRTId and id in:SetOfBic];
        }
        system.debug('BicListMimo'+BicListMimo);
        system.debug('BicListMoveIn'+BicListMoveIn);
        if(!BicListMimo.isEmpty()){
            for(Bathroom__c  each:BicListMimo){
                mimoMapBic.put(each.CheckFor__c,each);
            }
        }
        if(!BicListMoveIn.isEmpty()){
            for(Bathroom__c  each:BicListMoveIn){
                if(each.Tenant_Bathroom_Cleaned__c ==  MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO){
                    System.debug('***Tenant_Bathroom_Cleaned__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
                    c.Sub_Category__c=MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_BATHROOM_CLEANED;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = each.Work_Order__r.Tenant__c;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Tenant_Bathroom_Cleaned__c'&& op.New_Field_Value__c  == MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO){
                            c.Problem__c=op.Problem__c;
                            c.subject=op.Problem_Name__c;
                            
                        }
                    }
                    
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Water_Heater__c == MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING  || mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )){
                    System.debug('***Water_Heater__c**');
                    
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_ELECTRICAL;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_WATER_HEATER;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = each.Work_Order__r.Tenant__c;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Water_Heater__c'&& op.New_Field_Value__c  == MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING ){
                            c.Problem__c=op.Problem__c;
                            c.subject=op.Problem_Name__c;
                            
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Water_Heater__c ==MissingItemsConstants.WATER_HEATER_NOT_WORKING   )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING  || mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )){
                    System.debug('***Water_Heater__c**');
                    
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_ELECTRICAL;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_WATER_HEATER;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = each.Work_Order__r.Tenant__c;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Water_Heater__c'&& op.New_Field_Value__c  == MissingItemsConstants.WATER_HEATER_NOT_WORKING ){
                            c.Problem__c=op.Problem__c;
                            c.subject=op.Problem_Name__c;
                            
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Mirror__c == MissingItemsConstants.MIRROR_NOT_PRESENT  )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION   || mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION )){
                    System.debug('***Mirror__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_FURNISHING;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_MIRROR;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = each.Work_Order__r.Tenant__c;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Mirror__c'&& op.New_Field_Value__c  == MissingItemsConstants.MIRROR_NOT_PRESENT ){
                            c.Problem__c=op.Problem__c;
                            c.subject=op.Problem_Name__c;
                            
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Mirror__c	 ==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION)  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION   || mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION )){
                    System.debug('***Mirror__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_FURNISHING;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_MIRROR;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = each.Work_Order__r.Tenant__c;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Mirror__c'&& op.New_Field_Value__c  == MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION ){
                            c.Problem__c=op.Problem__c;
                            c.subject=op.Problem_Name__c;
                            
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
            }
        }
        
        System.debug('***************caseList: ' + caseList + '\n Size of caseList: ' + caseList.size());
        
        for(Case each: caseList){
            String CaseAccountId = each.AccountId;
            String caseSubject=each.subject;
            String caseIssueLevel='personal';
            String caseRelationshipType='tenant';
            String caeHouseSfid= each.House1__c;
            String caseDescription=each.subject;
            DAteTime caseServiceVisitDateTime= System.now()+1;
            String caseParentId= each.ParentId;
            String category =  each.Category__c;
            String mimoId = each.MimoHic__c;
            String TypeOfInspection = each.TypeOfInspection__c;
            String SubCategory = each.Sub_Category__c;
            
            ServiceRequestCaseCreationAPI.serviceRequestDetailsToWebAppFuture(CaseAccountId,caseSubject,caseIssueLevel,caseRelationshipType,
                                                                              caeHouseSfid,caseDescription,caseServiceVisitDateTime,caseParentId,category,mimoId,TypeOfInspection,SubCategory);
        }
    }
    Public Static Void afterUpdateMoveOut(Map<Id,Bathroom__c > newMap,Map<Id,Bathroom__c > oldMap){
        system.debug('oldmpa'+oldmap.values());
        system.debug('newmap'+newMap.values());
        Set<Id> SetOfBic = new Set<Id>();
        Set<Id> SetOfBath = new Set<Id>();
        Set<Id> SetOfcasebath = new Set<Id>();
        List<Case> caseList = new List<Case>();
        Set<Id> setofCaseTobeUpdated = new Set<Id>();
        List<Case> listOfCaseToBeInsert = new List<Case>();
        List<Case> listOfCaseToBeInsert1 = new List<Case>();
        List<Case> listOfCaseToBeUpdate = new List<Case>();
        List<Case> ListOfcaseupdate = new List<Case>();
        Map<Id,Case> mapOfCaseInsertedBefore = new  Map<Id,Case>();
        List<Case>  caseListInsertedBefore = new List<Case>();
        List<Bathroom__c > BicListMimo = new List<Bathroom__c >();
        List<Bathroom__c > BicListMoveOut = new List<Bathroom__c >();
        Map<id,Bathroom__c > mimoMapBic = new  Map<id,Bathroom__c >();
        Map<String,List<Case>> mapofBathroomandListOfcase = new Map<String,List<Case>>();
        Account ac= [select id from Account where name=: Label.Account_Name limit 1];
        List<Operational_Process__c> opList = [Select id,Field_API_Name__c ,New_Field_Value__c ,Previous_Field_Value__c,Problem__c   from Operational_Process__c where recordtypeId=:operationalRtMImoandService];
        for(Bathroom__c  	 each:newmap.values()){
            if(each.RecordTypeId == moveOutBicRTId){
                if(each.CheckList_Verified__c=='Yes' && oldMap.get(each.id).CheckList_Verified__c=='No'){
                    System.debug('CheckList_Verified__c');
                    SetOfBath.add(each.CheckFor__c);
                    SetOfBic.add(each.id); 
                }
            }
        }
        system.debug('SetOfBath'+SetOfBath);
        system.debug('SetOfBic'+SetOfBic);
        if(!SetOfBath.isEmpty()){
            BicListMimo=[Select id,HouseForMIMO__c,Mirror__c,Bucket_Mug__c,CheckFor__c,Toilet_Brush__c,Tenant_Bathroom_Cleaned__c,Water_Heater__c    from Bathroom__c  where recordtypeid =:mimoBicRTId and  CheckFor__c in:SetOfBath];
        }
        if(!SetOfBic.isEmpty()){
            BicListMoveOut=[Select id,HouseForMIMO__c,Mirror__c,Bucket_Mug__c,CheckFor__c,Toilet_Brush__c,Tenant_Bathroom_Cleaned__c ,Water_Heater__c ,Work_Order__r.caseid,Work_Order__r.Tenant__c from Bathroom__c  where recordtypeid =:moveOutBicRTId and id in:SetOfBic];
        }
        system.debug('BicListMimo'+BicListMimo);
        system.debug('BicListMoveOut'+BicListMoveOut);
        if(!BicListMimo.isEmpty()){
            for(Bathroom__c  each:BicListMimo){
                mimoMapBic.put(each.CheckFor__c,each);
            }
        }
        if(!BicListMoveOut.isEmpty()){
            for(Bathroom__c  each:BicListMoveOut){
                if(each.Tenant_Bathroom_Cleaned__c ==  MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO){
                    System.debug('***Tenant_Bathroom_Cleaned__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
                    c.Sub_Category__c=MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_BATHROOM_CLEANED;
                    c.Status='open';
                    c.Created_Automatically_during_MiMo__c =true;
                    c.Origin='Web';
                    c.AccountId = ac.id;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Tenant_Bathroom_Cleaned__c'&& op.New_Field_Value__c  == MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO){
                            c.Problem__c=op.Problem__c;
                        }
                    }
                    
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Water_Heater__c == MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING  || mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )){
                    System.debug('***Water_Heater__c**');
                    
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_ELECTRICAL;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_WATER_HEATER;
                    c.Status='open';
                    c.Origin='Web';
                    c.Created_Automatically_during_MiMo__c =true;
                    c.AccountId = ac.id;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Water_Heater__c'&& op.New_Field_Value__c  == MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING ){
                            c.Problem__c=op.Problem__c;
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Water_Heater__c ==MissingItemsConstants.WATER_HEATER_NOT_WORKING   )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING  || mimoMapBic.get(each.CheckFor__c).Water_Heater__c==MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING )){
                    System.debug('***Water_Heater__c**');
                    
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_ELECTRICAL;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_WATER_HEATER;
                    c.Status='open';
                    c.Origin='Web';
                    c.AccountId = ac.id;
                    c.Created_Automatically_during_MiMo__c =true;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Water_Heater__c'&& op.New_Field_Value__c  == MissingItemsConstants.WATER_HEATER_NOT_WORKING ){
                            c.Problem__c=op.Problem__c;
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Mirror__c == MissingItemsConstants.MIRROR_NOT_PRESENT  )  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION   || mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION )){
                    System.debug('***Mirror__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_FURNISHING;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_MIRROR;
                    c.Status='open';
                    c.Origin='Web';
                    c.Created_Automatically_during_MiMo__c =true;
                    c.AccountId = ac.id;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Mirror__c'&& op.New_Field_Value__c  == MissingItemsConstants.MIRROR_NOT_PRESENT ){
                            c.Problem__c=op.Problem__c;
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
                if((each.Mirror__c	 ==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION)  && (mimoMapBic.containsKey(each.CheckFor__c))&&(mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION   || mimoMapBic.get(each.CheckFor__c).Mirror__c==MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION )){
                    System.debug('***Mirror__c**');
                    Case c = new Case();
                    c.RecordTypeId = caseServiceRTId;
                    c.Category__c=Constants.CASE_CATEGORY_FURNISHING;
                    c.Sub_Category__c=Constants.CASE_SUB_CATEGORY_MIRROR;
                    c.Status='open';
                    c.Origin='Web';
                    c.Created_Automatically_during_MiMo__c =true;
                    c.AccountId = ac.id;
                    c.ParentId=each.Work_Order__r.caseid;
                    for(Operational_Process__c  op: opList){
                        if(op.Field_API_Name__c =='Mirror__c'&& op.New_Field_Value__c  == MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION ){
                            c.Problem__c=op.Problem__c;
                        }
                    }
                    c.House1__c = each.HouseForMIMO__c;
                    c.MimoBic__c=mimoMapBic.get(each.CheckFor__c).id;
                    
                    c.TypeOfInspection__c='Bic';
                    caseList.add(c);
                }
            }
        }
        //to get id of house of cases we are inserting
        
        if(!caseList.isEmpty()){
            for(Case each:caseList){
                mapofBathroomandListOfcase.put(each.Sub_Category__c,[Select MimoBic__c from Case where  recordtypeid=:caseServiceRTId and Sub_Category__c =:each.Sub_Category__c and status != 'closed']);
            }
        }
        
        if(!mapofBathroomandListOfcase.isEmpty()){
            system.debug('mapofBathroomandListOfcase'+mapofBathroomandListOfcase);
        }
        if(!caseList.isEmpty()){
            for(Case each:caseList){
                Integer count=0;
                if(!mapofBathroomandListOfcase.isEmpty() && (!mapofBathroomandListOfcase.get(each.Sub_Category__c).isEmpty())){
                    for(case eachnew:mapofBathroomandListOfcase.get(each.Sub_Category__c)){
                        if(each.MimoBic__c==eachnew.MimoBic__c){
                            setofCaseTobeUpdated.add(eachnew.id);
                            count++;
                        }
                    }
                    if(count==0){
                        listOfCaseToBeInsert.add(each) ;
                    }
                }
                else{
                    listOfCaseToBeInsert.add(each) ;
                }
            }
        }
        
        
        
        system.debug('listOfCaseToBeInsert'+listOfCaseToBeInsert);
        system.debug('setofCasetobeUpdated'+setofCasetobeUpdated);
        // inserting new case related to house
        if(!listOfCaseToBeInsert.isEmpty()){
            insert listOfCaseToBeInsert;
        }
        system.debug('listOfCaseToBeInsert1'+listOfCaseToBeInsert1);
        
        if(!listOfCaseToBeInsert1.isEmpty()){
            insert listOfCaseToBeInsert1;
        }
        // List of cases need to updated
        if(!setofCasetobeUpdated.isEmpty()){
            ListOfcaseupdate= [Select id,Number_Of_Same_Cases__c from case where id in:setofCasetobeUpdated];
        }
        system.debug('ListOfcaseupdate'+ListOfcaseupdate);
        
        // updating cases
        if(!ListOfcaseupdate.isEmpty()){
            for(case each:ListOfcaseupdate){
                each.Number_Of_Same_Cases__c= each.Number_Of_Same_Cases__c+1;
                listOfCaseToBeUpdate.add(each);
            }  
        }
        if(!listOfCaseToBeUpdate.isEmpty()){
            update listOfCaseToBeUpdate;
        }
        
    }
}