@ISTest
public class BiCTriggerahandlerTest {
      Public Static Id operationalRtMImoandService= Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(MissingItemsConstants.OPERATIONAL_PROCESS_RT_MIMO_SERVICE).getRecordTypeId();
	 Public Static Id moveOutBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();  
    Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    Public Static Id moveInBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
    Public Static Id mimoBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK).getRecordTypeId();    
      public Static TestMethod Void doTest(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
          Operational_Process__c o = new Operational_Process__c();
         o.recordtypeid=operationalRtMImoandService;
           insert o;
          Bathroom__c bc= new Bathroom__c();
          insert bc;
       
          Bathroom__c bc2= new Bathroom__c();
          bc2.RecordTypeId=mimoBicRTId;
          bc2.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING;
          bc2.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION;
          bc2.CheckFor__c= bc.id;
          insert bc2;
          
           Bathroom__c bc1= new Bathroom__c();
          bc1.CheckFor__c=bc.id;
          bc1.RecordTypeId=moveInBicRTId;
          bc1.Tenant_Bathroom_Cleaned__c = MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO;
          bc1.Water_Heater__c=MissingItemsConstants.WATER_HEATER_NOT_WORKING;
          bc1.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION;
          bc1.CheckList_Verified__c='No';
          insert bc1;
          bc1.CheckList_Verified__c = 'Yes';
          update bc1;
          
          
         
          system.debug('bc2'+bc2);
}
    public Static TestMethod Void doTest1(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Operational_Process__c o = new Operational_Process__c();
         o.recordtypeid=operationalRtMImoandService;
           insert o;
          Bathroom__c bc= new Bathroom__c();
          insert bc;
       
          Bathroom__c bc2= new Bathroom__c();
          bc2.RecordTypeId=mimoBicRTId;
          bc2.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING;
          bc2.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION;
          bc2.CheckFor__c= bc.id;
          insert bc2;
          
           Bathroom__c bc1= new Bathroom__c();
          bc1.CheckFor__c=bc.id;
          bc1.RecordTypeId=moveInBicRTId;
          bc1.Tenant_Bathroom_Cleaned__c = MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO;
          bc1.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING;
          bc1.Mirror__c=MissingItemsConstants.MIRROR_NOT_PRESENT;
          bc1.CheckList_Verified__c='No';
          insert bc1;
          bc1.CheckList_Verified__c = 'Yes';
          update bc1;
          
          
         
          system.debug('bc2'+bc2);
}
     public Static TestMethod Void doTest3(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
         Operational_Process__c o = new Operational_Process__c();
         o.recordtypeid=operationalRtMImoandService;
           insert o;
          Bathroom__c bc= new Bathroom__c();
          insert bc;
       
          Bathroom__c bc2= new Bathroom__c();
          bc2.RecordTypeId=mimoBicRTId;
          bc2.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING;
          bc2.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION;
          bc2.CheckFor__c= bc.id;
          insert bc2;
          
           Bathroom__c bc1= new Bathroom__c();
          bc1.CheckFor__c=bc.id;
          bc1.RecordTypeId=moveOutBicRTId;
          bc1.Tenant_Bathroom_Cleaned__c = MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO;
          bc1.Water_Heater__c=MissingItemsConstants.WATER_HEATER_NOT_WORKING;
          bc1.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION;
          bc1.CheckList_Verified__c='No';
          insert bc1;
          bc1.CheckList_Verified__c = 'Yes';
          update bc1;
          
          
         
          system.debug('bc2'+bc2);
}
    public Static TestMethod Void doTest4(){
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name =  Label.Account_Name;
        insert accObj;
        Operational_Process__c o = new Operational_Process__c();
         o.recordtypeid=operationalRtMImoandService;
           insert o;
          Bathroom__c bc= new Bathroom__c();
          insert bc;
       
          Bathroom__c bc2= new Bathroom__c();
          bc2.RecordTypeId=mimoBicRTId;
          bc2.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING;
          bc2.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION;
          bc2.CheckFor__c= bc.id;
          insert bc2;
          
           Bathroom__c bc1= new Bathroom__c();
          bc1.CheckFor__c=bc.id;
          bc1.RecordTypeId=moveOutBicRTId;
          bc1.Tenant_Bathroom_Cleaned__c = MissingItemsConstants.TENANT_BATHROOM_CLEANED_NO;
          bc1.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_NOT_WORKING;
          bc1.Mirror__c=MissingItemsConstants.MIRROR_NOT_PRESENT;
          bc1.CheckList_Verified__c='No';
          insert bc1;
          bc1.CheckList_Verified__c = 'Yes';
          update bc1;
          
          
         
          system.debug('bc2'+bc2);
}
}