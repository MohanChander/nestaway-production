@isTest
public Class SalesOrderTriggerHandlerTest{

    public Static TestMethod void salesOrderTest(){
        Test.StartTest();
          Account vendoracc = new account(name ='TestVendor',Contact_Email__c='ameed@warpdrivetech.in',TIN_No__c='tin12345',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',phone='2757676767') ;
        vendoracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        insert(vendoracc);
         Account owneracc = new account(FirstName='OWNER',LastName='test',Contact_Email__c='owner@warpdrivetech.in',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',phone='2757676767') ;
        owneracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert(owneracc);
         
        Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.Phone = '1234567890';
            insert accObj;
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
        
            insert opp;
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            //cont.PriceBook2Id = priceBk.Id;
            cont.SD_Upfront_Amount__c = 10;
            cont.Maximum_number_of_tenants_allowed__c = '2';
            cont.status = 'Draft';
            insert cont;
         
          City__c c = new City__c();
        c.name='New Delhi';
        insert c;
        //insert dummy house
        house__c h = new house__c(Name='white house', House_Layout__c='1 BHK', House_Owner__c=owneracc.id, City_Master__c = c.id);
        insert h;
          Product2 p1 = new Product2(Name='Prod 1', Family='Individual', Description='Prod 1 Description',Product_Type__c ='Furnishing',productcode='342344');
        insert p1;
        // Create a pricebook entry
Id pricebookId = Test.getStandardPricebookId();

PricebookEntry standardPrice = new PricebookEntry(
    Pricebook2Id = pricebookId, Product2Id = p1.Id,
    UnitPrice = 10000, IsActive = true);
insert standardPrice;

Pricebook2 pb  = new Pricebook2(Name='Custom Pricebook', isActive=true);
insert pb ;

PricebookEntry pbe  = new PricebookEntry(
    Pricebook2Id = pb.Id, Product2Id = p1.Id,
    UnitPrice = 12000, IsActive = true);
insert pbe ;
              order o = new order(accountId=owneracc.id,OpportunityId=opp.id, EffectiveDate = System.today(), Status ='Draft' ,Unit_Price__c =22,house__C=h.id,Pricebook2Id  = pb.ID, Show_Nestaway_Billing_Address__c =true,Type='sg',Category__c='Furnishing',Expected_Arrival_Date__c=system.today());
        o.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        insert o;
        o.Status='Cancelled';
       
            update o;
        Test.StopTest();
    }
}