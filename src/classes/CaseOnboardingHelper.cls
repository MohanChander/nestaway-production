/**********************************************************************************
Added by Baibhav
Purpose: For Onboading case
**********************************************************************************/
public class CaseOnboardingHelper {
/**********************************************************************************
Added by Baibhav
Purpose: For Creation of Onboarding Workorder
         1) Invoke on Creation of Onboading case for Insert of Onboading Workorder
         2) Invoke on Updation of Onboading case for Insert of Onboading Workorder
**********************************************************************************/
    public static void OnboadringWorkorder(List<Case> caselist, Id wrkRecordTypeId,String sub) {
        
        try{
            List<workorder> instWrkList=new List<workorder>();
             List<workorder> woList=new List<workorder>();
            for(Case ca:caselist){
                Workorder wrk=new Workorder();
                wrk.RecordTypeId=wrkRecordTypeId;
                wrk.CaseId=ca.id;
                wrk.Case_Status__c=ca.Status;
                //wrk.StartDate=System.now();
                wrk.Subject=sub+'-'+ca.casenumber;
                wrk.Status=Constants.WORK_STATUS_OPEN;
                wrk.House__c=ca.House__c;
                if(sub=='Make House Live' || sub=='Photography'){
                    wrk.ownerid=ca.ownerid;
                }
                else{
                   
                    woList.add(wrk); 
                }
                instWrkList.add(wrk);

            } 
            if(!instWrkList.isEmpty()){
                if(!woList.isEmpty()){
                         WorkOrderAPMAssignment.assignmentOfOwners(woList);
                     }
                insert instWrkList;
            }
            } Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder');
            }

        
    }
/**********************************************************************************
Added by Baibhav
Purpose: For Dropping of Onboarding Workorder
         1) Invoke on Closeing of Onboading case for Insert of Onboading Workorder
       
**********************************************************************************/
    public static void OnboadringWorkorderDrop(Map<id,Case> caseMap) {
    
    try{   
         List<Workorder> wrkList=WorkOrderSelector.getOnboardinfWorkOrdersBycaseid(caseMap.keySet());
         if(!wrkList.isEmpty()){
                for(workorder wrk:wrkList){
                    wrk.status=Constants.WORKORDER_STATUS_DROPPED;
                }
                update wrkList;
         }
     } Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder drop');
            }
    }
}