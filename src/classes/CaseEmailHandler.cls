/***********************************
Created By : Mohan
Purpose    : Email-to-Case requests are handled in this Class
             Emails with Same subject are threaded under one Case
************************************/
global class CaseEmailHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Id ServiceRequestRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();

        try{
              List<Case> caseList = new List<Case>();
              if(email.Subject != null){
                String subj = email.Subject;
                caseList = [select Id, CaseNumber from Case where Email_Subject__c =: subj Order by CreatedDate asc];

                System.debug('***************Initial Case List: ' + caseList + '\n Size of caseList: ' + caseList.size());

                //Check if the Subject Starts with Re: or Fwd: and remove the Re: and Fwd: part
                if(subj.startsWithIgnoreCase('Re:')){
                  subj = subj.substring(4, subj.length());
                }

                if(subj.startsWithIgnoreCase('Fwd:')){
                  subj = subj.substring(5, subj.length());
                }                

                if(caseList.isEmpty()){
                  caseList = new List<Case>();
                  String queryString = 'Select Id, CaseNumber from Case where RecordType.Name = \'Service Request\' and Email_Subject__c Like \'%' + subj + 
                                       '\' Order by CreatedDate asc';
                  caseList = Database.query(queryString);

                  System.debug('***************Second Case List: ' + caseList + '\n Size of caseList: ' + caseList.size());

                  if(!caseList.isEmpty() && caseList.size() > 1){

                    Set<String> caseNumberSet = new Set<String>();

                    for(Case c: caseList){
                      caseNumberSet.add(c.caseNumber);
                    }

                    //insert Log - when there are more than one Case for a given subject
                    Log__c log = new Log__c();
                    log.Date__c = System.today();
                    log.Message__c = 'There is no exact Match with the subject ' + subj + 
                                      'Email is attached to the case ' + caseList[0].CaseNumber + '\n' +
                                      'Matching Cases -' + caseNumberSet;
                    log.Source__c = 'Email to Case';

                    try{
                      insert log;
                    } catch(Exception e){
                      System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                      UtilityClass.insertGenericErrorLog(e, 'Log Insertion - CaseEmailHandler');      
                    }                       
                  }
                }
              }

              //get the configuration - to which Queue does the Case Go to 
              List<Email_to_Case_Routing__c> routingList = Email_to_Case_Routing__c.getall().values();
              Map<String, Email_to_Case_Routing__c> routingMap = new Map<String, Email_to_Case_Routing__c>();
              Map<String, Boolean> routingQueueMap = new Map<String, Boolean>();
              Set<String> userEmailSet = new Set<String>();

              for(Email_to_Case_Routing__c routing: routingList){
                routingMap.put(routing.Email_Address__c, routing);
                routingQueueMap.put(routing.Email_Address__c, routing.IsQueue__c);
                if(!routing.IsQueue__c){
                  userEmailSet.add(routing.Queue_or_User_Email__c);
                }
              }

              //get Contact and Account
              Account acc = new Account();
              List<Account> accList = [select Id from Account where PersonEmail =: email.fromAddress and 
                                       RecordType.Name =: Constants.ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT];
              if(!accList.isEmpty()){
                acc = accList[0];
              }                             

              Contact con = new Contact();
              if(acc.Id != null){
                List<contact> conList = [select Id from Contact where AccountId =: acc.Id];

                if(!conList.isEmpty()){
                  con = conList[0];
                }
              }

              //generate QueueMap
              Map<String, Id> queueMap = new Map<String, Id>();
              List<Group> queueList = [select Id, Name from Group where Type = 'Queue'];

              for(Group queue: queueList){
                queueMap.put(queue.Name, queue.Id);
              }

              //generate UserMap 
              Map<String, Id> userMap = new Map<String, Id>();
              if(!userEmailSet.isEmpty()){
                List<User> userList = [select Id, email from User where email =: userEmailSet];

                for(User u: userList){
                  userMap.put(u.email, u.Id);
                }
              }

              //Create a new Case if the subject is new Subject
              Case c = new Case();
              if(!caseList.isEmpty()){
                c = caseList[0];
              } else{
                c.Origin = 'Email';
                c.Subject = UtilityClass.limitLength(email.Subject, Case.Subject.getDescribe().getLength());
                c.Email_Subject__c = email.Subject;
                c.Description = email.plainTextBody;
                c.RecordTypeId = ServiceRequestRecordTypeId;

                if(acc != null){
                  c.AccountId = acc.Id;
                }

                if(con != null){
                  c.ContactId = con.Id;
                }

                //Queue Assignment
                Boolean isOwnerAssigned = false;
                if(email.toAddresses != null && email.toAddresses.size() > 0){
                  Set<String> toAddressesSet = new Set<String>(email.toAddresses);
                  for(String emailAddress: toAddressesSet){
                    if(routingMap.containsKey(emailAddress)){
                      if(routingQueueMap.get(emailAddress)){
                        c.OwnerId = queueMap.get(routingMap.get(emailAddress).Queue_or_User_Email__c);
                      } else {
                        c.OwnerId = userMap.get(routingMap.get(emailAddress).Queue_or_User_Email__c);
                      }
                      c.Email_Source__c = routingMap.get(emailAddress).Name;
                      isOwnerAssigned = true;
                    }
                  }                  
                }



                if(email.ccAddresses != null && email.ccAddresses.size() > 0 && !isOwnerAssigned){
                  Set<String> ccAddressesSet = new Set<String>(email.ccAddresses);
                  for(String emailAddress: ccAddressesSet){
                    if(routingMap.containsKey(emailAddress)){
                      if(routingQueueMap.get(emailAddress)){
                        c.OwnerId = queueMap.get(routingMap.get(emailAddress).Queue_or_User_Email__c);
                      } else {
                        c.OwnerId = userMap.get(routingMap.get(emailAddress).Queue_or_User_Email__c);
                      }
                      c.Email_Source__c = routingMap.get(emailAddress).Name;
                    }  
                  }                
                }                  
                insert c;
              }

              EmailMessage theEmail = new EmailMessage();
              theEmail.ParentId = c.Id;
              theEmail.Incoming = true;
              theEmail.Subject = UtilityClass.limitLength(email.Subject, EmailMessage.Subject.getDescribe().getLength());
              theEmail.MessageDate = datetime.now();
              theEmail.HtmlBody = UtilityClass.limitLength(email.htmlBody,EmailMessage.HtmlBody.getDescribe().getLength());   
              theEmail.TextBody = UtilityClass.limitLength(email.plainTextBody,EmailMessage.TextBody.getDescribe().getLength());
              theEmail.FromAddress = email.fromAddress;
              theEmail.FromName = email.fromName;

              if(email.toAddresses != null && email.toAddresses.size() > 0){
                String toAddress;
                for(String str: email.toAddresses){
                  toAddress = str + '; ';
                }
                theEmail.ToAddress = toAddress.substring(0, toAddress.length()-2);
              }

              if(email.ccAddresses != null && email.ccAddresses.size() > 0){
                String ccAddress;
                for(String str: email.ccAddresses){
                  ccAddress = str + '; ';
                }
                theEmail.CcAddress = ccAddress.substring(0, ccAddress.length()-2);
              }              
              insert theEmail;   

              List<Attachment> attachmentList = new List<Attachment>();

              // Save attachments, if any
              if(email.textAttachments != null && email.textAttachments.size() > 0){
                for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                  System.debug('*******************testEmail' + email.textAttachments.size());
                  Attachment attachment = new Attachment();

                  attachment.Name = tAttachment.fileName;
                  attachment.Body = Blob.valueOf(tAttachment.body);
                  attachment.ParentId = theEmail.Id;
                  attachmentList.add(attachment);
                }                
              }

              //Save any Binary Attachment
              if(email.binaryAttachments != null && email.binaryAttachments.size() > 0){
                for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                  System.debug('******************binaryEmail' + email.binaryAttachments.size());
                  Attachment attachment = new Attachment();

                  attachment.Name = bAttachment.fileName;
                  attachment.Body = bAttachment.body;
                  attachment.ParentId = theEmail.Id;
                  attachmentList.add(attachment);
                }   
              } 

              if(!attachmentList.isEmpty()){
                insert attachmentList;
              }    

              result.success = true;  

          } catch(Exception e){
              System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
              UtilityClass.insertGenericErrorLog(e, 'Case Email Handler');   
              result.success = false;   
          }
        
        return result;
    }
}