/*
* Description: It's extention class of FinalAgreementPDF page to generate PDF File.
*/


public class FinalAgreementPdf {
    
    Date myDate = system.today();
    Private Contract pageContract;
    public Boolean initialised{get; set;}
    public id PageOppId{get;set;}
    id contractId;
   // Order orderRec;
    Contract contractTerm;
    public boolean isError{get;set;}
    //Constructor
    public FinalAgreementPdf(ApexPages.StandardController stdController) {
        this.pageContract = (Contract)stdController.getRecord();
        contractId = ApexPages.currentPAge().getParameters().get('id');
        isError = false;
        system.debug('id = ' + contractId );
       // orderRec = [Select id,opportunityId,opportunity.Name,Status from Order where id =: orderId];
        contractTerm = new Contract();
        contractTerm = [Select id,isCloneContract__c,Booking_Type__c, Sales_Order__c, Opportunity__c, Opportunity__r.Name, Agreement_Type__c, Tenancy_Type__c, Status, Approval_Status__c from Contract where Id=: contractId limit 1];
        
        PageOppId = contractTerm.Opportunity__c;
        system.debug('PageOppId = ' + PageOppId);
        initialised=false;
        system.debug('---isErr---'+isError );
        
    }
    
    //method to insert attachement call it on page on click of button 
    public void saveAttachement() {
        system.debug('---isError---'+isError );
        system.debug('==initialised=='+initialised);system.debug('==PageOppId=='+PageOppId); //system.debug('==orderRec.Status=='+orderRec.Status);
        if (!initialised && PageOppId !=NULL && (contractTerm.Status == 'Final Contract' || contractTerm.Status == 'Final Contract Downloaded' || contractTerm.Status=='Final Version') && (contractTerm.Approval_Status__c == 'Approved by Central Team' ||  contractTerm.Approval_Status__c=='Cloned Contract Manually Approved by ZM' || (contractTerm.Approval_Status__c=='Approved by Owner' && contractTerm.isCloneContract__c ))) { system.debug('==inside first If condition=='); System.debug('contractTerm.Agreement_Type__c=='+contractTerm.Agreement_Type__c); system.debug('==contractTerm.Tenancy_Type__c=='+contractTerm.Tenancy_Type__c);
            if(contractTerm.Agreement_Type__c != 'LOI' || true){
                PageReference pdf;
                system.debug('==Booking Type Selected=='+contractTerm.Booking_Type__c);
                String attachmentFileName = null;
                if(contractTerm.Agreement_Type__c == 'LOI'){
                    system.debug('==Generating LOI agreement==');
                    pdf = Page.FinalLetterOfIntent;
                    attachmentFileName = contractTerm.opportunity__r.name + '-LOI' + myDate + '.pdf';
                }else if(contractTerm.Booking_Type__c == 'Full House'){
                    system.debug('==Generating Familiy agreement==');
                    pdf = Page.FinalAgreement;
                    attachmentFileName = contractTerm.opportunity__r.name + '-Family' + myDate + '.pdf';
                }else{
                    system.debug('==Generating Bachelors agreement==');
                    pdf = Page.FinalAgreementBachelors;
                    attachmentFileName = contractTerm.opportunity__r.name + '-Bachelors' + myDate + '.pdf';
                }
                pdf.getParameters().put('id',contractId);
                // create the new attachment
                Attachment attach = new Attachment();
                
                // the contents of the attachment from the pdf
                Blob body;
                
                try {
                    
                    // returns the output of the page as a PDF
                    body = pdf.getContentAsPDF();
                    system.debug('body should be fine');
                    
                    // need to pass unit test -- current bug    
                } catch (VisualforceException e) {
                    system.debug('in the catch block');
                    body = Blob.valueOf('Some Text');
                }
                
                attach.Body = body;
                attach.Name = attachmentFileName;
                attach.IsPrivate = false;
                // attach the pdf to the account
                attach.ParentId = PageOppId;
                insert attach;
                contractTerm.Status = 'Final Contract Downloaded';
                update contractTerm;
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Final Agreement cannot be generated for Agreement Type: LOI '));    
                isError = true;
                 system.debug('---isError2---'+isError );
            }
            //system.debug(attach);
            initialised=true;
        } else if(contractTerm.Approval_Status__c != 'Approved by Central Team'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Final Agreement can only generate after Appproval of Central Team.'));    
            isError = true;
             system.debug('---isError1---'+isError );
        }else system.debug('tried to run twice');
    }
    public PageReference redirectToParentPage(){
        PageReference redirectPage =  new PageReference('/'+PageOppId);
        redirectPage.setRedirect(true);
        return redirectPage;
    }
}