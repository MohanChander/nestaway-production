global class BatchHouseSync implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    public final boolean runHouseCreateJson;
    public final boolean runHouse2CreateJson;
    
    public BatchHouseSync(string query,boolean runHouseCreateJson,boolean runHouse2CreateJson){
        
        this.query=query;
        this.runHouseCreateJson=runHouseCreateJson;
        this.runHouse2CreateJson=runHouse2CreateJson;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> houseList) {
      
        try{
                
                for(House__c house: houseList){
                    
                    if(runHouseCreateJson){
                        
                         HouseJson.createJSONMethod(house.Id);
                    }
                    if(runHouse2CreateJson){
                        
                         House2Json.createJSONMethod(house.Id);
                    }
                   
                }
        
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            UtilityClass.insertGenericErrorLog(e, 'BatchHouseSync'); 
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchHouseSync executed');
    }
}