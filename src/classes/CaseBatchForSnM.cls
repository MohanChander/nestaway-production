global class CaseBatchForSnM implements Database.Batchable<sObject>{

   global final String Query;

   global CaseBatchForSnM(String q){

      Query=q;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Case> caseList){
     for(Case c: caseList){
        c.Labour_Cost__c = c.Service_Cost_Actual__c;      
     }

     StopRecursion.CASE_SWITCH = false;
     update caseList;
     StopRecursion.CASE_SWITCH = true;
    }

   global void finish(Database.BatchableContext BC){
   }
}