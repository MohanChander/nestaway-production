public class UtilityClass {
    
    public static List<wrapper> wrpList = new List<wrapper>();
    Public Boolean valid{get;set;}

    public static map<string,list<user>> GetMoveInExecutivesFromZoneCode(map<id,house__c> HouseAndzoneCodeMap) {
        System.debug('****HouseAndzoneCodeMap '+HouseAndzoneCodeMap+'*** '+HouseAndzoneCodeMap.keyset());
        
        id recordTypeId_ZoneAndOmMapping = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.Zone_and_OM_Mapping_RT_Move_In).getRecordTypeId();
        System.debug('****recordTypeId_ZoneAndOmMapping --  '+recordTypeId_ZoneAndOmMapping);
        list <Zone_and_OM_Mapping__c> UserAndZoneMappingMap = new list <Zone_and_OM_Mapping__c> ();
        set<string> ZoneCodesFromHouses = new set<string>();
        map<string,list<user>> ZoneCodeAndMoveInExecutivesMap = new map<string,list<user>> ();
        List<Zone_and_OM_Mapping__c> apmList = new  List<Zone_and_OM_Mapping__c>();

        for(house__c each :HouseAndzoneCodeMap.values()) {
            if(each.Onboarding_Zone_Code__c != null)
                ZoneCodesFromHouses.add(each.Onboarding_Zone_Code__c);
        }
        
        apmList = [select Id, Property_Manager__r.Zone__r.Zone_code__c, User__c,user__r.name,user__r.email from Zone_and_OM_Mapping__c 
                   where  Property_Manager__r.Zone__r.Zone_code__c  in: ZoneCodesFromHouses and RecordType.Name = 'Assistance Property Manager'];        
        
        for(Zone_and_OM_Mapping__c each : apmList){
            if(ZoneCodeAndMoveInExecutivesMap.containsKey(each.Property_Manager__r.Zone__r.Zone_code__c)){
                  user u = new user(id=each.User__c,email=each.user__r.email);
                    ZoneCodeAndMoveInExecutivesMap.get(each.Property_Manager__r.Zone__r.Zone_code__c).add(u);
            }
                else{
                    list<User> tempbedlist = new list <User> ();
                    user u = new user(id=each.User__c,email=each.user__r.email);
                    tempbedlist.add(u);
                    ZoneCodeAndMoveInExecutivesMap.put(each.Property_Manager__r.Zone__r.Zone_code__c,tempbedlist);
                }
            }
       // System.debug('***ZoneCodeAndAmpMap '+ZoneCodeAndAmpMap);
        return ZoneCodeAndMoveInExecutivesMap;
    }
    
    public static  map<string,list<user>> GetAPMFromZoneCode(ID HouseID) {
          Map<String, Set<Id>> zoneAndAPmMap = new Map<String, Set<Id>>();
       map<string,id> ZoneCodeAndAmpMap = new map<string,id> ();
          map<string,list<user>> ZoneCodeAndMoveInExecutivesMap = new map<string,list<user>> ();
       List<Zone_and_OM_Mapping__c> apmList = new  List<Zone_and_OM_Mapping__c>();

        House__C house = [Select id,Property_Manager__c,PM_Zone_Code__c,Onboarding_Zone_Code__c ,APM__c,city__c  from House__C  where id =: HouseID];
        
        apmList = [select Id, Property_Manager__r.Zone__r.Zone_code__c, User__c,user__r.name,user__r.email from Zone_and_OM_Mapping__c 
                   where  Property_Manager__r.Zone__r.Zone_code__c  =: house.Onboarding_Zone_Code__c and RecordType.Name = 'Assistance Property Manager'];
        
        
        for(Zone_and_OM_Mapping__c each : apmList){
            if(ZoneCodeAndMoveInExecutivesMap.containsKey(each.Property_Manager__r.Zone__r.Zone_code__c)){
                  user u = new user(id=each.User__c,email=each.user__r.email);
                    ZoneCodeAndMoveInExecutivesMap.get(each.Property_Manager__r.Zone__r.Zone_code__c).add(u);
            }
                else{
                    list<User> tempbedlist = new list <User> ();
                    user u = new user(id=each.User__c,email=each.user__r.email);
                    tempbedlist.add(u);
                    ZoneCodeAndMoveInExecutivesMap.put(each.Property_Manager__r.Zone__r.Zone_code__c,tempbedlist);
                }
            }
        System.debug('***ZoneCodeAndAmpMap '+ZoneCodeAndAmpMap);
        return ZoneCodeAndMoveInExecutivesMap;
    }
    

    public static map<string,list<user>> GetMoveOutExecutivesFromZoneCode(map<id,house__c> HouseAndzoneCodeMap) {

        System.debug('****HouseAndzoneCodeMap '+HouseAndzoneCodeMap+'*** '+HouseAndzoneCodeMap.keyset());
        
        id recordTypeId_ZoneAndOmMapping = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.Zone_and_OM_Mapping_RT_Move_Out).getRecordTypeId();
        System.debug('****recordTypeId_ZoneAndOmMapping --  '+recordTypeId_ZoneAndOmMapping);
        list <Zone_and_OM_Mapping__c> UserAndZoneMappingMap = new list <Zone_and_OM_Mapping__c> ();
        set<string> ZoneCodesFromHouses = new set<string>();
        map<string,list<user>> ZoneCodeAndMoveInExecutivesMap = new map<string,list<user>> ();
        List<Zone_and_OM_Mapping__c> apmList = new  List<Zone_and_OM_Mapping__c>();

        for(house__c each :HouseAndzoneCodeMap.values()) {
            if(each.Onboarding_Zone_Code__c != null)
                ZoneCodesFromHouses.add(each.Onboarding_Zone_Code__c);
        }
        
        apmList = [select Id, Property_Manager__r.Zone__r.Zone_code__c, User__c,user__r.name,user__r.email from Zone_and_OM_Mapping__c 
                   where  Property_Manager__r.Zone__r.Zone_code__c  in: ZoneCodesFromHouses and RecordType.Name = 'Assistance Property Manager'];
        
        
        for(Zone_and_OM_Mapping__c each : apmList){
            if(ZoneCodeAndMoveInExecutivesMap.containsKey(each.Property_Manager__r.Zone__r.Zone_code__c)){
                  user u = new user(id=each.User__c,email=each.user__r.email);
                    ZoneCodeAndMoveInExecutivesMap.get(each.Property_Manager__r.Zone__r.Zone_code__c).add(u);
            }
                else{
                    list<User> tempbedlist = new list <User> ();
                    user u = new user(id=each.User__c,email=each.user__r.email);
                    tempbedlist.add(u);
                    ZoneCodeAndMoveInExecutivesMap.put(each.Property_Manager__r.Zone__r.Zone_code__c,tempbedlist);
                }
            }
       // System.debug('***ZoneCodeAndAmpMap '+ZoneCodeAndAmpMap);
        return ZoneCodeAndMoveInExecutivesMap;
    }

    
    public static String SendEmailToUser(id userId,String houseName) {
        String msgReturned = '';
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>();
            list<User> usrRec = [Select id,Email,Username  from user where id =: userId];
            if(usrRec[0].email != null){
                sendTo.add(usrRec[0].email);
                mail.setToAddresses(sendTo);
                mail.setSenderDisplayName('Nestaway');
                mail.setSubject('House Location discrepancy,');
                String body = 'Dear ' + usrRec[0].Username  + ', \n ';
                body += 'There is a discrepancy in the House Lat Lon captured previously and what is captured now. \n' ;
                body += 'House name - '+houseName;
                mail.setHtmlBody(body);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                msgReturned = 'Email sent to'+usrRec[0].email;
             }
            else{
                msgReturned = 'No email id specified for zom ';
            }
            return msgReturned;
        }
        catch(exception e){
            
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
            msgReturned = e.getMessage();
            return msgReturned;
        }
        
        
        
    }
    
    public static map<id,Boolean> ValidateAllPhotosPresentForHouse(id houseId){
    
        map<id,boolean> houseHasAllPhotosMap = new map <id,boolean> ();
        set <id> roomTermsOfHouseId = new set <id>();
        try{
        list <house__C> h = [Select id, House_Layout__c from house__c where id =: houseId];
        list <Room_Terms__c> roomTermList = [Select id,name,house__C,(select id,Image_Tag__c,Is_Visible__c,House__c, Room_Term__c from Images__r where Is_Visible__c =: True) from Room_Terms__c where house__C = : houseId ];
        list <bathroom__c> bathroomsWithRoom = [Select id,name,room__c from bathroom__c where house__c =: houseId AND Room__c != null];
        list <Photo__c> photosForHouse = [select id,Image_Tag__c,Is_Visible__c,House__c, Room_Term__c from Photo__c where Image_For__c = : 'House' AND House__c = : houseId AND Is_Visible__c =: True];
        System.debug('*******roomTermList '+roomTermList);
        System.debug('*******bathroomsWithRoom '+bathroomsWithRoom);
        Boolean checkRoomValid = false;
        Boolean checkHousePicsValid = false;
        Boolean checkHouseIsValid = false;
        
        map<id,bathroom__c> roomAndBathroomMap = new map <id,bathroom__c> ();
        for(bathroom__c bthrm : bathroomsWithRoom ) {
           if(bthrm.room__c != null ) 
               roomAndBathroomMap.put(bthrm.room__c,bthrm);
        }
        System.debug('*******roomAndBathroomMap '+roomAndBathroomMap);
        
        if(photosForHouse.size() < 2){
            System.debug('***No of photos is less '+photosForHouse.size());
            checkHousePicsValid = false;
            checkHouseIsValid = false;
            houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
            return houseHasAllPhotosMap;
        }
        else {
            Integer noOfPhotosForRoom_livingRoom = 0;
            Integer noOfPhotosForRoom_diningRoom = 0;
            Integer noOfPhotosForRoom_Kitchen = 0;
            Integer noOfPhotosForRoom_Balcony = 0;
            Integer noOfPhotosForRoom_others = 0;
            for(Photo__c ph : photosForHouse){
                if(ph.Image_Tag__c == 'Living') {
                    noOfPhotosForRoom_livingRoom ++;
                } else if(ph.Image_Tag__c == 'Dining') {
                            noOfPhotosForRoom_diningRoom++;
                       } else if(ph.Image_Tag__c == 'Kitchen') {
                                  noOfPhotosForRoom_Kitchen++;
                              } else if(ph.Image_Tag__c == 'Balcony') {
                                         noOfPhotosForRoom_Balcony++ ;  
                                     } else if(ph.Image_Tag__c == 'Balcony') {
                                              noOfPhotosForRoom_others++;
                                            }
                
            } 
            if(h[0].House_Layout__c != null) {
              
              if(h[0].House_Layout__c.contains('BHK')) {

                  if(noOfPhotosForRoom_livingRoom > 0 && noOfPhotosForRoom_Kitchen >0){
                    System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                    System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
    
                    checkHousePicsValid = true;
                  } else {
                
                     System.debug('@@@@ ELSE ---- ');
                     System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                     System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
    
                     checkHouseIsValid = false;
                     houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
                     return houseHasAllPhotosMap;
                    } 

              }
              else {

                    if(noOfPhotosForRoom_livingRoom >= 0 && noOfPhotosForRoom_Kitchen >= 0){
                      System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                      System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
      
                      checkHousePicsValid = true;
                    } else {
                
                       System.debug('@@@@ ELSE ---- ');
                       System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                       System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
      
                       checkHouseIsValid = false;
                       houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
                       return houseHasAllPhotosMap;
                    } 


              }
                
             } else {
                 /* house does not have  a layout */
                 if(noOfPhotosForRoom_livingRoom > 0 && noOfPhotosForRoom_Kitchen >0 ){
                    System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                    System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
    
                    checkHousePicsValid = true;
                } else {
                
                     System.debug('@@@@ ELSE ---- ');
                     System.debug('@@@@ living'+noOfPhotosForRoom_livingRoom+'@@@@ dining'+ noOfPhotosForRoom_diningRoom);
                     System.debug('@@@@ noOfPhotosForRoom_Kitchen'+noOfPhotosForRoom_Kitchen+'@@@@ Balcony'+ noOfPhotosForRoom_Balcony);
    
                     checkHouseIsValid = false;
                     houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
                     return houseHasAllPhotosMap;
                 } 
             
             }
        }
        
        
        System.debug('******* roomTermList.size() '+roomTermList.size() );
        if(roomTermList.size() > 0) {
            Integer noOfPhotosForRoom = 0;
            
            for(Room_Terms__c r : roomTermList){
                System.debug('******* r.Images__r.size() '+r.Images__r.size());
                if( r.Images__r.size() > 0 ) {
                    Boolean alreadyInBedroom = false;
                    Boolean alreadyInBathroom = false;
                    SYSTEM.DEBUG('****No of photos for room '+ r.Images__r.size());
                    for(photo__c ph : r.Images__r) {
                        System.debug('***'+ph.Image_Tag__c + ph.id);
                        
                        if(roomAndBathroomMap.containsKey(r.id)) {
                            if(ph.Image_Tag__c == 'Bathroom' && alreadyInBathroom == false ){
                                noOfPhotosForRoom++;
                                alreadyInBathroom = true;
                                System.debug('****bathroom');
                            }
                        }
                        else{
                            noOfPhotosForRoom++;
                            alreadyInBathroom = true;
                            System.debug('****bathroom in else');
                        }
                        
                        if(ph.Image_Tag__c == 'Bedroom' && alreadyInBedroom == false) {
                            noOfPhotosForRoom++;
                            alreadyInBedroom = true;
                            System.debug('****Balcony');
                        }
                    }
                    alreadyInBedroom = false;
                    alreadyInBathroom = false;
                } else {
                checkRoomValid = false;
                System.debug('****No Image for a room '+r.id);
                 checkHouseIsValid = false;
                 houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
                 return houseHasAllPhotosMap;
            }
        }
        System.debug('****noOfPhotosForRoom '+noOfPhotosForRoom);
        if(noOfPhotosForRoom >= (2 * (roomTermList.size())) ) {
            checkRoomValid = true;
            System.debug('**** AAA');
        } else {
             checkRoomValid = false;
             System.debug('**** BBB');
             checkHouseIsValid = false;
                 houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
                 return houseHasAllPhotosMap;
        }
     } else {
         checkRoomValid = true;
         System.debug('No rrom terms with house');
     }
     
     if(checkRoomValid == true && checkHousePicsValid == true) {
         
         checkHouseIsValid = true;
         houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
         System.debug('If last'+houseHasAllPhotosMap);
         return houseHasAllPhotosMap;
     } else {
     
         checkHouseIsValid = false;
         houseHasAllPhotosMap.put(houseId,checkHouseIsValid) ;
         System.debug('else last'+houseHasAllPhotosMap);
         return houseHasAllPhotosMap;
     
     }
     }
     catch(exception e){
         System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
         UtilityClass.insertGenericErrorLog(e);
         return houseHasAllPhotosMap;
     }
        
    }
    
    public static map<id,user> getSubordinateUsers(String userIds){
        id userId = id.valueOf(userIds);
        Map<Id,User> users = new Map<Id, User>();
        // get requested user's role
        user zonal_manager = UtilityServiceClass.getUserRoleDetails(userid);
        id role_Id = zonal_manager.UserRoleId;
        String role_name = zonal_manager.UserRole.name ;
        System.debug('**** '+role_Id+' -- '+role_name );
        
        // get all of the roles underneath the user
        Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{role_Id});
        users = UtilityServiceClass.getUsers(allSubRoleIds);
        
        //String user_Role = [Select]
        return users;
    }
    
    private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        
        Set<ID> currentRoleIds = new Set<ID>();
        // get all of the roles underneath the passed roles
        list<UserRole> user_role = UtilityServiceClass.getRoleIdsFromParentRoleIds(roleIds);
        for(UserRole userRole : user_role ){
            currentRoleIds.add(userRole.Id);
        }
        // go fetch some more roles
        
        /*if(currentRoleIds.size() > 0)
currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));*/
        
        return currentRoleIds;
        
    }
    /*Function to validate */
    public static decimal validateWithinRadius(decimal strt_lat,decimal strt_lon,decimal end_lat,decimal end_lon,decimal radius_toCheck){
        // convert to radians
        boolean return_status = false;
        Double dDistance  = 1000;
        if(strt_lat!=null && strt_lon != null && end_lat!=null && end_lon != null && radius_toCheck !=null)
        {
            Double dDepartLat = strt_lat * 3.14159 / 180;
            Double dDepartLong = strt_lon * 3.14159 / 180;
            Double dArrivalLat = end_lat * 3.14159 / 180;
            Double dArrivalLong = end_lon * 3.14159 / 180;
            
            Double dDeltaLong = dArrivalLong - dDepartLong;
            Double dDeltaLat = dArrivalLat - dDepartLat;
            
            // calculate angle using the haversine formula
            Double dHaversineResult = Math.Sin( dDeltaLat / 2 ) * Math.Sin( dDeltaLat / 2 ) 
                + Math.Cos( dDepartLat ) * Math.Cos( dArrivalLat ) 
                * Math.Sin( dDeltaLong / 2 ) * Math.Sin( dDeltaLong / 2 );
            
            // calculate distance by multiplying arc-tangent by the planet radius in miles
            dDistance = 3958.76 * 2 * Math.Atan2( Math.Sqrt( dHaversineResult ), Math.Sqrt( 1 - dHaversineResult ) );
            
            System.debug('***Distance in nautical miles ->  '+dDistance);
            System.debug('***Distance in meters ->  '+dDistance*1.60934*1000);
            return dDistance*1.60934*1000;
            /*if(dDistance*1.60934 < = radius_toCheck){
return_status = true;
}
else{
return_status = false;
}*/
        }
        return dDistance;
    }
    
    //Function to make a GET request
    @future (callout=true)
    public static void getZoneCode(String end_Pt) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        req.setEndpoint('end_Pt');
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setCompressed(true); // otherwise we hit a limit of 32000
        
        try {
            res = http.send(req);
            if(res != null){
                String str = res.getBody();
                if(res.getStatusCode()==200){
                    System.debug(res);
                    System.debug('***List  str'+str);
                    System.debug('\n'+str.split('}')[0]);
                    wrpList = (List<Wrapper>)JSON.deserialize(str,List<wrapper>.class);
                    System.debug('wrapper list= '+wrpList);
                    System.debug('Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);
                    
                }
                
            }
        } 
        catch(System.CalloutException e) {
            System.debug('***EXCEPTION IN Callout : '+ e);
            System.debug('***Error' +res.toString());
            
        }
        
    }
    public static map<id,id> getUserFromZone(String ZoneCode,String Zone_Name){
        map<id,id> zoneAndUser_map = new map <id,id>();
        zoneAndUser_map = UtilityServiceClass.getUserFromZone(ZoneCode) ;
        return zoneAndUser_map;
    }

    /*  Created By : Mohan
        Purpose    : get set of City for City Name Validation */
    public static Set<String> getCitySet(){

        Set<String> citySet = new Set<String>();

        for(City__c city: CitySelector.getCityDetails()){
            // chandu:- adding city name in uppercase.
          if(city.Name!=null){  
            citySet.add(city.Name.toUpperCase());
          }
        }

        return citySet;
    }


    /*  Created By: Mohan - WarpDrive Tech Works
        Purpose   : To capture all the generic exceptions in the Log Object */
    public static void insertGenericErrorLog(Exception e){
        Log__c log = new Log__c();
        log.date__c=date.today();
        log.Source__c='Other';
        String msg = 'Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString();
        log.message__c = msg;
        insert log;        
    }

    /*  Created By: Mohan - WarpDrive Tech Works
        Purpose   : To capture all the exceptions with category in the Log Object */
    public static void insertGenericErrorLog(Exception e, String category){
        Log__c log = new Log__c();
        log.date__c=date.today();
        log.Tag__c = category;
        String msg = 'Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString();
        log.message__c = msg;
        insert log;        
    }  

    /*  Created By: Mohan - WarpDrive Tech Works
        Purpose   : To capture all the exceptions with Error records */
    public static void insertGenericErrorLog(Exception e, String category, String errorRecords){
        Log__c log = new Log__c();
        log.date__c=date.today();
        log.Tag__c = category;
        String msg = 'Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString();
        log.message__c = msg;
        log.Error_Records__c = errorRecords;
        insert log;        
    }      

    /*  Created By: Mohan 
        Purpose   : Restrict delete access to records - Only System Admins are permitted to Delete the records */
    public static void restrictDeleteAccess(List<sObject> sObjectList){

    }
    
    // Method to insert String in Error Log
    public static void insertStringInLog(String stringToLog) {
        Log__c log = new Log__c();
        log.date__c=date.today();
        log.Source__c = 'Fetching Zone Users';
        log.message__c = stringToLog;
        insert log;
    }
    
     // Method to insert log in the system.
 
 public static void insertErrorLog(string source,string message){
 
     Log__c log = new Log__c();
     log.date__c=date.today();
     log.Source__c=source;
     log.message__c=message;
     insert log;
 }   
    
    public class wrapper{
        public String area_type{get;set;}
        public String area_id{get;set;}
        public String area_name{get;set;}
        public String parent_id{get;set;}
        public String serviceable{get;set;}
    }

/********************************************
Created By : Mohan
Purpose    : Limits the length of the String to the given length if the length is greater than the permit
*********************************************/
    public static String limitLength(String str, Integer permittedLength){

        String returnString;

        if(str != null && str.length() > permittedLength){
            returnString = str.subString(0, permittedLength);
        } else if(str != null && str.length() < permittedLength){
            returnString = str;
        } else if (str == null){
            returnString = '';
        }

        return returnString;
    }    
    
    // 
  /*  public static map<string,Id> GetAPMFromZoneCodeForMovein(List<House__c> houseList) {
        System.debug('****houseList '+houseList);
        map<string,id> ZoneCodeAndAmpMap = new map<string,id> ();
        Map<String,Id> cityToHeadId = new Map<String,Id>();
        List<House__c> hosList = new List<House__c>();
        List<House__c> houseListTobeupdatedPM = new List<House__c>();
        List<House__c> houseListTobeupdatedAPM = new List<House__c>();
        Set<ID> Setofhouse = new Set<Id>();
        List<City__c> cityList= [ Select id,name, Operation_Head__c from City__c where Operation_Head__c!=null];
        for(City__c c: cityList){
            cityToHeadId.put(c.name,c.Operation_Head__c);
        }
        for(House__C each:houseList){
            Setofhouse.add(each.id);
        }
        hosList = [Select id,Property_Manager__c,PM_Zone_Code__c,Onboarding_Zone_Code__c ,APM__c,city__c  from House__C  where id =: Setofhouse];
        for(House__c house: hosList){
            if(House.APM__c != null){
                ZoneCodeAndAmpMap.put(house.Onboarding_Zone_Code__c,House.APM__c );
            }
            else{
                if(house.APM__c == null && house.Property_Manager__c != null && house.Onboarding_Zone_Code__c != null){
                    houseListTobeupdatedAPM.add(house);
                }
                if(house.APM__c == null && house.Property_Manager__c == null && house.Onboarding_Zone_Code__c != null){
                    houseListTobeupdatedPM.add(house);
                }
            }
        }
        if(!houseListTobeupdatedAPM.isEmpty()){
            HouseTriggerHelper.populateAPmOnHouse(houseListTobeupdatedAPM,true);
            
        }
        if(!houseListTobeupdatedPM.isEmpty()){
            HouseTriggerHelper.populateAPmOnHouse(houseListTobeupdatedPM,true);
            
        }
        
        for(House__c house: hosList){
            if(House.APM__c != null){
                ZoneCodeAndAmpMap.put(House.Onboarding_Zone_Code__c,House.APM__c );
            }
            else{
                if(House.Property_Manager__c != null){
                    ZoneCodeAndAmpMap.put(House.Onboarding_Zone_Code__c,House.Property_Manager__c );
                }
                else{
                    if(cityToHeadId.containsKey(House.city__c))
                        ZoneCodeAndAmpMap.put(House.Onboarding_Zone_Code__c,cityToHeadId.get(House.city__c) );
                }
            }
        }
        
        System.debug('***ZoneCodeAndAmpMap '+ZoneCodeAndAmpMap);
        return ZoneCodeAndAmpMap;
    }*/
    // Added by deepak
    // to Return the mail id of sdoc license user
    public static String getEmailIDofSDocUser(){
        String sdocUserEmailId;
         Org_Param__c param = Org_Param__c.getInstance();
            if(param!=null && param.Sdoc_Default_License_User__c !=null){
                sdocUserEmailId=param.Sdoc_Default_License_User__c;
            }
        return sdocUserEmailId;
    }
    
}