global without sharing class GernerateSocsClass {
    public GernerateSocsClass(ApexPages.StandardController controller) {
    }
    public  pageReference generateSdoc(){
        String SDOCNestawayFamilyAgreementId;
        String SDOCNestawayTenantAgreementId;
        String SDOCNestawayFamilyAgreementIdpdf;
        String SDOCNestawayTenantAgreementIdPdf;
        String family;
        String Tenant;
        String sdocUserEmailId;
        String sourceId= ApexPages.currentPage().getParameters().get('sourceId');
        Contract cont=[Select id,Booking_Type__c, Agreement_Generation_Date__c  from contract where id=:sourceId];
        cont.Agreement_Generation_Date__c = System.now();
        update cont;
        Org_Param__c param = Org_Param__c.getInstance();
        if(param!=null && param.Sdoc_Default_License_User__c !=null){
            
            sdocUserEmailId=param.Sdoc_Default_License_User__c;
        }
        system.debug('sdocUserEmailId'+sdocUserEmailId);
        String currentUserId=userinfo.getuserid();
        user u= [select name,username from user where id =:currentUserId];
        String IdOfcont =cont.id;
        SDOC__SDTemplate__c  SDOCNestawayFamilyAgreement=[Select id from SDOC__SDTemplate__c  where name='Nestaway Family Agreement'];
        if(SDOCNestawayFamilyAgreement.Id != null){
            SDOCNestawayFamilyAgreementId = SDOCNestawayFamilyAgreement.Id;
        }
        SDOC__SDTemplate__c  SDOCNestawayTenantAgreement=[Select id from SDOC__SDTemplate__c  where name='Nestaway Tenant Agreement'];
        if(SDOCNestawayTenantAgreement.Id != null){
            SDOCNestawayTenantAgreementId = SDOCNestawayTenantAgreement.Id;
        }
        SDOC__SDTemplate__c  SDOCNestawayFamilyAgreementpdf=[Select id from SDOC__SDTemplate__c  where name='Nestaway Family Agreement Pdf'];
        if(SDOCNestawayFamilyAgreement.Id != null){
            SDOCNestawayFamilyAgreementIdpdf = SDOCNestawayFamilyAgreementpdf.Id;
        }
        SDOC__SDTemplate__c  SDOCNestawayTenantAgreementpdf=[Select id from SDOC__SDTemplate__c  where name='Nestaway Tenant Agreement Pdf'];
        if(SDOCNestawayFamilyAgreement.Id != null){
            SDOCNestawayTenantAgreementIdPdf = SDOCNestawayTenantAgreementpdf.Id;
        }
        if(SDOCNestawayFamilyAgreementId != null &&  SDOCNestawayFamilyAgreementIdpdf != null){
            family = SDOCNestawayFamilyAgreementId+','+SDOCNestawayFamilyAgreementIdpdf;
        }
        if(SDOCNestawayTenantAgreementId != null && SDOCNestawayTenantAgreementIdPdf != null){
            Tenant = SDOCNestawayTenantAgreementId+','+SDOCNestawayTenantAgreementIdPdf;
        }
        
        if(cont.Booking_Type__c =='Full House'){
            CustomSdocAutomation.createSdocJobRecord(IdOfcont,'Contract',SDOCNestawayFamilyAgreementId,'0',true,sdocUserEmailId);
            CustomSdocAutomation.createSdocJobRecord(IdOfcont,'Contract',SDOCNestawayFamilyAgreementIdpdf,'0',true,sdocUserEmailId);
        }
        else{
            CustomSdocAutomation.createSdocJobRecord(IdOfcont,'Contract',SDOCNestawayTenantAgreementId,'0',true,sdocUserEmailId);
            CustomSdocAutomation.createSdocJobRecord(IdOfcont,'Contract',SDOCNestawayTenantAgreementIdPdf,'0',true,sdocUserEmailId);

            
        }
        pageReference ref = new PageReference('/'+cont.id); 
        ref.setRedirect(true); 
        return ref; 
    }
}