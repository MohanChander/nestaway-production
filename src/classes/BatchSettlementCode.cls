global class BatchSettlementCode implements Database.Batchable<sObject>{
    
    public final string query;
  
    public BatchSettlementCode (string query){
        
        this.query=query;
          
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query ='select id from case where id IN()'; */
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Case> caseLst) {
     
       string sourceIdCase;
        for(Case cas: caseLst){
        try{
             boolean updateSetlementRecord=true;
              sourceIdCase=cas.id;      
            /*Code added by Ameed from DEV*/
            Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
            Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
            /*Code added by Ameed from DEV*/
            Id caseSettlement_internal_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SETTLEMENT_INTERNAL_TRANSFER).getRecordTypeId();

            Id caseSettlement_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();
            
            List<Group> InternalTrasnfer = [Select id,name from Group where name =: Constants.QUEUE_NAME_INTERNAL_FINANCE  AND Type= 'Queue'];
            List<Group> DefaultSubcaseQueue = [Select id,name from Group where name =: Constants.QUEUE_NAME_FINANCE  AND Type= 'Queue'];

            System.debug('***** DefaultSubcaseQueue '+DefaultSubcaseQueue);

            List<Group> lstQueueRecords = new list<group>();
                lstQueueRecords = [Select id,name from Group where (name =: Constants.QUEUE_NAME_RENTDEFAULT_FINANCE OR name =: Constants.QUEUE_NAME_CANCELLATION_FINANCE OR
                                            name =: Constants.QUEUE_NAME_SD_DEFAULT_FINANCE OR name =: Constants.QUEUE_NAME_OwnerRejected_FINANCE OR
                                            name =: Constants.QUEUE_NAME_CANCELLATIONONTRIAL_FINANCE OR name =: Constants.QUEUE_NAME_MOVEOUT_FINANCE )
                                            AND Type= 'Queue'];
            id rentDefault_queue;
            id cancelation_queeue;
            id sd_default_queue;
            id cancelationOntrial_queue;
            id moveoutFinance_Queue;
            id ownerrejected_Queue;

            for(group each : lstQueueRecords )  {
                if(each.name == Constants.QUEUE_NAME_RENTDEFAULT_FINANCE ) {
                    rentDefault_queue = each.id;
                }
                else if(each.name == Constants.QUEUE_NAME_CANCELLATION_FINANCE) {
                    cancelation_queeue = each.id;
                }
                else if(each.name == Constants.QUEUE_NAME_SD_DEFAULT_FINANCE ) {
                    sd_default_queue = each.id ;
                }
                else if(each.name == Constants.QUEUE_NAME_CANCELLATIONONTRIAL_FINANCE ) {
                    cancelationOntrial_queue = each.id;
                }
                else if(each.name == Constants.QUEUE_NAME_MOVEOUT_FINANCE ) {
                    moveoutFinance_Queue = each.id;
                }
                else if(each.name == Constants.QUEUE_NAME_OwnerRejected_FINANCE) {
                    ownerrejected_Queue = each.id;
                }
            }                              

            System.debug('****** lstQueueRecords  '+lstQueueRecords);                                


            String sourceId= sourceIdCase;
            
            List<case> csLst=[select id from case where parentId=:sourceId and (recordtypeid=:caseSettlement_internal_RecordTypeId or recordtypeid=:caseSettlement_RecordTypeId)];
            
            case caserecord=[Select  Temp_Settlement_Error_Msg__c,Temp_Field_Settlement_Done__c,Temp_Field_Settlement_Updated__c,id,Initiate_Settlement__c,ContactId,Tenant__c,
                             AccountId,House__c,Booked_Object_ID__c,type,Booked_Object_Type__c,
                             Move_Out_Type__c,Move_Out_Date__c,Status,Deduction_Reason__c,
                             Net_Amount_to_Be_Refunded__c,Description,Origin,Bed_Released_API_Status__c,
                             Transaction_ID__c,SD_Amount__c,SD_Status__c,parentId,Parent_Case_Type__c,
                             Settle_Amount_To_Be_deducted__c,Settlement_Amount_Calculated_Via_System__c,
                             Bed_Released_API_Message__c,Priority,House_Id__c
                            from case where id =:sourceId];
            
            system.debug('***caserecord'+caserecord);
            list <Bank_Detail__c> tenatbankdetailslist = new list <Bank_Detail__c> ();

            if(caserecord != null  && caserecord.Tenant__c != null) {
                tenatbankdetailslist = [Select id,Account_Number__c, Bank_Name__c, Branch_Name__c,
                                         IFSC_Code__c, Name from Bank_Detail__c 
                                        where Related_Account__c =: caserecord.Tenant__c Order By createdDate Asc] ;

                system.debug('***tenatbankdetailslist'+tenatbankdetailslist);                           
            }
                             

            /*previous code (16 aug,2017)
    * if(caserecord.status == Constants.CASE_STATUS_MOVEDOUTALLOK){
    */
            if(csLst!=null && csLst.size()>0){
                system.debug('****coming inside');
                caserecord.Temp_Settlement_Error_Msg__c='Move Out Settlement case has been already created.';
               // return null;
            }
            else if(caserecord.status == Constants.CASE_STATUS_MOVEDOUT_MOVEOUTSCHEDULED  || caserecord.status == Constants.CASE_STATUS_NEW  || caserecord.status ==Constants.CASE_STATUS_MOVE_OUT_SCHEDULED ){
                 system.debug('****coming inside');              
                caserecord.Temp_Settlement_Error_Msg__c=' Settlement case can not be created.';
               // return null;
            }
            else if(caserecord.status == Constants.CASE_STATUS_MOVEDOUTWITHISSUE || caserecord.status=='Refund Requested') {
                if(caserecord.Settle_Amount_To_Be_deducted__c == null ){
                    system.debug('****coming inside');      
                   caserecord.Temp_Settlement_Error_Msg__c='Deducted Amount is needed for settlement case.';
               //     return null;
                }
                else if(caserecord.Settle_Amount_To_Be_deducted__c != null  && (caserecord.Bed_Released_API_Message__c == null  || caserecord.Bed_Released_API_Status__c == null || !caserecord.Bed_Released_API_Status__c.contains('True')) ){
                    system.debug('****coming inside');      
                    caserecord.Temp_Settlement_Error_Msg__c='Settlement case can not be created.Bed is not yet released.';
                  //  return null;
                }
              
                /*Code added by Ameed from DEV*/
                else {
                    if(caserecord.Settle_Amount_To_Be_deducted__c != null && caserecord.status == Constants.CASE_STATUS_MOVEDOUTWITHISSUE && caserecord.parentId ==null && caserecord.Parent_Case_Type__c !='Internal Transfer' && caserecord.Bed_Released_API_Message__c !=null && caserecord.Bed_Released_API_Status__c !=null && caserecord.Bed_Released_API_Status__c.contains('True')) {
                        updateSetlementRecord=false;

                        case subcase = new Case();
                        subcase.OwnerId=DefaultSubcaseQueue[0].id;
                        
                        if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_TENANT_MOVEOUT && moveoutFinance_Queue != null ) {
                            subcase.OwnerId = moveoutFinance_Queue;
                        }
                        else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_Cancellation && cancelation_queeue != null ) {
                            subcase.OwnerId = cancelation_queeue;
                        }
                        else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_CANCELLTIONONTRIAL && cancelationOntrial_queue != null ) {
                            subcase.OwnerId = cancelationOntrial_queue;
                        }
                        else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_RENTDEFAULT && rentDefault_queue != null ) {
                            subcase.OwnerId = rentDefault_queue;
                        }
                        else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_OWNERREJECTED && ownerrejected_Queue != null ) {
                            subcase.OwnerId = ownerrejected_Queue;
                        }
                        else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUT_SDDEFAULT && sd_default_queue != null ) {
                            subcase.OwnerId = sd_default_queue;
                        }

                        /*round robin logic for assignment to run*/

                        /*populate tenant bank details on sdub case */
                        if(tenatbankdetailslist != null && tenatbankdetailslist.size() > 0) {
                            if(tenatbankdetailslist[0].Account_Number__c != null )
                                subcase.Tenant_bank_A_c_No__c = tenatbankdetailslist[0].Account_Number__c;
                            if(tenatbankdetailslist[0].Branch_Name__c != null )
                                subcase.Tenant_Bank_Branch_Name__c = tenatbankdetailslist[0].Branch_Name__c;
                            if(tenatbankdetailslist[0].IFSC_Code__c != null )
                                subcase.Tenant_Bank_IFSC_Code__c = tenatbankdetailslist[0].IFSC_Code__c;
                            if(tenatbankdetailslist[0].Bank_Name__c != null )
                                subcase.Tenant_Bank_Name__c = tenatbankdetailslist[0].Bank_Name__c;
                            if(tenatbankdetailslist[0].name != null)
                                subcase.A_c_Holder_Name_Tenant__c = tenatbankdetailslist[0].name ;
                        }
                        
                        subcase.recordtypeid=caseSettlement_RecordTypeId;
                        subcase.ContactId=caserecord.ContactId;
                        subcase.Status='New';
                        subcase.Subject='Move Out Settlement';
                        subcase.Origin=caserecord.Origin;
                        if(caserecord.Settle_Amount_To_Be_deducted__c != null)
                            subcase.Settle_Amount_To_Be_deducted__c=caserecord.Settle_Amount_To_Be_deducted__c;
                        if(caserecord.Settlement_Amount_Calculated_Via_System__c != null)
                            subcase.Settlement_Amount_Calculated_Via_System__c=caserecord.Settlement_Amount_Calculated_Via_System__c;
                        subcase.Priority=caserecord.Priority;
                        subcase.Transaction_ID__c=caserecord.Transaction_ID__c;
                        subcase.Description=caserecord.Description;

                        if(caserecord.Settlement_Amount_Calculated_Via_System__c != null && caserecord.Settle_Amount_To_Be_deducted__c != null)
                            subcase.Net_Amount_to_Be_Refunded__c=caserecord.Settlement_Amount_Calculated_Via_System__c +caserecord.Settle_Amount_To_Be_deducted__c;
                        if(caserecord.House_Id__c != null)
                            subcase.MoveOut_House_ID__c = caserecord.House_Id__c;
                        
                        subcase.Deduction_Reason__c=caserecord.Deduction_Reason__c;
                        subcase.Type='MoveOut Settlement';
                        subcase.Parentid=caserecord.id;
                        subcase.Move_Out_Type__c=caserecord.Move_Out_Type__c;
                        subcase.Move_Out_Date__c=caserecord.Move_Out_Date__c;
                        subcase.Booked_Object_Type__c = caserecord.Booked_Object_Type__c ;
                        subcase.House__c = caserecord.House__c;
                        subcase.AccountId = caserecord.AccountId ;
                        
                        insert subcase;
                        if(caserecord.Settle_Amount_To_Be_deducted__c != null && caserecord.Settlement_Amount_Calculated_Via_System__c != null )
                            caserecord.Net_Amount_to_Be_Refunded__c =caserecord.Settlement_Amount_Calculated_Via_System__c + caserecord.Settle_Amount_To_Be_deducted__c;
                        caserecord.Status= Constants.CASE_STATUS_MOVEDOUT_REFUNDREQUESTED ;
                        caserecord.Initiate_Settlement__c='No';
                        caserecord.Temp_Field_Settlement_Updated__c=system.now();
                        caserecord.Temp_Field_Settlement_Done__c=true;
                        update caserecord;
                    }
                    else if(caserecord.Settle_Amount_To_Be_deducted__c != null && caserecord.status == Constants.CASE_STATUS_MOVEDOUTWITHISSUE && caserecord.parentId !=null && caserecord.Parent_Case_Type__c =='Internal Transfer' && caserecord.Bed_Released_API_Message__c !=null && caserecord.Bed_Released_API_Status__c !=null && caserecord.Bed_Released_API_Status__c.contains('True')){
                        
                       updateSetlementRecord=false;
                        case subcase = new Case();
                        subcase.OwnerId=InternalTrasnfer[0].id;
                        /*populate tenant bank details on sdub case */
                        if(tenatbankdetailslist != null && tenatbankdetailslist.size() > 0) {
                            if(tenatbankdetailslist[0].Account_Number__c != null )
                                subcase.Tenant_bank_A_c_No__c = tenatbankdetailslist[0].Account_Number__c;
                            if(tenatbankdetailslist[0].Branch_Name__c != null )
                                subcase.Tenant_Bank_Branch_Name__c = tenatbankdetailslist[0].Branch_Name__c;
                            if(tenatbankdetailslist[0].IFSC_Code__c != null )
                                subcase.Tenant_Bank_IFSC_Code__c = tenatbankdetailslist[0].IFSC_Code__c;
                            if(tenatbankdetailslist[0].Bank_Name__c != null )
                                subcase.Tenant_Bank_Name__c = tenatbankdetailslist[0].Bank_Name__c;
                            if(tenatbankdetailslist[0].name != null)
                                subcase.A_c_Holder_Name_Tenant__c = tenatbankdetailslist[0].name ;
                        }
                        subcase.recordtypeid=caseSettlement_internal_RecordTypeId;
                        subcase.ContactId=caserecord.ContactId;
                        subcase.Status='New';
                        subcase.Subject='Move Out Settlement - Internal transfer';
                        subcase.Origin='Web';
                        subcase.Settle_Amount_To_Be_deducted__c=caserecord.Settle_Amount_To_Be_deducted__c;
                        subcase.Settlement_Amount_Calculated_Via_System__c=caserecord.Settlement_Amount_Calculated_Via_System__c;
                        subcase.Priority='High';
                        subcase.Description='Move Out Settlement - Internal transfer - has been created';
                        subcase.Net_Amount_to_Be_Refunded__c=caserecord.Settlement_Amount_Calculated_Via_System__c +caserecord.Settle_Amount_To_Be_deducted__c;
                        subcase.Deduction_Reason__c=caserecord.Deduction_Reason__c;
                        subcase.Type='MoveOut Settlement - Internal Transfer';
                        subcase.Parentid=caserecord.id;
                        subcase.Move_Out_Type__c=caserecord.Move_Out_Type__c;
                        subcase.Move_Out_Date__c=caserecord.Move_Out_Date__c;
                        subcase.Booked_Object_Type__c = caserecord.Booked_Object_Type__c ;
                        subcase.House__c = caserecord.House__c;
                        subcase.AccountId = caserecord.AccountId ;
                        if(caserecord.House_Id__c != null)
                            subcase.MoveOut_House_ID__c = caserecord.House_Id__c;

                        insert subcase;
                         caserecord.Net_Amount_to_Be_Refunded__c =caserecord.Settlement_Amount_Calculated_Via_System__c +caserecord.Settle_Amount_To_Be_deducted__c;
                         caserecord.Status= Constants.CASE_STATUS_MOVEDOUT_REFUNDREQUESTED ;
                         caserecord.Initiate_Settlement__c='No';
                         caserecord.Temp_Field_Settlement_Updated__c=system.now();
                         caserecord.Temp_Field_Settlement_Done__c=true;
                        update caserecord;
                    }
                }
            }
            else {
                system.debug('****coming inside');      
               caserecord.Temp_Settlement_Error_Msg__c=' Settlement case has already been created';
              
            }
             
            if(updateSetlementRecord){
                 
                 update caserecord;
             }
         
        }
        catch(Exception e){

            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertErrorLog('Tempsettlement Batch','Exception in the record '+sourceIdCase+' Msg:'+e.getMessage()+' at line '+e.getLineNumber());
           // return null;
        }
      } 
    }
        
    
    global void finish(Database.BatchableContext BC) {
        
        
         
        
    }
    
}