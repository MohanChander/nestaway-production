global class HouseAddressPopulateExtension {
    global static id houseIdForRemote {get;set;}
    public static house__c h;
    public HouseAddressPopulateExtension(ApexPages.StandardController controller) {
        houseIdForRemote = ApexPages.currentPage().getParameters().get('id');
        
        System.debug('*** houseIdForRemote '+houseIdForRemote);
    }
    @remoteAction
    global static Boolean updateLocalityToHouse(String houseId,String locality,String subLocality,String SubLocalityLevel1,String city,String postalCode,String formattedaddress) {
        try{
            System.debug('***Valuses from Page @remote - '+locality+'***'+subLocality+'***'+SubLocalityLevel1+'***'+city+'***'+postalCode+'***'+formattedaddress);
            /*h = [Select id,Locality__c,Sub_Locality__c,Sub_Locality_Level_2__c,
                  Postal_Code_Locality__c ,Locality_city__c, name
                 from house__c where id = : houseIdForRemote];
            System.debug('*** house to be updated'+h);*/
            
            h = new house__c(id=houseId);
            System.debug('house h '+h);
            if(h.id != null){
                System.debug('***house - >'+h);
                if(locality != null && locality != ' ')
                    h.Locality__c = locality;
                if(subLocality != null && subLocality != ' ')
                    h.Sub_Locality__c = subLocality;
                if(SubLocalityLevel1 != null && SubLocalityLevel1 != ' ')
                    h.Sub_Locality_Level_2__c = SubLocalityLevel1;
                if(postalCode != null && postalCode != ' ')
                    h.Postal_Code_Locality__c = postalCode;
                if(city != null && city != ' ')
                    h.Locality_city__c = city;
                if(formattedaddress != null && formattedaddress != ' ')
                    h.Locality_Address__c  = formattedaddress;
                update h;
                return true;        
            }
            else
                return false;            
        } 
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
            UtilityClass.insertGenericErrorLog(e);
            return false;
        }
   }
    @remoteAction
    global static Boolean validCityRespond(String ct){
        try{
                List<City__c> ctList=[Select id,name,NewCity__c,NewCity__r.name from City__c];
                boolean valid=false;
                Set<String> city=new Set<String>();
                Map<String,String> validCityMap=new  Map<String,String>();
                List<Sobject> passObject=new List<Sobject>();
                for(City__c c:ctList){
                    if(c.NewCity__c==null){
                      city.add(c.name.toUpperCase());  
                    }
                    else {
                       validCityMap.put(c.name.toUpperCase(),c.NewCity__r.name) ;
                    }
                }
        
                 System.debug('***MapCity'+validCityMap);
                 System.debug('***MapCitySet'+validCityMap.keySet());
                 System.debug('***City'+city);
        
                valid=city.contains(ct.toUpperCase());
                if(valid==false){
                  valid = validCityMap.containsKey(ct.toUpperCase());   
                }
                System.debug('***city-test*'+valid);
                return valid;
            }
             catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+' <---> '+e);
            UtilityClass.insertGenericErrorLog(e,'validCityRespond for page');
            return false;
        }


    }
}