/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for Case with RecordType - Lead Escalation Tracking
**********************************************/
public class CaseLeadEscalationTrackingTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : Sync the Status of the Escalation Tracking Case with the Lead record
**********************************************/
    public static void syncLeadEscalationLevels(List<Case> caseList){
        try{    
        		Set<Id> leadIdSet = new Set<Id>();

        		for(case c: caseList){
        			leadIdSet.add(c.Lead__c);
        		}

        		Map<Id, Lead> leadMap = new Map<Id, Lead>([select Id, New_Stage_Escalation_Level__c, Open_Stage_Escalation_Level__c from Lead 
        							   where Id =: leadIdSet]);

        		for(case c: caseList){
        			if(leadMap.containsKey(c.Lead__c)){
        				leadMap.get(c.Lead__c).New_Stage_Escalation_Level__c = c.New_Stage_Escalation_Level_for_Lead__c;
        				leadMap.get(c.Lead__c).Open_Stage_Escalation_Level__c = c.Open_Stage_Escalation_Level_for_Lead__c;        				
        			}
        		}

        		if(!leadMap.isEmpty()){
        			update leadMap.values();
        		}


            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Case - syncLeadEscalationLevels');      
            }           
    }   
}