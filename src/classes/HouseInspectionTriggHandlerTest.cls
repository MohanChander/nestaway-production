@isTest(seealldata = True)
public Class HouseInspectionTriggHandlerTest{
    
    public Static TestMethod void houseInspectionTest(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s = 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.House_Lat_Long_details__Latitude__s = 13.0646914;
        houseIC1.House_Lat_Long_details__Longitude__s = 77.6296346;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.House_Layout__c = '1 R';
        houseIc1.House__c=hos.id;
        houseIC1.Status__c = 'House Inspection In Progress';           
        houseList.add(houseIC1);
        
        insert houseList;
        
        for(House_Inspection_Checklist__c hse : houseList){
            houseIdSet.add(hse.Id);
        }
        
        houseIC.House_layout__c = '4 BHK';    
        houseIC.ID_proof_number__c = '1234';
        houseIC.Identity_proof_document__c = 'PAN Card';
        update houseIC;
        
        houseIC1.House_layout__c = '1 BK';    
        houseIC1.ID_proof_number__c = '1234';
        houseIC1.Identity_proof_document__c = 'PAN Card';
        update houseIC1;
        
        for(Room_Inspection__c room : [SELECT Id,Area_of_the_room_in_sqft__c FROM Room_Inspection__c WHERE House_Inspection_Checklist__c in:houseIdSet]){
            room.Area_of_the_room_in_sqft__c = '10';
            roomList.add(room);
        }
        system.debug('-----'+roomList);
        update roomList;
        
        houseIC.Status__c = 'House Inspection Completed';
        update houseIC;
        
        houseIC1.Status__c = 'House Inspection Completed';
        update houseIC1;
        
        
        Test.StopTest();
    }
    
    
    public Static TestMethod void houseInspectionTest1(){
        Test.StartTest();
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Submitted'; 
        houseIC1.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC1.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC1.Doc_Reviewer_Approved__c = TRUE;
        houseIC1.Address_Proof_Document__c  = 'Aadhar Card';     
        houseIC1.Address_Proof_ID__c = '13441';
        houseIC1.Verified_Electric_Bill_For_No_Dues__c = 'No';
        houseIC1.Electric_Bill_Account_Number__c = '123444';
        houseIC1.PAN_Card_Available__c = 'Yes';
        houseIC1.PAN_Card_Number__c = 'AXSWE1234Q';
        houseIc1.House__c=hos.id;
        houseList.add(houseIC1);
        
        insert houseList;
        
      /*  houseIC1.Other_ID_Proof__c = 'Aadhar Card';
        houseIC1.ID_Proof_Number__c = '1234567';
        update houseIC1; */
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIc1.id,houseIc1);
        HouseInspectionTriggHandler.sendWebEntityHouseJson(newMap);
        Test.StopTest();
    }
    public Static TestMethod void houseInspectionTest3(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Verification_Done__c ='Yes';
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIc;
        
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.House_Layout__c = '1 BH';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        Test.StopTest();
    }
    public Static TestMethod void houseInspectionTest4(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIc;
        
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.House_Layout__c = '1 BK';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        Test.StopTest();
    }
    public Static TestMethod void houseInspectionTest5(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
        Bathroom__c batchRoom1= new Bathroom__c();
        batchRoom1.Checklist__c=houseIC.id;
        batchRoom1.name='Common Bathroom '; 
        batchRoom1.Type__c='Common';
        insert batchRoom1;
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
       public Static TestMethod void houseInspectionTest6(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=4;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
        Bathroom__c batchRoom1= new Bathroom__c();
        batchRoom1.Checklist__c=houseIC.id;
        batchRoom1.name='Common Bathroom '; 
        batchRoom1.Type__c='Common';
        insert batchRoom1;
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
      public Static TestMethod void houseInspectionTest7(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
      public Static TestMethod void houseInspectionTest8(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_attached_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
          houseIC.Number_of_attached_bathrooms__c=3;
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }


}