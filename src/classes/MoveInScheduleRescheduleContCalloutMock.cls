global class MoveInScheduleRescheduleContCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
       
       String str = req.getBody();
       Map<String, Object> inputMap = (Map<String, Object>) JSON.deserializeUntyped(str);
        
       WBMIMOJSONWrapperClasses.MIMODateJsonWrapper dateMock = new WBMIMOJSONWrapperClasses.MIMODateJsonWrapper();
       dateMock.success=true;
       dateMock.info='';
       dateMock.data= new WBMIMOJSONWrapperClasses.DataObject();
       dateMock.data.start_date='2017-08-21';
       dateMock.data.end_date='2017-08-21';
       dateMock.data.holidays = new List<WBMIMOJSONWrapperClasses.holidays>();
       WBMIMOJSONWrapperClasses.holidays holiday= new WBMIMOJSONWrapperClasses.holidays();
       holiday.date_value='2017-07-25';
       dateMock.data.holidays.add(holiday);
       string dateJson=JSON.serialize(dateMock);      
       
       // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');

        if(req.getEndpoint().contains('MoveInDateAPI')){
          response.setBody(dateJson);
        } 
        else if(req.getEndpoint().contains('MoveInFECalAPI')){
            
        WBMIMOJSONWrapperClasses.FECalendarWithinDateRange feCalendar;        
        feCalendar = (WBMIMOJSONWrapperClasses.FECalendarWithinDateRange) JSON.deserializeStrict(str, WBMIMOJSONWrapperClasses.FECalendarWithinDateRange.class);
            
             // Fe Calendar Response
        WBMIMOJSONWrapperClasses.FECalendarInfoParent calResp = new WBMIMOJSONWrapperClasses.FECalendarInfoParent();
        calResp.success=true;
        calResp.info='';
        calResp.data = new WBMIMOJSONWrapperClasses.FECalendarInfo();
        calResp.data.case_Id=feCalendar.case_Id;
        calResp.data.executives_calendar = new List<WBMIMOJSONWrapperClasses.Executives_Calendar>();
        WBMIMOJSONWrapperClasses.Executives_Calendar execCalData= new WBMIMOJSONWrapperClasses.Executives_Calendar();       
        execCalData.executive_id=feCalendar.field_executives.get(0).executive_id;
        execCalData.calendar = new LIst<WBMIMOJSONWrapperClasses.Calendar>();
        WBMIMOJSONWrapperClasses.Calendar cal = new WBMIMOJSONWrapperClasses.Calendar();
        cal.date_value= feCalendar.from_date;
        cal.slots = new List<WBMIMOJSONWrapperClasses.Slots>();
        WBMIMOJSONWrapperClasses.Slots slot = new WBMIMOJSONWrapperClasses.Slots();
        slot.from_time='2017-08-21';
        slot.to_time='2017-08-21';
        slot.to_time='2017-08-21';
        slot.text='2017-08-21';
        slot.note='2017-08-21';
        slot.available=true;
        cal.slots.add(slot);
        execCalData.calendar.add(cal);
        calResp.data.executives_calendar.add(execCalData);      
        
        string calResponse=JSON.serialize(calResp);
            
          response.setBody(calResponse);
        }
        else if(req.getEndpoint().contains('MoveInFESchAPI')){
            
             WBMIMOJSONWrapperClasses.BookFECalendarResponse slotResponse = new WBMIMOJSONWrapperClasses.BookFECalendarResponse();
             slotResponse.success=true;
             slotResponse.data='';
             slotResponse.info='';
             response.setBody(JSON.serialize(slotResponse));
        }
        else{
            
            
        }
        
        response.setStatusCode(200);
        return response; 
       
    }
}