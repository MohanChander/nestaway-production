public class MoveInScheduleRescheduleController {
    Public List<Case> caseList{get;set;}
    Public Case cs{get;set;}   
    public boolean hideshowContent {get;set;} 
    public string timei{get;set;}
    public String selectedDate{get;set;}
    public String selectedUsers{get;set;}
    public String SelectedSlot {get;set;}
    public String SelectedFe_user{get;set;}
    public boolean makeresvisible{get;set;}
    public boolean makeresvisible1{get;set;}
    public String stDate;
    public String endDate;
    private date stDate1;
    private date enddate1;
    public map<String,String> holidaysmap;
    public string  CityFromHouse;
    public map<id,house__c> HouseAndzoneCodeMap{get;set;}
    public map<string,list<user>> HouseAndusers{get;set;}
    public map<Id,User> users{get;set;}
    public boolean makeresvisiblefes{get;set;}
    public boolean makeZoneFeVisible{get;set;}
    public boolean makeCityFeVisible{get;set;}
    public List<WBMIMOJSONWrapperClasses.holidays> holidays;
    public String CAseRecordType;
    public List<SelectOption> dateOptions;
    public List<SelectOption> slotoption;
    public boolean showMsgFirstTime;
    public string buttonLabel {get;set;}
    public map<string,WBMIMOJSONWrapperClasses.BookFECalendar> availableSlots;
    public List<SelectOption> usersoption{get;set;}
    public List<SelectOption> Fe_user{get;set;}
    public boolean hasMoveIn{get;set;} 
    public String HouseId;
    public MoveInScheduleRescheduleController (ApexPages.StandardController sController) {
        makeresvisible=false;
        showMsgFirstTime=false;
        makeresvisiblefes=false;
        makeZoneFeVisible=true;
        makeCityFeVisible=false;
        hasMoveIn=false;

        hideshowContent = true;

        buttonLabel='Show City Users';
        slotoption = new List<SelectOption>();
        usersoption = new List<SelectOption>();
        Fe_user = new List<SelectOption>();
        holidays= new List<WBMIMOJSONWrapperClasses.holidays>();
        availableSlots = new  map<string,WBMIMOJSONWrapperClasses.BookFECalendar>();
        dateOptions  = new List<SelectOption>();
        users = new Map<Id,User>();     
        cs=[select Id,Move_in_date__c,MoveOut_Slot_Start_Time__c,MoveOut_Slot_End_Time__c,
                Move_Out_Date__c,House__C,SD_Status__c,Documents_Uploaded__c,Tenant_Profile_Verified__c,
                MoveIn_Slot_End_Time__c,MoveIn_Slot_Start_Time__c,House_Id__c,Move_Out_Status__c,
                Onboarding_Zone_Code__c,Move_Out_Type__c,RecordType.Name ,status,Parent_Case_Type__c,Ekyc_Verified__c
                from Case  WHERE id =: ApexPages.currentPage().getParameters().get('id')];
        
        System.debug('*****cs '+cs+cs.House__C);
        List<house__c> houseRecordLst =[select id,Onboarding_Zone_Code__c,City_Master__c,City__c from house__c where id  =: cs.House__C];
        
       if(houseRecordLst.size()>0){ 
        house__c houseRecord=houseRecordLst.get(0);
        HouseId = houseRecord.id; 
       // CityFromHouse=houseRecord.City_Master__c;
        CityFromHouse=houseRecord.City__c;
        CAseRecordType=cs.RecordType.Name;
        System.debug('***houseRecord '+houseRecord);
        System.debug('***CityFromHouse '+CityFromHouse);
        
        slotoption.add(new SelectOption('-None-','-None-'));

        HouseAndzoneCodeMap = new map<id,house__c>();
        if(houseRecord.Onboarding_Zone_Code__c != null) {
            HouseAndZoneCodeMap.put(houseRecord.id,houseRecord);
            
        }
        if(CAseRecordType=='MoveIn'){
           hasMoveIn=true;
           //chandu: by pass sd default case in internal transfer.
           
           if(CAseRecordType == 'MoveIn'  && ((cs.Documents_Uploaded__c == false && cs.Ekyc_Verified__c==false) ||  cs.Tenant_Profile_Verified__c == false || cs.status == 'Closed' || (cs.Parent_Case_Type__c != 'Internal Transfer' && (cs.SD_Status__c == 'Pending' || cs.SD_Status__c == 'SD Default')))) {               
            
                hideshowContent = false;
                hasMoveIn=false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Movein can be scheduled only if documents are completed and profile is completed and SD is paid and the case is not closed.'));
             
           }
           //HouseAndusers = UtilityClass.GetMoveInExecutivesFromZoneCode( HouseAndzoneCodeMap);
            HouseAndusers = UtilityClass.GetAPMFromZoneCode(HouseId);

        }
        else{
             hasMoveIn=false;
             if(CAseRecordType == 'Move Out'  && cs.status != null ) {
                /* && (! (cs.Move_Out_Type__c == 'Cancellation' || cs.Move_Out_Type__c == 'SD Default' || cs.Move_Out_Type__c == 'Owner Rejected Booking') && */
                if( !(cs.status == 'New' || cs.status == 'Move Out Inspection Pending' || cs.status == 'Schedule MoveOut') ) {
                    hideshowContent = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Schedule MoveOut is possible only if status is New or Move Out Inspection Pending or Schedule MoveOut.'));
                    
                }
                if(cs.Move_Out_Status__c == 'Cancelled' || cs.Move_Out_Status__c == 'Tenant Cancelled MoveOut' || cs.Move_Out_Status__c == 'Closed' || cs.Move_Out_Status__c == 'MoveOut Cancelled on behalf of Tenant' ) {
                   // hideshowContent = false;
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Schedule MoveOut is not possible if moveout status is cancelled or closed.'));
                }
            }
             HouseAndusers = UtilityClass.GetAPMFromZoneCode(HouseId);
           // HouseAndusers = UtilityClass.GetMoveOutExecutivesFromZoneCode( HouseAndzoneCodeMap);
        }
       
        usersoption=allUsersoption();
        string respBody=getDatesFromNAApp(cs.Id);
          System.debug('*****respBody '+respBody);
        WBMIMOJSONWrapperClasses.MIMODateJsonWrapper JSONDeserialized;
        try{

              JSONDeserialized = (WBMIMOJSONWrapperClasses.MIMODateJsonWrapper) JSON.deserialize(respBody, WBMIMOJSONWrapperClasses.MIMODateJsonWrapper.class);
        }
        catch(exception e){
            
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
        }
        
         
        //System.debug('****JSONDeserialized '+JSONDeserialized);
        if(JSONDeserialized!=null && JSONDeserialized.data != null && JSONDeserialized.data.start_date!=null && JSONDeserialized.data.end_date!=null) {
            
            System.debug('****APi response JSONDeserialized'+JSONDeserialized);
            
            /*stDate = JSONDeserialized.start_date;
            endDate = JSONDeserialized.end_date;
            holidays=JSONDeserialized.holidays;
            */
            stDate = JSONDeserialized.data.start_date;
            endDate = JSONDeserialized.data.end_date;
            holidays = JSONDeserialized.data.holidays;
            showDateOptions();
        }
        else{
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
        }
        
      }
      else{
             hideshowContent = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'House or House id is not preset in salesforce.'));
      }   
       
        
      /*  if(JSONDeserialized.holidays != null)
            holidaysmap = JSONDeserialized.holidays;
       */
    }
    
    public string getDatesFromNAApp(Id caseId){
               
        /*API Callout to get dates*/
        HttpRequest req = new HttpRequest();
        
        List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
        String authToken=nestURL[0].Webapp_Auth__c;
        String strEndPoint;
       if(CAseRecordType=='MoveIn'){
           strEndPoint= nestURL[0].MIMO_Calendar_Date_API__c+'&auth='+authToken;
       }
       else{
           strEndPoint= nestURL[0].Moveout_Calendar_Date_API__c+'&auth='+authToken; 
       }
        
        ///services/apexrest/
       // String strEndPoint = 'https://nestaway--dev.cs58.my.salesforce.com/services/apexrest/GetMoveInExecforaZone';
        //req.setEndPoint(strEndPoint);
      //  req.setHeader('Content-Type', 'application/json');
       // req.setHeader('Accept', 'application/json');
      //  req.setMethod('POST');
        //string jsonString = '{"end_date":"20-07-2017","holidays":{"0":"15-07-2017","1":"16-07-2017"},"start_date":"13-07-2017"}';
        string jsonBody='';
        WBMIMOJSONWrapperClasses.MoveInDates moveInCaseId = new WBMIMOJSONWrapperClasses.MoveInDates();
        moveInCaseId.case_id=caseId;
        jsonBody=JSON.serialize(moveInCaseId);
      /* req.setBody(jsonString);
         req.setTimeout(120000);        
        Http http = new Http();
        HttpResponse res;
        res = http.send(req);       
        String str = res.getBody();
        System.debug('str '+str);
        */
        HttpResponse res;
        res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
        String respBody = res.getBody();
        
        return respBody;
    }
    
    
    public void populateFesWithSelecteddate() {
        system.debug('**** populateFesWithSelecteddate -> '+ selectedDate);
        if(HouseAndzoneCodeMap !=null && HouseAndzoneCodeMap.size() >0 ) {
           // HouseAndusers = UtilityClass.GetMoveInExecutivesFromZoneCode( HouseAndzoneCodeMap);
           // System.debug('*****HouseAndusers '+HouseAndusers);
        }
        
        //selectedDate='';
        selectedUsers='-None-';
        SelectedSlot='-None-';
        slotoption = new List<SelectOption>();
        slotoption.add(new SelectOption('-None-','-None-'));
        
        makeresvisible=true;
    }
    
    
     public List<SelectOption> getslotoption()
    {
                      
        
        return slotoption;
    }
    
    public void populateTimeSlotsWithFEs() {
        
        system.debug('***populateTimeSlotsWithFEs ');
        SelectedSlot='-None-';
        /*API Callout to get the FEs and their calender*/
        
       // String jSONResponseBody ='{"Date_Selected":"2017-7-09","Case_Id" : "37483745893","FeildExecutive_Data":[{"Fe_id":"xyz1234567324","Fe_Email_Id":"xyz1234567324","Date_Data":[{"Date_fromDate_Data":"12/12/1222","Time_Data":{"Time_Slot" : "12:00 - 1:00 AM","Slot_Id": "xyz1234567324","Visible": true}},{"Date_fromDate_Data":"12/12/1222","Time_Data":{"Time_Slot" : "12:00 - 1:00 AM","Slot_Id": "xyz1234567324","Visible": true}}]},{"Fe_id":"xyz1234567324","Fe_Email_Id":"xyz1234567324","Date_Data":[{"Date_fromDate_Data":"12/12/1222","Time_Data": {"Time_Slot" : "12:00 - 1:00 AM","Slot_Id": "xyz1234567324","Visible": true}},{"Date_fromDate_Data":"12/12/1222","Time_Data": {"Time_Slot" : "12:00 - 1:00 AM","Slot_Id": "xyz1234567324","Visible": true} }]}]}';
        slotoption = new List<SelectOption>();
        slotoption.add(new SelectOption('-None-','-None-'));
        List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
        String authToken=nestURL[0].Webapp_Auth__c;
        String strEndPoint;
         if(CAseRecordType=='MoveIn'){
           strEndPoint= nestURL[0].MIMO_FE_Calendar_API__c+'&auth='+authToken;
        }
        else{
            strEndPoint= nestURL[0].Moveout_FE_Calendar_API__c+'&auth='+authToken;
        }
     if(selectedUsers!=null && selectedUsers!='-None-' && selectedDate!=null && selectedDate!='-None-'){
               WBMIMOJSONWrapperClasses.FECalendarWithinDateRange feCalendar = new   WBMIMOJSONWrapperClasses.FECalendarWithinDateRange();
               feCalendar.field_executives = new List<WBMIMOJSONWrapperClasses.Field_Executives>();
               WBMIMOJSONWrapperClasses.Field_Executives fe = new WBMIMOJSONWrapperClasses.Field_Executives();
               feCalendar.case_id=cs.id;
               feCalendar.from_date=selectedDate;
               feCalendar.to_date=selectedDate;
               User u=users.get(selectedUsers);
               fe.executive_id=u.id;
               fe.executive_email_id=u.email;
               feCalendar.field_executives.add(fe);
               
                HttpRequest req = new HttpRequest();
                string jsonBody='';
                jsonBody=JSON.serialize(feCalendar);
                system.debug('***jsonBody for fe selected.'+jsonBody);
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,120000);
                String respBody = res.getBody();
                system.debug('***Respbody'+respBody);
                
                 WBMIMOJSONWrapperClasses.FECalendarInfoParent calenderInfo;
                try{
                     calenderInfo = (WBMIMOJSONWrapperClasses.FECalendarInfoParent) JSON.deserializeStrict(respBody, WBMIMOJSONWrapperClasses.FECalendarInfoParent.class);
                     System.debug('***calenderInfo '+calenderInfo);
                }    
                catch(exception e){

                    System.debug('***'+e);
                    
                    //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
                }
                
                 
              
                if(calenderInfo!=null){
                    
                     system.debug('***calenderInfo.data.executives_calendar'+calenderInfo.data.executives_calendar);
                    
                    if(calenderInfo.data != null && calenderInfo.data.executives_calendar!=null && calenderInfo.data.executives_calendar.size()>0){
                        
                      WBMIMOJSONWrapperClasses.Executives_calendar  execuCalendar;
                        for(WBMIMOJSONWrapperClasses.Executives_calendar execCal : calenderInfo.data.executives_calendar){
                            
                            if(execCal.executive_id==selectedUsers){
                                
                                execuCalendar=execCal;                          
                                
                            }
                            
                            
                        }
                        if(execuCalendar!=null && execuCalendar.calendar!=null && execuCalendar.calendar.size()>0){
                            
                            WBMIMOJSONWrapperClasses.Calendar cal;
                            
                            for(WBMIMOJSONWrapperClasses.Calendar webappCal: execuCalendar.calendar){
                            
                                if(webappCal.date_value!=null && webappCal.date_value==selectedDate){
                                    
                                    cal=webappCal;                          
                                    
                                }
                                
                                
                            }
                            system.debug('***cal'+cal); 
                            if(cal!=null && cal.slots!=null && cal.slots.size()>0){
                                
                                 
                                  
                                    WBMIMOJSONWrapperClasses.Slots slot;
                                    Integer i=0;                                    
                                    for(WBMIMOJSONWrapperClasses.Slots feSlot: cal.slots){

                                      if(feSlot.text!=null && feSlot.available==true){
                                          
                                            slotoption.add(new SelectOption(string.valueOf(i),feSlot.text));
                                            WBMIMOJSONWrapperClasses.BookFECalendar feCalendarData= new WBMIMOJSONWrapperClasses.BookFECalendar();
                                            feCalendarData.case_id=cs.Id;
                                            feCalendarData.date_value=cal.date_value;
                                            feCalendarData.executive_id=execuCalendar.executive_id;
                                            feCalendarData.executive_email_id=execuCalendar.executive_email_id;
                                            feCalendarData.from_time=feSlot.from_time;
                                            feCalendarData.to_time=feSlot.to_time;                                                      
                                            availableSlots.put(string.valueOf(i),feCalendarData);
                                            i++;
                                      }
                                     
                                    }
                                
                                
                            }
                            else{
                                
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting Fe Calendar from webapp.'));
                            }
                            
                            
                        }
                        else{
                        
                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting Fe Calendar from webapp.'));
                        }
                    
                        
                        
                        
                    }
                    else{
                        
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting Fe Calendar from webapp.'));
                    }
                    
                    
                    
                }
                else{
                    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting Fe Calendar from webapp.'));
                }
               
               
               
                
                makeresvisible1=true;
       }
        else{
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select Date or Fe to get available slots.'));
        }
        
        showMsgFirstTime=true;
    }
    
    public void showDateOptions(){      
        dateOptions  = new List<SelectOption>();
        List<Date> datesBetweenStartEnd = new List<Date>();
        List<Date> datetoremoved = new List<Date>();
        stDate1=date.valueOf(stDate);
        endDate1=date.valueOf(endDate);
        for(WBMIMOJSONWrapperClasses.holidays holiday: holidays){
            
            datetoremoved.add(date.valueOf(holiday.date_value));
        }
        while(stDate1 <=endDate1) {
            datesBetweenStartEnd.add(stDate1);
            stDate1=stDate1.adddays(1);
        }
        
        Set<date> newDateList = new set<date> ();
        for(date each :datesBetweenStartEnd) {
            
         if(datetoremoved.size()>0){
             
             for(date eachInternal :datetoremoved) {
                if(!(each == eachInternal)) {
                    newDateList.add(each);
                }
            }
         }  
         else{
             
             newDateList.add(each);
         }
            
        }
        system.debug('****newDateList'+newDateList);
        dateOptions.add(new SelectOption('-None-','-None-'));
        for(Date each1:newDateList){
            dateOptions.add(new SelectOption(String.valueof(each1),String.valueof(each1)));
        }
        
        
        
        
        
        
  // commenting  the old code       
        /* dateOptions  = new List<SelectOption>();
        List<Date> datesBetweenStartEnd = new List<Date>();
        List<Date> datetoremoved = new List<Date>();
        System.debug('***** stDate'+stDate);
        
        String dt = stDate;
        List<String> dateSplit=dt.split('-');
        date mydate = date.parse('27/7/2009');

        string newDt =dateSplit.get(2)+'/'+dateSplit.get(1)+'/'+dateSplit.get(0);
        stDate1= date.parse(newDt);
        String dt1 = endDate;
        dateSplit=dt1.split('-');
        string newDt2 = dateSplit.get(2)+'/'+dateSplit.get(1)+'/'+dateSplit.get(0);
        endDate1= date.parse(newDt2);
        system.debug('stDate1'+stDate1);
        system.debug('stDate1'+enddate1);
        while(stDate1 <=endDate1) {
            datesBetweenStartEnd.add(stDate1);
            stDate1=stDate1.adddays(1);
        }
        system.debug('datesBetweenStartEnd'+datesBetweenStartEnd);
       /* for(String each:holidaysmap.values()){
            string newDt3 = each.replace('-', '/');
            datetoremoved.add(date.parse(newDt3));
        }
        
    //  /*
        system.debug('datetoremoved'+datetoremoved);
        Set<date> newDateList = new set<date> ();
        for(date each :datesBetweenStartEnd) {
            for(date eachInternal :datetoremoved) {
                if(!(each == eachInternal)) {
                    newDateList.add(each);
                }
            }
        }
        system.debug('newDateList'+newDateList);
        dateOptions.add(new SelectOption('','-None-'));
        for(Date each1:newDateList){
            dateOptions.add(new SelectOption(String.valueof(each1),String.valueof(each1)));
        }
        */
        
    }
    public List<SelectOption> getdateOptions() {

        return dateOptions;
    }
    
  //  public List<SelectOption> getusersoption() //usersoption
    public List<SelectOption> allUsersoption(){
        List<SelectOption> usersoption = new List<SelectOption>();
         List<User> userList = new List<User>();
       // List<User> userlist=[Select id,Name from User limit 10];
        //system.debug('list of user'+userlist);
       /* if(HouseAndusers != null && HouseAndusers.size() > 0) {
        userList =HouseAndusers.values()];
        }*/
        usersoption.add(new SelectOption('','-None-'));
        System.debug('***HouseAndusers '+HouseAndusers);
      
             if(HouseAndusers != null && HouseAndusers.size() > 0) {
            for(String each : HouseAndusers.keySet()) {
                for(user eachUser : HouseAndusers.get(each)){
                    usersoption.add(new SelectOption(eachUser.Id,eachUser.email));
                    users.put(eachUser.Id,eachUser);
                }
            } 
        }
       /* if(HouseAndusers != null && HouseAndusers.size() > 0) {
        userList = [Select id ,email  from user where id in :HouseAndusers.values()];
        }
        if(!userList.isEmpty()){
        for(user eachUser : userList){
                   usersoption.add(new SelectOption(eachUser.Id,eachUser.email));
                    users.put(eachUser.Id,eachUser);
                }
        }*/
        system.debug('list of user'+usersoption);
        return usersoption;
    }
    
    
    
  
   public pagereference cancel(){
       
        PageReference parentPage;
        if(cs!=null){
            parentPage = new PageReference('/' + cs.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        
        return null;

    }
    
     public pagereference showCityUsers(){
       
       makeZoneFeVisible=false;
       makeCityFeVisible=true;     
       Fe_user=new List<SelectOption>();
       Fe_user.add(new SelectOption('','-None-'));   
       Fe_user=allFe_user();
        
     
       return null;
   }
   
   
   public pagereference showZoneUsers(){
       
       makeZoneFeVisible=true;
       makeCityFeVisible=false; 
       usersoption=new List<SelectOption>();
       usersoption.add(new SelectOption('','-None-'));
       //HouseAndusers = UtilityClass.GetMoveInExecutivesFromZoneCode(HouseAndzoneCodeMap);
       HouseAndusers = UtilityClass.GetAPMFromZoneCode(HouseId);

       usersoption=allUsersoption();       
     
       return null;
   }
   
    public pagereference saveMIMOFESlot(){
       
       if(SelectedSlot!=null && SelectedSlot!='-None-' && selectedUsers!=null && selectedUsers!='-None-' && selectedDate!=null && selectedDate!='-None-'){
           
           
            if(availableSlots.containsKey(SelectedSlot)){
                
                WBMIMOJSONWrapperClasses.BookFECalendar feCalendarData=availableSlots.get(SelectedSlot);
                List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
             //   String strEndPoint = nestURL[0].MIMO_Calendar_Schedule_API__c+'?auth='+authToken;
                String strEndPoint;
                if(CAseRecordType=='MoveIn'){
                    strEndPoint= nestURL[0].MIMO_Calendar_Schedule_API__c+'&auth='+authToken;
                }
                else{
                    strEndPoint= nestURL[0].Moveout_Calendar_Schedule_API__c+'&auth='+authToken;
                }
                HttpRequest req = new HttpRequest();
                string jsonBody='';
                jsonBody=JSON.serialize(feCalendarData);
                system.debug('***jsonBody'+jsonBody);
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,120000);
                String respBody = res.getBody();
                system.debug('***Respbody'+respBody);

                WBMIMOJSONWrapperClasses.BookFECalendarResponse slotResponse;
                try{
                 slotResponse = (WBMIMOJSONWrapperClasses.BookFECalendarResponse) JSON.deserialize(respBody, WBMIMOJSONWrapperClasses.BookFECalendarResponse.class);
                }    
                catch(exception e){

                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
                }
                
                if(slotResponse!=null){
                    
                    if(slotResponse.success !=null && slotResponse.success && slotResponse.data != null ){
                        
                         if(CAseRecordType=='MoveIn'){
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,' - Movein Inspection scheduled successfully'));    
                          }
                        if(CAseRecordType=='Move Out'){
                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,' - MoveOut Inspection scheduled successfully'));
                        }
                    }
                    else{
                        
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,slotResponse.data));
                    }
                    
                }
                else{
                    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in booking slot of FE.'));
                    
                }
                
            }
            else{
                
                
            }
           
           
           
       }
       else{
           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select Date or FE or slot.'));
       } 
       
       
       
       return null;
   }
   
   
   
    public List<SelectOption> allFe_user()
    {
        Set<String> setofzones= new  Set<String>();
        Set<Id> setofUsers= new  Set<Id>();
        List<string> cityNameLst = new List<string>();
        if(CityFromHouse!=null){
            
            
            if(CityFromHouse=='Bengaluru' || CityFromHouse=='Bangalore'){
                cityNameLst.add('Bengaluru');
                cityNameLst.add('Bangalore');
            }
            if(CityFromHouse=='New Delhi' || CityFromHouse=='Delhi'){       
                cityNameLst.add('New Delhi');
                cityNameLst.add('Delhi');
            }
            if(CityFromHouse=='Greater Noida' || CityFromHouse=='Noida'){       
                cityNameLst.add('Greater Noida');
                cityNameLst.add('Noida');
            }
            if(CityFromHouse=='Gurgaon' || CityFromHouse=='Gurugram'){      
                cityNameLst.add('Gurgaon');
                cityNameLst.add('Gurugram');
            }
            if(CityFromHouse=='Mumbai' || CityFromHouse=='Navi Mumbai'){        
                cityNameLst.add('Mumbai');
                cityNameLst.add('Navi Mumbai');
            }
            
            cityNameLst.add(CityFromHouse);
        }
        List<Zone_and_OM_Mapping__c> ZoMList= new List<Zone_and_OM_Mapping__c>();
        List<Zone__c> zonesList=[Select id,Zone_code__C from Zone__c where City__c!=null and City__r.name IN:cityNameLst and recordtype.name='Property Management Zone'];
        for(Zone__C zc:zonesList){
            setofzones.add(Zc.Zone_code__C);
        }
         system.debug('setofzones'+ZoMList);
        /*if(CAseRecordType=='MoveIn'){
         ZoMList = [Select id,MoveIn_Executive__c from Zone_and_OM_Mapping__c where MoveIn_Executive__c!=null and Zone__r.id in:setofzones and recordtype.name='Move In'];
        }
         if(CAseRecordType=='Move Out'){
        ZoMList= [Select id,MoveIn_Executive__c from Zone_and_OM_Mapping__c where MoveIn_Executive__c!=null and Zone__r.id in:setofzones and recordtype.name='Move Out'];
        }*/
        if(!setofzones.isEmpty()){
         ZoMList= [select Id, Property_Manager__r.Zone__r.Zone_code__c, User__c from Zone_and_OM_Mapping__c 
                           where Property_Manager__r.Zone__r.Zone_code__c  in :setofzones and RecordType.Name = 'Assistance Property Manager'];
        }
        system.debug('ZoMList'+ZoMList);

        for(Zone_and_OM_Mapping__c z:ZoMList){
            setofUsers.add(z.User__c);
        }
        List<SelectOption> Fe_user = new List<SelectOption>();
        List<User> userlist=[Select id,email,Name from User where id in:setofUsers and  IsActive=true];
        
        Fe_user.add(new SelectOption('','-None-'));
        for(user eachUser : userlist){
            Fe_user.add(new SelectOption(eachUser.Id,eachUser.email));
            users.put(eachUser.Id,eachUser);
        }
        return Fe_user;
    }
    
    
    public class fromJSONWrapper {
        public String start_date;   //13-07-2017
        public String end_date; //20-07-2017
        public map<String,String> holidays; //15-07-2017,17-07-2017,17-07-2017
        
    }
    
    
}