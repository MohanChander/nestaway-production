/*  Created By: Mohan - WarpDrive Tech Works
    Purpose   : When a Service Request case is created perform the Assignment based on the Number of cases assigned to the Agent. */

public class CaseServiceRequestAssignment {

    static Id serviceRequestMappingRTId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();
    static Id CaseServiceRequestRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId(); 
    static Map<String, Map<String, Map<Id, Integer>>> caseLoadMap = new Map<String, Map<String, Map<Id, Integer>>>();
    static Map<String, Map<Id, Integer>> queueLoadMap = new Map<String, Map<Id, Integer>>();
    Static Id serviceRequestZoneRTId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_SERVICE_REQUEST_ZONE).getRecordTypeId();
    static Id defaultEntitlementId =  [select Id from Entitlement where Account.Name = 'NestAway' limit 1].Id;

    /*  Created By : Mohan
        Purpose    : Perform Case Assignment   */
    public static void serviceRequestCaseAssignment(List<Case> caseList){

            System.debug('************serviceRequestCaseAssignment');

        try{
                Set<String> zonecode = new Set<String>();
                Map<Id, Integer> openCaseMap = new Map<Id, Integer>();
                Map<String, Id> zoneManagerMap = new Map<String, Id>();
                Map<String, Id> romMap = new Map<String, Id>();
                Map<Id, String> queueMap = new Map<Id, String>();             //Map of global Queues
                Map<Id, User> managerMap = new Map<Id, User>(UserSelector.getAllUserDetails());
                List<Zone__c> zoneList = new List<Zone__c>(ZoneAndUserMappingSelector.getZoneRecordsForRecordTypeId(serviceRequestZoneRTId));
                set<String> caseStatus = new Set<String>{
                    Constants.CASE_STATUS_OPEN,
                    Constants.CASE_STATUS_WORK_IN_PROGRESS,
                    Constants.CASE_STATUS_WAITING_ON_CUSTOMER,
                    Constants.CASE_STATUS_REOPENED
                };
          system.debug('caseList'+caseList);
                //Global Queues - Assignment will run on the SF Queues present in the Custom Label
                set<String> globalQueues = new Set<String>(Label.Global_Care_Queues.split(';'));

                for(City__c city: CitySelector.getCityDetails()){
                    romMap.put(city.Name, city.ROM__c);
                }

                for(Zone__c zone: zoneList){
                    zoneManagerMap.put(zone.Zone_Code__c, zone.ZOM__c);
                }

                for(AggregateResult ar: CaseSelector.getNoOfOpenCasesPerUser(caseStatus, CaseServiceRequestRTId)){
                    Integer noOfCases;
                    if((Integer)ar.get('expr0') == null){
                        noOfCases = 0;
                    } else {
                        noOfCases = (Integer)ar.get('expr0');
                    }
                    openCaseMap.put((Id)ar.get('OwnerId'), noOfCases);
                }

                System.debug('*****************openCaseMap ' + openCaseMap);

                for(Case c: caseList){
                    zonecode.add(c.Zone_Code__c);
                }

                for(Group grp: [select Id, Name from Group where Name =: globalQueues]){
                    queueMap.put(grp.Id, grp.Name);
                    queueLoadMap.put(grp.Name, new Map<Id, Integer>{});
                }

                for(GroupMember member: [Select UserOrGroupId, Group.Name, GroupId From GroupMember where Group.Name =: globalQueues
                                         and UserOrGroupId in (select Id from User where isActive = true and isAvailableForAssignment__c = true)]){
                    if(queueLoadMap.containsKey(member.Group.Name)){
                        queueLoadMap.get(member.Group.Name).put(member.UserOrGroupId, openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId));
                    } else{
                        queueLoadMap.put(member.Group.Name, new Map<Id, Integer>{member.UserOrGroupId => openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId)});
                    }  
                }   

                System.debug('****************queueLoadMap' + queueLoadMap);             

                List<Zone_and_OM_Mapping__c> ZoneWithUserList = [select Id, Name, User__c, isActive__c, Zone__r.Zone_Code__c, 
                                                                 Zone__r.ZOM__c, Available_Queues__c
                                                                from Zone_and_OM_Mapping__c where RecordTypeId =: serviceRequestMappingRTId
                                                                and isActive__c = true and User__c in (select Id from User where 
                                                                isActive = true and isAvailableForAssignment__c = true)];

                // Populate the CaseLoadMap
                for(Zone_and_OM_Mapping__c zu: ZoneWithUserList){
                    if(caseLoadMap.containsKey(zu.Zone__r.Zone_Code__c) && zu.Available_Queues__c != null){
                        for(String str: zu.Available_Queues__c.split(';')){
                            if(caseLoadMap.get(zu.Zone__r.Zone_Code__c).containsKey(str))
                                caseLoadMap.get(zu.Zone__r.Zone_Code__c).get(str).put(zu.User__c, openCaseMap.get(zu.User__c) == null ? 0 : openCaseMap.get(zu.User__c));
                            else
                                caseLoadMap.get(zu.Zone__r.Zone_Code__c).put(str, new Map<Id, Integer>{zu.User__c => openCaseMap.get(zu.User__c) == null ? 0 : openCaseMap.get(zu.User__c)});
                        }
                    } else {
                        Map<String, Map<Id, Integer>> innerMap = new Map<String, Map<Id, Integer>>();
                        if(zu.Available_Queues__c != null){
                            for(String str: zu.Available_Queues__c.split(';')){
                                innerMap.put(str, new Map<Id, Integer>{zu.User__c => openCaseMap.get(zu.User__c) == null ? 0 : openCaseMap.get(zu.User__c)});
                            }
                            caseLoadMap.put(zu.Zone__r.Zone_Code__c, innerMap);
                        }
                    }
                }

                System.debug('******************caseLoadMap ' + caseLoadMap);
                System.debug('******************zoneManagerMap ' + zoneManagerMap);

                //Case assignment Logic
                for(Case c: caseList){

                    // when the Email-to-Case is assigned to the User skip the Assignment Logic 
                    if(c.Origin == Constants.CASE_ORIGIN_EMAIL && string.valueOf(c.OwnerId).startsWith('005') && c.Queue__c == null){

                    }
                    //Logic for Email-to-Case Assignment when the Case is Assigned to Queue Or Case needs to be assigned to a Global Queue
                    else if((c.Origin == Constants.CASE_ORIGIN_EMAIL && string.valueOf(c.OwnerId).startsWith('00G') && c.Queue__c == null && queueMap.containsKey(c.OwnerId)) || (c.Queue__c != null && globalQueues.contains(c.Queue__c))){
                        System.debug('************* Email-to-Case Assignment or Global Queue Assignment');
                        String queueName;
                        if(c.Origin == Constants.CASE_ORIGIN_EMAIL && c.Queue__c == null && queueMap.containsKey(c.OwnerId))
                            queueName = queueMap.get(c.OwnerId);
                        else if(c.Queue__c != null)
                            queueName = c.Queue__c;

                        if(!queueLoadMap.isEmpty() && queueLoadMap.containsKey(queueName) && queueLoadMap.get(queueName).isEmpty()){
                            c.OwnerId = CaseServiceRequestTriggerHandler.unassignedCaseQueueId;
                        } else {
                            List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                            for(Id userId: queueLoadMap.get(queueName).keySet()){
                                awList.add(new AssignmentWrapper(userId, queueLoadMap.get(queueName).get(userId)));
                            }  
                            awList.sort();
                            c.OwnerId = awList[0].UserId;
                            Integer noOfCases = queueLoadMap.get(queueName).get(awList[0].UserId);
                            queueLoadMap.get(queueName).put(awList[0].UserId, noOfCases+1);                            
                        }
                    }

                    //when Zone and Queue are present on the Case
                    else if(c.Zone_Code__c != null && c.Queue__c != null && !caseLoadMap.isEmpty() && caseLoadMap.containsKey(c.Zone_Code__c)){

                        System.debug('*********Case Queue ' + c.Queue__c + 'Case Zone ' + c.Zone_Code__c);

                        //assign the Case to the Agent with least number of Cases
                        if(!caseLoadMap.isEmpty() && caseLoadMap.containsKey(c.Zone_Code__c) && caseLoadMap.get(c.Zone_Code__c).containsKey(c.Queue__c) && caseLoadMap.get(c.Zone_Code__c).get(c.Queue__c) != null){
                            System.debug('**********************Inside the round robin if block');
                            List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                            for(Id userId: caseLoadMap.get(c.Zone_Code__c).get(c.Queue__c).keySet()){
                                awList.add(new AssignmentWrapper(userId, caseLoadMap.get(c.Zone_Code__c).get(c.Queue__c).get(userId)));
                            }
                            awList.sort();
                            c.OwnerId = awList[0].UserId;
                            Integer noOfCases = caseLoadMap.get(c.Zone_Code__c).get(c.Queue__c).get(awList[0].UserId);
                            caseLoadMap.get(c.Zone_Code__c).get(c.Queue__c).put(awList[0].UserId, noOfCases+1);                            
                        } 
                        //if there is no user for that particular Queue/ There is no Queue - assign the ticket to Zone incharge/ROM/Ussigned Queue
                        else {
                            if(zoneManagerMap.containsKey(c.Zone_Code__c) && zoneManagerMap.get(c.Zone_Code__c) != null)
                                c.OwnerId = zoneManagerMap.get(c.Zone_Code__c);
                            else if(c.House_City__c != null && romMap.containsKey(c.House_City__c) && romMap.get(c.House_City__c) != null)
                                c.OwnerId = romMap.get(c.House_City__c);
                            else 
                                c.OwnerId = CaseServiceRequestTriggerHandler.unassignedCaseQueueId;                            
                        }
                    } 
                    // if there is no Zone Code and City on the House - assign the Case to Queue
                    else if(c.Zone_Code__c == null && c.House_City__c == null){
                        c.OwnerId = CaseServiceRequestTriggerHandler.unassignedCaseQueueId;
                    } 
                    // when there is Zone Code on the House - assign it ZOM/ROM/Queue
                    else if(c.Zone_Code__c != null){
                        if(zoneManagerMap.containsKey(c.Zone_Code__c) && zoneManagerMap.get(c.Zone_Code__c) != null)
                            c.OwnerId = zoneManagerMap.get(c.Zone_Code__c);
                        else if(c.House_City__c != null && romMap.containsKey(c.House_City__c) && romMap.get(c.House_City__c) != null)
                            c.OwnerId = romMap.get(c.House_City__c);
                        else 
                            c.OwnerId = CaseServiceRequestTriggerHandler.unassignedCaseQueueId;                     
                    } 
                    // if there is City on the House - assign to ROM/Queue
                    else if(c.House_City__c != null){
                        if(c.House_City__c != null && romMap.containsKey(c.House_City__c) && romMap.get(c.House_City__c) != null)
                            c.OwnerId = romMap.get(c.House_City__c);
                        else 
                            c.OwnerId = CaseServiceRequestTriggerHandler.unassignedCaseQueueId;                     
                    } 

                    if(c.OwnerId.getSObjectType() == User.SObjectType){
                        c.Next_Escalation_to__c = managerMap.get(c.OwnerId).ManagerId;
                        c.Next_Escalation_Email__c = managerMap.get(c.OwnerId).Manager.Email;
                    }

                    //set Entitlement after Assignment
                    if(c.EntitlementId == null){
                        c.EntitlementId = defaultEntitlementId;
                        c.SlaStartDate = System.now();
                    }
                }
            } catch (Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'Care');              
            }
    }

    /*  Created By : Mohan
        Purpose    : Assign the Move Out Case to the Queue Members */
    public static void moveOutAssignment(List<Case> caseList){

        try{
                List<Case> updatedCaseList = new List<Case>();
                Set<String> caseClosedStatus = new Set<String>(Label.Move_Out_Case_Closed_Status.split(';'));
                Set<String> moveOutQueues = new Set<String>(Label.Move_Out_Case_Queues.split(';'));
                Id moveOutCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
                Set<Id> caseRecordTypeIds = new Set<Id>{moveOutCaseRecordTypeId};

                Map<Id, Map<Id, Integer>> queueLoadMap = getCaseLoadForSelectedQueues(moveOutQueues, caseRecordTypeIds, caseClosedStatus);

                for(Case c: caseList){
                    //if there is no member in the Queue - Allow the Case to remain in the Queue
                    if(!queueLoadMap.isEmpty() && queueLoadMap.containsKey(c.OwnerId) && !queueLoadMap.get(c.OwnerId).isEmpty()){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: queueLoadMap.get(c.OwnerId).keySet()){
                            awList.add(new AssignmentWrapper(userId, queueLoadMap.get(c.OwnerId).get(userId)));
                        }
                        awList.sort();
                        c.OwnerId = awList[0].UserId; 
                        caseList.add(c);               
                    }
                }

                if(!updatedCaseList.isEmpty()){
                    update updatedCaseList;
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'Move Out Case Assignment');
            }
    }  
    
    /*  Created By : Mohan
        Purpose    : Generic Method to give the Queue Load Map */ 
    public static Map<Id, Map<Id, Integer>> getCaseLoadForSelectedQueues(Set<String> queues, Set<Id> caseRecordTypeIds, Set<String> caseStatus){

        Map<Id, Integer> openCaseMap = new Map<Id, Integer>();
        Map<Id, Map<Id, Integer>> queueLoadMap = new Map<Id, Map<Id, Integer>>();

        for(AggregateResult ar: [select OwnerId, count(Id) from case where status NOT IN : caseStatus and 
                                 RecordTypeId =: caseRecordTypeIds group by OwnerId]){
            openCaseMap.put((Id)ar.get('OwnerId'), (Integer)ar.get('expr0'));            
        } 

        for(Group grp: [select Id, Name from Group where Name =: queues]){
            queueLoadMap.put(grp.Name, new Map<Id, Integer>{});
        }

        for(GroupMember member: [Select UserOrGroupId, Group.Name, GroupId From GroupMember where Group.Name =: queues
                                 and UserOrGroupId in (select Id from User where isActive = true and isAvailableForAssignment__c = true)]){
            if(queueLoadMap.containsKey(member.GroupId)){
                queueLoadMap.get(member.GroupId).put(member.UserOrGroupId, openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId));
            } else{
                queueLoadMap.put(member.GroupId, new Map<Id, Integer>{member.UserOrGroupId => openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId)});
            }  
        }   

        return queueLoadMap;          
    }  


    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }

        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }   
}