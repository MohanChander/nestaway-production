@isTest
public  class OnBoardingWorkOrderControllerTest {
    
    public Static TestMethod void accTest(){
        Test.StartTest();
        Id recordTypeWorkOrderOnboarding = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_ON_BOARDING).getRecordTypeId();
        Id furnishedCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        House_Inspection_Checklist__c hic = new House_Inspection_Checklist__c();
        hic.House_ID__c=hos.id;
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Number_of_common_bathrooms__c=2;
        hic.Number_of_attached_bathrooms__c=2;
        insert hic;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.RecordTypeId=furnishedCaseRecordTypeId;
        c.House__c =hos.id;
        c.Status='Open';
        insert c;
        
        workOrder wrko=new workOrder();
        wrko.Status='Open';
        wrko.House__c=hos.id;
        wrko.RecordTypeId=recordTypeWorkOrderOnboarding;
        wrko.CaseId=c.id;
        insert wrko;
        
        Apexpages.currentPage().getParameters().put('id', wrko.id);
        ApexPages.StandardController sC = new ApexPages.standardController(c);
        OnBoardingWorkOrderController oc = new OnBoardingWorkOrderController(sc);
        oc.save();
        Test.StopTest();
    }
}