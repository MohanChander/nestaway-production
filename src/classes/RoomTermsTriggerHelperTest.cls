@isTest 
public class RoomTermsTriggerHelperTest {
    public Static TestMethod void testMethod2(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        hos.Booking_Type__c='Shared House';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        List<Room_terms__c> rtList= new List<Room_terms__c>();
        
        map<id,Room_terms__c> mapnew = new map<id,room_terms__c>();
        map<id,Room_terms__c> mapold = new map<id,room_terms__c>();
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        rt.Payment__c='Off';
         rt.Number_of_beds__c='1';
        rt.Actual_Room_Rent__c=2386;
       
        insert rt;
       
        Set<id> roomIdSet= new Set<id>();
        roomIdSet.add(rt.Id);
      
        mapnew.put(rt.id,rt);
        rt.Payment__c='on';
         rt.Actual_Room_Rent__c=222386;
        update rt;
        mapold.put(rt.id,rt);
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
       /* rtlist.add(rt);
        Bed__c bd = new bed__C();
       bd.Room_Terms__c=rt.Id;
           insert bd;*/
          List<AggregateResult> bedAggregateList = BedSelector.getBedAggregationFromRoomTerms(roomIdSet);
        
        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        RoomTermsTriggerHelper.checkRoomTermValidationBeforeUpdate(mapnew,mapold);
        
    }
     public Static TestMethod void testMethod3(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        hos.Booking_Type__c='Shared House';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        List<Room_terms__c> rtList= new List<Room_terms__c>();
        
        map<id,Room_terms__c> mapnew = new map<id,room_terms__c>();
        map<id,Room_terms__c> mapold = new map<id,room_terms__c>();
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        rt.Payment__c='Off';
        
       
        insert rt;
       
        Set<id> roomIdSet= new Set<id>();
        roomIdSet.add(rt.Id);
        List<AggregateResult> bedAggregateList = BedSelector.getBedAggregationFromRoomTerms(roomIdSet);
        mapnew.put(rt.id,rt);
        rt.Payment__c='on';
       
        update rt;
        mapold.put(rt.id,rt);
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        rtlist.add(rt);
      
        
        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        RoomTermsTriggerHelper.checkRoomTermValidationBeforeUpdate(mapnew,mapold);
        
    }
}