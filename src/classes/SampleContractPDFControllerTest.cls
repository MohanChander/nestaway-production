@isTest
public Class SampleContractPDFControllerTest{

    public Static TestMethod void contractPDFTest(){
        Test.StartTest();
            
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            accObj.phone = '1234567890';
            accObj.BillingCity            ='Test';
            accObj.BillingCountry         ='India';
            accObj.BillingPostalCode      ='560034';
            accObj.BillingState           ='Karnataka'; 
            accObj.BillingStreet          ='116, B2, NGV'; 
            insert accObj;
            
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            insert opp;
            
            Bank_Detail__c bankDetails = new Bank_Detail__c();
            bankDetails.Account_Number__c = '12312312312312312313';
            bankDetails.Bank_Name__c = 'Test Bank Name';
            bankDetails.Branch_Name__c = 'Test Branch Name';
            bankDetails.IFSC_Code__c = 'UTIB0000206';
            bankDetails.Related_Account__c = accObj.Id;
            insert bankDetails;
            
            PriceBook2 priceBk = new PriceBook2();
            priceBk.Name = 'TestPriceBook';
           // priceBk.isStandard = FALSE;
            insert priceBk;
            
            Product2 prod = new Product2();
            prod.Name = 'testProd';
            prod.isActive = TRUE;
            insert prod;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            cont.PriceBook2Id = priceBk.Id;
            cont.status = 'Draft';
            insert cont;
            
            Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
            House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
            houseIC.RecordTypeId = recordTypeId;
            houseIC.Opportunity__c = opp.Id;
            houseIC.Status__c = 'House Inspection In Progress';           
            insert houseIC;
            
            Room_Terms__c roomTerm = new Room_Terms__c();
            roomTerm.Name = 'Test Room';
            roomTerm.Contract__c = cont.Id;
            roomTerm.Bedroom_Number__c = 'Bedroom No.1';
            roomTerm.Monthly_Base_Rent_Per_Room__c = 12000;
            roomTerm.Number_of_beds__c = '2';
            insert roomTerm;
            
            PageReference pageRef = Page.FinalAgreement;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cont);
            ApexPages.currentPage().getParameters().put('id',cont.id);
            SampleContractPDFController ec = new SampleContractPDFController(sc);
            ec.getPermanentAddress();
            //ec.getAddressDetails();
            SampleContractPDFController.AddressDetails addDetail = new SampleContractPDFController.AddressDetails('test','test','test','560068','test');
            addDetail.street();
            addDetail.city();
            addDetail.state();
            addDetail.pincode();
            addDetail.country();
            addDetail.shippingAdress();
            
        Test.StopTest();
        
    }
}