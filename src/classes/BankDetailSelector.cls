public class BankDetailSelector {
    public static list<Bank_Detail__c> getBankDetailByAccount(Set<id> accIdSet)
    {
        return  [Select id,name,Account_Number__c,Bank_Name__c,Branch_Name__c,IFSC_Code__c,Related_Account__c,Api_Success__c 
                 from Bank_Detail__c where Related_Account__c=:accIdSet];
    }
    public static list<Bank_Detail__c> getBankDetailByBankId(Set<id> bnkIdlist)
    {
        return  [Select id,name,Account_Firstname__c,Account_Number__c,Bank_Name__c,Branch_Name__c,IFSC_Code__c,Related_Account__c,Api_Success__c 
                 from Bank_Detail__c where Id=:bnkIdlist];
    }
    public static list<Bank_Detail__c> getBankDetailByContact(Set<id> contIdSet)
    {
        return  [Select id,name,Account_Number__c,Bank_Name__c,Branch_Name__c,IFSC_Code__c,
                 Related_Account__c,Contact__c,Api_Success__c 
                 from Bank_Detail__c where Contact__c =: contIdSet];
    }
}