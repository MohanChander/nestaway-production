/*  Created By deeepak 
*  Purpose : To handle checklist functionality*/

public class HouseInspectionTriggerHelper {

    // Variable Initialization
    public static List<House_Inspection_Checklist__c> mimoHicList = new List<House_Inspection_Checklist__c>();
    
    /*  Created By deeepak 
     *Purpose :To update */
    public static void UpdatePanCardOnAccount(Map<Id,House_Inspection_Checklist__c> newMap){
        system.debug('*******UpdateAccountPanCard');
        try{
            Set<Id> SetAccountId = new Set<Id>();
           map<Id,String> MapAccountPanCard = new map<Id,String>();
            List<Account> accountList= new List<Account>();
            List<House_Inspection_Checklist__c> hicList= [Select id,PAN_Card_Number__c,Opportunity__r.accountid from House_Inspection_Checklist__c where id in:newMap.keyset()];
            System.debug('****hicList'+hicList);
            for(House_Inspection_Checklist__c HIC:hicList){
                SetAccountId.add(hic.Opportunity__r.accountid);
                MapAccountPanCard.put(hic.Opportunity__r.accountid ,hic.PAN_Card_Number__c);
            }
            List<Account> accList= [Select id,PAN_Number__c from Account where id in :SetAccountId];
            for(Account acc:accList){
                acc.PAN_Number__c=MapAccountPanCard.get(Acc.id);
                accountList.add(acc);
            }
            update accountList;
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        } 
    }

    /*  Created By : Mohan
        Purpose    : To Update the MIMO HIC when Move in or Move Out is Updated*/
    public static void updateMimoHic(List<House_Inspection_Checklist__c> hicList){

        Map<Id, Id> latestMoveInChecklistMap = new Map<Id, Id>();
        Map<Id, Id> latestMoveOutChecklistMap = new Map<Id, Id>();
        Map<Id, Id> mimoChecklistMap = new Map<Id, Id>();
        Set<Id> houseIdSet = new Set<Id>();

        for(House_Inspection_Checklist__c hic: hicList){
            houseIdSet.add(hic.House__c);
        }

        //All the Hic's related to the House
        List<House_Inspection_Checklist__c> queriedHicList = ChecklistSelector.getMoveInMoveOutChecklistRelatedToHouse(houseIdSet);

        for(House_Inspection_Checklist__c hic: queriedHicList){
            if(hic.Type_Of_HIC__c == Constants.CHECKLIST_TYPE_MOVE_IN_CHECK && !latestMoveInChecklistMap.containsKey(hic.HouseForMIMO__c)){
                latestMoveInChecklistMap.put(hic.HouseForMIMO__c, hic.Id);
            } else if(hic.Type_Of_HIC__c == Constants.CHECKLIST_TYPE_MOVE_OUT_CHECK && !latestMoveOutChecklistMap.containsKey(hic.HouseForMIMO__c)){
                latestMoveOutChecklistMap.put(hic.HouseForMIMO__c, hic.Id);
            } else if(hic.Type_Of_HIC__c == Constants.CHECKLIST_TYPE_MIMO_CHECK){
                mimoChecklistMap.put(hic.HouseForMIMO__c, hic.Id);
            }
        }

        for(House_Inspection_Checklist__c hic: hicList){
            if(hic.Type_Of_HIC__c == Constants.CHECKLIST_TYPE_MOVE_IN_CHECK && !latestMoveInChecklistMap.isEmpty()
               && latestMoveInChecklistMap.containsKey(hic.HouseForMIMO__c) && latestMoveInChecklistMap.get(hic.HouseForMIMO__c) == hic.Id){
                updateMimoHicHelper(hic, mimoChecklistMap.get(hic.HouseForMIMO__c));
            } else if(hic.Type_Of_HIC__c == Constants.CHECKLIST_TYPE_MOVE_OUT_CHECK && !latestMoveInChecklistMap.isEmpty()
               && latestMoveInChecklistMap.containsKey(hic.HouseForMIMO__c) && latestMoveInChecklistMap.get(hic.HouseForMIMO__c) == hic.Id){
                updateMimoHicHelper(hic, mimoChecklistMap.get(hic.HouseForMIMO__c));
            }
        }

        if(!mimoHicList.isEmpty())
            update mimoHicList;
    } 

    /*  Created By : Mohan
        Pupose     : Update the Move In and Move Out Checklist fields to MIMO */
    public static void updateMimoHicHelper(House_Inspection_Checklist__c hic, Id mimoHicId){
        House_Inspection_Checklist__c syncedHic = new House_Inspection_Checklist__c();
        syncedHic = hic.clone(false, true, false, false);
        syncedHic.Id = mimoHicId;
        mimoHicList.add(syncedHic);
    }

/*    *************************Added BY BAIBHAV
    *******************************/

     public static id recordTypeIddocumentCollectionChk=Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_TYPE_DCC).getRecordTypeId();
   public static id recordTypeIdHicChk=Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_TYPE_HIC).getRecordTypeId();


    public static void Dccvalidation(List<House_Inspection_Checklist__c> chkList)
    {
       for(House_Inspection_Checklist__c hic:chkList)
        { 
                if((hic.Req_Skip_Doc_Reviewer__c==false) || (hic.Req_Skip_Doc_Reviewer__c==true && hic.Approval_Status__c==Constants.HIC_APPROVAL_STATUS_DOC_REVIEW_NEEDED))
                {
                    if((hic.Address_Proof_Document__c==null || hic.Address_Proof_ID__c==null) && (hic.CreatedDate!=null))
                    hic.adderror('Address prove document And Address prove id is required');

                     if((hic.Verified_Electric_Bill_For_No_Dues__c==null || hic.Electric_Bill_Account_Number__c==null) && (hic.CreatedDate!=null))
                    hic.adderror('Electricity Bill and Bill Account number Required');

                     if((hic.House_Ownership_Document__c==null) && (hic.CreatedDate!=null))
                    hic.adderror('House Ownership Document is Required');
                }
                if((hic.Status__c ==Constants.CHECKLIST_APPROVAL_STATUS_HOUSE_INSPECT_COMP) && (hic.House_Lat_Long_details__Latitude__s==null || hic.House_Lat_Long_details__Latitude__s==null))
                 hic.adderror('Please enter House Lat Long Details before doing House Inspection Checklist is completed');
        }

    }

/**********************************************
    Created By : Mohan
    Purpose    : When the MIMO HIC is updated - Update all the open Move in and Move Out HIC's
**********************************************/
    public static void syncMoveInMoveOutChecklistsWithMIMO(List<House_Inspection_Checklist__c> mimoHicList){

        System.debug('*******************syncMoveInMoveOutChecklistsWithMIMO');

        try{
                Set<Id> houseIdSet = new Set<Id>();
                Map<Id, House_Inspection_Checklist__c> mimoHicMap = new Map<Id, House_Inspection_Checklist__c>();
                List<House_Inspection_Checklist__c> UpdatedHicList = new List<House_Inspection_Checklist__c>();

                for(House_Inspection_Checklist__c mimoHic: mimoHicList){
                    houseIdSet.add(mimoHic.HouseForMIMO__c); 
                    mimoHicMap.put(mimoHic.HouseForMIMO__c, mimoHic);
                }    

                //query for the unverified Move In and Move Out Hic checklists
                List<House_Inspection_Checklist__c> queriedHicList = ChecklistSelector.getUnverifiedChecklists(houseIdSet);

                for(House_Inspection_Checklist__c hic: queriedHicList){
                    House_Inspection_Checklist__c updatedHic = new House_Inspection_Checklist__c();
                    updatedHic = mimoHicMap.get(hic.HouseForMIMO__c).clone(false, true, false, false);
                    updatedHic.Id = hic.Id;
                    updatedHic.Name = hic.Name;
                    updatedHic.CheckList_Verified__c = 'No';
                    updatedHic.Work_Order__c = hic.Work_Order__c;
                    updatedHic.RecordTypeId = hic.RecordTypeId;
                    updatedHic.Type_Of_HIC__c = hic.Type_Of_HIC__c;
                    updatedHic.MIMO_Comment__c = hic.MIMO_Comment__c;
                    updatedHic.Agreement__c = hic.Agreement__c;
                    UpdatedHicList.add(updatedHic);
                }

                if(!UpdatedHicList.isEmpty()){
                    update UpdatedHicList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Sync Move In Move Out HIC with MIMO HIC');      
            }                    
    }
    // Added By deepak
    // To change the reocrd type id of bathroom and ric
    /*public static void changeRecordTypeIdOfRicAndBic(List<House_Inspection_Checklist__c> hicList){
        Id furnishedRecordTypeIdBath = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get('Precheck Furnished').getRecordTypeId();
        Id unfurnishedRecordTypeIdBath = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get('Precheck Unfurnished').getRecordTypeId();
        Id furnishedRecordTypeIdRoom = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get('Precheck Furnished').getRecordTypeId();
        Id unfurnishedRecordTypeIdRoom = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get('Precheck Unfurnished').getRecordTypeId();
        Set<Id> setOfHic = new Set<Id>();
        List<Room_Inspection__c> ricList = new  List<Room_Inspection__c>();
        List<Bathroom__c> bathList = new List<Bathroom__c>();
        List<House_Inspection_Checklist__c> houseICList = new List<House_Inspection_Checklist__c>();
        try{
            for(House_Inspection_Checklist__c each: hicList){
                setOfHic.add(each.id);
            }
            if(!setOfHic.isEmpty()){
                houseICList= [Select id,Furnishing_condition__c,(Select id,RecordTypeId,Furnishing_Type__c  from Room_Inspection__R),(Select id,RecordTypeId from Bathrooms__r),(Select id,RecordTypeId from AttachedBathroom__r) from House_Inspection_Checklist__c where id in:setOfHic];
            }
            if(!houseICList.isEmpty()){
                for(House_Inspection_Checklist__c hic :houseICList){
                    for(Room_Inspection__c ric : hic.Room_Inspection__R){
                        if(hic.Furnishing_condition__c == Constants.FURNISHING_CONDITION_UNFURNISHED){
                            ric.recordtypeId=unfurnishedRecordTypeIdRoom;
                        }
                        else{
                            ric.RecordTypeId=furnishedRecordTypeIdRoom;
                        }
                        ricList.add(ric);
                    }
                    for(Bathroom__c bic : hic.Bathrooms__r){
                        if(hic.Furnishing_condition__c == Constants.FURNISHING_CONDITION_UNFURNISHED){
                            bic.recordtypeId=unfurnishedRecordTypeIdBath;
                        }
                        else{
                            bic.RecordTypeId=furnishedRecordTypeIdBath;
                        }
                        bathList.add(bic);
                    }
                    for(Bathroom__c bic1 : hic.AttachedBathroom__r){
                        if(hic.Furnishing_condition__c == Constants.FURNISHING_CONDITION_UNFURNISHED){
                            bic1.recordtypeId=unfurnishedRecordTypeIdBath;
                        }
                        else{
                            bic1.RecordTypeId=furnishedRecordTypeIdBath;
                        }
                        bathList.add(bic1);
                    }
                }
                if(!ricList.isEmpty()){
                    update ricList;
                }
                if(!bathList.isEmpty()){
                    update bathList;
                }
            }
        }
        Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'Contract stage time');
        }
    }*/

}