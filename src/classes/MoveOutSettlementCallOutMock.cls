@isTest
global class MoveOutSettlementCallOutMock implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest request) {

        SendAPIRequests.RPDataWrapper rpWrap = new SendAPIRequests.RPDataWrapper();
        rpWrap.salesforce_mappable_id = 'TLX6LDGZYJRIC0';
        rpWrap.Id = 12345;

        SendAPIRequests.MoveoutWithIssueRPSettlementRespWrapper settlementWrap = new SendAPIRequests.MoveoutWithIssueRPSettlementRespWrapper();
        settlementWrap.success = 'true';
        settlementWrap.info = 'abc';
        settlementWrap.data = rpWrap;
        settlementWrap.respCode = '200';
        settlementWrap.jsonRespBody = 'abc';

        String jsonString = JSON.serialize(settlementWrap);

        WBMIMOJSONWrapperClasses.Data bedReleaseDataObj = new WBMIMOJSONWrapperClasses.Data();
        bedReleaseDataObj.deduction_amount = '-1000';
        bedReleaseDataObj.refund_amount = '1000';
        bedReleaseDataObj.total_charges = '1000';

        WBMIMOJSONWrapperClasses.BedreleaseResponse bedReleaseRespObj = new WBMIMOJSONWrapperClasses.BedreleaseResponse();
        bedReleaseRespObj.data = bedReleaseDataObj;
        bedReleaseRespObj.success = true;
        bedReleaseRespObj.info = 'abc';

        String bedReleaseJsonString = JSON.serialize(bedReleaseRespObj);

        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');

        if(request.getEndpoint().contains('MoveOutSettlementRPAPI')){
            response.setBody(jsonString);
        } else {
            response.setBody(bedReleaseJsonString);
        }
        
        response.setStatusCode(200);
        return response; 
    }
}