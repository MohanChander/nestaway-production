/******************************************************************
Added By baibhav
date: 13/11/2017
****************************************************************/
@Istest
public  class workOrderOnboardingHelperTest {

 public static TestMethod void  Test1(){
 	Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

 	 House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
		
      	 Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        update c;
      
         Test.StopTest();

 }
  public static TestMethod void  Test11(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_VERIFICATION;
        update c;
        Test.StopTest();

 }
   public static TestMethod void  Test12(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        update c;

       
         Test.StopTest();

 }
   public static TestMethod void  Test13(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        update c;

       
         Test.StopTest();

 }
 public static TestMethod void  Test2(){
 	Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
 	
 	 House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_VERIFICATION;
        update c;

     
        Test.StopTest();
     
 }	
  public static TestMethod void  Test21(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        update c;

       
        Test.StopTest();
     
 }  

  public static TestMethod void  Test22(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        update c;

       
        Test.StopTest();
     
 }  
  
 public static TestMethod void  Test3(){
 	Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
 	
 	 House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
        	wrk.Status='Completed';
        }
      
        update wrklist;
        
         Test.StopTest();  
 }	
  public static TestMethod void  Test30(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_KEY_HANDLING;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        List<Detail__c> delist=new List<Detail__c>();
        for(workorder wrk:wrklist){
            Detail__c d=new Detail__c();
            d.name='main door';
            d.Keys_Collected_For__c='Main Door';
            d.Number_of_Keys_collected__c='1';
            d.Work_Order__c=wrk.id;
            delist.add(d);
            wrk.Status='Completed';

        }
        insert delist;
      
        update wrklist;
        
         Test.StopTest();  
 }  
  public static TestMethod void  Test31(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
     
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test32(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_VERIFICATION;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
   
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test33(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
      

        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test34(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }

        update wrklist;
        
         Test.StopTest();  
 } 
  public static TestMethod void  Test4(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
      
        update wrklist;
        
         Test.StopTest();  
     }
  public static TestMethod void  Test40(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_KEY_HANDLING;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        List<Detail__c> delist=new List<Detail__c>();
        for(workorder wrk:wrklist){
            Detail__c d=new Detail__c();
            d.name='main door';
            d.Keys_Collected_For__c='Main Door';
            d.Number_of_Keys_collected__c='1';
            d.Work_Order__c=wrk.id;
            delist.add(d);
            wrk.Status='Completed';

        }
        insert delist;
      
        update wrklist;
        
         Test.StopTest();  
 }  
  public static TestMethod void  Test41(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
     
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test42(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_VERIFICATION;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
   
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test43(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
      

        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test44(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }

        update wrklist;
        
         Test.StopTest();  
 }
   public static TestMethod void  Test5(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
      
        update wrklist;
        
         Test.StopTest();  
     } 
 public static TestMethod void  Test50(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_KEY_HANDLING;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        List<Detail__c> delist=new List<Detail__c>();
        for(workorder wrk:wrklist){
            Detail__c d=new Detail__c();
            d.name='main door';
            d.Keys_Collected_For__c='Main Door';
            d.Number_of_Keys_collected__c='1';
            d.Work_Order__c=wrk.id;
            delist.add(d);
            wrk.Status='Completed';

        }
        insert delist;
      
        update wrklist;
        
         Test.StopTest();  
 }  
  public static TestMethod void  Test51(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
     
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test52(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_VERIFICATION;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
   
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test53(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
      

        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test54(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }

        update wrklist;
        
         Test.StopTest();  
 }   
  public static TestMethod void  Test61(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
     
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test62(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='Studio';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_VERIFICATION;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
   
        update wrklist;
        
         Test.StopTest();  
 }  
  public static TestMethod void  Test71(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
     
        update wrklist;
        
         Test.StopTest();  
 }  
 public static TestMethod void  Test72(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
    
     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 R';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_UNFURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_VERIFICATION;
        c.House__c = hos.Id;
        insert c;
         Test.StartTest();
        List<workorder> wrklist=[select id,recordtypeID,status,case_status__c,house__c,caseID from workorder where caseID=:c.id];
        for(workorder wrk:wrklist){
            wrk.Status='Completed';
        }
   
        update wrklist;
        
         Test.StopTest();  
 }  
}