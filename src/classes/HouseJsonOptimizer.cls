/*   Created By: Mohan - WarpDrive Tech Works
     Purpose   : When any House Child Object/ House Object is edited fire an API for Web Entity = House */


public class HouseJsonOptimizer {

    public static Boolean FIRST_RUN = true;
    public static Boolean HOUSE_WEB_ENTITY_HOUSE_FLAG = false;      //flag to allow the Web Entity House API to fire
    public static Boolean CONTRACT_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean CHECKLIST_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean ROOMTERMS_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean BED_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean BATHROOM_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean PHOTO_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean ROOMINSPECTION_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean ROOMRENT_WEB_ENTITY_HOUSE_FLAG = false;
    public static Boolean RECORDTYPEISMOVEIN = false;
    public static Boolean RECORDTYPEISMOVEOUT = false;

    private static boolean run = true;
    public static Integer triggerCount;
    public static boolean runOnce(){
        if(run){
         run=false;
         return true;
        }else{
            return run;
        }
    }


    //Initializes the Boolean variable for the Object for which transaction has started
    public static void houseJsonObjectInitializer(String objectName){

        System.debug('*********houseJsonObjectInitializer ' + objectName);
        System.debug('First run flag ' + FIRST_RUN);

        if(FIRST_RUN){
            
            FIRST_RUN = false;

            if(objectName == Constants.OBJECT_HOUSE)
                HOUSE_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_CONTRACT)
                CONTRACT_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_CHECKLIST)
                CHECKLIST_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_ROOMTERMS)
                ROOMTERMS_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_BED)
                BED_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_BATHROOM)
                BATHROOM_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.OBJECT_PHOTO)
                PHOTO_WEB_ENTITY_HOUSE_FLAG = true;     
            else if(objectName == Constants.OBJECT_ROOMINSPECTION)      
                ROOMINSPECTION_WEB_ENTITY_HOUSE_FLAG = true;
            else if(objectName == Constants.ROOM_RENT_PAGE)      
                  ROOMRENT_WEB_ENTITY_HOUSE_FLAG = true;
        }                                             
    }

    //send callout to the webapp for the set of House Id's
    public static void doCallout(Set<Id> houseIdSet){
        if(!houseIdSet.isEmpty()){
            List<House__c> houseList = HouseSelector.getHouseStatus(houseIdSet);

            for(House__c house: houseList){
              if(house.Stage__c == Constants.HOUSE_STAGE_HOUSE_LIVE || house.Stage__c == Constants.HOUSE_STAGE_OFFBOARDED){
                House2Json.createJson(house.Id);
              }
            }
        } 
    }
}