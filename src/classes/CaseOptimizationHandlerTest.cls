/******************************************************************
Added By baibhav
date: 13/11/2017
****************************************************************/
@Istest
public class CaseOptimizationHandlerTest {
     public static TestMethod void  Test1(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_KEY_HANDLING;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        update c;
      
         Test.StopTest();

 }
     public static TestMethod void  Test10(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

        c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        update c;
      
         Test.StopTest();

 }
  public static TestMethod void  Test11(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

    
        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_VERIFICATION;
        update c;
        Test.StopTest();

 }
   public static TestMethod void  Test12(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;


        
        Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_PHOTOGRAPHY;
        update c;

       
         Test.StopTest();

 }
   public static TestMethod void  Test13(){
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

     House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.House_Layout__c='1 BHK';
        hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;

        
         Test.StartTest();
        Case c=  new Case();
        c.RecordTypeId=devRecordTypeId;
        c.House__c=hos.id;
        c.Status=Constants.CASE_STATUS_MAINTENANCE;
        c.House__c = hos.Id;
        insert c;

     

        c.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        update c;

       
         Test.StopTest();

 }
 public static TestMethod void  Test14(){
Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();

 House__c hos= new House__c();
    hos.name='house1';
    hos.Onboarding_Zone_Code__c = '123';
    hos.Assets_Created__c=false;
    hos.Booking_Type__c = 'Shared House';
    hos.House_Layout__c='1 BHK';
    hos.Furnishing_Type__c=Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
    insert hos;

    House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);
    
     Test.StartTest();
    Case c=  new Case();
    c.RecordTypeId=devRecordTypeId;
    c.House__c=hos.id;
    c.Status=Constants.CASE_STATUS_MAINTENANCE;
    c.House__c = hos.Id;
    insert c;

    c.Status=Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
    update c;
   // hos.Offboarding_Reason__c='Interfering Owner';
   // update hos;

   // c.Status=Constants.WORKORDER_STATUS_DROPPED;
    //c.Dropping_Reason__c='test';
    // update c;
  
     Test.StopTest();

}
}