@IsTest
public class BedReleaseClassTest {
    public static   Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
    
    Public Static TestMethod Void doTest(){
       
        Case c=  createmoveout();
        c.Bed_Released_API_Status__c='False';
        c.Status=CONSTANTS.CASE_STATUS_MOVEDOUTALLOK;
        update c;
        Apexpages.currentPage().getParameters().put('sourceId', c.id);
        ApexPages.StandardController sC = new ApexPages.standardController(c);
        BedReleaseClass bc= new BedReleaseClass(Sc);
        bc.BedRelease();
          
    }
    Public Static TestMethod Void doTest1(){
       
        Case c=  createmoveout();
        c.Bed_Released_API_Status__c='False';
        c.Status=CONSTANTS.CASE_STATUS_MOVEDOUTWITHISSUE;
        update c;
        Apexpages.currentPage().getParameters().put('sourceId', c.id);
        ApexPages.StandardController sC = new ApexPages.standardController(c);
        BedReleaseClass bc= new BedReleaseClass(Sc);
        bc.BedRelease();
       
    }
    Public Static TestMethod Void doTest2(){
      
        
        Case c=  createmoveout();
        c.Bed_Released_API_Status__c='True';
        update c;
        Apexpages.currentPage().getParameters().put('sourceId', c.id);
        ApexPages.StandardController sC = new ApexPages.standardController(c);
        BedReleaseClass bc= new BedReleaseClass(Sc);
        bc.BedRelease();
       
    }
    public static CAse createmoveout()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.OwnerId=newuser.id;        
        insert hos;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Move_in_date__c=System.today()-2;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='SD Default';
        c.Booked_Object_Type__c='House';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MoveOut';
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;
    }
}