@isTest
public class CaseServiceRequestMiMoHandlerTest {
    public static Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
    Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    public static   Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
   Public Static Id mimoBicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK).getRecordTypeId(); 
     Public Static Id operationalRtMImoandService= Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(MissingItemsConstants.OPERATIONAL_PROCESS_RT_MIMO_SERVICE).getRecordTypeId();
    Public Static Id moveInHicRTId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MOVE_IN_CHECK).getRecordTypeId();
    Public Static Id hicRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('House Inspection Checklist').getRecordTypeId();
    Public Static Id mimoHicRTId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK).getRecordTypeId();
    Public Static Id moveOutHicRTId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MOVE_OUT_CHECK).getRecordTypeId();
	 Public Static Id moveInRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_IN_CHECK).getRecordTypeId();
    Public Static Id moveOutRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
    Public Static Id mimoRicRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_TYPE_MIMO_CHECK).getRecordTypeId();
    Public Static TestMethod Void doTest(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_AGREEMENT;
        c.Sub_Category__c = Constants.CASE_SUB_CATEGORY_AGREEMENT;
        cList.add(c);
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
    }
    Public Static TestMethod Void doTest1a(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_LIVING_ROOM;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_DINING_ROOM;
        cList.add(c1);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
      Public Static TestMethod Void doTest1b(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c2=  creatSeervice();
        c2.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
        c2.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_ROOM_CLEANED;
        cList.add(c2);
        Case c3=  creatSeervice();
        c3.Category__c=MissingItemsConstants.CASE_CATEGORY_CLEANING;
        c3.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_TENANT_BATHROOM_CLEANED;
        cList.add(c3);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest2(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_CONNECTIVITY;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_DTH;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_CONNECTIVITY;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_INTERNET;
        cList.add(c1);
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
    }
    
    Public Static TestMethod Void doTest3a(){
        
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRICAL;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_TUBELIGHT;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRICAL;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_CEILING_FAN;
        cList.add(c1);
        
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        Test.stopTest();
        
    }
    Public Static TestMethod Void doTestb(){
        
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c3=  creatSeervice();
        c3.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRICAL;
        c3.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_Fans;
        cList.add(c3);
        Case c4=  creatSeervice();
        c4.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRICAL;
        c4.Sub_Category__c = Constants.CASE_SUB_CATEGORY_TUBELIGHT;
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        Test.stopTest();
        
    }
    Public Static TestMethod Void doTest4(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRONICS;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_TV;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRONICS;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_FRIDGE;
        cList.add(c1);
        Case c2=  creatSeervice();
        c2.Category__c=MissingItemsConstants.CASE_CATEGORY_ELECTRONICS;
        c2.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_WASHING_MACHINE;
        cList.add(c2);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
    }
    Public Static TestMethod Void doTest5a(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_SOFA;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_CENTER_TABLE;
        cList.add(c1);
        Case c2=  creatSeervice();
        c2.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c2.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_DINING_TABLE;
        cList.add(c2);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest5b(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        
        Case c3=  creatSeervice();
        c3.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c3.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_DINING_CHAIRS;
        cList.add(c3);
        Case c4=  creatSeervice();
        c4.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c4.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_GAS_PIPES;
        cList.add(c4);
        Case c5=  creatSeervice();
        c5.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c5.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_STOVE;
        cList.add(c5);      
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest5ca(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        
        Case c6=  creatSeervice();
        c6.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c6.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_CROCKERY_SET;
        cList.add(c6);
        Case c64=  creatSeervice();
        c64.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c64.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_PILLOW;
        cList.add(c64);
        Case c7=  creatSeervice();
        c7.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c7.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_BED_SHEET;
        cList.add(c7);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest5cv(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c11=  creatSeervice();
        c11.TypeOfInspection__c='hic';
        c11.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c11.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_WINDOW_CURTAINS;
        cList.add(c11);
        Case c111=  creatSeervice();
        c111.TypeOfInspection__c='ric';
        c111.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c111.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_WINDOW_CURTAINS;
        cList.add(c111);
        Case ca=  creatSeervice();
        ca.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        ca.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_COT;
        cList.add(ca);
        
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest5cva(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        
        Case c09=  creatSeervice();
        c09.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c09.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_SIDE_TABLE;
        cList.add(c09);
        Case c08=  creatSeervice();
        c08.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c08.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_MIRROR;
        cList.add(c08);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest5cvav(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case cav=  creatSeervice();
        cav.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        cav.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_MATTRESS;
        cList.add(cav);  
        Case c00=  creatSeervice();
        c00.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c00.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_AC;
        cList.add(c00);
        Case c001=  creatSeervice();
        c001.Category__c=MissingItemsConstants.CASE_CATEGORY_FURNISHING;
        c001.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_CHAIR;
        cList.add(c001);
        
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
        
    }
    Public Static TestMethod Void doTest6(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        
        Case c2=  creatSeervice();
        c2.Category__c=MissingItemsConstants.CASE_CATEGORY_KEYS;
        c2.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_ROOM_KEYS;
        cList.add(c2);
        
        Case c4=  creatSeervice();
        c4.Category__c=MissingItemsConstants.CASE_CATEGORY_KEYS;
        c4.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_WARDROBE_KEYS;
        cList.add(c4);
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
    }
    
    Public Static TestMethod Void doTest6b(){
        List<Case>  cList =  new List<Case>();
        Set<Id>  updatedcList =  new Set<Id>();
        Test.startTest();
        Case c=  creatSeervice();
        c.Category__c=MissingItemsConstants.CASE_CATEGORY_KEYS;
        c.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_WARDROBE_KEYS;
        cList.add(c);
        Case c1=  creatSeervice();
        c1.Category__c=MissingItemsConstants.CASE_CATEGORY_KEYS;
        c1.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_ROOM_KEYS;
        cList.add(c1);
        Case c3=  creatSeervice();
        c3.Category__c=MissingItemsConstants.CASE_CATEGORY_KEYS;
        c3.Sub_Category__c = MissingItemsConstants.CASE_SUB_CATEGORY_MAIN_DOOR_KEYS;
        cList.add(c3);
        
        Test.stopTest();
        CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(cList);
        
    }
    
    public static CAse creatSeervice()
    {    
         Bathroom__c bc= new Bathroom__c();
          insert bc;
         Bathroom__c bc2= new Bathroom__c();
          bc2.RecordTypeId=mimoBicRTId;
          bc2.Water_Heater__c=MissingItemsConstants.WATER_HEATER_PRESENT_AND_WORKING;
          bc2.Mirror__c=MissingItemsConstants.MIRROR_PRESENT_AND_IN_GOOD_CONDITION;
          bc2.CheckFor__c= bc.id;
          insert bc2;
        problem__c p= new problem__c();
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;        
        House_Inspection_Checklist__c bc21= new House_Inspection_Checklist__c();
        bc21.RecordTypeId=mimoHicRTId;
        bc21.DTH__c=MissingItemsConstants.DTH_INSTALLED;
        bc21.TV__c = MissingItemsConstants.TV_PRESENT_AND_NOT_WORKING;
        bc21.Fridge__c=MissingItemsConstants.FRIDGE_PRESENT_AND_WORKING;
        bc21.Washing_Machine__c = MissingItemsConstants.WASHING_MACHINE_PRESENT_AND_NOT_WORKING;
        bc21.Fridge__c=MissingItemsConstants.FRIDGE_PRESENT_AND_WORKING;
        bc21.Washing_Machine__c = MissingItemsConstants.WASHING_MACHINE_PRESENT_AND_NOT_WORKING;
        bc21.Sofa__c=MissingItemsConstants.SOFA_YES;
        bc21.Center_Table__c = MissingItemsConstants.CENTER_TABLE_PRESENT_AND_IN_GOOD_CONDITION;
        bc21.Dining_Table__c=MissingItemsConstants.DINING_TABLE_YES;
        bc21.Dining_Chairs__c = MissingItemsConstants.DINING_CHAIRS_SOME_CHAIR_MISSING;
        bc21.Dining_Table__c=MissingItemsConstants.DINING_TABLE_YES;
        bc21.Gas_Regulator__c = MissingItemsConstants.GAS_PIPES_PRESENT_AND_NOT_WORKING;
        bc21.Stove__c = MissingItemsConstants.STOVE_PIPES_PRESENT_AND_WORKING;
        bc21.Crockery_Set__c = MissingItemsConstants.CROCKERY_SET_YES_AND_COMPLETE;
        bc21.HouseForMIMO__c= hos.id;
        insert bc21;
         Room_Inspection__c bc111= new Room_Inspection__c();
        insert bc111;
        Room_Inspection__c bc2321= new Room_Inspection__c();
        bc2321.RecordTypeId=mimoRicRTId;
        bc2321.AC__c=MissingItemsConstants.AC_PRESENT_AND_NOT_WORKING;
        bc2321.Chair__c = MissingItemsConstants.CHAIR_YES;
        bc2321.CheckFor__c= bc111.id;
        insert bc2321;
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        Case c=  new Case();
        c.Status='Waiting On Tenant Approval';
        c.House__c=hos.Id;
        c.Problem__c=p.id;
        c.MimoBic__c= bc2.id;
        c.MimoHic__c= bc21.id;
        c.MimoRic__c = bc2321.id;
        c.RecordTypeId=devRecordTypeId;
        //  c.ownerId=newuser.Id;
        c.Subject='test';
        insert c;
        return c;        
    }
    public static CAse createmoveincase()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.House_Owner__c=accObj.id;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.id;
        c.ownerId=newuser.Id;
        c.AccountId=accobj.id;
        c.MoveIn_Slot_End_Time__c=system.today();
        c.MoveIn_Slot_Start_Time__c=system.today();
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='House';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOvein';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_start_Date__c=system.today();
        c.Move_in_date__c=system.today();
        c.License_Start_Date__c=system.today();
        c.Requires_Owner_Approval__c=true;
        c.Owner_approval_status__c='Pending';
        c.Contract_End_Date__c=system.today();
        c.Documents_Verified__c=true;
        c.Tenant_Profile_Verified__c=true;
        c.Documents_Uploaded__c=true;
        insert c;
        return c;
        
    }
    Public Static TestMethod Void doTestbsdd(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  createmoveincase();
        Case ca=  creatSeervice();
        ca.parentid=c.id;
        Map<id,Case> newMap  = new Map<ID,case>();
        newMap.put(ca.id,ca);
        CaseServiceRequestMiMoHandler.UpdateMoveInCaseStausToClosed(newMap);
    }
    
}