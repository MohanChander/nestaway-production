/*  Purpose : Contains Boolean Variables to avoid recursion and stop Triggers from running when not necessary */

Public Class StopRecursion{


    Public Static Boolean isLeadAfterUpdate = TRUE;
    public static Boolean HouseSwitch = true; 
    public static Boolean CASE_SWITCH = true;
    
    //Flags to stop trigger in custom code.
    public static boolean DisabledHICTrigger=false;
    public static boolean DisabledRoomTrigger=false;
    public static boolean DisabledRoomInspectionTrigger=false;
    public static boolean DisabledBedTrigger=false;
    public static boolean DisabledBathroomTrigger=false;
    public static boolean DisabledPhotoTrigger=false;
    public static boolean DisabledContractTrigger = false;
    public static boolean DisabledOpportunityTrigger = false;
    public static boolean DisabledAccountTrigger = false;
    public static boolean DisabledBankTrigger = false;
    public static boolean DisabledWorkOrderTrigger = false;
    public static boolean DisabledBathRoomValidationOnWOPage = false;
    
    
    // Flags to Stop the API firing for the object.
    public static boolean DisabledRoomTermHouseSyncAPI=false;
   
}