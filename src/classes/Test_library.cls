@IsTest
public with sharing class Test_library {    
    
    // user constants
    //static String defaultUserProfile = 'US Aimia Administrator';
    static String defaultUserProfile = 'COO';
    
    // create standard users
    public static User createStandardUser ( Integer userNumber) 
    {
        User newUser = createStandardUser( userNumber, defaultUserProfile);
        return newUser;
    }
   
     public static User createStandardUser ( Integer userNumber, String userProfileName) 
    {   
        Profile prof = profileNameToObjectMap.get( userProfileName );
        System.assertNotEquals(null, prof, 'createStandardUser: profile not found: ' + userProfileName);
        List<User> eUsers = [Select Id,email,ProfileId,UserName,Alias,CommunityNickName,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,FirstName,LastName From User where UserName = 'tuser@aimia.com'];
        if(eUsers.size() > 0)
            return eUsers[0];
        
        User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' + String.valueOf(userNumber)
                    , CommunityNickName='cspu'+ String.valueOf(userNumber) 
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'+ userProfileName
                    , LastName = 'Test' + String.valueOf(userNumber) 
                    , Phone = '9620600507'
                   );
        return newUser;
    }

    public static User createStandardUserWithRole ( Integer userNumber) 
    {
        User newUser = createStandardUserWithRole( userNumber, defaultUserProfile);
        return newUser;
    }
   
     public static User createStandardUserWithRole ( Integer userNumber, String userProfileName) 
    {   
        Profile prof = profileNameToObjectMap.get( userProfileName );
        System.assertNotEquals(null, prof, 'createStandardUser: profile not found: ' + userProfileName);
        List<User> eUsers = [Select Id,email,ProfileId,UserName,Alias,CommunityNickName,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,FirstName,LastName From User where UserName = 'tuser@aimia.com'];
        if(eUsers.size() > 0)
            return eUsers[0];
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
        
        User newUser = new User( email='firstlast@aimia.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@aimia.com'
                    , Alias='cspu' + String.valueOf(userNumber)
                    , CommunityNickName='cspu'+ String.valueOf(userNumber) 
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'+ userProfileName
                    , LastName = 'Test' + String.valueOf(userNumber) 
                    
                    ,UserRoleId = ur.Id);
        return newUser;
    }
    
    
    // Profiles
    public static Map<String,Profile> profileNameToObjectMap
    {
        get{
            if (profileNameToObjectMap == null) {
                initializeProfileMap();
            }
            return profileNameToObjectMap;
        }
        private set {
            profileNameToObjectMap = value;
        }
    }
    
    
    private static void initializeProfileMap()
    {
        
        Map<Id,Profile> profileIdToObjectMap = new Map<Id,Profile> ([select Id, Name
            from Profile 
            order by Name]);
        profileNameToObjectMap = new Map<String,Profile>();
        for (Profile pr : profileIdToObjectMap.values()) {
            profileNameToObjectMap.put( pr.Name, pr);
        }

    }
    public static Case createCased()
    {
         Case c=  new Case();
       c.MoveIn_Slot_End_Time__c=System.today();
       c.MoveIn_Slot_Start_Time__c=System.today();
       c.Move_Out_Date__c=System.today();
       c.Booked_Object_ID__c='162';
       c.Move_Out_Status__c='In Progress';
       c.Move_Out_Type__c='Rent Default';
       c.Booked_Object_Type__c='Room';
       c.Contract_End_Date__c=system.today();
       return c;
    }


    public static Account createAccount()
    {
        Account accObj = new Account();
       accObj.Name = 'TestAcc';
       return accObj;
    }


    public static House_Inspection_Checklist__c createHIC()
    {
       House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
       hic.name='test';
       hic.TV__c='Yes and Not Working';
      // hic.House__c=hos.Id;
       //hic.Work_Order__c=wd.id;
       hic.Fridge__c='Yes and Working';
       hic.Washing_Machine__c='Yes and Not Working';
       hic.Type_Of_HIC__c='House Inspection Checklist';
       hic.Kitchen_Package__c='Completely Present';
       return hic;
    }
    //added by baibhav
    Public static zone__c createZone(String zonecode,String zoneRt)
    {   
        User newuser;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            newuser=createStandardUser(1);
            insert newuser;            
        }

        Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(zoneRt).getRecordTypeId();
        zone__c zc= new zone__c();
        zc.Zone_code__c =zonecode;
        zc.ZOM__c = newuser.Id;
        zc.ZOM_as_Onboarding_Manager__c = true;
        zc.Name='Test';
        zc.RecordTypeId = ZoneRecordTypeId;
        return zc;
    }

    //added by baibhav
     Public static  House__c createHouse(String zonecode,String zoneRt)
    {
         zone__c zc=createZone(zonecode,zoneRt);
         insert zc;    

         Account ownerAccount = createOwnerAccount();

        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Owner__c = ownerAccount.Id;
        hos.Onboarding_Zone__c=zc.id;
        hos.Onboarding_Zone_Code__c =zc.Zone_code__c;
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        return hos;
    }

    //added by Baibahv
    public static Contract CreateContract()
    {   
        Contract cont=new Contract();
        Account acc=createAccount();
        insert acc;
        cont.Accountid=acc.id;
        cont.Booking_Type__c='Full House';
        cont.Tenancy_Type__c='Family';
        cont.Base_House_Rent__c=5000;
        cont.Parking_Availability__c='yes';
        cont.Type_Of_Parking_Available__c ='Outside';
        cont.Number_Of_Bike_Parking__c='1';
        cont.Number_Of_Car_Parking__c='1';
        cont.Number_of_Bedrooms__c='1';
        cont.Number_of_Bathrooms__c='2';
        cont.Tenancy_Approval_time_period__c='0 hrs';
        cont.Is_MG_Applicable__c='No';
        cont.Who_pays_Move_in_Charges__c='Nestaway';
        cont.Who_pays_Society_Maintenance__c='Nestaway';
        cont.Who_pays_Electricity_Bill__c='Nestaway';
        cont.Who_pays_Common_Area_Charges__c='Nestaway';
        cont.Who_pays_DTH__c='Nestaway';
        cont.Who_pays_Water_Bill__c='Nestaway';
        cont.Who_pays_for_Internet__c='Nestaway';
        cont.Who_pays_Deposit__c='Tenant';
        cont.Deposit_Amount_Equivalent_to__c='2 month';
        cont.Security_Deposit_Payment_Mode__c='Online';
        cont.Furnishing_Type__c='Semi-furnished';
        cont.Furnishing_Plan__c='Owner Complete';
        cont.Rental_Plan__c='Revenue Share';
        cont.Annual_Rental_Increase__c=11;
        cont.Monthly_Rental_Commission__c=11;
        cont.Name__c='test';
        cont.Marital_Status__c='Married';
        cont.Date_Of_Birth__c=System.today();
        cont.Age__c=15;
        cont.Father_Husband_Name__c='test father';
        cont.Relation_with_Guardian__c='son';
        cont.Service_Agreement_Making_Date__c=System.today().addDays(75);
        cont.Expected_Date_Of_House_Going_Live__c=System.today().addDays(75);
        cont.Service_Agreement_In_Effect_Date__c=System.today().addDays(75);
        cont.Number_Of_Common_Bathrooms__c=1;
        cont.Number_of_attached_Bathrooms__c=1;
        cont.Agreement_Type__c='E-Stamped';
        return cont;
    }
    

/****************************************
Created By : Mohan
Purpose    : Create Service Request Case
*****************************************/
    public static Case createServiceRequestCase(){

        Id serviceRequestRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();  

        //create all the required custom settings
        createNestAwayCustomSetting();       

        //create House for the Service Request Case
        House__c house = createHouse('123', 'HO Zone');
        insert house;

        //create Tenant
        Account tenantAccount = createTenantAccount();

        System.debug('**inserted House ' + house);
        
        //create Service Request Case
        Case c = new Case();
        c.AccountId = tenantAccount.Id;
        c.RecordTypeId = serviceRequestRtId;
        c.House1__c = house.Id;
        insert c; 

        System.debug('**inserted Service Request Case ' + c);

        return c;    
    }   


/****************************************
Created By : Mohan
Purpose    : Create NestAway End Point Custom Settings
*****************************************/
    public static void createNestAwayCustomSetting(){
        
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        cusSet.Webapp_Auth__c = 'auth123';
        cusSet.SnM_Notification__c = 'www.test.com';
        cusSet.Move_Out_Settlement_RP_API__c = 'www.MoveOutSettlementRPAPI.com';
        cusSet.MoveOut_Bed_Release_API__c = 'www.BedReleaseAPI.com';
        cusSet.Phone_URL__c = 'www.testphoneurl.com';
        insert cusSet;     

        Org_Param__c orgParam = new Org_Param__c();
        orgParam.Move_Out_RP_Reason__c = 'MaintenanceMoveOutCharges';
        insert orgParam;       
    }    


/****************************************
Created By : Mohan
Purpose    : create Tenant Account 
*****************************************/
    public static Account createTenantAccount(){
        
        Id personRtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account accObj = new Account();
        accObj.FirstName = 'TestAcc';
        accObj.LastName = 'test';
        accObj.RecordTypeId = personRtId;
        insert accObj;

        Bank_Detail__c bankDetail = createBankDetail();
        bankDetail.Related_Account__c = accObj.Id;
        insert bankDetail;

        return accObj;        
    }    

/****************************************
Created By : Mohan
Purpose    : create Bank Detail Obj
*****************************************/
    public static Bank_Detail__c createBankDetail(){
        
        Bank_Detail__c bankDetail = new Bank_Detail__c();
        bankDetail.Name = 'Test';
        bankDetail.Bank_Name__c = 'HDFC';
        bankDetail.Branch_Name__c = 'Mahendra City';
        bankDetail.Account_Number__c = '1234';
        bankDetail.IFSC_Code__c = 'HDFC0001876';

        return bankDetail;       
    }           


/****************************************
Created By : Mohan
Purpose    : create Owner Account 
*****************************************/
    public static Account createOwnerAccount(){
        
        Id personRtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account accObj = new Account();
        accObj.FirstName = 'TestAcc';
        accObj.LastName = 'test';
        accObj.Phone = '1212124321';
        accObj.PersonEmail = 'mohan123@mohan123.com';
        accObj.RecordTypeId = personRtId;
        insert accObj;

        return accObj;        
    }   

/****************************************
Created By : Mohan
Purpose    : returns an initializes Work Order Obj 
*****************************************/
    public static WorkOrder createWorkOrder(){        

        WorkOrder woObj = new WorkOrder();

        return woObj;        
    }    

/****************************************
Created By : Mohan
Purpose    : returns a initalized Move Out Case Obj
*****************************************/
    public static Case createMoveOutCase(){ 

        Id moveOutCaseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();  

        House__c house = createHouse('123', 'Property Management Zone');
        house.APM__c = UserInfo.getUserId();
        insert house;     

        Account tenantAccount = createTenantAccount();

        Case c = new Case();
        c.RecordTypeId = moveOutCaseRtId;
        c.House__c = house.Id;
        c.Move_Out_Date__c = System.now().addDays(3).date();
        c.Tenant__c = tenantAccount.Id;

        return c;        
    }       


/****************************************
Created By : Mohan
Purpose    : returns a initalized Issue Obj
*****************************************/
    public static Issue__c createIssue(){ 

        Issue__c issue = new Issue__c();
        issue.Issue_Item__c = 'Others';
        issue.Amount__c = 100;
        issue.Comment__c = 'Test Comment';        

        return issue;        
    } 
    
    
    public static Zing_API_URL__c createZingAPIURL(){
        Zing_API_URL__c zing = new Zing_API_URL__c();
        zing.Name          = 'Zing URL';
        zing.Auth_Token__c = 'ftLKMp_z_e3xX9Yiax-q';
        zing.Email__c      = 'unknownadmin@nestaway.com';
        zing.Tag__c        = 'OwnerAcquisition';
        zing.URL__c        = 'http://40.122.207.71/admin/zinc/lat_long?';
        
        insert zing;
        return zing;
    }
    
    public static Lead createLead(){
         
            Lead objLead = new Lead();
        	//mandatory fileds
        	objLead.FirstName                  = 'Test';
        	objLead.Phone                      = '9066955369';  
        	objLead.LastName                   = 'Test';
         	objLead.Email                      = 'Test@test.com';
        	objLead.Primary_Contact_Number__c  = '9066955369'; 
        	objLead.Tenancy_Type__c			= 'Singles';
        	objLead.Furnishing_Type__c = 'Fully Furnished';
        	objLead.House_Layout__c = '1 BHK';
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.IsValidPrimaryContact__c   = True; 
        	objLead.House_Visit_Scheduled_Date__c = System.now().addHours(1);
        
        	User user = Test_library.createStandardUser(1);
        	insert user;
        	objLead.OwnerId = user.Id;
            return objLead;
    }

/****************************************
Created By : Mohan
Purpose    : Create Service Resource 
*****************************************/
    public static ServiceResource createServiceResource(){ 

        User u = createStandardUser(1, 'System Administrator');
        insert u;

        ServiceResource sr = new ServiceResource();
        sr.RelatedRecordId = u.Id;
        sr.Name = 'Test User';
        sr.IsActive = true;

        return sr;
    }     


/****************************************
Created By : Mohan
Purpose    : Create Resource Absence
*****************************************/
    public static ResourceAbsence createResourceAbsence(){ 

        ServiceResource sr = createServiceResource();
		insert sr;
        
        ResourceAbsence ra = new ResourceAbsence();
        ra.ResourceId = sr.Id;
        ra.Start = System.now();
        ra.End = System.now().addDays(1);

        return ra;
    }  


/****************************************
Created By : Mohan
Purpose    : Method to create a Content Document
*****************************************/
    public static ContentVersion createContentDocument(){ 

        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;   

        return cv;       
    }   

/****************************************
Created By : Mohan
Purpose    : Method to create Content Document Link record
*****************************************/
    public static ContentDocumentLink createContentDocumentLink(){ 

        ContentVersion cv = createContentDocument();
        insert cv;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        
        return cdl;      
    }               
    
}