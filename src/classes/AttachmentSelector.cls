/***********************************
 * Created By : Mohan
 * Purpose    : Selector Class for Attachement Object
 * ********************************/
public class AttachmentSelector {
  
    public static List<Attachment> getAttachementsForParentIdSet(Set<Id> parentIdSet){
        return [select Id, ParentId from Attachment where ParentId =: parentIdSet];
    }
}