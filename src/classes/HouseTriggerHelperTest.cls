/*  Created By : Mohan - WarpDrive Tech Works
Purpose    : Test Class for HouseTriggerHelper */

@isTest
public class HouseTriggerHelperTest {
    
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
    
    
    Public Static TestMethod Void doTest1(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        contract c= new Contract();
        c.accountid=accobj.id;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        Room_Terms__c rc= new Room_Terms__c();
        insert rc;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c=c.id;
        
        hos.House_Owner__c=accObj.id;
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c =	Constants.HOUSE_STAGE_HOUSE_DRAFT;
        
        hos.Initiate_Offboarding__c='Yes';
        hos.OwnerId=newuser.id;
        insert hos;
      
        Bathroom__c bac= new Bathroom__c ();
        bac.House__c=hos.Id;
        insert bac;
        Case c1=  new Case();
        c1.HouseForOffboarding__c =hos.Id;
        c1.RecordTypeId=caseOccupiedFurnishedRecordtype;
        c1.Active_Tenants__c=3;   
        c1.Status='Open';
        insert c1;
        List<House__c> houseList= new   List<House__c>();
        houseList.add(hos);
        Set<Id> sethouse= new Set<id>();
        setHouse.add(hos.id);
        Map<id,House__c> housemap= new Map<id,House__c>();
        houseMap.put(hos.id,hos);
        Test.StartTest();
        //HouseTriggerHelper.Oppupadte(houseList);
        HouseTriggerHelper.updateRoomAndBedAndRIC(houseList);
        HouseTriggerHelper.houseUpdateLive(houseList);
        HouseTriggerHelper.checkBedStatus(houseMap);
        HouseTriggerHelper.insurnaceForHouse(houseList);
        HouseTriggerHelper.updateContactTenancyType(houseMap);
        HouseTriggerHelper.verifyRentonHouse(housemap,housemap);
        HouseTriggerHelper.houseOffboardingCaseCreation(houseList);
        HouseTriggerHelper.validateInitateOffboarding(houseList);
        HouseTriggerHelper.sendCalloutToOffboarded(sethouse,houseMap);
        HouseTriggerHelper.updateRoomAndBedStatus(setHouse);
        HouseTriggerHelper.initiateOffBoarding(hos.id);
        HouseTriggerHelper.checkOnboardingworkflow(houseList,housemap);
        Test.stopTest();
    }
}