public class BedTriggerHandler {
    public static boolean checkrec = false;
    
    /* Mohan - Commented - 24/10/2017 - SD Payment Deployment
    public static void UpdateHouseOccupancy_beforeUpdateHouse(set<id> houseIdsWhereHouseOccupancyIsToBeUpdated ) {
        
        try{
            list<house__c> hlist = new list<house__c> ();
            boolean bedsavailable = true ;
            boolean bedssold = true ;
            hlist = [Select id, House_Occupancy__c,Stage__c, (Select id, Status__c from Beds__r) 
                     from house__C where id IN : houseIdsWhereHouseOccupancyIsToBeUpdated ];
            
            for(house__C each : hlist ) {
                if(each.Stage__c == 'House Live') {
                    bedsavailable = true ;
                    bedssold = true ;
                    for(bed__c BedRec : each.Beds__r) {
                        if(BedRec.Status__c == null  ) {
                            bedssold = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_DRAFT) {
                            bedssold = false;
                        }
                        else if (BedRec.Status__c == Constants.BED_STATUS_AVAILABLE) {
                            bedssold = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_SOLD_OUT) {
                            bedsavailable = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_INACTIVE) {
                            bedsavailable = false;
                        }
                        System.debug('***bedssold '+bedssold+'\n **** bedsavailable'+bedsavailable);
                        
                        if(bedsavailable == false && bedssold == false) {
                            break;
                        }
                    }
                    if(bedsavailable == true ) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_HOUSE_EMPTY ;
                    }
                    if(bedssold == true ) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_SOLDOUT ;
                    }
                    if(bedsavailable == false && bedssold == false) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_PARTIALLY_FILLED ;
                    }
                }
            }
        }
        Catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }  
    } */
    
    public static void UpdateHouseOccupancy(set<id> houseIdsWhereHouseOccupancyIsToBeUpdated ) {
        try{
            list<house__c> hlist = new list<house__c> ();
            boolean bedsavailable = true ;
            boolean bedssold = true ;
            hlist = [Select id, House_Occupancy__c,Stage__c, (Select id, Status__c from Beds__r) 
                     from house__C where id IN : houseIdsWhereHouseOccupancyIsToBeUpdated ];
            System.debug('***hlist*** '+hlist);
            for(house__C each : hlist ) {
                if(each.Stage__c == 'House Live') {
                    bedsavailable = true ;
                    bedssold = true ;
                    for(bed__c BedRec : each.Beds__r) {
                        if(BedRec.Status__c == null  ) {
                            bedssold = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_DRAFT) {
                            bedssold = false;
                        }
                        else if (BedRec.Status__c == Constants.BED_STATUS_AVAILABLE) {
                            bedssold = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_SOLD_OUT) {
                            bedsavailable = false;
                        }
                        else if(BedRec.Status__c == Constants.BED_STATUS_INACTIVE) {
                            bedsavailable = false;
                        }
                        System.debug('***bedssold '+bedssold+'\n **** bedsavailable'+bedsavailable);
                        
                        if(bedsavailable == false && bedssold == false) {
                            break;
                        }
                    }
                    if(bedsavailable == true ) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_HOUSE_EMPTY ;
                    }
                    if(bedssold == true ) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_SOLDOUT ;
                    }
                    if(bedsavailable == false && bedssold == false) {
                        each.House_Occupancy__c = Constants.OCCUPANCY_STATUS_PARTIALLY_FILLED ;
                    }
                }
            }
            
            if(hlist != null && hlist.size() > 0) { 
                System.debug('*** hlist '+hlist);
                update hlist;
                System.debug('*** hlist '+hlist);
            }
        }
        catch(Exception e){
            System.debug('*** UpdateHouseOccupancy Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
        
    }
    /*  public static void afterInsert(list <bed__c> newlist){
try{
list<id> bedIdList = new list<id>();
list <Room_Terms__c> rtListToupdate = new list<Room_Terms__c> ();
list<bed__c> bedlistToUpdate = new list <bed__c> ();
for(bed__c bd : newlist){
bedIdList.add(bd.id);
}
list <bed__c> roomTermsWithBed = [Select id,Actual_Rent__c ,Actual_Room_Rent__c,
Initial_Rent__c ,Room_Terms__c,
Room_Terms__r.Monthly_Base_Rent_Per_Room__c,
Room_Terms__r.Actual_Room_Rent__c,
Room_Terms__r.Number_of_beds__c,
Room_Terms__r.Monthly_Base_Rent_Per_Bed__c,
Room_Terms__r.house__c
from bed__c where id in : bedIdList ];

for(bed__c bed : roomTermsWithBed){
if(bed.Actual_Rent__c == null){
bed.addError('Cannot leave Actual Rent field blank');
}
else if(bed.Room_Terms__c == null){
bed.addError('Cannot leave Room Term field blank'); 
}
else{
bed.Actual_Room_Rent__c = bed.Room_Terms__r.Actual_Room_Rent__c;
bed.House__c = bed.Room_Terms__r.house__c;
bed.Initial_Rent__c = bed.Room_Terms__r.Monthly_Base_Rent_Per_Bed__c;
bedlistToUpdate.add(bed);
Room_Terms__c rt = new Room_Terms__c(id=bed.Room_Terms__c); 
rt.Actual_Room_Rent__c = bed.Room_Terms__r.Actual_Room_Rent__c + bed.Actual_Rent__c;
rt.Monthly_Base_Rent_Per_Room__c = bed.Room_Terms__r.Monthly_Base_Rent_Per_Room__c + bed.Room_Terms__r.Monthly_Base_Rent_Per_Bed__c;
rt.Number_of_beds__c= String.valueOf(Integer.valueOf(bed.Room_Terms__r.Number_of_beds__c) + 1);
rtListToupdate.add(rt);
}

}
if(rtListToupdate.size()>0){
insert bedlistToUpdate; 
}
if(rtListToupdate.size()>0){
System.debug('***rtListToupdate'+rtListToupdate); 
update rtListToupdate;
}
}
catch(exception e){
System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
UtilityClass.insertGenericErrorLog(e);
}
}*/
    public static void afterupdate(list <bed__c> newlist,map<id,bed__c> newMap, map<id,bed__c> oldMap) {
        if(checkrec == false){
            checkrec = true;
            set<id> setOfBedIds = new set<id>();
            set<id> roomTermsidsWithBed = new set<id>();
            map<id,bed__c> bdmapToUpdate = new map<id,bed__c>();
            set<id> houseIdsWhereHouseOccupancyIsToBeUpdated = new set<id> ();
            map<id,Room_Terms__c> rtmapToupdate = new map<id,Room_Terms__c>();
            List<Bed__c> bedList = new List<Bed__c>();

            /*Check if bed rent changed*/
            
            for(bed__c bed : newMap.values()) {
                 if( bed.Status__c != null && bed.Status__c != oldMap.get(bed.id).Status__c && bed.Status__c == 'Sold Out') {
                    bedList.add(bed);
                }
                
                if( bed.Status__c != null && bed.Status__c != oldMap.get(bed.id).Status__c && bed.House__c != null) {
                    houseIdsWhereHouseOccupancyIsToBeUpdated.add(bed.House__c);
                }
                
                if(bed.Actual_Rent__c == null) {
                    //  bed.addError('Actual Bed Rent cannot be Empty - Bed: ' + Bed.Name);
                }
                else if(bed.Room_Terms__c == null){
                    //   bed.addError('There is no room associated with the Bed: ' + Bed.Name);
                }
                else if(oldMap.ContainsKey(bed.id) && bed.Actual_Rent__c != oldMap.get(bed.id).Actual_Rent__c){
                    setOfBedIds.add(bed.id);
                    roomTermsidsWithBed.add(bed.Room_Terms__c);
                }
            }
            
            /*LOGIC to Update housr occupancy in the house */
            
            if(houseIdsWhereHouseOccupancyIsToBeUpdated != null && houseIdsWhereHouseOccupancyIsToBeUpdated.size() > 0 ) {
                UpdateHouseOccupancy( houseIdsWhereHouseOccupancyIsToBeUpdated );
            }
            
            
            /* implement bed rent updation logic */
            
            if(setOfBedIds.size() > 0) 
            {
                
                map<id,Decimal>roomIdandMaxBedRentMap = new map<id,decimal>();
                list <bed__C> tempBedlist;
                Decimal bedRentHighest = 0.0;
                
                Decimal min_bedRent = Decimal.valueOf(Label.Minimum_Bed_rent_in_Rs );
                Decimal Max_Difference_b_w_bed_rents_Per = (Decimal.valueOf(Label.Max_Difference_b_w_bed_rents_Per)).setscale(2);
                
                list <bed__c> bedsWithSameRoom = [Select id,name,Actual_Rent__c,
                                                  Actual_Room_Rent__c,Initial_Rent__c ,Room_Terms__c,
                                                  Room_Terms__r.Actual_Room_Rent__c 
                                                  from bed__C where Room_Terms__c in : roomTermsidsWithBed];
                
                tempBedlist = [Select id,name,Actual_Rent__c,
                               Actual_Room_Rent__c,Initial_Rent__c ,Room_Terms__c,
                               Room_Terms__r.Actual_Room_Rent__c 
                               from bed__C where id in : setOfBedIds];
                
                System.debug('***bedsWithSameRoom  '+bedsWithSameRoom);
                for(bed__C beds: bedsWithSameRoom) {
                    System.debug('*****beds '+beds);
                    if(roomIdandMaxBedrentMap.containskey(beds.room_terms__c)){
                        System.debug('roomIdandMaxBedrentMap.containskey(beds.room_terms__c)');
                        if(oldMap.ContainsKey(beds.id) && oldMap.get(beds.id).Actual_Rent__c != null) {
                            if(oldMap.ContainsKey(beds.id) && (oldMap.get(beds.id).Actual_Rent__c ) > bedRentHighest){
                                roomIdandMaxBedrentMap.put(beds.room_terms__c,oldMap.get(beds.id).Actual_Rent__c);
                                bedRentHighest = oldMap.get(beds.id).Actual_Rent__c;
                            }
                        }
                        else{
                            roomIdandMaxBedrentMap.put(beds.room_terms__c,0.0);
                        }
                        System.debug('**** In loop roomIdandMaxBedrentMap '+roomIdandMaxBedrentMap);
                        System.debug('**** In loop bedRentHighest '+bedRentHighest);
                    }
                    else{
                        if(oldMap.ContainsKey(beds.id) && oldMap.get(beds.id).Actual_Rent__c != null) {
                            roomIdandMaxBedrentMap.put(beds.room_terms__c,oldMap.get(beds.id).Actual_Rent__c);
                            bedRentHighest = oldMap.get(beds.id).Actual_Rent__c;
                        }
                        else{
                            roomIdandMaxBedrentMap.put(beds.room_terms__c,0);
                        }
                        System.debug('**** In loop else roomIdandMaxBedrentMap '+roomIdandMaxBedrentMap);
                        System.debug('**** In loop else  '+bedRentHighest);
                    }
                    
                }
                
                System.debug('roomIdandMaxBedrentMap -- '+roomIdandMaxBedrentMap);
                
                
                
                for(bed__C bed: tempBedlist) {
                    System.debug('bed.id - > ' + bed.id + '\n (bed.Room_Terms__c) -- ' + bed.Room_Terms__c);
                    System.debug('roomIdandMaxBedrentMap.get(bed.Room_Terms__c) -- '+roomIdandMaxBedrentMap.get(bed.Room_Terms__c));
                    if(bed.Actual_Rent__c < min_bedRent){
                        System.debug('bed validation ist if');
                        //   newmap.get(bed.id).addError('Bed rent of '+bed.name +' cannot be less than the configured Min rent which is - '+min_bedRent);
                    }
                    else if(bed.Actual_Rent__c > (roomIdandMaxBedrentMap.get(bed.Room_Terms__c) + Max_Difference_b_w_bed_rents_Per * roomIdandMaxBedrentMap.get(bed.Room_Terms__c)/100)){
                        System.debug('bed validation 2nd if ');
                        //  newmap.get(bed.id).addError('Bed rent of '+bed.name +' cannot be greater than than '+
                        //                                Decimal.valueOf(Label.Max_Difference_b_w_bed_rents_Per)+'% of '+roomIdandMaxBedrentMap.get(bed.Room_Terms__c ));
                    }
                    else {
                        /*Update Room rent as it passed all validations*/
                        
                        Room_Terms__c rt = new Room_Terms__c(id=bed.Room_Terms__c);
                        Decimal compareVar = 0;
                        if(oldMap.containsKey(bed.id) && oldMap.get(bed.id).Actual_Rent__c!= null){
                            Decimal room_actualrent;
                            if(bed.Room_Terms__r.Actual_Room_Rent__c != null){
                                room_actualrent = bed.Room_Terms__r.Actual_Room_Rent__c; 
                            }
                            else{
                                room_actualrent = 0.0;
                            }   
                            rt.Actual_Room_Rent__c = room_actualrent - oldMap.get(bed.id).Actual_Rent__c + bed.Actual_Rent__c;
                            rtmapToupdate.put(rt.id,rt);
                            bed.Actual_Room_Rent__c = room_actualrent - oldMap.get(bed.id).Actual_Rent__c+bed.Actual_Rent__c;
                            bdmapToUpdate.put(bed.id,bed);
                        }
                    }
                }
                
            }
            
            if(bdmapToUpdate.size() > 0){
                update bdmapToUpdate.values();
            }
            //update bdlistToUpdate;
            if(rtmapToupdate.size()>0){
                update rtmapToupdate.values();
            }
              if(!bedList.isEmpty()){
                BedTriggerHelper.ChnageInsuranceStatus(bedList);
            }
            
            
            
        }
    }
    /* public static void beforedelete( map<id,bed__c> oldMap){
try{
list<id> bedIdList = new list<id>();
list <Room_Terms__c> rtListToupdate = new list<Room_Terms__c> ();

for(bed__c bd : oldMap.values()){
bedIdList.add(bd.id);
}
System.debug('***bedIdList'+'\n***'+oldMap.values());
list <bed__c> roomTermsWithBed = [Select id,Actual_Rent__c ,Actual_Room_Rent__c,
Initial_Rent__c ,Room_Terms__c,
Room_Terms__r.Monthly_Base_Rent_Per_Room__c,
Room_Terms__r.Actual_Room_Rent__c,
Room_Terms__r.Number_of_beds__c
from bed__c where id in : bedIdList ];
for(bed__c bed : roomTermsWithBed){
Room_Terms__c rt = new Room_Terms__c(id=bed.Room_Terms__c); 
rt.Actual_Room_Rent__c = bed.Room_Terms__r.Actual_Room_Rent__c - bed.Actual_Rent__c;
rt.Monthly_Base_Rent_Per_Room__c = bed.Room_Terms__r.Monthly_Base_Rent_Per_Room__c - bed.Initial_Rent__c;
rt.Number_of_beds__c= String.valueOf(Integer.valueOf(bed.Room_Terms__r.Number_of_beds__c) - 1);
rtListToupdate.add(rt);
}

if(rtListToupdate.size()>0){
System.debug('***rtListToupdate'+rtListToupdate); 
update rtListToupdate;
}
}
catch(exception e)
{
System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
UtilityClass.insertGenericErrorLog(e);
}

}*/
    
    /*   
public static void beforeInsert(list <bed__c> newlist) {
try{

set<id> houseIdsWhereHouseOccupancyIsToBeUpdated = new set<id> ();
for(bed__c bed : newlist ) {
if( each.Status != null && each.Status != oldMap.get(each.id).Status && each.House__c != null) {
houseIdsWhereHouseOccupancyIsToBeUpdated.add(each.House__c);
}
}

}
catch(exception e){
System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
UtilityClass.insertGenericErrorLog(e);
}
}
*/
    
    /*  Created By: Mohan - WarpDrive Tech Works
Purpose   : When RIC is edited it should sync with the webapp system - Make an API call to Web Entity : House */
    public static void sendWebEntityHouseJson(Map<Id, Bed__c> newMap){
        
        try {
            Set<Id> bedIdSet = new Set<Id>();
            
            for(Bed__c bed: newMap.values()){
                if(bed.House__c != null){
                    bedIdSet.add(bed.Id);
                }
            }  
            
            if(!bedIdSet.isEmpty()){
                List<bed__c> bedList = BedSelector.getBedsWithHouseStatus(bedIdSet);
                
                for(Bed__c bed: bedList){
                    if(bed.House__c != null && bed.House__r.Stage__c != Constants.HOUSE_STAGE_HOUSE_DRAFT && bed.House__r.Stage__c != Constants.HOUSE_STAGE_ONBOARDING){
                        House2Json.createJson(bed.House__c);
                    }
                }
            }      
            
        } Catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }         
        
    }   
    /*********************Added By Baibhav         }      
***************************************************************************************************************/            
    public static void onAfterUpdatebedBed(List<Bed__c> newbedlist, List<Bed__c> oldbedlist)        
    {       
        List<Bed__c> bedList = new List<Bed__c>();      
        for(bed__c bd:newbedlist)       
        {       
            for(bed__c b:oldbedlist)        
            {       
                if(b.house__c == bd.house__c && b.Status__c==Constants.BED_STATUS_SOLD_OUT && bd.Status__c==Constants.BED_STATUS_AVAILABLE)     
                    bedList.add(bd);        
            }       
        }       
        BedTriggerHelper.BedTriggerTanentOffboardingUpdate(bedList);        
    }  
    
      /*********************Added By Deepak - to not allow to delete the bed which are sold out       }      
***************************************************************************************************************/            
    public static void beforeDelete(MAp<Id,Bed__C> oldmap)        
    {    
        system.debug('beforeDelete');
        system.debug('newMap'+oldmap);
        try{
            for(bed__c bd:oldmap.Values())       
            {      
                if(bd.Status__c  == 'Sold Out') {
                    oldmap.get(bd.id).addError('Sold Out Bed Can not be deleted');
                      system.debug('Sold Out Bed Can not be deleted');
                }
            }
        }  
        catch(exception e)
        {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'before delete for bed.');
        }
    } 
    
}