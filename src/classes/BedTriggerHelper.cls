/*Added By Baibhav
purpose to change status of tanent WorkOrder
*/

public class BedTriggerHelper 
{
    public static void BedTriggerTanentOffboardingUpdate(List<Bed__c> bedList)
    {
        Set<Id> houseSet = new Set<Id>();
        for(Bed__c bd:bedList)
        {
            houseSet.add(bd.house__c);
        } 
         System.debug('bony'+ houseSet);
        List<Bed__c> allBedList =BedSelector.getBedsWithRent(houseSet);
        System.debug('bony'+ allBedList);
        Map<Id,list<Bed__c>> bedMapHouseid = new Map<Id,list<Bed__c>>();
        for(Bed__c bd:allBedList)
        {
            List<Bed__c> beList = new List<Bed__c>(); 
            if(bedMapHouseid.containsKey(bd.house__c))
            {
                beList=bedMapHouseid.get(bd.house__c);
            }
            beList.add(bd);
            bedMapHouseid.put(bd.house__c,beList);
        }

        List<WorkOrder> allTanentWrkoList = WorkOrderSelector.getTanentOffBoardingWorkOrders(houseSet);
        System.debug('bony'+ allTanentWrkoList);
        Map<Id,List<WorkOrder>> tanentwrkoMapHouseid = new Map<Id,List<WorkOrder>>();
        for(WorkOrder wrko:allTanentWrkoList)
        {
            List<WorkOrder> wrkoList = new List<WorkOrder>(); 
            if(tanentwrkoMapHouseid.containsKey(wrko.house__c))
            {
                wrkoList=tanentwrkoMapHouseid.get(wrko.house__c);
            }
            wrkoList.add(wrko);
            tanentwrkoMapHouseid.put(wrko.house__c,wrkoList);
        }
        System.debug('Map'+ tanentwrkoMapHouseid);

         List<WorkOrder> tanentWrkoListUpdate = new List<WorkOrder>(); 
         map<id,WorkOrder> tanentWrkoListUpdateMap = new map <id,WorkOrder> ();

         for(Bed__c bd:bedList)
         {
            Boolean bedsoldout = false;
            List<Bed__c> beList= bedMapHouseid.get(bd.house__c);
            if(beList!=null)
            {
                for(Bed__c b:beList)
                {
                    if(b.Status__c == Constants.BED_STATUS_SOLD_OUT)
                    {
                     bedsoldout=true;
                     break;
                    }
                }
            }
            if(bedsoldout==false)
            {
                List<WorkOrder> tanentwrkolist = tanentwrkoMapHouseid.get(bd.house__c);
                System.debug('Tan'+ tanentwrkolist);
                if(tanentwrkolist!=null)
                {
                    for(WorkOrder wrk:tanentwrkolist)
                    {
                        wrk.Status=Constants.WORKORDER_STATUS_CLOSED;
                        //tanentWrkoListUpdate.add(wrk);
                        tanentWrkoListUpdateMap.put(wrk.id,wrk);
                    }
                }

            }
         }
         System.debug(tanentWrkoListUpdate);
         if(tanentWrkoListUpdateMap !=null && tanentWrkoListUpdateMap.size() > 0)
          update tanentWrkoListUpdateMap.values();
    }
    // added by deepak 
    // to chnage insurance status
      public static void ChnageInsuranceStatus(List<Bed__c> bedList){
        Set<Id> setOfHouse = new Set<Id>();
        List<House__c> houseList = new List<House__c>();
        List<Insurance__c> insList = new List<Insurance__c>();
        try{
            for(Bed__c each : bedList){
                if(each.house__c != null){
                setOfHouse.add(each.house__c);
            }
            }
            if(!setOfHouse.isEmpty()){
                houseList = [Select Id,(Select Id,Status__c from Beds__r where status__c =: Constants.BED_STATUS_SOLD_OUT ),(Select id,status__c from  Insurances__r) from house__c where id in:setOfHouse];
             if(!houseList.isEmpty()){
                for(House__c each : houseList){
                    if(each.Beds__r.size() >= 1){
                        for(Insurance__c eachIns : each.Insurances__r){
                            if(eachIns.status__c == 'new'){
                                eachIns.status__c =  Constants.INSURANCE_STATUS_TYPE_TO_BE_QUEUED;
                                insList.add(eachIns);
                            }
                        }
                    }
                }
            }
                  if(!insList.isEmpty()){
                update insList;
            }
            }
        } catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'bed insurance');
        }
    }
}