/*  Created By : Mohan
    Purpose    : Test Class for Case Trigger */

@isTest
public class CaseCommentTriggerTest {

    @isTest
    public static void caseCommentTestMethod(){

        TestClassUtility.createTestEntitilement();

        Id serviceRequestRecordTypeId = [select Id from RecordType where Name = 'Service Request'].Id;

        Case c = new Case();
        c.RecordTypeId = serviceRequestRecordTypeId;
        insert c;

        Case_Comment__c cc = new Case_Comment__c();
        cc.Case__c = c.Id;
        cc.Comment__c = 'Test';
        insert cc;

        Case queriedCase = [select Id, No_of_Agent_Comments__c from Case where Id =: c.Id];

        //System.assertEquals(1, queriedCase.No_of_Agent_Comments__c, 'Did not match the expected no of Agent Comments');
    }
}