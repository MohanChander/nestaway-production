/*  Created By : Mohan - WarpDrive Tech Works
Purpose    : Helper Methods for TaskTriggerHandler */

public class TaskTriggerHelper {
    
    /*  Created By : Mohan - WarpDrive Tech Works
Purpose    : Update the Case Status to Closed when Make House Live Task is Closed */
    public static void UpdateCaseStatusToClosed(Set<Id> caseIdSet){
        
        try{
            List<Case> caseList = new List<Case>();
            for(Id caseId: caseIdSet){
                Case c = new Case();
                c.Id = caseId;
                c.Status = Constants.CASE_STATUS_CLOSED;
                caseList.add(c);
            }
            
            if(!caseList.isEmpty())
                update caseList;			
        } Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);			
        }
    }
    
    /*  Created By : Mohan - WarpDrive Tech Works
Purpose    : 1) Validates if the Rent Verified is checked before closing the Make House Live Task and return CaseIdSet 
: 2) Validate if the DCC is approved by Doc Reviewer */	
    public static Set<Id> makeHouseLiveTaskValidator(Map<Id, Task> taskMap, Map<Id, Task> newMap){
        
        try{
            Set<Id> houseIdSet = new Set<Id>();
            Set<Id> caseIdSet = new Set<Id>();     //caseIdSet to be returned
            Set<Id> oppIdSet = new Set<Id>();
            Map<Id, House_Inspection_Checklist__c> dccMap = new Map<Id, House_Inspection_Checklist__c>();    // Dcc records to verify approval
            Map<Id, House__c> houseMap; 
            
            for(Task t: taskMap.values()){
                if(t.House__c != null){
                    houseIdSet.add(t.House__c);
                } else {
                    newMap.get(t.Id).addError('Task is not associated with the House. Please contact your Administrator');
                }
            }
            
            if(!houseIdSet.isEmpty()){
                houseMap = new Map<Id, House__c>(HouseSelector.getHousesWithRentVerification(houseIdSet));
                
                for(House__c house: houseMap.values()){
                    if(house.Opportunity__c != null)
                        oppIdSet.add(house.Opportunity__c);
                }
                
                List<House_Inspection_Checklist__c> dccList = HouseInspectionCheckListSelector.getDCCListForOpportunities(oppIdSet);
                
                for(House_Inspection_Checklist__c dcc: dccList){
                    dccMap.put(dcc.Opportunity__c, dcc);
                }
                
                for(Task t: taskMap.values()){
                    if(t.House__c != null){
                      
                        if(!houseMap.isEmpty() && houseMap.containsKey(t.House__c) && houseMap.get(t.House__c).Opportunity__c != null && !dccMap.isEmpty() && dccMap.containsKey(houseMap.get(t.House__c).Opportunity__c) && !dccMap.get(houseMap.get(t.House__c).Opportunity__c).Doc_Reviewer_Approved__c)
                            newMap.get(t.Id).addError('You Can not make house Live because document checklist is not yet approved by Doc Reviewer');
                        if(!houseMap.isEmpty() && houseMap.containsKey(t.House__c) && houseMap.get(t.House__c).Rent_Verified__c)
                            caseIdSet.add(t.WhatId);   
                        if(!houseMap.isEmpty() && houseMap.containsKey(t.House__c) && !houseMap.get(t.House__c).Rent_Verified__c)
                            newMap.get(t.Id).addError('Please verify the Rents for the House to close the Task');
                        if(!houseMap.isEmpty() && houseMap.containsKey(t.House__c) && (houseMap.get(t.House__c).Locality__c == null || houseMap.get(t.House__c).Sub_Locality__c== null)){
                            system.debug('Please fill the');
                            newMap.get(t.Id).addError('Please fill the Locailty Section using Get Locailty Button');
                        }
                    }
                }
            }
            
            return caseIdSet;			
        } catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);	
            return null;				
        }
        
        
    } // end of the method makeHouseLiveTaskValidator
}