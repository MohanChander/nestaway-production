public class FeedItemTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : Whenever a FeedItem is created for a Service Request case Owner and Follower
                 should get an email
**********************************************/
    public static void sendCaseFeedItemAsEmail(List<FeedItem> feedItemList, Map<Id, Case> caseMap) {

        System.debug('**sendCaseFeedItemAsEmail');

        try{
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();            

                for(FeedItem fi: feedItemList){
                    if(fi.IsRichText){
                        Case c = caseMap.get(fi.ParentId);
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        if(string.valueOf(c.OwnerId).startsWith('005')){
                            message.toAddresses = new String[] {String.valueOf(c.OwnerId)};
                        } else {
                            continue;
                        }                        

                        if(c.Follower__c != null && fi.InsertedById != c.Follower__c){
                            message.ccAddresses = new String[] {String.valueOf(c.Follower__c)};
                        }                        
                        message.subject = 'New Chatter Post on Case ' + c.CaseNumber;
                        String str = '<p>Hi</p><p>There is a Chatter post on the Case ' + c.CaseNumber + 
                                     '. Please take the necessary action</p><p>Chatter Post:</p>';
                        String str2 = '<p>Regards,<br/>NestAway<p>';
                        message.setHtmlBody(str + fi.body + str2);                        
                        messages.add(message);
                    }                 
                }  

                System.debug('**messages size ' + messages.size());             

                if(!messages.isEmpty()){
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    if (results[0].success) {
                        System.debug('The email was sent successfully.');
                    } else {
                        System.debug('The email failed to send: '
                                  + results[0].errors[0].message);
                    } 
                }                               
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'sendCaseFeedItemAsEmail');      
            }           
    }
}