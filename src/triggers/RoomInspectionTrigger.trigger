/*
* Description: update & delete Room Term object records based on few conditions
*/

trigger RoomInspectionTrigger on Room_Inspection__c (After Update,After Delete) {
    
    boolean runTriggers=true;    
    if(!Test.IsRunningTest() && !trigger.isDelete){   
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Room_Inspection__c ric: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && ric.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
         }      
    } 
    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledRoomInspectionTrigger){
             runTriggers=false;
         }
    }   
    
    if(runTriggers){
    
    
        If(Trigger.IsAfter && Trigger.IsDelete){
            RoomInspectionHandler.AfterDelete(Trigger.OldMap);
        }
        
        If(Trigger.IsAfter && Trigger.IsUpdate){
            RoomInspectionHandler.AfterUpdate(Trigger.OldMap,Trigger.NewMap);
            // added by warpdrive
            RoomInspectionHandler.createBathrooms(Trigger.NewMap,Trigger.OldMap);
        }

        if(Trigger.isAfter && Trigger.isUpdate){
          RoomInspectionHandler.sendWebEntityHouseJson(Trigger.newMap);
        }
    }   
}