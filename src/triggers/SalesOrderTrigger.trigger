/*
* Description: update opportunity records based on few conditions.
*/

trigger SalesOrderTrigger on Order (before insert, before update, after Update) {

    if(Trigger.isBefore){
    	if(Trigger.isInsert){
    		SalesOrderTriggerHandler.beforeInsert(Trigger.new);
    	}

        if(Trigger.isUpdate){
            SalesOrderTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }

    if(Trigger.isAfter){
    	if(Trigger.isUpdate){
    		SalesOrderTriggerHandler.AfterUpdate(Trigger.NewMap , Trigger.OldMap);
    		SalesOrderTriggerHandler.generatePurchaseOrders(Trigger.NewMap , Trigger.OldMap);
    	}
    }   
    
}