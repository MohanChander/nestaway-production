trigger ResourceAbsenceTrigger on ResourceAbsence (after insert, after update) {

	if(Trigger.isAfter){
		if(Trigger.isInsert){
			ResourceAbsenceTriggerHandler.rescheduleServiceAppointments(Trigger.newMap);
		}

		if(Trigger.isUpdate){
			ResourceAbsenceTriggerHandler.rescheduleServiceAppointments(Trigger.newMap);
		}
	}
}