trigger RoomTermsTrigger on Room_Terms__c (before Insert,after insert,before update,After update,before delete) {
    
     boolean runTriggers=true; 
     if(!Test.IsRunningTest()){      
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
           if(Trigger.New!=null){
        for(Room_Terms__c rt: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && rt.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
         }      
    } 
     }
    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledRoomTrigger){
             runTriggers=false;
         }
    }   
    if(runTriggers){
    
        if(Trigger.isBefore && Trigger.isUpdate){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_ROOMTERMS);
        }
        
        If(Trigger.IsBefore && Trigger.IsInsert){
            System.debug('*** RoomTermsTrigger IsBefore && Trigger.IsInsert');
           // RoomTermsTriggerHandler.beforeInsert(Trigger.New);
        }
        If(Trigger.IsAfter && Trigger.IsInsert){
             System.debug(' *** RoomTermsTriggerIsAfter && Trigger.IsInsert');
            RoomTermsTriggerHandler.AfterInsert(trigger.new,Trigger.NewMap,Trigger.oldMap);
        }

        If(Trigger.IsBefore && Trigger.IsUpdate){
            System.debug(' *** RoomTermsTriggerIsbefore && Trigger.IsUpdate');
            RoomTermsTriggerHandler.beforeUpdate(Trigger.NewMap,Trigger.oldMap);
        }
        
        If(Trigger.IsAfter && Trigger.IsUpdate){
            System.debug(' *** RoomTermsTriggerIsafter && Trigger.IsUpdate');
            RoomTermsTriggerHandler.AfterUpdate(Trigger.NewMap,Trigger.oldMap);
        }
          If(Trigger.Isbefore && Trigger.Isdelete){
            RoomTermsTriggerHandler.beforeDelete(Trigger.oldMap);
        }
        
        if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.ROOMTERMS_WEB_ENTITY_HOUSE_FLAG){
            RoomTermsTriggerHandler.sendWebEntityHouseJson(Trigger.newMap);
        }
        
    }   
}