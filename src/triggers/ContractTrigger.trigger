/*
* Description: validate contract data.
*/


trigger ContractTrigger on Contract (After Update,Before Update,Before Insert) {



    boolean runTriggers=true; 
    if(!Test.IsRunningTest()){
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Contract con: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && con.Data_Migration__c){
    
                runTriggers=false;
                break;
            }
    
        }  
    } 

    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledContractTrigger){
             runTriggers=false;
         }
    }

    if(runTriggers){

        if(Trigger.IsBefore && Trigger.isUpdate ){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_CONTRACT); 
        } 
         if(Trigger.IsBefore && Trigger.isInsert ){
            ContractTriggerHandler.BeforeInsert(trigger.new); 
        }  

        if(Trigger.IsAfter && Trigger.IsUpdate){
           //added by deeepak
            ContractTriggerMoveInHandler.AfterUpdate(Trigger.NewMap, Trigger.OldMap);
            ContractTriggerHandler.AfterUpdate(Trigger.NewMap, Trigger.OldMap);
             // Added By: Sanjeev Shukla- KVP Business Solutions
            ContractTriggerHandler.shareAccountAndContract(Trigger.NewMap, Trigger.OldMap);
        }
        if(Trigger.IsBefore && Trigger.IsUpdate){
            ContractTriggerHandler.BeforeUpdate(Trigger.NewMap, Trigger.OldMap);    
        }
    // chandu : Commenting this becuase we have to send this only after approval. Once new version is approved will send this update in webapp.
     /*   if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.CONTRACT_WEB_ENTITY_HOUSE_FLAG){
            ContractTriggerHandler.sendWebEntityHouseJson(Trigger.newMap);
        }
        
    */
    }   
}