trigger BankDetailTrigger on Bank_Detail__c (before insert,after insert,before update,after update)
{

    boolean fireTrigger=true;    
    Org_Param__c param = Org_Param__c.getInstance();
    
    if(!Test.IsRunningTest()){             
        if(StopRecursion.DisabledBankTrigger){
            fireTrigger=false;
           
        }
    }
     
    if(fireTrigger){   
    
      if(Trigger.isBefore)
      {
          if(Trigger.isInsert)
        {   //chandu:commenting as we dont need to move this code now.
            //  BankDetailTriggerHandler.beforeInsert(trigger.new);
        }
        if(Trigger.isUpdate)
        {   //chandu:commenting as we dont need to move this code now.
             // BankDetailTriggerHandler.beforeUpdate(trigger.newMap,trigger.oldMap);
        }
      }
      
      if(Trigger.isAfter && Trigger.isInsert){            
            
                BankDetailTriggerHandler.updateAccountForBankDetail(trigger.new);
      }

       if(Trigger.isAfter && Trigger.isUpdate){            
            
                BankDetailTriggerHandler.afterUpdate(trigger.newMap,trigger.oldMap);
      }
      
      if(!param.Disable_Bank_Sync_API__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            
            
                BankDetailTriggerHandler.syncBank(Trigger.newMap);

      }
     
    }  

}