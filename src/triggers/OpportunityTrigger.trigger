/*
* Description: Create & update Contract, Quote,HIC and order records based on few conditions
*/


trigger OpportunityTrigger on Opportunity (before insert, before update, After Insert, After Update) {
   
    boolean runTriggers=true;
    if(!Test.IsRunningTest()){       
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Opportunity op: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && op.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
         }      
    }  
    
    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledOpportunityTrigger){
             runTriggers=false;
         }
    }   
    
    
    if(runTriggers){
        if(Trigger.IsBefore && Trigger.isInsert){
            
             if(TriggerContextUtilityForOpp.isBeforeInsertFirstRun()){
                 TriggerContextUtilityForOpp.setBeforeInsertFirstRunFalse();   
               // Enable this after data migration   
               //   OpportunityTriggerHandler.cityNameValidator(Trigger.new);
                OpportunityTriggerHandler.beforeInsert(Trigger.new); 
             }   
        }
        
        if(Trigger.IsAfter && Trigger.IsInsert){
          if(TriggerContextUtilityForOpp.isAfterInsertFirstRun()){
              TriggerContextUtilityForOpp.setAfterInsertFirstRunFalse(); 
              OpportunityTriggerHandler.AfterInsert(Trigger.New);
          }  
        }
        
        if(Trigger.IsBefore && Trigger.isUpdate){
            OpportunityTriggerHandler.Oppvalidation(Trigger.New,Trigger.OldMap);
             if(TriggerContextUtilityForOpp.isBeforeUpdateFirstRun()){
                 TriggerContextUtilityForOpp.setBeforeUpdateFirstRunFalse();
                  // Enable this after data migration  
                 // OpportunityTriggerHandler.cityNameValidator(Trigger.new);
                 OpportunityTriggerHandler.BeforeUpdate(Trigger.NewMap,Trigger.OldMap); 
             }   
        } 
        
        if(Trigger.IsAfter && Trigger.IsUpdate){
          if(TriggerContextUtilityForOpp.isAfterUpdateFirstRun()){
              TriggerContextUtilityForOpp.setAfterUpdateFirstRunFalse();            
              OpportunityTriggerHandler.AfterUpdate(Trigger.oldMap,Trigger.NewMap);
          } 
        }   
             
    }
}