trigger AssetTrigger on Asset (before insert, after insert) {
	
   if(Trigger.isBefore){
    	if(Trigger.isInsert){
    		System.debug('**assetTrigger**');
    		AssetTriggerHandler.beforeInsert(Trigger.new);
    	}
    }
}