/*
* Description: wrote custom lead conversion functionality
*/

trigger LeadTrigger on Lead (After Insert,After Update, before insert, before update) {

  // city validation method 
   
   if(Trigger.isBefore && Trigger.isUpdate) {
       
       LeadTriggerHandler.cityNameValidator(Trigger.new); 
       LeadTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);     
   } 
   
   If(Trigger.IsInsert && Trigger.IsAfter){ 
       LeadTriggerHandler.AfterIsert(Trigger.NewMap);
       LeadTriggerHandler.afterInsert(Trigger.NewMap);
   }
   If(Trigger.IsUpdate && Trigger.IsAfter){
       system.debug('---InTrigger--');
       LeadTriggerHandler.LeadAutoConvert(Trigger.OldMap, Trigger.NewMap);
      
       if(StopRecursion.isLeadAfterUpdate){
            StopRecursion.isLeadAfterUpdate=False;
            LeadTriggerHandler.AfterUpdate(Trigger.OldMap, Trigger.NewMap);
       }
   } 
   
   if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
       if(Trigger.IsUpdate){
            System.debug('************is before Update');
           LeadTriggerHandler.BeforeValidation(Trigger.OldMap,Trigger.NewMap);
       }   
       LeadTriggerHandler.mapDuplicateLead(Trigger.new);    
      // added by chandu: If lead is created from non-serviceable city then mark the lead as Disqualified
      if(Trigger.isInsert){
          
           LeadTriggerHandler.beforeInsert(Trigger.new);
      }
           
   }
   
   
   
    
}