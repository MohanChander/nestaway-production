trigger HouseTrigger on House__c (before insert, before Update, after insert, after Update) {

    if(HouseJsonOptimizer.runOnce() && Trigger.isBefore){        
        HouseJsonOptimizer.triggerCount = 1;
    } else {
        HouseJsonOptimizer.triggerCount = HouseJsonOptimizer.triggerCount + 1;
    }

    if(Trigger.isBefore){
        System.debug('**TriggerHouseJsonOptimizer.triggerCount' + HouseJsonOptimizer.triggerCount);
    }

    boolean runTriggers=true; 
    if(!Test.IsRunningTest()){   
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(House__c hs: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && hs.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
        }       
    }   
    if(runTriggers){

    //static variable to switch on and off the trigger during the transaction
    if(StopRecursion.HouseSwitch){
        if(Trigger.IsBefore && Trigger.isUpdate ){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_HOUSE);
            HouseTriggerHandler.BeforeUpdate(Trigger.NewMap, Trigger.oldMap);
            HouseOffboardingTriggerHandler.onBeforeUpdateHouse(Trigger.New);
           
        }
        if(Trigger.IsAfter && (Trigger.isUpdate || Trigger.IsInsert) ){
            if(Trigger.isUpdate){
                HouseTriggerHandler.afterUpdate(Trigger.newMap,Trigger.OldMap);
                //Commented By - Mohan : there is no need to send API to Web Entity - House Term when status changes 
                //HouseTriggerHandler.AfterUpdateJSON(Trigger.newMap, Trigger.oldMap);
                HouseOffboardingTriggerHandler.onAfterUpdateHouse(Trigger.newMap, Trigger.oldMap);
            }else{
                // Added by Warpdrive
                HouseTriggerHandler.AfterInsert(Trigger.new);

                //Added by WarpDrive
                HouseTriggerHandler.populateHouseChilds(Trigger.new);
                HouseTriggerHandler.AfterInsertJSON(Trigger.newMap);
            }
        }

        if(Trigger.isBefore &&  Trigger.isInsert){
            HouseTriggerHandler.populateHouseFieldsOnInsert(Trigger.New);
            HouseTriggerHandler.googleMapLinkOnInsert(Trigger.New,null);
            HouseTriggerHandler.BeforeInsert(Trigger.New);

        }
        if(Trigger.isBefore &&  Trigger.isUpdate)
        {
         
            HouseTriggerHandler.googleMapLinkOnInsert(Trigger.New,Trigger.oldMap);
            
        }
        //At the end of the Transaction send HouseJson to WebApp System
        if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.HOUSE_WEB_ENTITY_HOUSE_FLAG && HouseJsonOptimizer.triggerCount == 1)
        {
            HouseTriggerHandler.sendWebEntityHouseJson(Trigger.newMap);
        }
      }
    } 

    if(HouseJsonOptimizer.runOnce() && Trigger.isAfter){
        HouseJsonOptimizer.triggerCount = 1;
    } else {
        HouseJsonOptimizer.triggerCount = HouseJsonOptimizer.triggerCount - 1;
    } 

    if(Trigger.isAfter){
        System.debug('**TriggerHouseJsonOptimizer.triggerCount' + HouseJsonOptimizer.triggerCount);
    }           
}