trigger InvoiceTrigger on Invoice__c (after insert, before update, after update, after delete, after undelete) {

    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            InvoiceTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }

    if(Trigger.isAfter){

        if(Trigger.isInsert){
            InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
        }

        if(Trigger.isUpdate){
            InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
            InvoiceTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }

        if(Trigger.isDelete){
            InvoiceTriggerHandler.updateHouseEconomics(Trigger.old);
        }       

        if(Trigger.isUndelete){
            InvoiceTriggerHandler.updateHouseEconomics(Trigger.new);
        }       
    }
}