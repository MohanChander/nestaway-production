trigger BedTrigger on Bed__c (before Insert, after Insert, before update, after update, before Delete) {

   boolean runTriggers=true; 
   if(!Test.IsRunningTest() && !trigger.isDelete){   
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(Bed__c bed: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c &&  bed.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
        }       
    } 
   if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledBedTrigger){
             runTriggers=false;
         }
    }
    
   if(runTriggers){ 

        if(Trigger.isBefore && Trigger.isUpdate){
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_BED);
        }    
        
        System.debug('BedTrigger initiated');
        /*If(Trigger.IsAfter && Trigger.IsInsert){
            System.debug('IsAfter && Trigger.IsInsert');
            BedTriggerHandler.afterupdate(Trigger.New);
        }*/
        
        If(Trigger.IsAfter && Trigger.IsUpdate){
            System.debug('IsAfter && Trigger.Isupdate');
            BedTriggerHandler.afterupdate(Trigger.New,trigger.newMap,trigger.oldMap);
            BedTriggerHandler.onAfterUpdatebedBed(Trigger.new,Trigger.old);
        }
       /*  If(Trigger.IsBefore && Trigger.IsInsert){
            BedTriggerHandler.beforeInsert(Trigger.New);
        }
  			*/
        
        If(Trigger.Isbefore && Trigger.IsDelete){
            System.debug('beforeDelete && Trigger.beforeDelete');
            BedTriggerHandler.beforedelete(trigger.oldMap);
        }
       

        if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.BED_WEB_ENTITY_HOUSE_FLAG){
            BedTriggerHandler.sendWebEntityHouseJson(Trigger.newMap);
        }    
   }
}