/*
* Description: validate the House that can't make house live because document checklist is not yet approved by Doc Reviewer.
*/

trigger HouseInspectionCheckTrigger on House_Inspection_Checklist__c (After Insert,After Update,Before Update,Before Delete) {
    
      
    boolean runTriggers=true;  
    if(!Test.IsRunningTest() && !trigger.isDelete){  
        NestAway_End_Point__c checkMigration = NestAway_End_Point__c.getall().values(); 
        for(House_Inspection_Checklist__c hic: Trigger.New){
            if(checkMigration.Disable_Data_Migration_Triggers__c && hic.Data_Migration__c){
                 
                 runTriggers=false;
                 break;
            }
               
        }   
    }
    if(!Test.IsRunningTest()){  
       
         if(StopRecursion.DisabledHICTrigger){
             runTriggers=false;
         }
    }
    if(runTriggers){
    
        if(Trigger.IsAfter && Trigger.IsUpdate){
            
          if(TriggerContextUtilityForHICTrigger.isAfterUpdateFirstRun()){
            TriggerContextUtilityForHICTrigger.setAfterUpdateFirstRunFalse();   
            System.debug('Update trigger Initiated');
            HouseInspectionTriggHandler.AfterUpdate(Trigger.OldMap , Trigger.NewMap);
            //added by warpdrive
            HouseInspectionTriggHandler.createBathrooms(trigger.NewMap,trigger.OldMap);
          } 
        }
        if(Trigger.IsInsert && Trigger.IsAfter){
            
          if(TriggerContextUtilityForHICTrigger.isAfterInsertFirstRun()){
            TriggerContextUtilityForHICTrigger.setAfterInsertFirstRunFalse();       
            HouseInspectionTriggHandler.AfterInsert(Trigger.New);
            //added by warpdrive
            HouseInspectionTriggHandler.createBathrooms(trigger.NewMap,null);
          } 
        }
        if(Trigger.IsBefore && Trigger.isUpdate){
           if(TriggerContextUtilityForHICTrigger.isBeforeUpdateFirstRun()){
            TriggerContextUtilityForHICTrigger.setBeforeUpdateFirstRunFalse();      
            HouseJsonOptimizer.houseJsonObjectInitializer(Constants.OBJECT_CHECKLIST);
            HouseInspectionTriggHandler.BeforeUpateValidation(Trigger.NewMap,Trigger.OldMap);
            if(!StopRecursion.DisabledBathRoomValidationOnWOPage){
               HouseInspectionTriggHandler.noOfAttachedBathroomsValidation(Trigger.NewMap,Trigger.OldMap);
            }            
             HouseInspectionTriggHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
           }    
        }

        if(Trigger.isAfter && Trigger.isUpdate && HouseJsonOptimizer.CHECKLIST_WEB_ENTITY_HOUSE_FLAG){
            HouseInspectionTriggHandler.sendWebEntityHouseJson(Trigger.newMap);
        } 
        if(Trigger.isBefore &&  Trigger.isDelete)
        {
          CannotDelete.onlyAdmin(Trigger.old);
        }       
    
    }
}