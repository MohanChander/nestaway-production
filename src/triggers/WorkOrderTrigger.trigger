trigger WorkOrderTrigger on WorkOrder (before Insert, after insert, before Update, after Update) {
    
    //Boolean variable to control the trigger execution
    if(!StopRecursion.DisabledWorkOrderTrigger){
        
        if(Trigger.isAfter && Trigger.isInsert){
            WorkOrderTriggerHandler.afterInsert(Trigger.newMap);
            WorkOrderTriggerHandler.onAfterInsertOffboardingWordkorder(Trigger.new);
        }
        if(Trigger.isbefore && Trigger.isUpdate) {            
            WorkOrderTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.OldMap);
        
        }
        if(Trigger.isAfter && Trigger.isUpdate){
         //         if(checkRecursive.runOnce())
        
            WorkOrderTriggerHandler.afterUpdate(Trigger.newMap, Trigger.OldMap);
            WorkOrderTriggerHandler.onAfterUpdateOffboardingWordkorder(Trigger.New,Trigger.oldMAp);
              

        }
         if(Trigger.isBefore && Trigger.isDelete)
         {
            //CannotDelete.onlyAdmin(Trigger.old);
         }
        if(Trigger.isbefore && Trigger.isInsert)
        {    
            //APMAssignment Commented by Mohan during FSL Deployemnt - APMAssignment not present in Production
            //WorkOrderAPMAssignment.assignmentOfOwners(Trigger.New);
           WorkOrderTriggerHandler.onbeforeInsert(trigger.new);
        }
    }
}