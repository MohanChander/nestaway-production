trigger AccountTrigger on Account (before insert, after insert, before Update, after Update, before Delete) {

        boolean fireTrigger=true;    
        Org_Param__c param = Org_Param__c.getInstance();
        
       if(!Test.IsRunningTest()){             
            if(StopRecursion.DisabledAccountTrigger){
                fireTrigger=false;
               
            }
        }
         
       if(fireTrigger){    
            if(Trigger.isBefore && Trigger.isUpdate){
                AccountTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
            }
            if(Trigger.isAfter && trigger.isUpdate) {
                AccountTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
            }
            //chandu:commenting for now
         /*  if(Trigger.isBefore &&  Trigger.isDelete){
                  CannotDelete.onlyAdmin(Trigger.old);
               } */
        
           /*
            *  Written By: Sanjeev Shukla
            *  Description: Validate duplicate account- based on Email and Phone for Person Account Only.
            */
        
            if(Trigger.isInsert && Trigger.isBefore){
                AccountTriggerHandler.validateDuplicateAccount(Trigger.new);
                AccountTriggerHandler.beforeInsert(Trigger.new);
            }
            system.debug('****param.Disable_Account_Sync_API__c'+param.Disable_Account_Sync_API__c);
            if(!param.Disable_Account_Sync_API__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            
            
                AccountTriggerHandler.syncAccount(Trigger.newMap);
            }
         }   
}