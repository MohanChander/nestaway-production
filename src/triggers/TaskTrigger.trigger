/*
* Description: update HIC records based on few conditions.
*/

trigger TaskTrigger on Task (before update, after Update, before Insert,after Insert ) {
    if(Trigger.IsBefore && Trigger.IsInsert){
        TaskTriggerHandler.beforeInsertTask(Trigger.New);
    } 
    
    if(Trigger.IsAfter && Trigger.IsInsert){
        System.debug('****After insert');
        TaskTriggerHandler.afterInsertTask(Trigger.New);
    }
    if(Trigger.IsBefore && Trigger.IsUpdate) {
        TaskTriggerHandlerAddOn1.beforeUpdate(Trigger.newMap,Trigger.oldMap);
    }
    
    If(Trigger.IsAfter && Trigger.IsUpdate){
        System.debug('qwerty');
        TaskTriggerHandler.AfterUpdate(Trigger.NewMap, Trigger.OldMap);
        TaskTriggerHandler.newAfterUpdate(Trigger.newMap, Trigger.oldMap);
        TaskTriggerHandlerAddOn1.afterUpdate(Trigger.newMap,Trigger.oldMap);
    }    
}